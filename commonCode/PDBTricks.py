### standard
import ast
### custom
import os
from datetime import datetime
import pandas as pd

### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import streamlit as st

### common stuff
import commonCode.StreamlitTricks as stTrx
import commonCode.PDBQueries as pdbQ


#####################
### useful functions
#####################
def CheckToken():
    # check if token is available
    token_check = False
    try:
        if st.session_state.myClient:
            token_check = True
        if st.session_state.debug:
            st.write(":white_check_mark: Got Token")
    except AttributeError:
        st.write("No token")

    # stop execution if token is not available
    if not token_check:
        st.stop()


def SetCompStage(compCode, stageCode, verbose=False):
    """Set the stage of a component
    compCode: component code, can be either code or Serial Number
    stageCode: stage code
    """
    try:
        setVal=DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':compCode, 'stage':stageCode})
        if verbose:
            st.write("### **Successful stage set** (",stageCode,") for:",FindKey(setVal,'id'))
        return True
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: Stage (",pageDict['stage'],")setting **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        return False

def UploadData(upCmd,upSchema):

    ### add reason to delete schema
    if "delete" in upCmd.lower():
        try:
            upSchema['reason']+=" -> deleting from webApp"
        except KeyError:
            upSchema['reason']="deleting from webApp"

    try:
        upVal=DBaccess.DbPost(st.session_state.myClient, upCmd, upSchema)
        st.balloons()
        #st.write(upVal)
        if verbose:
            if "component" in upCmd.lower():
                st.write("### **Successful "+upCmd+" **:",FindKey(upVal))
            elif "testrun" in upCmd.lower():
                st.write("### **Successful "+upCmd+" **:",FindKey(upVal['testRun'],'id'))
            else:
                st.write("### **Successful "+upCmd+" **")
        return upVal
    except itkX.BadRequest as b:
        if verbose:
            st.write("### :no_entry_sign: "+upCmd+" **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        if verbose:
            st.write(upCmd+": Don't have return value :(")
    return None

def GetDataType(inDict, inVal, inKey=None):
    ### define map dataType
    matchMap={'int':int,'integer':int,'float':float,'list':list,'dict':dict,'str':str,'string':str,'bool':bool}
    
    if inDict['dataType'] in ["string","integer","float"]:
        if inVal!=None:
            retVal=st.text_input(inDict['name']+" :", value=inVal, key=inKey)
        else:
            retVal=st.text_input(inDict['name']+" :", key=inKey)
        
    elif inDict['dataType']=="bool":
        if inVal!=None:
            retVal=st.radio(inDict['name']+" :", [True, False], index=["True", "False"].index(str(inVal)), key=inKey)
        else:
            retVal=st.radio(inDict['name']+" :", [True, False], key=inKey)

    elif inDict['dataType']=="codeTable":
        # st.write("found codeTable")
        # st.write(inDict['codeTable'])
        if inVal!=None:
            try:
                inIdx=[ct['code'] for ct in inDict['codeTable']].index(inVal)
            except ValueError:
                inIdx=0
            val=st.selectbox("code :", [ct['value'] for ct in inDict['codeTable']], index=inIdx, key=inKey)
        else:
            val=st.selectbox("code :", [ct['value'] for ct in inDict['codeTable']], key=inKey)
        retVal= next((item['code'] for item in inDict['codeTable'] if item['value'] == val), None)

    else:
        st.write("No match to dataType:",inDict['dataType'])
        return retVal

    ### map value to dataType (unless codeTable!)
    if inDict['dataType']!="codeTable":
        retVal= matchMap[inDict['dataType']](retVal)
    # st.write("returning:",retVal)
    return retVal

def InputType(inDict, inVal, inKey=None):

    retVal=None
    if inDict['valueType']=="single":
        retVal=GetDataType(inDict, inVal, inKey=None)

    elif inDict['valueType']=="array":
        st.write("Please select Array size")
        max=10
        if len(inVal)>max:
            max=len(inVal)
        numList=list(range(1,max+1,1))
        retSize=st.selectbox("Size :", numList, index=numList.index(len(inVal)), key=inKey)

        retVal=[None for x in range(0,int(retSize),1)]
        for e,x in enumerate(retVal):
            st.write("entry",e)
            try:
                retVal[e]=GetDataType(inDict, inVal[e], inKey=e+500)
            except IndexError:
                retVal[e]=GetDataType(inDict, None, inKey=e+500)

    else:
        if inDict['dataType']=="codeTable":
            retVal=GetDataType(inDict, inVal, inKey=None)
        else:
            st.write("No match to valueType:",inDict['valueType'])
    
    # st.write("more return:",retVal) 
    return retVal


def SelectCheck(k,v,inJson, inKey=None):
    try:
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError: # non string types will not have lower() attribute
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    inVal=stTrx.Tryeval(val)
    inVal=stTrx.MatchType(v,inVal)
    return inVal

def EditJson(inJson):
    for k,v in inJson.items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                # inJson[k][l]=SelectCheck(l,w)
                inJson[k][l]=SelectCheck(l,w,inJson)
        else:
            # inJson[k]=SelectCheck(k,v)
            inJson[k]=SelectCheck(k,v,inJson)
    return inJson


# evaluate input string (try to catch lists)
    for k,v in inJson.items():

        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                inJson[k][l]=SelectCheck(l,w)
        else:
            inJson[k]=SelectCheck(k,v)
    return inJson

def FindKey(inDict, myKey='serialNumber'):
    try:
        return inDict[myKey]
    except KeyError:
        for k,v in inDict.items():
            if type(v)==type({}):
                if myKey in v.keys():
                    return inDict[k][myKey]
    return myKey+" not found"

def SelectManyDf(pageDict,df,colName,dictKey=None):
    if dictKey==None:
        tmpKey=colName+"_many"
    else:
        tmpKey=dictKey
    infra.SelectBox(pageDict,tmpKey,df[colName].unique(),'Select '+colName+':')
    return df.query(colName+'=="'+pageDict[tmpKey]+'"')

def SelectOneDf(pageDict,df,colName,dictKey=None,altID=False):
    if dictKey==None:
        tmpKey=colName+"_one"
    else:
        tmpKey=dictKey
    df_many=SelectManyDf(pageDict,df,colName,dictKey)
    ident='serialNumber'
    if altID: ident='alternativeIdentifier'
    infra.SelectBoxDf(pageDict,tmpKey,df_many,'Select individual:',ident)
    return

def getRightType(field, fileTypes=["pdf", "png", "jpeg"]):
    code = field["code"]
    label = field['name']
    helpDescription = field['description']
    dataType = field['dataType']
    required = field['required']
    if required:
        label += " (required)"
    if dataType == "string":
        return st.text_input(label=label,value="",help=helpDescription)
    elif dataType == "codeTable":
        options = [table["value"] for table in field['codeTable']]
        selectedValue = st.selectbox(label=label, options=options, index=0)
        # infra.SelectBox(pageDict, f'compBatchNumber{key}',df_complistBatches['number'].unique(),"Select batch number:")
        return field['codeTable'][options.index(selectedValue)]["code"]
    elif dataType == "integer":
        return st.number_input(label=label,value=0,help=helpDescription)
    elif dataType == "binary":
        return st.file_uploader(label=label,help=helpDescription, type=fileTypes)
    elif dataType == "float":
        return st.number_input(label=label,value=0.0,help=helpDescription)
    elif dataType == "boolean":
        return st.checkbox(label=label,help=helpDescription)
    else:
        raise Exception("Unknown dataType: " + dataType)

def AddFileToSchema(schema, filesSchema, file, code, required=False):

    if file is None and not required:
        if code in filesSchema:
            del filesSchema[code]
            del schema[code]
        return schema, filesSchema
    
    filesSchema[code] = file
    schema[code] = "undefined"
    return schema, filesSchema

def get_default_value(param_name):
    defaultsTest = {
        "institution": st.session_state.Authenticate['inst']['code'],
        "date": TimeStampConverter(datetime.now().strftime("%Y-%m-%dT%H:%MZ"), "%Y-%m-%dT%H:%MZ"),
        "component": None,
        "removeProblem": False,
        "removeComments": False,
        "commentsLabel": "Description of the problems:",
        "removePassed": False,
    }

    # Issue: the field is always created if in defaultsTest, even if it is not used
    if param_name == "runNumber":
        return st.text_input("Run number:", value="1", key="runNumber")
    return defaultsTest.get(param_name)

def get_optional_param(kwargs, param_name):
    return kwargs.get(param_name, get_default_value(param_name))

def check_required_fields(dict_required_elements):
    """
    Checks whether all required fields in the dictionary have been filled in.

    Args:
        dict_required_elements (dict): A dictionary of required elements.

    Returns:
        A boolean indicating whether all required fields have been filled in.
    """
    for variableType in dict_required_elements:
        for code in dict_required_elements[variableType]:
            if dict_required_elements[variableType][code] in ["", None]:
                st.error("Please fill in all required fields")
                return False
    return True

def fill_properties(properties):
    dict_required_elements = {}
    testSchemaProperties = {}

    for property in properties:
        code = property["code"]
        required = property['required']
        testSchemaProperties[code] = getRightType(property)
        if required:
            dict_required_elements[code] = testSchemaProperties[code]

    return testSchemaProperties, dict_required_elements

def fill_results(parameters):
    """
    Fills the results of a test schema based on the given parameters.

    Args:
        parameters (list): A list of parameters to add to the test schema.

    Returns:
        A tuple containing a dictionary of test schema results, a dictionary of required elements and a dictionary of test files.
    """
    dict_required_elements = {}
    testSchemaResults = {}
    testFiles = {}

    for parameter in parameters:
        code, required, dataType = parameter["code"], parameter['required'], parameter['dataType']
        parameterValue = getRightType(parameter)

        if dataType == 'string':
            if required:
                testSchemaResults[code] = parameterValue
                dict_required_elements[code] = parameterValue
            elif parameterValue != "":
                testSchemaResults[code] = parameterValue
        elif dataType == 'binary':
            testSchemaResults, testFiles = AddFileToSchema(testSchemaResults, testFiles, parameterValue, code, required=required)
            if required:
                dict_required_elements[code] = testFiles[code]

    return testSchemaResults, dict_required_elements, testFiles

def fill_problems(kwargs):
    """
    Fills the problems and comments of a test schema based on the given arguments.

    Args:
        kwargs (dict): A dictionary of optional parameters.

    Returns:
        A tuple containing a boolean indicating whether there were problems and a list of comments.
    """
    if get_optional_param(kwargs, 'removeProblem'):
        return None, []
        
    val = st.radio("Anomaly to report:", ["Yes", "No"], index=["Yes", "No"].index("No"))
    val = val == "Yes"
    inVal = stTrx.MatchType(True, stTrx.Tryeval(val))
    
    testProblemsDescription = None
    if inVal and not get_optional_param(kwargs, 'removeComments'):
        testProblemsDescription = st.text_input(get_optional_param(kwargs, 'commentsLabel'), value="")
    
    return inVal, [testProblemsDescription]

def FillTestSchema(testSchema, properties, parameters, **kwargs):
    """fill_problems
    Fills a test schema based on the given arguments.
    
    Args:
        testSchema (dict): A dictionary representing the test schema.
        properties (list): A list of properties to add to the test schema.
        parameters (list): A list of parameters to add to the test schema.
        **kwargs (dict): A dictionary of optional parameters.
    """
    dict_required_elements = {}

    testSchema["institution"] = get_optional_param(kwargs, "institution")
    testSchema["date"] = get_optional_param(kwargs, "date")
    component = get_optional_param(kwargs, "component")
    if component is not None:
        testSchema["component"] = component
    
    if "runNumber" in kwargs:
        testSchema["runNumber"] = kwargs["runNumber"]
    else:
        testSchema["runNumber"] = get_optional_param(kwargs, "runNumber")

    # testDate = st.date_input("Test date")
    # testDateTime = st.time_input("Test date time")

    if properties or not "properties" in testSchema:
        testSchema["properties"], dict_required_elements["properties"] = fill_properties(properties)

    # removeProblem = False
    # if 'removeProblem' in kwargs:
    #     removeProblem = kwargs['removeProblem']

    removeComments = False
    if 'removeComments' in kwargs:
        removeComments = kwargs['removeComments']

    if not get_optional_param(kwargs, "removeProblem"):
        # val = st.radio("Problems during the test:", [True, False], index=["True", "False"].index("False"))
        val = st.radio("Anomaly to report:", ["Yes", "No"], index=["Yes", "No"].index("No"))
        # map Yes/No to True/False
        testSchema['problems'] = val == "Yes"
        if removeComments:
            pass
        elif testSchema['problems']:
            # pageDict['testProblemsDescription'] = st.text_input("Description of the problems:",value="")
            if 'commentsLabel' in kwargs:
                testProblemsDescription = st.text_input(kwargs['commentsLabel'],value="")
            else:
                testProblemsDescription = st.text_input("Description of the problems:",value="")
            testSchema['comments'] = [testProblemsDescription]
        else:
            if 'comments' in testSchema:
                del testSchema['comments']

    if parameters or not "results" in testSchema:
        testSchema["results"], dict_required_elements["results"], testFiles = fill_results(parameters)
    else:
        testFiles = {}

    ### Verify that all required fields are filled in
    requiredFieldsFilledIn = check_required_fields(dict_required_elements)

    # TODO:There must be a better way to do this
    removePassed = False
    if 'removePassed' in kwargs:
        removePassed = kwargs['removePassed']

    # infra.ToggleButton(pageDict,'testPassed',"Passed?")
    if not removePassed:
        val = st.radio("Passed:", ["Yes", "No"], index=["Yes", "No"].index("Yes"))
        # map Yes/No to True/False
        testSchema['passed'] = val == "Yes"
    elif st.session_state.debug:
        st.warning("Passed button has been removed, it must be set in another way")

    return testFiles, requiredFieldsFilledIn

def selectBatch(pageDict, refreshQueries, key="", compBatchTypesError=[], componentType=None):
    """ Select a batch from the list of batches for the project and component type
    
    Parameters
    ----------
    pageDict : dict
        Dictionary containing the page state
    refreshQueries : bool
        If True, refresh the queries
    key : str
        Key to use for the pageDict
    compBatchTypesError : list
        List of batch types that are not correctly querried for the component type
        
    Returns
    -------
    selectedBatch : dict
        Dictionary representing the selected batch
    """

    if componentType is None:
        componentType = pageDict['componentType']
    proj_code = st.session_state.Authenticate['proj']['code']
    ### Get the list of batches
    if not f"listBatches{key}" in pageDict or refreshQueries:
        pageDict[f'listBatches{key}'] = pdbQ.listBatches(filterMap={
                                                        "project": proj_code,
                                                        "state": ["ready", "requestedToDelete"]
                                                        })
        if len(pageDict[f'listBatches{key}']) == 0:
            st.error(f"No batch for project: {proj_code}")
            st.stop()

    ### Get the list of batch types for the component type
    if not f"compBatchTypes{key}" in pageDict or refreshQueries:
        pageDict[f'compBatchTypes{key}'] = pdbQ.listBatchTypes(project=proj_code, componentType=componentType)
        if len(pageDict[f'compBatchTypes{key}']) == 0:
            st.error(f"No batch type for component type: {componentType}")
            # This is just to get something working:
            # st.stop()
            pageDict[f'compBatchTypes{key}'] = [{"code":batchTypeCode} for batchTypeCode in compBatchTypesError]

    if compBatchTypesError:
        pageDict[f'compBatchTypes{key}'] = [batchType for batchType in pageDict[f'compBatchTypes{key}'] if batchType['code'] in compBatchTypesError]
    else:
        pageDict[f'compBatchTypes{key}'] = [batchType for batchType in pageDict[f'compBatchTypes{key}']]
            
    ### Get the list of batches for the component type
    import pandas as pd
    df_listBatches = pd.json_normalize(pageDict[f'listBatches{key}'], sep="_")
    
    # debug output
    stTrx.DebugOutput("df_listBatches:",df_listBatches)
    stTrx.DebugOutput("componentType:",pageDict["componentType"])
    stTrx.DebugOutput(f"compBatchTypes{key}:",pageDict[f'compBatchTypes{key}'])

    compBatchQueryStr = ' | '.join(f'batchType_code==\"{c["code"]}\"' for c in pageDict[f'compBatchTypes{key}'])
    if compBatchQueryStr == "":
        st.write("No previous batch")
        st.stop()
    df_complistBatches = df_listBatches.query(compBatchQueryStr)

    ### Select the batch by its number and query the batch
    # infra.SelectBox(pageDict, f'compBatchNumber{key}',df_complistBatches['number'].unique(),"Select batch number:")
    infra.SelectBox(pageDict, f'compBatchNumber{key}',df_complistBatches['number'].unique(),"Select batch name:")
    if f"compBatchNumber{key}" not in pageDict.keys():
        st.write("No batch selected.")
    compBatch = df_complistBatches.query(f'number==\"{pageDict[f"compBatchNumber{key}"]}\"')
    compBatchID = compBatch['id'].to_list()[0]

    doQueryBatch = not f'selectedBatchID{key}' in pageDict or refreshQueries
    if not doQueryBatch:
        if f'selectedBatchID{key}' in pageDict.keys():
            if pageDict[f'selectedBatchID{key}'] != compBatchID:
                doQueryBatch = True
    pageDict[f'selectedBatchID{key}'] = compBatchID

    if doQueryBatch:
        pageDict[f'selectedBatch{key}'] = pdbQ.getBatch(id=compBatchID)
    stTrx.DebugOutput(f"selectedBatch{key}:",pageDict[f'selectedBatch{key}'])

    return pageDict[f'selectedBatch{key}']

def get_component_type(pageDict):
    """ Get the component type from pageDict """
    return pageDict['componentType']

def getProjectCode():
    """ Get the project code from session state """
    return st.session_state.Authenticate['proj']['code']

def get_list_batches(pageDict, proj_code, refreshQueries, key=""):
    """ Get the list of batches """
    updatePageDict = getDoQuery(pageDict, f'listBatches{key}', refreshQueries)
    doQueryBatches = updatePageDict or pageDict is None
    if doQueryBatches:
        listBatches = pdbQ.listBatches(filterMap={
                                        "project": proj_code,
                                        "state": ["ready", "requestedToDelete"]
                                        })
        if pageDict is None:
            return listBatches

    if not f"listBatches{key}" in pageDict or refreshQueries:
        pageDict[f'listBatches{key}'] = listBatches
        if len(pageDict[f'listBatches{key}']) == 0:
            st.error(f"No batch for project: {proj_code}")
            st.stop()
    return pageDict[f'listBatches{key}']

def get_comp_batch_types(pageDict, proj_code, componentType, key, compBatchTypesError, refreshQueries):
    """ Get the list of batch types for the component type """
    if not f"compBatchTypes{key}" in pageDict or refreshQueries:
        pageDict[f'compBatchTypes{key}'] = pdbQ.listBatchTypes(project=proj_code, componentType=componentType)
        if len(pageDict[f'compBatchTypes{key}']) == 0:
            st.error(f"No batch type for component type: {componentType}")
            pageDict[f'compBatchTypes{key}'] = [{"code":batchTypeCode} for batchTypeCode in compBatchTypesError]
    return pageDict

def get_list_batches_comp(pageDict, df_listBatches, key=""):
    """ Get the list of batches for the component type """
    compBatchQueryStr = ' | '.join(f'batchType_code==\"{c["code"]}\"' for c in pageDict[f'compBatchTypes{key}'])
    if compBatchQueryStr == "":
        st.write("No previous batch")
        st.stop()
    df_complistBatches = df_listBatches.query(compBatchQueryStr)
    return df_complistBatches

def select_batch_by_number(pageDict, df_complistBatches, key=""):
    """ Select the batch by its number """
    infra.SelectBox(pageDict, f'compBatchNumber{key}',df_complistBatches['number'].unique(),"Select batch name/number:")
    if f"compBatchNumber{key}" not in pageDict.keys():
        st.write("No batch selected.")
    compBatch = df_complistBatches.query(f'number==\"{pageDict[f"compBatchNumber{key}"]}\"')
    compBatchID = compBatch['id'].to_list()[0]
    return compBatchID, pageDict

def do_query_batch(pageDict, batchID, refreshQueries, key=""):
    """ Query the batch """
    doQueryBatch = getDoQuery(pageDict, f'selectedBatchID{batchID}', refreshQueries, batchID)

    if doQueryBatch:
        pageDict[f'selectedBatch{key}'] = pdbQ.getBatch(id=batchID)
    return pageDict[f'selectedBatch{key}']

def select_batch(pageDict, refreshQueries, key="", compBatchTypesError=[], componentType=None):
    """ Select a batch from the list of batches for the project and component type """
    
    if componentType is None:
        componentType = get_component_type(pageDict)
    proj_code = getProjectCode()
    
    list_batches = get_list_batches(proj_code=proj_code, refreshQueries=refreshQueries, pageDict=pageDict, key=key)
    pageDict = get_comp_batch_types(pageDict, proj_code, componentType, key, compBatchTypesError, refreshQueries)

    df_listBatches = pd.json_normalize(list_batches, sep="_")
    df_complistBatches = get_list_batches_comp(pageDict, df_listBatches, key)
    
    compBatchID, pageDict = select_batch_by_number(pageDict, df_complistBatches, key)
    selectedBatch = do_query_batch(pageDict, compBatchID, refreshQueries, key=key)
    
    stTrx.DebugOutput(f"selectedBatch{key}:",selectedBatch)

    return selectedBatch



def getSharedComponentTypesCodes():
    """Get the list of component types for the current institution and project"""
    inst = st.session_state.Authenticate['inst']
    inst_code = inst['code']
    proj_code = st.session_state.Authenticate['proj']['code']

    stTrx.DebugOutput("inst:",inst)

    # Initialize the shared variables
    if not "SharedVariables" in list(st.session_state.keys()):
        st.session_state.SharedVariables = {}
        # TODO: SharedVariables should be deleted when the institution or project changes
        st.session_state.SharedVariables["inst"] = None
        st.session_state.SharedVariables["proj"] = None

    SharedVariables = st.session_state.SharedVariables

    # Check if institution or project changed since last call
    instChanged = SharedVariables['inst'] != inst_code
    projChanged = SharedVariables['proj'] != proj_code
    stTrx.DebugOutput("instChanged:",instChanged)
    stTrx.DebugOutput("projChanged:",projChanged)

    needUpdate = instChanged or projChanged
    
    # If neither changed, return the component types codes
    if not needUpdate:
        return SharedVariables["ComponentTypesCodes"], needUpdate
    
    # Update institution and project codes
    if st.session_state.debug:
        st.write("Institution changed")

    # Update the shared variables
    SharedVariables['inst'] = inst_code
    SharedVariables['proj'] = proj_code
    SharedVariables["ComponentTypesCodes"] = None
    
    # Find the right project in the institution
    for instProjComponentTypes in inst['componentTypes']:
        if instProjComponentTypes['code'] != proj_code:
            continue
        SharedVariables["ComponentTypesCodes"] = instProjComponentTypes['itemList']
        break

    return SharedVariables["ComponentTypesCodes"], needUpdate

def getInstitutionComponentTypes(pageDict=None,instSelection=False):
    proj_code = st.session_state.Authenticate['proj']['code']

    instCompTypesCodes, needUpdate = getSharedComponentTypesCodes()
    SharedVariables = st.session_state.SharedVariables
    needUpdate = needUpdate or not "ComponentTypes_df" in SharedVariables.keys()
    if not needUpdate:
        return SharedVariables["ComponentTypes_df"]
    
    # Check list
    if "ComponentTypes" not in list(SharedVariables.keys()) or needUpdate:
        SharedVariables["ComponentTypes"] = pdbQ.listComponentTypes(project=proj_code)

    # Check if any components were found
    if len(SharedVariables["ComponentTypes"]) < 1:
        st.write("## No components found")
        st.stop()

    # only if needed
    # # Convert the list values to strings
    # for i in range(len(SharedVariables["ComponentTypes"])):
    #     for key, value in SharedVariables["ComponentTypes"][i].items():
    #         if type(value) == list:
    #             SharedVariables["ComponentTypes"][i][key] = str(value)

    # Get the query to select the right component types
    instQueryStr = ' | '.join(f'code==\"{c["code"]}\"' for c in instCompTypesCodes)

    # Normalize the component list into a dataframe
    df_compListTypes = pd.json_normalize(SharedVariables["ComponentTypes"], sep="_")
    df_compListTypes = df_compListTypes.query(instQueryStr)
    SharedVariables["ComponentTypes_df"] = df_compListTypes
    return SharedVariables["ComponentTypes_df"]

def SlotFinder(parent,compType,pos):
    for child in parent['children']:
        stTrx.DebugOutput("checking children "+child['componentType']['code']+": ",child['order'])
        if child['componentType']['code']==compType and str(child['order'])==pos:
            stTrx.DebugOutput("Found "+compType+" slot: ",pos)
            return child['id']
    ### if fail then return None
    return None

def TimeStampConverter(inStr,inPat):
    """Convert time stamp from one format to another
    inStr: input string
    inPat: input pattern
    """
    timeObj=None
    if inStr=="now":
        timeObj=datetime.now()
    else:
        timeObj=datetime.strptime(inStr, inPat)
    # return standard formatted timestamp

    if timeObj.date() > datetime.now().date():
        st.write(f"🛑 __Date Issue__: This date looks to be in the _future_ (local date: {datetime.now().date()}).")
        st.write(f" - Please check format: {inPat}")
        st.stop()
    return timeObj.strftime("%Y-%m-%dT%H:%MZ")


def GetSNFromString(pageDict, someStr):
    ### Get serialNumber from string (e.g. file name) if possible - offer user input if not
    # check file name
    if "compSN" not in pageDict.keys() or st.button("re-check SN"):
        serialNumber=None
        st.write(f"Try extracting serialNumber from: {someStr}")
        # drop extension if exists
        dropExt=os.path.splitext(someStr)[0]
        # st.write(f" - drop ext: {dropExt}")
        # loop over parts
        for x in dropExt.split('_'):
            for y in x.split('-'):
                snIdx=y.find('20U')
                # st.write(f" - check sub: {y} - ind: {snIdx}")
                if snIdx>-1:
                    serialNumber=y[snIdx:snIdx+14]
                    break
            if serialNumber!=None:
                break
        # 
        if serialNumber!=None:
            st.write(f" - extracted serialNumber: {serialNumber}")
            pageDict['compSN']=serialNumber
        else:
            st.write(" - cannot find suitable string")
    else:
        st.write(f"Using component serial number: {pageDict['compSN']}")
    # manually set
    if "compSN" not in pageDict.keys() or st.checkbox(f"Set serialNumber?"):
        pageDict['compSN']=st.text_input(f"Enter module serialNumber:")
        st.write(f" - {pageDict['compSN']} updated")
    return pageDict['compSN']

#####################################
### component stage/test checklist
#####################################
### get tests per stage from componentType
def GetCheckList(compInfo):
    compTypeInfo=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode', {'code':compInfo['componentType']['code'],'project':compInfo['project']['code']}) 
    # st.write(compTypeInfo)
    ### stages
    df_stageList=pd.DataFrame(compTypeInfo['stages'])
    df_stageList=df_stageList.rename(columns={k:'stage_'+k for k in ['code','name','order','alternative','initial','final']})
    df_stageList['stage_order']=df_stageList['stage_order'].apply(lambda x: int(x) if not pd.isnull(x) else pd.NA)
#     df_stageList
    ### then tests
    df_checkList=df_stageList.explode('testTypes').reset_index(drop=True)
    df_checkList.iloc[0]['testTypes']
    for col in ['order','nextStage','receptionTest','receptionTestOnly']:
        df_checkList['test_'+col]=df_checkList['testTypes'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_checkList['test_order']=df_checkList['test_order'].apply(lambda x: int(x) if not pd.isnull(x) else pd.NA)
    df_checkList['test_code']=df_checkList['testTypes'].apply(lambda x: x['testType']['code'] if type(x)==type({}) and "testType" in x.keys() and type(x['testType'])==type({}) and "code" in x['testType'].keys() else None)
#     df_checkList

    ### drop receptionOnly tests
    df_checkList=df_checkList.query('test_receptionTestOnly!=True').reset_index(drop=True)

    return df_checkList

### get component tests
def GetTestInfo(compInfo):
    
    if testingKey in pageDict.keys():
        if pageDict[testingKey] != testingID:
            doQueryID = True

    if replace:
        pageDict[testingKey] = testingID
    
    return doQueryID

def getInstitution(pageDict, refreshQueries):
    # Authenticate page use listInstitutions command
    inst = st.session_state.Authenticate['inst']
    stTrx.DebugOutput("inst:",inst)
    stTrx.DebugOutput("inst authenticate session:",st.session_state.Authenticate['inst'])

    if "inst" not in list(pageDict.keys()) or refreshQueries:
        pageDict['inst'] = DBaccess.DbGet(
            st.session_state.myClient, 
            "getInstitutionByCode", 
            {
                "code": st.session_state.Authenticate['inst']['code'], 
            }, 
            False
        )
        assert pageDict['inst'] is not None
        assert pageDict['inst']['code'] == st.session_state.Authenticate['inst']['code']
        assert pageDict['inst']['componentTypes'] == st.session_state.Authenticate['inst']['componentTypes']
        if pageDict['inst'] is not None:
            inst = pageDict['inst']
    else:
        inst = pageDict['inst']

    stTrx.DebugOutput("pageDict['inst']:",pageDict['inst'])

    return inst

from typing import Any, Callable, Dict, List, Optional

def searchTable(table: List[Dict[Any, Any]], 
                search_key: Any, 
                search_value: Optional[Any] = None, 
                return_key: Optional[Any] = None, 
                custom_search: Optional[Callable[[Dict[Any, Any]], bool]] = None) -> Optional[Any]:
    """
    Generalized search in a table for a given key-value pair or custom search condition.
    Returns either a specific value or the entire entry.

    :param table: A list of dictionaries representing the table.
    :param search_key: The key to use for the standard search.
    :param search_value: The value to search for under the search_key. Default is None.
    :param return_key: The key whose value should be returned if a match is found. If None, the entire entry is returned.
    :param custom_search: An optional custom function that takes a dictionary and 
                          returns True if it's a match. Default is None.

    :return: Value associated with return_key, the entire entry, or None.

    :raises ValueError: If neither search_value nor custom_search is provided.
    """
    if search_value is None and custom_search is None:
        raise ValueError("Either 'search_value' or 'custom_search' must be provided")

    for entry in table:
        value_match = search_value is not None and entry.get(search_key) == search_value
        custom_match = custom_search is not None and custom_search(entry)

        if value_match or custom_match:
            return entry if return_key is None else entry.get(return_key)

    return None

def addBatchToSchema(schema: Dict[str, Any], 
                      batchType: str,
                      batchNumber: str):
    """
    Adds the batch information to the schema.

    :param schema: The schema to add the batch information to.
    :param batchType: The type of the batch.
    :param batchNumber: The number of the batch.
    """
    batchSchema = {
        batchType: batchNumber
    }
    # Create the batches key if it doesn't exist
    batches = schema.setdefault("batches", {})
    if batchType in batches and batches[batchType] != batchNumber:
        st.warning(f"Batch type '{batchType}' already exists in the schema with value '{batches[batchType]}'. Overwriting with '{batchNumber}'.")
    batches.update(batchSchema)

    return schema

### check children 
def CheckChildCoherence(df_children, col="currentStage"):
    
    ### check coherency of child stages
    coherentCheck=True
    if len(df_children[col].unique())==1:
        st.write(f"✔ child _{col}_ agree: {df_children[col].unique()}")
    else:
        st.write(f"❌ child _{col}_ do not agree: {df_children[col].unique()}")
        coherentCheck=False
    
    return coherentCheck

### Check coherence w.r.t parent
def CheckParentCoherence(compInfo, df_children, col="currentStage"):
    
    # match child stage to component (parent) by default
    coherentCheck=True
    st.write(f"Checking _{col} coherence_ w.r.t {compInfo['componentType']['code']}")
    st.write(" - Re-run page for parent component if necessary")
    parValue=compInfo[col]['code'] #GetCoherentStage(compInfo)

    st.write(f"Expect children to match {col}: **{parValue}**")

    for i,row in df_children.iterrows():
        if row[col]==parValue:
            st.write(f"✔ {row['serialNumber']} ({row['componentType']}) @ {row[col]} is in _coherent_ {col}")
        else:
            st.write(f"❌ {row['serialNumber']} ({row['componentType']}) @ {row[col]} is **not** in _coherent_ {col}")
            coherentCheck=False
    
    return coherentCheck

### highlight matched (by column value) rows
def Highlighter(s, col, matchVal):
    if s[col]==matchVal:
        return ['background-color: lightgreen'] * len(s)
    else:
        return ['background-color: red'] * len(s)

### force stage coherence with parent
def ForceStageCoherence(myClient, compInfo, df_children, debug=False):
    
    parStage=compInfo['currentStage']['code']
    ### loop over children and update stage
    for i,row in df_children.iterrows():
        st.write(f"working on component: {row['serialNumber']} ({row['componentType']}) @ {row['currentStage']}")
        # st.write(f"\ttry to move to: {parStage}")
        try:
            retVal=myClient.post('setComponentStage', json={'component':row['serialNumber'], 'stage':parStage})
            st.write("\t\t - __Move complete__:", retVal['component']['currentStage'])
            ### debugging
            if debug:
                st.write(f"\ttry to move back: {row['currentStage']}")
                retVal=myClient.post('setComponentStage', json={'component':row['serialNumber'], 'stage':row['currentStage']})
                st.write("\t\t - __Move complete__:", retVal['component']['currentStage'])
        except itkX.BadRequest as b:
            st.write(" - :no_entry_sign: **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                
    else:
        st.write("__Done, Re-run Stage Coherency check__") # Reset component to see update__")

### combined function for stage coherence
def CheckStageCoherence(myClient, compId, debug=False):

    st.write("### :stopwatch: Checking Stage Coherency")
    ### get most complex relative
    parInfo=GetMostParent(myClient, compId)

    df_children=GetChildrenDF(myClient, parInfo)
    # st.write(df_children)    

    ### check stage coherence
    scCheck= ( CheckChildCoherence(df_children) and CheckParentCoherence(parInfo, df_children) )

    ### st.write coloured dataframe
    st.write(f"Check components match: **{parInfo['currentStage']['code']}** _of {parInfo['serialNumber']} ({parInfo['componentType']['code']})_")
    st.write( df_children.style.apply(Highlighter, matchVal=parInfo['currentStage']['code'], col='currentStage', axis=1) )
    
    ### what to do
    if not scCheck:
        st.write("Components are __not__ _Stage Coherent_.")
        # if not st.checkbox("Continue anyway?"):
        #     st.write(f"Force all (grand)children components stages to match (grand)parent components stage ({parInfo['currentStage']['code']})")
        if st.button("Force Stage Coherence"):
            ForceStageCoherence(myClient, parInfo, df_children, debug)

    else:
        st.write("Components are _Stage Coherent_.")
    
    st.stop()

### bonus location coherency
def CheckLocationCoherence(myClient, compId, debug=True):

    st.write("### :stopwatch: Checking Location Coherency")
    ### get most complex relative
    parInfo=GetMostParent(myClient, compId)

    df_children=GetChildrenDF(myClient, parInfo, colSet=['serialNumber','componentType','currentLocation'])
    # st.write(df_children)    

    ### check location coherence
    scCheck= ( CheckChildCoherence(df_children, "currentLocation") and CheckParentCoherence(parInfo, df_children, "currentLocation") )

    ### st.write coloured dataframe
    st.write(f"Check components match: **{parInfo['currentLocation']['code']}** _of {parInfo['serialNumber']} ({parInfo['componentType']['code']})_")
    st.write( df_children.style.apply(Highlighter, matchVal=parInfo['currentLocation']['code'], col='currentLocation', axis=1) )
    
    ### what to do
    if not scCheck:
        st.write("Components are __not__ _Location Coherent_.")

    else:
        st.write("Components are _Location Coherent_.")
    
    st.stop()

#####################################
### 
#####################################
### check testRun data for match
def CheckTestDataMatch(myClient, matchValue, compId, testTypeCode, paraCode, subCode=False, altIdFlag=False):    
    ### get component
    compInfo=myClient.get('getComponent', json={'component':compId, 'alternativeIdentifier':altIdFlag}) 
    
    ### get matching testType (should only be one)
    df_test=pd.DataFrame([x for x in compInfo['tests'] if x['code']==testTypeCode])
    
    ### return if no tests found
    if df_test.empty:
        st.write(f"No tests found with code: {testTypeCode}")
        return False
        
    ### format and order testRuns by date
    df_test=df_test.explode('testRuns').reset_index(drop=True)
    for col in ["id","cts"]:
        df_test["testRun_"+col]=df_test['testRuns'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_test['testRun_cts']=pd.to_datetime(df_test['testRun_cts'])
    
    ### get testRun data
    df_test['testData']=df_test['testRun_id'].apply(lambda x: myClient.get('getTestRun', json={'testRun':x}) if x!=None else None)
    
    ### extract parameter value for input parameter code
    def GetParaValue(testObj, paraCode):
        ### loop over test properties and results
        paraValue=None
        for pr in ['properties','results']:
            st.write(f"- checking testRun _{pr}_")
            try:
                if testObj[pr]==None:
                    st.write("  - None found")
                    continue
                paraValue=next((para['value'] for para in testObj[pr] if para['code']==paraCode), "NOT FOUND")
                if paraValue!="NOT FOUND":
                    st.write(f"  - Found {paraCode}") # value: {paraValue}")
                    return paraValue
                    break
            except KeyError:
                st.write(f"  - No {pr} here")
            return paraValue
    
    ### extract parameter value
    st.write(f"Check {compId} _{testTypeCode}_ testRuns for {paraCode}")
    df_test['paraValue']=df_test['testData'].apply(lambda x: GetParaValue(x,paraCode) if type(x)==type({}) else None)
#     st.write(df_test)
    
    ### get parameter subCode if required
    if subCode!=None:
        st.write(f"Getting sub key code: _{subCode}_")
        df_test['paraValue']=df_test['paraValue'].apply(lambda x: x[subCode] if type(x)==type({}) and subCode in x.keys() else None)
    
    ### check match
    st.write("Test matching...")
    df_match=df_test.query(f'paraValue=="{matchValue}"')
    
    ### returns
    # - none
    if df_match.empty:
        st.write(" - no matches found :(")
        st.write(df_test)
        return False
    
    # - many
    if len(df_match)>1:
        st.write(f" - multiple matches found: {len(df_match)}")
        st.write(df_match)
        return True
    # - one
    else:
        st.write(f" - single match found: {df_match.iloc[0]['paraValue']}")
        return True
    
    ### catch-all
    st.write("Somehow got here?!")
    return False
