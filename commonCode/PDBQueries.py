### standard
import ast
### custom
import os
from datetime import datetime
import pandas as pd

### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import streamlit as st

### common stuff
import commonCode.StreamlitTricks as stTrx


#####################
### useful functions
#####################
def UploadFile(pageDict,upID,fileKey,title=None):
    try:
        ### faff to create file for binary upload - reading direct from buffer loses name(?!)
        tmpPath="/tmp"
        fnew=open(tmpPath+"/"+pageDict[fileKey].name, "w")
        ### haxk to use latin-1 encoding: https://stackoverflow.com/questions/5552555/unicodedecodeerror-invalid-continuation-byte
        fnew.write(pageDict[fileKey].getvalue().decode('latin-1'))
        fnew.close()
        if st.session_state.debug:
            st.write([f for f in os.listdir(tmpPath) if os.path.isfile(os.path.join(tmpPath, f))])
            st.write("use:",tmpPath+"/"+pageDict[fileKey].name)
        f = open(tmpPath+"/"+pageDict[fileKey].name, "rb")
        attVal=st.session_state.myClient.post('createTestRunAttachment', data=dict(testRun=upID,title="resultsFile",type="file"), files=dict(data=f))
        st.write("### **Successful File Upload**:",attVal['filename'])
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: File Upload **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

def DeleteData(delCmd,delSchema):
    try:
        delVal=DBaccess.DbPost(st.session_state.myClient,delCmd, delSchema)
        #st.write("delVal:",delVal)
        st.write("### **Successful "+delCmd+" **")#":",FindKey(delVal,id))
        return delVal
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: "+delCmd+" **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        st.write(delCmd+": Don't have return value :(")
    return None

def CheckComponent(sn, compType=None, curLoc=None, debug=False):
    ### mostly Carl's comp check code

    compCheck=""
    if debug: st.write("getComponent")
    try:
        retVal=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':sn})
        if debug: st.write("found:",sn,"("+retVal['componentType']['code']+")")
    except itkX.BadRequest:
        if debug: st.write("No component found")
        return "False: No component found "

    # Check type of component is as expected
    if debug: st.write("check type")
    try:
        if retVal['componentType']['code'] != compType:
            if debug: st.write("Unexpected component type for "+compType+":", retVal['componentType']['code'])
            compCheck+="False: Wrong componentType "
    except TypeError:
        if debug: st.write("Problem with database return")
        compCheck+="False: return issue "

    # Check at correct institution
    if debug: st.write("check inst.")
    try:
        if retVal['currentLocation']['code'] != curLoc:
            if debug: st.write("Component not "+curLoc+", it\'s:", retVal['currentLocation']['code'])
            compCheck="False: Wrong currentLocation "
    except TypeError:
        st.write("Problem with database return")
        compCheck="False: return issue "

    # Check not already attached to a bare module
    try:
        if retVal["parents"] is None:
                used = []
        else:
            used = [c["componentType"]["id"] for c in retVal["parents"] if c["componentType"]["code"] == "BARE_MODULE"]
        if len(used)>0:
            if debug: st.write("Component already attached to bare module:", used[0])
            compCheck="False: attach issue "
    except TypeError:
        if debug: st.write("Problem with database return")
        compCheck="False: return issue "

    # Check at correct institution
    if debug: st.write("check state")
    try:
        if retVal['state'] != "ready":
            if debug: st.write("Component not ready:", retVal['state'])
            compCheck="False: Wrong state "
    except TypeError:
        st.write("Problem with database return")
        compCheck="False: return issue "

    return compCheck

def DeleteCoTeSt(pageDict,dictKey,delCmd,ident):
    infra.ToggleButton(pageDict,dictKey,'Delete?')
    # st.write(ident)
    if pageDict[dictKey]:
        if st.button("Are you sure?",key=dictKey+delCmd):
            try:
                idType=delCmd.replace('delete','').lower()
                ### hack to recover camelCase
                if idType=="testrun":
                    idType="testRun"
                delVal=DBaccess.DbPost(st.session_state.myClient,delCmd, {idType:ident} )
                st.write("**Successful "+delCmd+"**:",ident)
            except itkX.BadRequest as b:
                st.write(":no_entry_sign: "+delCmd+" **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

def GetComponentByIdentifier(ident,type="SN"):
    comp=None
    if type=="SN" or type=="serialNumber":
        try:
            comp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':ident})
        except KeyError:
            st.write("- no ASN found.")
    elif type=="alternativeIdentifier" or type=="altID":
        try:
            comp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':ident,'alternativeIdentifier':True})
        except KeyError:
            st.write("- no ASN found.")
    return comp

def getComponent(component):
    """Get component by id or code"""
    assert component is not None
    return DBaccess.DbGet(
        st.session_state.myClient, 
        "getComponent", 
        {
            "component": component
        },
        listFlag=False
    )

def getComponentBulk(components, identifierType="SN", outputType="full", **kwargs):
    """Get component by bulk identifier
    components: ["..."], // array of component identificators
    identifierType: oneOf(["serialNumber", "id", "code"]),
    Max. number of component identificators in an array is 1000.
    """
    # Verify that the number of components is not too large
    if len(components) > 1000:
        st.error("Max. number of component identificators in an array is 1000.")
        st.stop()

    # Replace the identifier type with the correct one
    if identifierType == "SN":
        identifierType = "serialNumber"

    # Verify that the identifier type is valid
    if identifierType not in ["serialNumber", "id", "code"]:
        st.error("Invalid identifier type, must be either 'SN', 'id' or 'code'")
        st.stop()

    # Verify that the output type is valid
    if outputType not in ["full", "object"]:
        st.error("Invalid output type, must be either 'full' or 'object'")
        st.stop()

    return DBaccess.DbGet(
        st.session_state.myClient, 
        "getComponentBulk", 
        {
            "component": components,
            "identifierType": identifierType,
            "outputType": outputType,
            **kwargs
        },
        listFlag=True
    )




def listComponents(by=None, **kwargs):
    """List components
    alternativeIdentifier: "...", // alternative identifier of the component, can be used as regex
    """
    # Capitalize first letter
    if by is not None:
        by = by[0].upper() + by[1:]

        # Name of the command
        command = "listComponentsBy" + by
    else:
        command = "listComponents"

    # Verify that the command exists
    if not by in [None, "SN", "Property", "User"]:
        st.error(f"Invalid command: {command}")
        st.stop()

    # Get the parameters for the command
    parameters = kwargs
    if by is None or by == "Property":
        parameters = {"filterMap": parameters}

    return DBaccess.DbGet(
            st.session_state.myClient, 
            command, 
            parameters,
            listFlag=True
        )

def listMyComponents(filterMap={"state": ["ready", "requestedToDelete"]}, **kwargs):
    """List my components"""
    kwargs["filterMap"] = filterMap
    return DBaccess.DbGet(
            st.session_state.myClient, 
            "listMyComponents", 
            kwargs,
            listFlag=True
    )

def listMyTestRuns(**kwargs):
    """List my test runs"""
    return DBaccess.DbGet(
            st.session_state.myClient, 
            "listMyTestRuns", 
            kwargs,
            listFlag=True
    )
    

def listComponentTypes(project):
    """List component types"""
    return DBaccess.DbGet(
        st.session_state.myClient, 
        "listComponentTypes", 
        {
            "project": project
        }, 
        listFlag=True
    )

def getComponentType(id=None, project=None, code=None):
    """Get component type by id or project and code"""
    assert (code is not None and project is not None) or (id is not None)
    if id is not None:
        return DBaccess.DbGet(
            st.session_state.myClient, 
            "getComponentType", 
            {
                "id": id
            },
            listFlag=False
        )
    else:
        return DBaccess.DbGet(
            st.session_state.myClient, 
            "getComponentTypeByCode", 
            {
                "project": project,
                "code": code
            },
            listFlag=False
        )

def getTestType(id=None, componentType=None, project=None, code=None):
    """Get test type by id or component type and code"""
    assert (code is not None and project is not None and componentType is not None) or (id is not None)
    if id is not None:
        return DBaccess.DbGet(
            st.session_state.myClient, 
            "getTestType", 
            {
                "id": id
            },
            listFlag=False
        )
    else:
        return DBaccess.DbGet(
            st.session_state.myClient, 
            "getTestTypeByCode", 
            {
                "project": project,
                "componentType": componentType,
                "code": code
            },
            listFlag=False
        )

def listTestTypes(componentTypeCode, project):
    """List test types"""
    return DBaccess.DbGet(
            st.session_state.myClient, 
            "listTestTypes", 
            {
                "project": project,
                "componentType": componentTypeCode,
            }, 
            listFlag=True
        )

def deleteComponent(component, reason="Deletion requested from web application"):
    """Delete Component
    component: "...", // component identification, it can be either code or ATLAS Serial Number
    reason: "...", // reason for deletion
    """
    return DBaccess.DbPost(
            st.session_state.myClient, 
            "deleteComponent", 
            {
                "component": component,
                "reason": reason,
            }
        )

def deleteTestRun(testRun, reason="Deletion requested from web application"):
    """Delete Test Run
    testRun: "...", // test run identification
    reason: "...", // reason for deletion
    """
    return DBaccess.DbPost(
            st.session_state.myClient, 
            "deleteTestRun", 
            {
                "testRun": testRun,
                "reason": reason,
            }
        )

def setComponentProperty(component, code, value):
    """Set Component Property
    component: "...", // component identification, it can be either code or ATLAS Serial Number
    code: "...", // property code
    value: "...", // property value
    """
    return DBaccess.DbPost(
            st.session_state.myClient, 
            "setComponentProperty", 
            {
                "component": component,
                "code": code,
                "value": value,
            }
        )

def getBatch(id=None, project=None, batchType=None, number=None, **kwargs):
    """Get batch by id or project and batchType and number"""
    # batchId is an alias for id
    if "batchId" in kwargs:
        id = kwargs["batchId"]
        del kwargs["batchId"]
    # Verify that the parameters are correct
    assert (batchType is not None and project is not None and number is not None) or (id is not None)
    if id is not None:
        return DBaccess.DbGet(
            st.session_state.myClient, 
            "getBatch", 
            {
                "id": id,
                **kwargs
            },
            listFlag=False
        )
    else:
        return DBaccess.DbGet(
            st.session_state.myClient, 
            "getBatchByNumber", 
            {
                "project": project,
                "batchType": batchType,
                "number": number,
                **kwargs
            },
            listFlag=False
        )

def listBatches(filterMap={"state": ["ready", "requestedToDelete"]}, **kwargs):
    """List batches"""
    kwargs["filterMap"] = filterMap
    return DBaccess.DbGet(
            st.session_state.myClient, 
            "listBatches", 
            kwargs, 
            listFlag=True
        )

def listBatchTypes(project, componentType):
    """List batch types"""
    return DBaccess.DbGet(
            st.session_state.myClient, 
            "listBatchTypesByComponentType", 
            {
                "project": project,
                "componentType": componentType,
            },
            listFlag=True
        )

def listTestRuns(by="component", **kwargs):
    """List test runs by component, childComponent, parameter, property, testType or user"""
    # Capitalize first letter
    by = by[0].upper() + by[1:]

    # Name of the command
    command = "listTestRunsBy" + by

    # Verify that the command exists
    if not by in ["Component", "ChildComponent", "Parameter", "Property", "TestType", "User"]:
        st.error(f"Invalid command: {command}")
        st.stop()

    # Get the parameters for the command
    parameters = kwargs
    if by == "Component":
        parameters = {"filterMap": parameters}

    return DBaccess.DbGet(
            st.session_state.myClient, 
            command, 
            parameters,
            listFlag=True
        )
   
def updateBatch(batchId, number):
    """Update batch"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "updateBatch", 
        {"id": batchId, "number": number}
    )

def getTestRun(testRunId):
    """Get test run"""
    return DBaccess.DbGet(
        st.session_state.myClient, 
        "getTestRun", 
        {"testRun": testRunId},
        listFlag=False
    )


def addBatchComponent(batchId, component):
    """Add component to batch
    batchId: "...", // identification of the batch (id)
    component: "..." // component identification (either ATLAS SN, the alternative identifier or code)
    """
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "addBatchComponent", 
        {"id": batchId, "component": component}
    )

def createBatch(project, batchType, batchNumber, institute):
    """Create batch"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "createBatch", 
        {"project": project, "batchType": batchType, "number": batchNumber, "ownerInstituteList": [institute]}
    )

def setBatchProperty(batchId, code, value):
    """Set batch property"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "setBatchProperty", 
        {"id": batchId, "code": code, "value": value}
    )

def addComponentFlag(component, flag):
    """Add flag to component"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "addComponentFlag", 
        {"component": component, "flag": flag}
    )

def setComponentFlags(component, flags, revision):
    """Set component flags"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "setComponentFlags", 
        {"component": component, "flags": flags, "revision": revision}
    )

def createComponentComment(component, comment):
    """Create comment for the component"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "createComponentComment", 
        {"component": component, "comments": [comment]}
    )

def addComponentToTestRun(testRun, components):
    """Add components to test run"""
    # transform components to list if it is not
    if not isinstance(components, list):
        components = [components]
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "addComponentToTestRun", 
        {"testRun": testRun, "components": components}
    )

def createBatchAttachment(batchId, title="", description="", attachementType="file", **kwargs):
    """Create attachment for the batch"""
    if attachementType == "file":
        if "data" not in kwargs:
            st.error("File not provided")
            st.stop()
        return st.session_state.myClient.post('createBatchAttachment', 
                                              data={"batch": batchId, "title": title, "description": description, 
                                                    "type": attachementType}, 
                                              files={"data":(kwargs["data"].name, kwargs["data"].getvalue())})
    elif attachementType == "link":
        if "url" not in kwargs:
            st.error("Link not provided")
            st.stop()
    else:
        st.error("Invalid attachment type")
        st.stop()

    return DBaccess.DbPost(
        st.session_state.myClient, 
        "createBatchAttachment", 
        {"batch": batchId, "title": title, "description": description, "type": attachementType, **kwargs}
    )

def removeBatchComponent(batchId, component):
    """Remove component from batch"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "removeBatchComponent", 
        {"id": batchId, "component": component}
    )

def deleteBatch(batchId, force=False):
    """Delete batch (force=True to delete even if it is not empty)"""
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "deleteBatch", 
        {"id": batchId, "force": force}
    )

def listBatchTypesByComponentType(project, componentType):
    """List batch types by component type"""
    return DBaccess.DbGet(
        st.session_state.myClient, 
        "listBatchTypesByComponentType", 
        {"project": project, "componentType": componentType},
        listFlag=True
    )

def registerComponent(schema, success_message=True):
    """Register component"""
    try:
        component = DBaccess.DbPost(
            st.session_state.myClient, 
            "registerComponent", 
            schema
        )
        if success_message:
            st.balloons()
            st.success(f"Component {component['component']['serialNumber']} registered")
        #st.write("### **Successful register component **:",component["alternativeIdentifier"])
        return component
    except itkX.BadRequest as b:
        st.error(f"Component not registered")
        st.write("### :no_entry_sign: register component **Unsuccessful**")
        if st.session_state.debug:
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        # Friendly error message
        st.error(f"Component not registered")
        if st.session_state.debug:
            st.error("registerComponent: Don't have return value :(")
    return None

def addInstitutionComponentType(institution, project, componentType):
    """
    Add institution component type
    institution: "...", // institution code 
    project: "...", // project code
    componentType: "...", // component type code

    return: institution object or None
    """
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "addInstitutionComponentType", 
        {"institution": institution, "project": project, "componentType": componentType}
    )

def assembleComponent(parent, child, alternativeIdentifierParent=False, alternativeIdentifierChild=False, properties={}):
    """
    Assemble component
    parent: "...", // identification of parent component, it can be either code or ATLAS Serial Number
    alternativeIdentifierParent: false, // mark if the value in dtoIn.parent represents an alternative identifier
    child: "...", // identification of child component, it can be either code or ATLAS Serial Number
    alternativeIdentifierChild: false, // mark if the value in dtoIn.child represents an alternative identifier
    properties: { // collection of properties
        "...": "...", // code and value of property must match the component type specification
    }
    """
    kwargs = {
        "parent": parent, 
        "child": child, 
        "alternativeIdentifierParent": alternativeIdentifierParent, 
        "alternativeIdentifierChild": alternativeIdentifierChild, 
        "properties": properties
    }
    if len(properties) == 0:
        del kwargs["properties"]
    return DBaccess.DbPost(
        st.session_state.myClient, 
        "assembleComponent", 
        kwargs
    )