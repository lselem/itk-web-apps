### loop over project codes
### download csv list
### remove date stamp if present on last line
declare -a arr=("P" "S" "CE" "CM")
for pc in "${arr[@]}"
do
        echo "working on $pc"
        wget -O ./commonCode/stageTestList_$pc.csv https://wraight.web.cern.ch/itk-pdb-structures/stageTestList_$pc.csv
        if grep -q "Stamp" ./commonCode/stageTestList_$pc.csv
        then
                # in docker OS
                cat ./commonCode/stageTestList_$pc.csv | tac | tail -n +2 | tac > ./commonCode/tmp.csv
                # mac OS
                # cat ./commonCode/stageTestList_$pc.csv | tail -r | tail -n +2 | tail -r > ./commonCode/tmp.csv
                mv ./commonCode/tmp.csv ./commonCode/stageTestList_$pc.csv
        fi
done

