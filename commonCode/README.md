# CommonCode

Useful code imported into applications.

---

## Useful Imports

### StreamlitTricks
- Useful functions for common streamlit code chunks

### PDBTricks
- Useful functions for common PDB interactions

### codeChunks
  * Common code chunks used across several apps

---

## Useful Scripts

### get_stages_and_tests

Script to copy pre-made (_csv_) lists of stages and tests per component for each project from the web.
