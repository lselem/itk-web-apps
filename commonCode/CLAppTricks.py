import streamlit as st
import core.DBaccess as DBaccess
###
import itkdb
import itkdb.exceptions as itkX


def AllSessionStates():
    #-- Initialise whole app session states
    if 'allmsg' not in st.session_state.keys():
        st.session_state.allmsg = list()
    if 'testMode' not in st.session_state.keys():
        st.session_state.testMode = True
    if 'refLoaded' not in st.session_state.keys():
        st.session_state.refLoaded = False
    if 'currentObj' not in st.session_state.keys():
        st.session_state.currentObj = ''

    #-- Toggle to avoid constrains from format and ITk ProdDB
    isTest = st.checkbox("TEST - No interaction with ProdDB",True)
    st.session_state.testMode = isTest

def Print_out_messages():
    """
    Function to dump the messages session state 'allmsg' at once with a color
    depending on each message level. The session state entry is subsequently
    cleared.
    """
    if len(st.session_state.allmsg) > 0 :
        for msg in st.session_state.allmsg:
            if msg[0] == 0 : st.error(msg[1])
            if msg[0] == 1 : st.warning(msg[1])
            if msg[0] == 2 : st.info(msg[1])
            if msg[0] == 3 : st.success(msg[1])
    st.session_state.allmsg = list()

def CheckSN(obj):
    """ Check the format of the obj matches the ATLAS Serial Number
    specification.
    """
    try:
        obj = str(obj)
        assert obj != ""
        assert len(obj) > 0
    except AssertionError: #--obj is empty
        st.session_state.allmsg.append( [0, "Loaded empty Serial number !"] )
        return False
    except Exception as err: #--Error in formating of the obj ( str() for now )
        st.session_state.allmsg.append( [0,str(err)] )
        return False

    if (
       (len(obj) != 14)             #Serial number of 14 character
       or (obj[:3] != "20U")        #Standard ATLAS Upgrade prefix
       or (not obj[7:].isnumeric()) #Last 7 characters are numbers
       ):
        st.session_state.allmsg.append( [0,obj+" - wrong SN format"] )
        return False

    return True

def CheckComponent(sn, compType=None, curLoc=None, debug=False):

    """ Stolen and adapted from
    https://gitlab.cern.ch/jecouthu/itk-web-apps-pigtails/
    -/blob/JC-pigtails/commonCode/PDBQueries.py

    The ITk ProdDB is queried to check the Serial Number indeed corresponds to
    a component:
        1. that exists in ITk ProdDB
        2. corresponds to the expected component Type
        3. actually present in the laboratory.
    """

    isOk=True

    #-- Try getting the component from its SN
    if debug: st.write("getComponent")
    try:
        #retVal=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':sn})
        retVal=st.session_state.myClient.get('getComponent', json={'component':sn})
        if debug: st.write("found:",sn,"("+retVal['componentType']['code']+")")
    except itkX.BadRequest as err: # catch double registrations
        iMessage  = str(err).find('"message": ')+len('"message": ')
        iNext = str(err)[iMessage:].find(',')
        err = '**'+str(err)[ iMessage : iMessage + iNext]+'**' 
        if debug: st.write("No component found")
        st.session_state.allmsg.append( [1,sn+" - Component not found !"+"\n --> "+str(err)] )
        return None

    #-- Check type of component is as expected
    if debug: st.write("check type")
    try:
        if retVal['componentType']['code'] != compType:
            if debug: st.write("Unexpected component type for "+compType+":", retVal['componentType']['code'])
            st.session_state.allmsg.append( [1,sn+" - Component not of the type "+compType+"."] )
            isOk=False
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,"Problem in return format from itkdb"] )
        isOk=False

    #-- Check at correct institution
    if debug: st.write("check inst.")
    try:
        if retVal['currentLocation']['code'] != curLoc:
            if debug: st.write("Component not "+curLoc+", it\'s:", retVal['currentLocation']['code'])
            st.session_state.allmsg.append( [1,compType+" not in the current lab "+curLoc] )
            isOk=False
    except TypeError:
        if debug: st.write("Problem with database return")
        st.session_state.allmsg.append( [1,"Problem in return format from itkdb"] )
        isOk=False

    if not isOk:
        return None
    return retVal

def ShowSN():
    digits = st.session_state.inputRef
    yycode = "P1"
    if not st.session_state.testMode:
        yycode = "QR"
    object = "20U"+st.session_state.subproject['code']+ yycode + digits
    object = object.upper()  # Only upper case serial number

    if not CheckSN(object):
        return

    #FIXME
    LAB_NAME = st.session_state.Authenticate["inst"]["code"]
    COMP_TYPE="IS_TYPE1_POWER"
    if st.session_state.subproject["code"] == "PI":
        COMP_TYPE="IS_TYPE1_POWER"
    elif st.session_state.subproject["code"] == "PB":
        COMP_TYPE="OB_TYPE1_POWER_SUPERBUNDLE"
    elif st.session_state.subproject["code"] == "PE":
        st.session_state.allmsg.append( [0,"No Type-1 power bundle defined yet for Outer End-cap"] )
        return False
    else:
        st.session_state.allmsg.append( [0,"Subproject should be among PI, PB and PE"] )
        return False

    st.write(LAB_NAME + "  " + COMP_TYPE )

    if not st.session_state.testMode:
        itkdbVal = CheckComponent(object,COMP_TYPE,LAB_NAME, True)
        if itkdbVal == None:
            st.session_state.allmsg.append( [0,"This SN was not found"] )
            return False

    st.session_state.currentObj = object
    st.session_state.refLoaded = True
    return

def ResetSN():
    """
    Upon clicking on the reset button,
    - reset the object SN and make it interactive again
    - remove the Reset button
    """

    st.session_state.currentObj = ''
    st.session_state.inputRef  = ''
    st.session_state.refLoaded = False

    st.session_state.allmsg.append( [2,"*Reset*"])
    return

def GetSerialNumber():

    inputRef = st.text_input("Enter here last 7 digits of the component SN",
                             help="20UXXYY1234567",
                             #value= st.session_state.currentObj,
                             disabled= (st.session_state.currentObj != ''),
                             key="inputRef",
                             on_change=ShowSN,
                            )

    yycode = "P1"
    if not st.session_state.testMode:
        yycode = "QR"
    if st.session_state.currentObj == '' :
        st.write("**Component Serial Number** : 20U"+st.session_state.subproject['code']+ yycode +" ...")
    else:
        st.write("**Component Serial Number** : 20U"+st.session_state.subproject['code']+ yycode + st.session_state.currentObj[-7:])

    
    if st.session_state.refLoaded :
        resetBtn = st.button( "Reset", on_click=ResetSN )

    Print_out_messages()
    
