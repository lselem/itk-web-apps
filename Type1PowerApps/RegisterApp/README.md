# Pixels Type-1 Services webApp

### [Streamlit](https://www.streamlit.io) (python**3**) code to check component/test-stage relations from production database (PDB)

**NB** Powered by [itkdb](https://pypi.org/project/itkdb/)

Check requirements file for necessary libraries.

See [itk-web-apps README](https://gitlab.cern.ch/wraight/itk-web-apps/) for instructions on running via Docker.

---

## Pages

TBD
