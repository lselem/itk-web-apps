### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
import commonCode.CLAppTricks as clTrx
### analyses

#####################
### useful functions
#####################

#TODO

#####################
### main part
#####################
infoList=[" "]

class Page2(Page):
    def __init__(self):
        super().__init__("Visual Inspection", "Connector Visual Inspection", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput("Got Token")
        except AttributeError:
            st.write("No token")

        ##-- Gatekeeping
        if not doWork:
            st.stop()

        clTrx.AllSessionStates()

        if not st.session_state.refLoaded:
            st.warning("Enter a valid Serial number in the **Reception** page to be able to upload test results.")
            st.stop()

        ### General set-up
        pageDict=st.session_state[self.name]

        inst_code = st.session_state.Authenticate['inst']['code']
        proj_code = st.session_state.Authenticate['proj']['code']
        subproj_code =  st.session_state.subproject['code'] 

        #-- SN digit with special meaning depending on subproject
        digit1 =  int(st.session_state.currentObj[-7])
        digit2 =  int(st.session_state.currentObj[-7 +1])

        ### Sub-project dependent part
        if subproj_code == 'PI' :
            pageDict['componentType'] = "IS_TYPE1_POWER"
            pageDict['testCode'] = "VISUAL_INSPECTION"
            compName = "IS Type-1 power bundle"
            st.info(f"Considering a {compName}.")

        elif subproj_code == 'PB' :
            pageDict['componentType'] = "OB_TYPE1_POWER_SUPERBUNDLE"
            pageDict['testCode'] = "CONNECTOR_VISUAL_INSPECTION"
            compName = "OB Type-1 power superbundle"

            flatOrInclined = "Flat"
            if digit1 == 1:
                flatOrInclined = "Inclined"
            elif digit1 != 0:
                st.session_state.allmsg.append( [0,"Wrong SN format in 1st digit !"] )
                clTrx.Print_out_messages()
                st.stop()
            if digit2 != 2 and digit2 != 4:
                st.session_state.allmsg.append( [0,"Wrong SN format in 2nd digit !"] )
                clTrx.Print_out_messages()
                st.stop()
            clTrx.Print_out_messages()
            st.info(f"Considering a __{digit2}SP__ __{flatOrInclined}__ {compName}.")
            NbSingle = digit2

            componentType_code = pageDict['componentType']

            # TODO: query the test to see if it already exists
            # Print if the test already exists, if so, print if there is a problem
            # -> must be able to select any existing batch
            #doQueryTestType = pdbTrx.getDoQuery(pageDict, 'selectedTest', refreshQueries)
            #doQueryTestType = pdbTrx.getDoQuery(pageDict, 'selectedTest', True)
            #if doQueryTestType:
            #    pageDict['selectedTest'] = pdbQ.getTestType(project=proj_code, code=pageDict['testCode'], componentType=componentType_code)
            #    pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':proj_code, 'componentType':componentType_code, 'code':pageDict['testCode'], 'requiredOnly':True})
            #    pageDict['fullSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':proj_code, 'componentType':componentType_code, 'code':pageDict['testCode'], 'requiredOnly':False})
            #    pageDict['testSchema'] = copy.deepcopy(pageDict['origSchema'])



            parameters_visual = list()
            parameters_prepot = list()
            operator_name = st.session_state.Authenticate['user']['firstName'] \
                    +" "+st.session_state.Authenticate['user']['middleName'] \
                    +" "+st.session_state.Authenticate['user']['lastName']
            properties = [ operator_name ]

            st.write("### Photos of the PP0 connector __BEFORE__ potting *(should have been sent by the company)*")


            PhotoNoPotPP01 = st.file_uploader(label="Quadrant 1 bundle PP0", type=['png','jpg','jpeg'])
            parameters_prepot.append( PhotoNoPotPP01 )
            PhotoNoPotPP02 = st.file_uploader(label="Quadrant 2 bundle PP0", type=['png','jpg','jpeg'])
            parameters_prepot.append( PhotoNoPotPP02 )
            if NbSingle == 4:
                PhotoNoPotPP03 = st.file_uploader(label="Quadrant 3 bundle PP0", type=['png','jpg','jpeg'])
                parameters_prepot.append( PhotoNoPotPP03 )
                PhotoNoPotPP04 = st.file_uploader(label="Quadrant 4 bundle PP0", type=['png','jpg','jpeg'])
                parameters_prepot.append( PhotoNoPotPP04 )

            st.write("### Photos of the PP0 connector __AFTER__ potting, upon reception at the QC site")
            PhotoWithPotPP01 = st.file_uploader(label="Quadrant 1 bundle PP0", type=['png','jpg','jpeg'], key="pp0withpot1")
            PhotoWithPotPP02 = st.file_uploader(label="Quadrant 2 bundle PP0", type=['png','jpg','jpeg'], key="pp0withpot2")
            if NbSingle == 4:
                PhotoWithPotPP03 = st.file_uploader(label="Quadrant 3 bundle PP0", type=['png','jpg','jpeg'], key="pp0withpot3")
                PhotoWithPotPP04 = st.file_uploader(label="Quadrant 4 bundle PP0", type=['png','jpg','jpeg'], key="pp0withpot4")

            st.write("### Photos of the PP1 connector *add figure explaining front-back-left-right*")
            PhotoPP1Front = st.file_uploader(label="Front Picture", type=['png','jpg','jpeg'])
            PhotoPP1Back  = st.file_uploader(label="Back Picture", type=['png','jpg','jpeg'])
            PhotoPP1Left  = st.file_uploader(label="Left Picture", type=['png','jpg','jpeg'])
            PhotoPP1Right = st.file_uploader(label="Right Picture", type=['png','jpg','jpeg'])

            # Schema of Connector Visual inspection OB
            #schemaConnector = {
            #        "component":"..."
            #        "testType":"CONNECTOR_VISUAL_INSPECTION"
            #        "institution":"..."
            #        "runNumber":"..."
            #        "date":"2024-05-08T07:58:16.227Z"
            #        "passed":true
            #        "problems":false
            #        "properties":{
            #            "OPERATOR_NAME":"VAFWLBZJWHGJFIHORRVUWBBWHSAPGU"
            #            }
            #        "results":{
            #            "PP1_FRONT":NULL
            #            "PP1_BACK":NULL
            #            "PP1_LEFT":NULL
            #            "PP1_RIGHT":NULL
            #            "Q1_PP0_POTTED":NULL
            #            "Q2_PP0_POTTED":NULL
            #            "Q3_PP0_POTTED":NULL
            #            "Q4_PP0_POTTED":NULL
            #            }
            #        }
            thisTestJson = {
                    "component": st.session_state.currentObj,
                    "testType":"CONNECTOR_VISUAL_INSPECTION",
                    "institution": inst_code,
                    "runNumber": 1, #Get previous run number and increment ?
                    "date":"2024-05-08T07:58:16.227Z",
                    "passed": True,
                    "problems": False,
                    "properties": properties,
                    "results":{
                        "PP1_FRONT": PhotoPP1Front,
                        "PP1_BACK": PhotoPP1Back,
                        "PP1_LEFT": PhotoPP1Left,
                        "PP1_RIGHT": PhotoPP1Right,
                        "Q1_PP0_POTTED": PhotoWithPotPP01,
                        "Q2_PP0_POTTED": PhotoWithPotPP02,
                        "Q3_PP0_POTTED": PhotoWithPotPP03,
                        "Q4_PP0_POTTED": PhotoWithPotPP04,
                        },
                    }
            #
            #filled = pdbTrx.FillTestSchema( schemaConnector, [operator_name], 
            if st.button("Load to DB") and not st.session_state.testMode:
                testRun = st.session_state.myClient.post('uploadTestRunResults', json= thisTestJson )
