### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
import commonCode.CLAppTricks as clTrx
### analyses

#####################
### useful functions
#####################

#TODO

#####################
### main part
#####################
infoList=[" "]

class Page6(Page):
    def __init__(self):
        super().__init__("Power Test", "Power Test", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput("Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        clTrx.AllSessionStates()

        if not st.session_state.refLoaded:
            st.warning("Enter a valid Serial number in the **Reception** page to be able to upload test results.")
            st.stop()

        digit1 =  int(st.session_state.currentObj[-7])
        digit2 =  int(st.session_state.currentObj[-7 +1])
        if st.session_state.subproject['code'] == 'PI' :
            compName = "IS Type-1 power bundle"
            st.info(f"Considering a {compName}.")

        elif st.session_state.subproject['code'] == 'PB' :
            compName = "OB Type-1 power superbundle"
            flatOrInclined = "Flat"
            if digit1 == 1:
                flatOrInclined = "Inclined"
            elif digit1 != 0:
                st.session_state.allmsg.append( [0,"Wrong SN format in 1st digit !"] )
                clTrx.Print_out_messages()
                st.stop()
            if digit2 != 2 and digit2 != 4:
                st.session_state.allmsg.append( [0,"Wrong SN format in 2nd digit !"] )
                clTrx.Print_out_messages()
                st.stop()

            st.info(f"Considering a __{digit2}SP__ __{flatOrInclined}__ {compName}.")
            NbSingle = digit2
            clTrx.Print_out_messages()

            st.write(f"### Enter Serial power chain Voltage drop")
            SP1V = st.number_input(label="SP1 Voltage Drop [V]",
                                       value=10.0, min_value = 8.0, max_value=20.0)
            SP2V = st.number_input(label="SP2 Voltage Drop [V]",
                                       value=10.0, min_value = 8.0, max_value=20.0)
            if NbSingle == 4:
                SP3V = st.number_input(label="SP3 Voltage Drop [V]",
                                           value=10.0, min_value = 8.0, max_value=20.0)
                SP4V = st.number_input(label="SP4 Voltage Drop [V]",
                                           value=10.0, min_value = 8.0, max_value=20.0)

        Commentaires=st.text_area(label="Comment", height=20, value='')
