### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
#import userPages.QCApp.CLAppTricks as clTrx
import commonCode.CLAppTricks as clTrx
### analyses

#####################
### useful functions
#####################

#TODO

#####################
### main part
#####################
infoList=[" "]

class Page1(Page):
    def __init__(self):
        super().__init__("Reception", "Enter Type-1 component Serial Number", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput("Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        #-- Set general session state objects 
        clTrx.AllSessionStates()

        #-- Get subproject
        st.write(f"### Select subproject")
        if 'subproject' not in st.session_state.keys():
            # TODO Assign default subproject from Authenticate[inst] value (eg. LPSC --> PB) 
            st.session_state.subproject = st.session_state.Authenticate["proj"]["subprojects"][0]

        def updateSubProject():
            st.session_state.subproject = st.session_state.selSubProj 
        st.selectbox("Sub-project:", 
                st.session_state.Authenticate['proj']['subprojects'], format_func=lambda x: x['name'], 
                index=st.session_state.Authenticate['proj']['subprojects'].index(st.session_state.subproject),
                key = "selSubProj", on_change = updateSubProject )

        #-- Enter SN of received component 
        clTrx.GetSerialNumber()  # 


        if st.session_state.currentObj == '':
            clTrx.Print_out_messages()
            st.stop()

        digit1 =  int(st.session_state.currentObj[-7])
        digit2 =  int(st.session_state.currentObj[-7 +1])
        if st.session_state.subproject['code'] == 'PI' :
            compName = "IS Type-1 power bundle"
            st.info(f"Considering a {compName}.")

        elif st.session_state.subproject['code'] == 'PB' :
            compName = "OB Type-1 power superbundle"
            flatOrInclined = "Flat"
            if digit1 == 1:
                flatOrInclined = "Inclined"
            elif digit1 != 0:
                st.session_state.allmsg.append( [0,"Wrong SN format in 1st digit !"] )
                clTrx.Print_out_messages()
                st.stop()
            if digit2 != 2 and digit2 != 4:
                st.session_state.allmsg.append( [0,"Wrong SN format in 2nd digit !"] )
                clTrx.Print_out_messages()
                st.stop()

            st.info(f"Considering a __{digit2}SP__ __{flatOrInclined}__ {compName}.")
            clTrx.Print_out_messages()

