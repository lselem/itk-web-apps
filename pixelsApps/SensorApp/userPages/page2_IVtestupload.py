### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### CV script
from . import analyseIV_data

#####################
### useful functions
#####################
def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    try: # avoid bool trouble
        if val.isnumeric(): # for integers formatted as strings by schema
            val=int(val)
    except AttributeError:
        pass
    return val

def ColorCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)

def EditJson(inJson):
    for k,v in inJson.items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                inJson[k][l]=SelectCheck(l,w)
        else:
            inJson[k]=SelectCheck(k,v)
    return inJson


infoList=["  * upload _csv_ IV file",
        "  * review test schema",
        "   * reset if required",
        "   * edit if required",
        "  * upload test schema",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("IV Data Upload", ":microscope: Upload Module IV Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="SENSOR_TILE"
        if "code" not in pageDict.keys():
            pageDict['code']="IV_MEASURE"
        if "stage" not in pageDict.keys():
            pageDict['stage']="sensor_manufacturer"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        ### hidden changer
        if st.session_state.debug:
            st.write("**DEBUG** Hidden changer")
            infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
            if pageDict['toggleChanger']:
                infra.TextBox(pageDict,'componentType',"Enter componentType code:")
                infra.TextBox(pageDict,'code',"Enter testType _code_:")
                infra.TextBox(pageDict,'stage',"Enter testStage _code_:")
                infra.TextBox(pageDict,'project',"Enter project _code_:")


        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['code']+"@"+pageDict['stage']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':True})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["json"])
        if st.session_state.debug: st.write(pageDict['file'])

        if pageDict['file'] is not None:
            headerList, dataList = GetGleanList(pageDict['file'])
        else:
            st.write("No data file set")
            st.stop()

        # To convert to a string based IO:
        stringio = StringIO(pageDict['file'].getvalue().decode("ISO-8859-1"))
        #st.write(stringio)
        # To read file as string:
        string_data = stringio.read()
        compio= StringIO(string_data)

        try:
            db_sensorID, db_institute, db_date, db_humidity, db_temperature, timedata, xdata, ydata, yerr, tempdata, humidata, Vbd, Ilc, plot_path, total_flag, fig = analyseIV_data.analyseIV(compio,os.getcwd()+"\temp.png")
        except TypeError as e:
            st.write("EXCEPTION")
            st.write("\tKeyError:",e)

        # df_data=pd.DataFrame({
        # "time" : timedata,
        # "voltage": xdata,
        # "current": ydata,
        # "sigma current": yerr,
        # "temperature": tempdata,
        # "humidity": humidata
        # })

        st.dataframe(df_data)

        st.write("Analysed data")
        st.write(fig)

        ### show figure for visualisation
        tmpPath="/tmp"
        fig.savefig(tmpPath+'/IVimg.png'.format(), dpi=300)
        st.write([f for f in os.listdir(tmpPath) if os.path.isfile(os.path.join(tmpPath, f))])

        # st.write("### Visualisation")
        # st.write("**IV info.**")
        # IVchart=alt.Chart(df_data).mark_line().transform_fold(
        #     ['current', 'temperature', 'humidity']
        # ).encode(
        #         x='voltage:Q',
        #         y='value:Q',
        #         color='key:N',
        #         tooltip=['key:N','voltage:Q','value:Q']
        # ).properties(width=600).interactive()
        # st.altair_chart(IVchart)

        if "testSchema" not in pageDict.keys() or st.button("re-read file"):
            pageDict['testSchema']=pageDict['origSchema']
            # update json
            pageDict['testSchema']['component'] = db_sensorID
            pageDict['testSchema']['institution'] = db_institute
            pageDict['testSchema']['date'] = pdbTrx.TimeStampConverter(db_date, "%d.%m.%Y %H:%M")
            pageDict['testSchema']["passed"] = total_flag
            pageDict['testSchema']['properties']['HUM'] = db_humidity
            pageDict['testSchema']['properties']['TEMP'] = db_temperature
            pageDict['testSchema']["results"]["IV_ARRAY"] = {
            "time" : timedata,
            "voltage": xdata,
            "current": ydata,
            "sigma current": yerr,
            "temperature": tempdata,
            "humidity": humidata
            }
            pageDict['testSchema']['results']['BREAKDOWN_VOLTAGE'] = Vbd
            pageDict['testSchema']['results']['LEAK_CURRENT'] = Ilc
            pageDict['testSchema']['results']['IV_IMG']=tmpPath+'/IVimg.png'

        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write("### Test Schema")
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        if st.button("Upload Test"):
            ### set stage
            try:
                pageDict['setVal']=DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':pageDict['testSchema']['component'], 'stage':pageDict['stage']})
                st.write("### **Successful Stage Set** (",pageDict['stage'],") for:",pageDict['setVal']['serialNumber'])
                # df_set=pd.json_normalize(pageDict['setVal'], sep = "_")
                # st.dataframe(df_set)
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Stage (",pageDict['stage'],")setting **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            ### upload data
            try:
                pageDict['upVal']=DBaccess.DbPost(st.session_state.myClient,'uploadTestRunResults', pageDict['testSchema'])
                st.write("### **Successful Upload**:",pageDict['upVal']['componentTestRun']['date'])
                st.balloons()
                st.write(pageDict['upVal'])
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Upload **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            ### upload images
            try:
                f = open(pageDict['testSchema']['results']['IV_IMG'], "rb")
                pageDict['imgVal']=st.session_state.myClient.post('createBinaryTestRunParameter', data=dict(testRun=pageDict['upVal']['testRun']['id'],parameter="IV_IMG"), files=dict(data=f))
                st.write("### **Successful Image Upload**:",pageDict['imgVal']['testRunParamter']['code'])
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Image Upload **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        if 'upVal' in pageDict.keys():
            try:
                st.write("last successful _upload_:",pageDict['upVal']['testRun']['id'])
                if st.button("Delete testrun"):
                    ### delete data
                    try:
                        pageDict['delVal'] = DBaccess.DbPost(st.session_state.myClient,'deleteTestRun', {'testRun':pageDict['upVal']['testRun']['id']})
                        st.write("### **Successful Deletion**:",pageDict['delVal']['testRun']['id'])
                        st.balloons()
                        st.write(pageDict['delVal'])
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: Deletion unsuccessful")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            except KeyError:
                pass
        if 'delVal' in pageDict.keys():
            try:
                st.write("last successful _deletion_:",pageDict['delVal']['testRun']['id'])
            except KeyError:
                pass
