### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from datetime import datetime
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

def GetCompsOfType(df_list,compType):
    df_out=df_list.query('componentType_code=="'+compType+'"').sort_values(by=['serialNumber'])
    st.write(compType,"found at institution:",len(df_out))
    return df_out

infoList=[  " * Get OEC_HALF_SANDWICHs, OEC_CARBON_FOAM_TRAPEZOIDS_HRs and OEC_PRE_PREG_HRs at institute",
            " * Select OEC layer: L2/L3/L4",
            "  - Change GP occupancy",
            " * Select OEC_CARBON_FOAM_TRAPEZOIDS_HR components",
            "  - Choose slot",
            " * Select OEC_PRE_PREG_HR",
            " * Select OEC_HALF_SANDWICHs",
            " * Assemble parts to OEC_HALF_SANDWICH",
            "  - Disassemble optional"]
#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("HS Assembly", ":microscope: Half Sandwich Assembly", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### select components at institute
        #compTypes=["BARE_MODULE","GELPACK"]
        compTypes=["OEC_HALF_SANDWICH","OEC_CARBON_FOAM_TRAPEZOIDS_HR","OEC_PRE_PREG_HR"]
        st.write("## Get components at institute")
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** "+", ".join(compTypes)+" components for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'], 'componentType':compTypes },True)
        else:
            st.write("**Got** "+", ".join(compTypes)+" components for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            st.write("(colour--> _",'componentType_code',"_)")
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        colz=['serialNumber','componentType_name','componentType_code','assembled']

        if df_compList.empty:
            st.write("No components found")
            st.stop()

        ### make all columns strings
        for c in colz:
            df_compList[c]=df_compList[c].astype(str)

        infra.Radio(pageDict,'removeAss',['all','only assembled','not assembled'],"Keep components in list?")
        if pageDict['removeAss']=="not assembled":
            df_compList=df_compList.query('assembled=="False"')
        if pageDict['removeAss']=="only assembled":
            df_compList=df_compList.query('assembled=="True"')

        try:
            st.dataframe(df_compList[colz].sort_values(by=['serialNumber']).style.apply(stTrx.ColourCells, df=df_compList[colz], colName='componentType_code', flip=True, axis=1))
        except KeyError:
            st.write("No componentTypes found:",",".join(compTypes))
            st.stop()

        st.write("---")

        occupancyList=[{'layer':"L2",'count':7},{'layer':"L3",'count':9},{'layer':"L4",'count':11}]
        infra.SelectBox(pageDict, 'occupancy', occupancyList, 'Select OEC layer:','layer')

        df_HSs=GetCompsOfType(df_compList,"OEC_HALF_SANDWICH")
        df_pregs=GetCompsOfType(df_compList,"OEC_PRE_PREG_HR")
        df_CFTs=GetCompsOfType(df_compList,"OEC_CARBON_FOAM_TRAPEZOIDS_HR")
        # df_HSs=GetCompsOfType(df_compList,"GELPACK")
        # df_pregs=GetCompsOfType(df_compList,"BARE_MODULE")
        # df_CFTs=GetCompsOfType(df_compList,"BARE_MODULE")

        #list of dictionaries
        #[{'comb':{'SN':,'compType':},'parts':[{'SN':,'compType':}]}]


        st.write("## Select CF Trapezoids")
        st.write("### Up to "+str(pageDict['occupancy']['count'])+" components per Half Sandwich")


        if "CFparts" not in pageDict.keys() or st.button("reset CFT dictionary"):
           pageDict['CFparts']=[]
           for tc in [x for x in pageDict.keys() if "togComp_" in x]:
               pageDict[tc]=False

        for i,row in df_CFTs.iterrows():
            sn=str(row['serialNumber'])
            altID=str(row['alternativeIdentifier'])
            infra.ToggleButton(pageDict,'togComp_'+sn,sn+" ("+altID+")")
            if pageDict['togComp_'+sn]:
                selPos=st.selectbox("Position:",[x for x in range(0,pageDict['occupancy']['count'],1)], key=sn+'_pos')
                #selOri=st.selectbox("Orientation:",['normal','reversed'], key=sn+'b')
                if sn in [x['SN'] for x in pageDict['CFparts']]:
                    ind=[x['SN'] for x in pageDict['CFparts']].index(sn)
                    if pageDict['CFparts'][ind]['slot']==selPos:
                        st.write("**same slot selected **")
                    else:
                        if selPos in [x['slot'] for x in pageDict['CFparts']]:
                            st.write("**position already chosen** choose again")
                        else:
                            pageDict['CFparts'][ind]['slot']=selPos
                else:
                    if selPos in [x['slot'] for x in pageDict['CFparts']]:
                        st.write("**position already chosen** choose again")
                    else:
                        pageDict['CFparts'].append({'compType':"OEC_CARBON_FOAM_TRAPEZOIDS_HR",'SN':sn,'slot':selPos})
            ### remove if exists
            else:
                if sn in [x['SN'] for x in pageDict['CFparts']]:
                    pageDict['CFparts'].remove(next(item for item in pageDict['CFparts'] if item['SN'] == sn))


        st.write("selected CFTs:",len(pageDict['CFparts']))
        stTrx.DebugOutput("selected components: ",pageDict['CFparts'])

        st.write("---")

        if len(pageDict['CFparts'])<1:
            st.stop()
        elif len(pageDict['CFparts'])>pageDict['occupancy']['count']:
            st.write("Too many selections. Choose up to",pageDict['occupancy']['count'])
            st.stop()

        st.write("## Select Pre-preg")
        infra.SelectBox(pageDict,'prePreg',df_pregs['serialNumber'].to_list(),'Select Pre-preg')

        st.dataframe(pageDict['CFparts']+[{'compType':"OEC_PRE_PREG_HR",'SN':pageDict['prePreg']}])

        st.write("---")

        st.write("## Select Half Sandwich")
        infra.SelectBox(pageDict,'HS',df_HSs['serialNumber'].to_list(),'Select Half Sandwich')

        ### ASSEMBLE PARTS
        if st.button("Assemble"):
            # default dictionary assembly flag to false
            pageDict['assDict']={'comb':{'SN':pageDict['HS'],'compType':"OEC_HALF_SANDWICH"},
                                 'parts':pageDict['CFparts']+[{'compType':"OEC_PRE_PREG_HR",'SN':pageDict['prePreg']}],
                                 'assembly':False }
            ### assemble function
            pdbTrx.AssembleComponent(pageDict['assDict'],True)

        if "assDict" not in pageDict.keys():
            st.stop()

        st.write("### Assembled:")
        if pageDict['assDict']['assembly']==True:
            infra.ToggleButton(pageDict,'togAss',pageDict['assDict']['comb']['SN'])
            #st.write("Component:",ba['comb']['SN'])
            if pageDict['togAss']:
                combComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {"component" : pageDict['assDict']['comb']['SN']} )
                st.write("Children:",[x['component']['serialNumber'] for x in combComp['children'] if x['component']!=None])
                stTrx.DebugOutput("Children details: ",combComp['children'])

                positions=[]
                for x in combComp['children']:
                    if x['component']==None: continue
                    positions.append({'x':int(x['order'])/2,'y':int(x['order'])%2,'text':x['component']['serialNumber']})
                stTrx.VisOrientation(pd.DataFrame(positions))

                if st.button("disassemble"):
                    pdbTrx.DisassembleComponent(combComp)
