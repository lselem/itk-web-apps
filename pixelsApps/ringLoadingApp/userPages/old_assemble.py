### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

infoList=["  * Load and register a ring in PDB"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Assemble", ":microscope: Check component in PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # box
        st.write("### Complex selection")
        infra.TextBox(pageDict,'compCode',"Input Ring ID:")
        ## get ring component
        ## check constituents
        ## show fileds per component, pre-filled if constituent exists
        ## add checks for components

        # parts
        st.write("### Constituent selection")

        for i in range(0,22,1):
            infra.TextBox(pageDict,'pixModCode'+str(i),"Input pixel module "+str(i)+" ID:")
            if st.button("module check", key=str(100+i)):
                compCheck=True
                pageDict['pixMod'+str(i)]=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pageDict['pixModCode'+str(i)]})
                if st.session_state.debug: st.write(pageDict['pixMod'+str(i)])
                try:
                    if pageDict['pixMod'+str(i)]['componentType']['code']!="MODULE":
                        st.write("Problem with component type:",pageDict['sComp']['componentType']['code'])
                        compCheck=False
                except TypeError:
                    st.write("Problem with database return")
                    compCheck=False

        if st.button("Assemble!"):
            st.balloons()
            st.write("**To Do** assemble")
