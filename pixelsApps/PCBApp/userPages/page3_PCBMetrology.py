### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ pull test file",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("PCB Metrology", ":microscope: Upload PCB Metrology Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="PCB"
        if "testCode" not in pageDict.keys():
            pageDict['testCode']="METROLOGY"
        if "stage" not in pageDict.keys():
            pageDict['stage']="PCB_RECEPTION_MODULE_SITE"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_P.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testCode']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testCode'])
        st.dataframe(df_stageTest)

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")


        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])


        selMode=st.radio("Select input file style:",["Glasgow","Argonne"])

        if selMode=="Glasgow":
            st.write("__Using Glasgow style input__")
            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="PCB_METROLOGY_GL_example.csv"
            df_in=chnx.UploadFileChunk(pageDict,["csv"],filePath+"/"+exampleFileName, None, None, None, -1)

        if selMode=="Argonne":
            st.write("__Using Argonne style input__")

            ### upload measurement file
            filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
            exampleFileName="PCB_METROLOGY_ANL_example.txt"
            df_in=chnx.UploadFileChunk(pageDict,["txt"],filePath+"/"+exampleFileName, None, None, '|', -1)


        st.write("## Read Data")
        st.write(df_in)

        ### TODO add smartscope option and analysis

        # get test schema
        if "testSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testCode']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['testSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testCode']})#, 'requiredOnly':True})
            # hack to coordinate with chnx.RegChunk
        # add defaults
        pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
        stTrx.DebugOutput("Original *schema*:",pageDict['testSchema'])

        ### wipe results
        stTrx.SetDictValsNone(pageDict['testSchema']['results'])

        if selMode=="Glasgow":
            ### update json
            # header
            st.write("Read serial number in file")
            snStr=df_in.query('a.str.contains("Serial")', engine='python' )['b'].values[0]
            pageDict['testSchema']['component']=pdbTrx.GetSNFromString(pageDict, snStr)
            dateStr=df_in.query('a.str.contains("Date")', engine='python' )['b'].values[0]
            pageDict['testSchema']['date']=datetime.strptime(dateStr, "%d/%m/%Y %H:%M").strftime("%Y-%m-%dT%H:%MZ")
            # properties
            pageDict['testSchema']['properties']['ANALYSIS_VERSION']="webApp "+chnx.GetCommitSHA()
            # instrument
            if df_in.query('a.str.contains("perator")', engine='python' ).empty:
                pageDict['testSchema']['properties']['OPERATOR']="Unknown"
            else:
                pageDict['testSchema']['properties']['OPERATOR']=df_in.query('a.str.contains("perator")', engine='python' )['b'].to_list()[0]
            # operator
            if df_in.query('a.str.contains("nstrument")', engine='python' ).empty:
                pageDict['testSchema']['properties']['INSTRUMENT']="Unknown"
            else:
                pageDict['testSchema']['properties']['INSTRUMENT']=df_in.query('a.str.contains("nstrument")', engine='python' )['b'].to_list()[0]
            # results
            pageDict['testSchema']['results']['X_DIMENSION']=df_in.query('a.str.contains("X")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['Y_DIMENSION']=df_in.query('a.str.contains("Y")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['AVERAGE_THICKNESS_FECHIP_PICKUP_AREAS']=df_in.query('a.str.contains("GA")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['STD_DEVIATION_THICKNESS_FECHIP_PICKUP_AREAS']=df_in.query('a.str.contains("GA")', engine='python' )['b'].astype(float).std()
            pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS']=df_in.query('a.str.contains("HV")', engine='python' )['b'].astype(float).mean()
            pageDict['testSchema']['results']['AVERAGE_THICKNESS_POWER_CONNECTOR']=df_in.query('a.str.contains("FTM")', engine='python' )['b'].astype(float).mean()

        if selMode=="Argonne":
            ### update json
            # header
            st.write("Read serial number from file name")
            pageDict['testSchema']['component']=pdbTrx.GetSNFromString(pageDict,pageDict['file'].name)
            dateStr=df_in.query('a.str.contains("DATE")', engine='python' )['b'].values[0]
            dateStr+=" "+df_in.query('a.str.contains("TIME")', engine='python' )['b'].values[0]
            pageDict['testSchema']['date']=datetime.strptime(dateStr, "%m:%d:%y %H:%M:%S").strftime("%Y-%m-%dT%H:%MZ")
            # properties
            pageDict['testSchema']['properties']['ANALYSIS_VERSION']="webApp"
            # instrument
            if df_in.query('a.str.contains("perator")', engine='python' ).empty:
                pageDict['testSchema']['properties']['OPERATOR']="Unknown"
            else:
                pageDict['testSchema']['properties']['OPERATOR']=df_in.query('a.str.contains("perator")', engine='python' )['b'].to_list()[0]
            # operator
            if df_in.query('a.str.contains("nstrument")', engine='python' ).empty:
                pageDict['testSchema']['properties']['INSTRUMENT']="Unknown"
            else:
                pageDict['testSchema']['properties']['INSTRUMENT']=df_in.query('a.str.contains("nstrument")', engine='python' )['b'].to_list()[0]
            # results
            pageDict['testSchema']['results']['X_DIMENSION']=df_in.query('b.str.contains("_X")', engine='python' )['d'].astype(float).mean()
            pageDict['testSchema']['results']['Y_DIMENSION']=df_in.query('b.str.contains("_Y")', engine='python' )['d'].astype(float).mean()
            jigValue=df_in.query('b.str.contains("Jig")', engine='python' )['d'].astype(float).mean()
            pageDict['testSchema']['results']['AVERAGE_THICKNESS_FECHIP_PICKUP_AREAS']=df_in.query('b.str.contains("Pickup")', engine='python' )['d'].astype(float).mean() - jigValue
            pageDict['testSchema']['results']['STD_DEVIATION_THICKNESS_FECHIP_PICKUP_AREAS']=df_in.query('b.str.contains("Pickup")', engine='python' )['d'].astype(float).std()
            pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS']=df_in.query('b.str.contains("HV")', engine='python' )['d'].astype(float).mean() - jigValue
            pageDict['testSchema']['results']['AVERAGE_THICKNESS_POWER_CONNECTOR']=df_in.query('b.str.contains("PC")', engine='python' )['d'].astype(float).mean()  - jigValue


        ### analysis
        # X-Y envelop
        st.write(f"__Analyse X-Y Dimensions...__")
        pageDict['testSchema']['results']['X-Y_DIMENSION_WITHIN_ENVELOP']=False
        st.write(f"Checking serial number for type...")
        if "20U" not in pageDict['testSchema']['component']:
            st.write(f" - issue with serial number {pageDict['testSchema']['component']}, cannot identify flex digit")
            st.stop()
        else:
            if pageDict['testSchema']['component'][7] in ["2","4"]:
                st.write(" - this looks like an __outer__ system flex. Check X=(39.5,39.7)mm and Y=(40.5,40.7)")
                if 39.5<pageDict['testSchema']['results']['X_DIMENSION'] and pageDict['testSchema']['results']['X_DIMENSION']<39.7:
                    st.write(" - X dimension __ok__ ✅")
                    if 40.5<pageDict['testSchema']['results']['Y_DIMENSION'] and pageDict['testSchema']['results']['Y_DIMENSION']<40.7:
                        st.write(" - Y dimension __ok__ ✅")
                        pageDict['testSchema']['results']['X-Y_DIMENSION_WITHIN_ENVELOP']=True
                    else:
                        st.write(f" - Y dimension __out of spec__ ❌: {pageDict['testSchema']['results']['Y_DIMENSION']}")
                else:
                    st.write(f" - X dimension __out of spec__ ❌: {pageDict['testSchema']['results']['X_DIMENSION']}")
            elif pageDict['testSchema']['component'][7] in ["3","5"]:
                st.write(" - this looks like an __inner__ system flex. Check X=(39.5,39.7)mm and Y=(40.3,40.5)")
                if 39.5<pageDict['testSchema']['results']['X_DIMENSION'] and pageDict['testSchema']['results']['X_DIMENSION']<39.7:
                    st.write(" - X dimension __ok__ ✅")
                    if 40.3<pageDict['testSchema']['results']['Y_DIMENSION'] and pageDict['testSchema']['results']['Y_DIMENSION']<40.5:
                        st.write(" - Y dimension __ok__ ✅")
                        pageDict['testSchema']['results']['X-Y_DIMENSION_WITHIN_ENVELOP']=True
                    else:
                        st.write(f" - Y dimension __out of spec__ ❌: {pageDict['testSchema']['results']['Y_DIMENSION']}")
                else:
                    st.write(f" - X dimension __out of spec__ ❌: {pageDict['testSchema']['results']['X_DIMENSION']}")
            else:
                st.write(f" - cannot identify type using digit: {pageDict['testSchema']['component'][7]}")
        # HV capicitor envelope
        st.write(f"__Analyse HV capacitor thickness...__")
        pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS_WITHIN_ENVELOP']=False
        if 1.701<pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS'] and pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS']<2.111:
            st.write(" - HV capacitor thickness __ok__ ✅")
            pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS_WITHIN_ENVELOP']=True
        else:
            st.write(f" - HV capacitor thickness __out of spec__ ❌: {pageDict['testSchema']['results']['HV_CAPACITOR_THICKNESS']}")
        
        ### summy for the moment - should remove from testType structure and omit later
        pageDict['testSchema']['results']['DIAMETER_DOWEL_HOLE_A']=-999
        pageDict['testSchema']['results']['WIDTH_DOWEL_SLOT_B']=-999


        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict,'testSchema')

        # st.write("### :construction: Page under construction :construction:")
        # st.stop()

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")
