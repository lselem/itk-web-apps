### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import pandas as pd
import datetime
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

import smtplib 

#####################
### useful functions
#####################

def SendEmail(eDict,txt=None):
    ### assume Cern, port from:
    # https://espace.cern.ch/mmmservices-help/AccessingYourMailbox/Pages/default.aspx
    deets={'smtp':"smtp.cern.ch",'port':587}
    
    psswrd=st.text_input("email password:",value="password")
    
    if st.button("Send email"):
        # creates SMTP session 
        s = smtplib.SMTP(deets['smtp'], deets['port']) 
        # start TLS for security 
        s.starttls() 
        # Authentication 
        try:
            s.login(eDict['from'], psswrd)
            # message to be sent 
            message= f"From: {eDict['from']}\r\nTo: {eDict['to']}\r\nSubject: {eDict['subject']}\r\n\r\n"
            message+= f"{eDict['body']} \r\n\r\n" 
            # sending the mail 
            s.sendmail(eDict['from'], eDict['to'], message) 
            # terminating the session 
            st.success("Email sent")
        except:
            st.error("login error: please check email settings")
        s.quit()

    return None 


infoList=[" * check component sub-type",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Review SQ", ":microscope: Update Site Qualification", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULESQ"
        if "project" not in pageDict.keys():
            pageDict['project']="P"
        if "institution" not in pageDict.keys():
            pageDict['institution']=st.session_state.Authenticate['inst']['code']
        
        if "df_sqObjs" not in pageDict.keys():
            sqObjs = DBaccess.DbGet(st.session_state.myClient,'listComponents', {'project':pageDict['project'], 'componentType':pageDict['componentType']},True)
            pageDict['df_sqObjs']=pd.json_normalize(sqObjs,sep='_')

        stTrx.DebugOutput("All SQ components",pageDict['df_sqObjs'])
      
        if "userType" not in pageDict.keys():
            pageDict['userType']="reviewer"
        
        st.write("Some instruction on using the app can be found [here](https://indico.cern.ch/event/1258215/contributions/5285054/attachments/2598717/4487552/WP13_23_2_23_KGW.pdf)")

        infra.Radio(pageDict,'userType',["Site","Reviewer"],"Select user type:")
        

        #################
        ### reviewer user
        #################
        if pageDict['userType'].lower()=="reviewer":

            infra.ToggleButton(pageDict,'tog_file',"Input reviewer file")
            if pageDict['tog_file']:
                st.write("## Input reviewer file")
                pageDict['file']= st.file_uploader("Add file", type=["csv"])

                if pageDict['file']==None:
                    st.write("Please upload reviewer file")
                    st.stop()
            
                st.write("### Reviews to match")
                df_review = pd.read_csv(pageDict['file'])
                st.write(df_review)

                st.write("---")
                ### found tests
                st.write("## Matched Reviews")
                testRunIDs=[]
                for i,row in df_review.iterrows():
                    st.write(f"searching for test: {row['testCode']} @ {row['instCode']}")
                    
                    sn_match=pageDict['df_sqObjs'].query('institution_code=="'+row['instCode']+'"')['serialNumber'].values[0]
                    st.write("match:",sn_match)
                    compObj= DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':sn_match})

                    # st.write(compObj['tests'])

                    testRunList=[y['id'] for x in compObj['tests'] for y in x['testRuns'] if x['code']==row['testCode'] and y['state']=="ready"]
                    st.write(f"found {len(testRunList)} tests of type: {row['testCode']}")
                    testRunIDs.extend(testRunList)

            else:
                st.write("## Select tests by code")
                # st.write(pageDict['df_sqObjs'])
                sqCompType = DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode', {'project':pageDict['project'], 'code':pageDict['componentType']})
                # st.write(sqCompType)
                ### get stage
                sel_sqStage=st.selectbox("SQ stage :", [x['code'] for x in sqCompType['stages']])
                ### get test
                sel_sqTest=st.selectbox("SQ test :", [y['testType']['code'] for x in sqCompType['stages'] for y in x['testTypes'] if x['code']==sel_sqStage])

                st.write(f"Getting {sel_sqTest} tests")

                df_stageComps=pageDict['df_sqObjs'].explode('stages').reset_index(drop=True)
                df_stageComps['stages']=df_stageComps['stages'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
                
                infra.ToggleButton(pageDict,'tog_sqAll',"See all stage uploads")
                if pageDict['tog_sqAll']:
                    st.write(df_stageComps.groupby(by=['serialNumber','stages']).first().reset_index(drop=True))

                df_selComps=df_stageComps.query('stages=="'+sel_sqStage+'"').reset_index(drop=True)
                # st.write(df_selComps[['code','serialNumber','stages']])

                ### get component bulk
                compSet = DBaccess.DbGet(st.session_state.myClient,'getComponentBulk', {'component':df_selComps['code'].to_list()})
                df_compSet=pd.DataFrame(compSet)
                df_compSet=df_compSet.explode('tests')
                df_compSet['testCode']=df_compSet['tests'].apply(lambda x: x['code'] if type(x)== type({}) and "code" in x.keys() else x)
                df_compSet['institution']=df_compSet['institution'].apply(lambda x: x['code'] if type(x)== type({}) and "code" in x.keys() else x)
                df_compSet=df_compSet.query('testCode=="'+sel_sqTest+'"').reset_index(drop=True)
                df_compSet['testRuns']=df_compSet['tests'].apply(lambda x: x['testRuns'] if type(x)== type({}) and "testRuns" in x.keys() else x)
                df_compSet=df_compSet.explode('testRuns').reset_index(drop=True)
                df_compSet['testRunID']=df_compSet['testRuns'].apply(lambda x: x['id'] if type(x)== type({}) and "id" in x.keys() else x)
                df_compSet_vc=df_compSet[['institution','testCode']].value_counts().reset_index()
                if df_compSet_vc.empty:
                    st.write("No tests found")
                else:
                    st.write("Tests found summary")
                    st.write(df_compSet_vc)

                ### get testRun ids
                testRunIDs=df_compSet['testRunID'].to_list()
                # st.write(testRunIDs)

        #################
        ### site user
        #################
        else:
            pageDict['institution']=st.session_state.Authenticate['inst']['code']
            st.write(f"### Searching for tests from: {pageDict['institution']}")

            # get site qualification object (should be exactly per site)
            if "sqObj" not in pageDict.keys() or st.button("re-check component"):
                sqObjs = DBaccess.DbGet(st.session_state.myClient,'listComponents', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'institution':pageDict['institution']},True)

                # st.write(pageDict['sqObj'])

                if len(sqObjs)>1:
                    st.write(":exclamation: Found more than one?!")
                    st.write([sqo['serialNumber'] for sqo in sqObjs])
                    st.write(f"Will continue with first {sqObjs[0]['serialNumber']}!")
                elif len(sqObjs)<1:
                    st.write(f":exclamation: No SQ components found for {pageDict['institution']}!")
                    st.stop()
                else:
                    stTrx.DebugOutput(f"Got the one {sqObjs[0]['serialNumber']} :)")
                
                pageDict['sqObj']=DBaccess.DbGet(st.session_state.myClient,'getComponent',{'component':sqObjs[0]['serialNumber'] })
            
            # full info. option
            infra.ToggleButton(pageDict,'tog_full',"See full component info")
            if pageDict['tog_full']:
                st.write(pageDict['sqObj'])

            df_test=pd.DataFrame(pageDict['sqObj']['tests'])
            # st.write(df_test.explode('testRuns')['testRuns'].apply(pd.Series))

            testRunIDs=df_test.explode('testRuns')['testRuns'].apply(pd.Series)['id'].to_list()
            # st.write(testRunIDs)

        #################
        ### all users
        #################
        # st.write(testRunIDs)
        if "testRunInfo" not in pageDict.keys() or st.button("re-check test info.") or len(testRunIDs)!=len(pageDict['testRunInfo']) or pageDict['testRunInfo'][0]['id']!=testRunIDs[0]:
            pageDict['testRunInfo']= DBaccess.DbGet(st.session_state.myClient,'getTestRunBulk', {'testRun':testRunIDs}, True)
        # st.write(pageDict['testRunInfo'])
        df_testRuns= pd.DataFrame(pageDict['testRunInfo'])

        if df_testRuns.empty:
            st.write("No tests found. Please check inputs")
            st.stop()

        # st.write(df_testRuns.columns)
        st.write("### Matched test runs:")
        for cn in ['testType','institution']:
            df_testRuns[cn]=df_testRuns[cn].apply(lambda x: x['code'] if "code" in x.keys() else x)
        df_testRuns['nickName']=df_testRuns.apply(lambda row: row['testType']+"_"+row['institution']+"_"+row['date'], axis=1)
        
        ### tidy & order 
        df_testRuns['date']=pd.to_datetime(df_testRuns['date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
        df_testRuns=df_testRuns.sort_values(by=["testType","date"])

        ### revierw get latest, site select by testType
        df_matched=pd.DataFrame()
        if pageDict['userType'].lower()=="reviewer":
            st.write("Getting latest tests for reviewer") 
            infra.ToggleButton(pageDict,'revAll',"Show me all uploads")
            
            for inst in df_testRuns['institution'].unique():
                for tt in df_testRuns['testType'].unique():
                    if pageDict['revAll']:
                        st.write(f" - get _all_ {tt} @ {inst}")
                        if df_matched.empty:
                            df_matched=df_testRuns.query('institution=="'+inst+'" &testType=="'+tt+'"')
                        else:
                            df_matched=pd.concat([df_matched, df_testRuns.query('institution=="'+inst+'" &testType=="'+tt+'"')])
                    else:
                        st.write(f" - get _last_ {tt} @ {inst}")
                        if df_matched.empty:
                            df_matched=df_testRuns.query('institution=="'+inst+'" &testType=="'+tt+'"').tail(1)
                        else:
                            df_matched=pd.concat([df_matched, df_testRuns.query('institution=="'+inst+'" &testType=="'+tt+'"').tail(1)])
            
        else:
            infra.ToggleButton(pageDict,'selectType',"Select test type")
            if pageDict['selectType']:
                sel_testType=st.selectbox("Test Type :", df_testRuns['testType'].unique())
                df_matched=df_testRuns.query('testType=="'+sel_testType+'"')
            else:
                df_matched=df_testRuns
            
        df_matched=df_matched[['id','testType','state','institution','date','nickName']].reset_index(drop=True)


        infra.ToggleButton(pageDict,'removeDel',"Ignore deleted tests?")
        if pageDict['removeDel']:
            df_matched=df_matched.query('state=="ready"')
        
        st.dataframe(df_matched.drop(columns=['nickName']))


        ### select test
        # infra.SelectBox(pageDict,'selTest',df_matched['nickName'].to_list(),'Select testRun:')
        pageDict['selTest']=st.selectbox('Select testRun:', df_matched['nickName'].to_list())


        st.write("---")

        st.write("### SQ Test Details")
        # need to reset index so all entries are 0th entry
        testDict=df_testRuns.query('nickName=="'+pageDict['selTest']+'"').reset_index().to_dict()

        infra.ToggleButton(pageDict,'tog_fullTestRun',"See full testRun object?")
        if pageDict['tog_fullTestRun']:
            st.write("Full testDict",testDict)

        st.write("__Test__:",testDict['testType'][0],"from",testDict['institution'][0])
        st.write("__Submitted__:",testDict['date'][0],"by",testDict['user'][0]['firstName'],testDict['user'][0]['lastName'])
        
        # find status object by null value (hack)
        # statusObj=next((item for item in testDict['results'][0] if "codeTable" in item.keys()), None)
        # find status object by SQ_STATUS code
        statusObj=next((item for item in testDict['properties'][0] if item['code'] == "SQ_STATUS"), None)
        st.write("__Current SQ status__:",statusObj['name'])
        st.write("\t",statusObj['value'])

        snObj=next((item for item in testDict['results'][0] if item['code'] == "ITEM_SN"), None)
        st.write("__Serial Numbers__:",snObj['name'])

        if snObj['value']!=None:
            for sn in snObj['value']:
                st.write(f"- serialNumber: {sn}")
                try:
                    snObj= DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':sn})
                    snLink="https://uuapp.plus4u.net/ucl-itkpd-maing01/dcb3f6d1f130482581ba1e7bbe34413c/componentView?code="
                    snLink+=snObj['code']
                    st.write(f"- component [link]({snLink})")
                except:
                    st.write(f"- link generation failed. Please try component look-up [here](https://uuapp.plus4u.net/ucl-itkpd-maing01/dcb3f6d1f130482581ba1e7bbe34413c/components)")
        else:
            st.write("- No serial numbers found")

        linkObj=next((item for item in testDict['results'][0] if item['code'] == "LINK"), None)
        st.write("__Link__:",linkObj['name'])
        st.write(f"- {linkObj['value']}" )

        st.write("__Attachments__:")
        
        testRunLink="https://uuapp.plus4u.net/ucl-itkpd-maing01/dcb3f6d1f130482581ba1e7bbe34413c/testRunView?id="
        testRunLink+=testDict['id'][0]
        st.write("- See bottom of test page:")
        st.write(f"- test [link]({testRunLink})")


        try:
            [st.write(e,":",c['filename']) for e,c in enumerate(testDict['attachments'][0])]
        except KeyError:
            st.write("No attachments found")

        st.write("__Comments__:")
        try:
            [st.write(e,":",c['comment']) for e,c in enumerate(testDict['comments'][0])]
        except KeyError:
            st.write("No comments found")
        except TypeError:
            st.write("No comments found")

        st.write("---")

        #########################
        ### end of page options
        #########################

        if pageDict['userType'].lower()=="reviewer":

            st.write("## Reviewer section")

            st.write("### Update test status ")
            # links to components/ get comp data
            # embed link?

            codeTable=[
                {"code":"-1","value":"not there"},
                {"code":"0","value":"ready for review"},
                {"code":"1","value":"under review"},
                {"code":"2","value":"reviewed - update required"},
                {"code":"3","value":"passed"}
            ]
            ### option to check map
            if st.checkbox("See code map"):
                st.dataframe(codeTable)
            ### hack to handle unmapped entries (PDB codeTable issue)
            if statusObj['value'] in [ct['value'] for ct in codeTable]:
                inIdx=[ct['value'] for ct in codeTable].index(statusObj['value'])
            elif statusObj['value'] in [ct['code'] for ct in codeTable]:
                inIdx=[ct['code'] for ct in codeTable].index(statusObj['value'])
            else:
                st.write(f"Cannot identify codeTable entry for {statusObj['value']}")
                st.stop()
            ### user selection
            val=st.selectbox("SQ status :", [ct['value'] for ct in codeTable], index=inIdx)
            pageDict['selStatus']= next((item for item in codeTable if item['value'] == val), None)

            st.write(f"- set status (code): {pageDict['selStatus']['value']} ({pageDict['selStatus']['code']})")

            ### get and populate new testRun object
            # uploadJson={'testRun':testDict['id'][0],'code':statusObj['code'],'value':pageDict['selStatus']['code']}
                    # get test schema
            if "testSchema" not in pageDict.keys() or st.button("Reset Schema: "+testDict['testType'][0]+" for "+pageDict['componentType']) or testDict['institution'][0]!=pageDict['testSchema']['institution'] or testDict['testType'][0]!=pageDict['testSchema']['testType']:
                # get schema form PDB
                pageDict['testSchema']=DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample',  {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':testDict['testType'][0]}) 
                # copy over current info.
                pageDict['testSchema']['component']=testDict['components'][0][0]['serialNumber']
                for k in ["institution","runNumber","passed","problems"]:
                    pageDict['testSchema'][k]=testDict[k][0]
                for k in ["ITEM_SN","LINK"]:
                    pageDict['testSchema']['results'][k]=next( (x['value'] for x in testDict['results'][0] if x['code']==k) , None)
                # hack to coordinate with chnx.RegChunk
                pageDict['stage']=testDict['components'][0][0]['testedAtStage']['code']

            pageDict['testSchema']['properties']['SQ_STATUS']=pageDict['selStatus']['code']
            stTrx.DebugOutput("upload schema:",pageDict['testSchema'])

            st.write("### Upload reviewed test to PDB")
            st.write(f"__{pageDict['testSchema']['testType']}__ for {pageDict['testSchema']['component']} @ {pageDict['testSchema']['institution']}")
            if st.checkbox("See upload json?"):
                st.write(pageDict['testSchema'])
            ### upload
            chnx.RegChunk(pageDict, "TestRun", "testSchema")


            infra.ToggleButton(pageDict,'tog_email',"Send email to site?")
            if pageDict['tog_email']:
                st.write(":email: Send notification email to test upload user (uses Cern SMTP).")
                emailDict={}
                try:
                    emailDict['from']=st.session_state.Authenticate['user']['email']
                    #st.write("Sender email:",st.session_state.Authenticate['user']['email'])
                except KeyError:
                    emailDict['from']=None
                    st.write("no _sender_ email key found")  

                sendeeObj= DBaccess.DbGet(st.session_state.myClient,'getUser', {'userIdentity':testDict['user'][0]['userIdentity']})
                try:
                    emailDict['to']=sendeeObj['email']
                    #st.write("Sendee email:",sendeeObj['email'])
                except KeyError:
                    emailDict['to']=None
                    st.write("no _sendee_ email key found")
                
                emailDict['subject']= f"SQ Review Update: {testDict['testType'][0]} from {testDict['institution'][0]}"
            
                emailDict['body']= f"Hello, \r\n\r\nYour SQ status a test has been updated. \r\n\r\n"
                emailDict['body']+= f"{testDict['testType'][0]} : {pageDict['selStatus']['value']} \r\n\r\n" 
                emailDict['body']+= f"You can find more information: https://uk-itk-pdb-webapp-pixels.web.cern.ch \r\n\r\n" 
                emailDict['body']+= f"Regards, \r\n\r\nRobin Goodfellow \r\n(aka Puck: https://en.wikipedia.org/wiki/Puca ) \r\n\r\n"

                infra.ToggleButton(pageDict,'tog_emailDeets',"Change email settings?")
                if not pageDict['tog_emailDeets']:
                    st.json(emailDict)
                else:
                    pdbTrx.EditJson(emailDict)

                SendEmail(emailDict)

        else:
            ### delete option for site
            pdbTrx.DeleteCoTeSt(pageDict,'delTest','deleteTestRun',testDict['id'][0])
