# Module App

Tools for Pixels Modules.

---

## Useful Documents

Thick uncoated module [drawing](https://edms.cern.ch/ui/file/2363543/10/AT2-0000020505_THICK_UNCOATED_MODULE_v11.PDF).

---

## Pages

### Trim Check
 * get chip ITRIM values form Module, Bare Module or Module Carrier serial number

### Translator
 * get sensor, chip serial number based on input identifier: alternative identifier, manufacturer serial number

