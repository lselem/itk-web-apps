### standard
import streamlit as st
from core.Page import Page
import os
from io import BytesIO
from io import StringIO
### custom
import pandas as pd
import ast
import csv
import json
from pathlib import Path
import plotly.graph_objects as go
from datetime import datetime
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### analyses
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################
def ToExcel(df,shtNm):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, index=False, sheet_name=shtNm, header=False)
    workbook = writer.book
    worksheet = writer.sheets[shtNm]
    format1 = workbook.add_format({'num_format': '0.00'})
    worksheet.set_column('A:A', None, format1)
    writer.save()
    processed_data = output.getvalue()
    return processed_data

def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    try: # avoid bool trouble
        if val.isnumeric(): # for integers formatted as strings by schema
            val=int(val)
    except AttributeError:
        pass
    return val

def ColorCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)

def EditJson(inJson):
    for k,v in inJson.items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                inJson[k][l]=SelectCheck(l,w)
        else:
            inJson[k]=SelectCheck(k,v)
    return inJson




infoList=["  * upload wirebonding information file",
        "  * review test schema",
        "   * reset if required",
        "   * edit if required",
        "  * upload test schema",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Wirebonding Info", ":microscope: Upload Wirebonding Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "code" not in pageDict.keys():
            pageDict['code']="WIREBONDING"
        if "stage" not in pageDict.keys():
            pageDict['stage']="MODULE/WIREBONDING"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        ### hidden changer
        if st.session_state.debug:
            st.write("**DEBUG** Hidden changer")
            infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
            if pageDict['toggleChanger']:
                infra.TextBox(pageDict,'componentType',"Enter componentType code:")
                infra.TextBox(pageDict,'code',"Enter testType code:")
                infra.TextBox(pageDict,'stage',"Enter testStage _code_:")
                infra.TextBox(pageDict,'project',"Enter project code:")


        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['code']+"@"+pageDict['stage']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':True})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["json"])
        if st.session_state.debug: 
            st.write(pageDict['file'])

        ### offer example if no file
        if pageDict['file'] is None:
            ### delete existing info (defined below)
            try:
                del pageDict['regList']
            except KeyError:
                pass
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="wirebonding_example.json"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
                st.write(os.listdir(filePath[:filePath.rfind('/')]))
            st.download_button(label="Download example", data=Path(filePath[:filePath.rfind('/')]+"/"+exampleFileName).read_text(), file_name=exampleFileName)
            for k in pageDict.keys()-['file']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()

        ### read in formatted json file
        if "json" in pageDict['file'].type:
            st.write("_json_ file recognised ")
            data=json.loads(StringIO(pageDict['file'].getvalue().decode("utf-8")).read())
        ### unknown file type
        else:
            st.write(f"_{pageDict['file'].type}_ file unrecognised. Please use _json_.")
            st.stop()

        ### add gleaned information to test schema
        # header
        pageDict['testSchema']=data

        ### Get serialNumber from file
        # check file name
        serialNumber = pageDict['testSchema']['component']


        ### show/edit data for upload
        st.write("---")
        st.write("### Review test Schema")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        st.write("## Test Registration")

        # # check existing component tests
        # chnx.StageCheck(pageDict)

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        chnx.RegChunk(pageDict, "TestRun", "testSchema")


        ### ToDo?: look up module carrier in database and find (child) module
        # infra.TextBox(pageDict,'carSN',"Enter carrier serialNumber:")
        # # find carrier and module
        # pageDict['testSchema']['component']=pageDict['modSN']

        # # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        # if st.button("Upload Test"):
        #     ### set stage
        #     try:
        #         pageDict['setVal']=DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':pageDict['testSchema']['component'], 'stage':pageDict['stage']})
        #         st.write("### **Successful stage set** (",pageDict['stage'],") for:",pageDict['setVal']['serialNumber'])
        #         # df_set=pd.json_normalize(pageDict['setVal'], sep = "_")
        #         # st.dataframe(df_set)
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: Stage (",pageDict['stage'],")setting **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     ### upload data
        #     try:
        #         pageDict['upVal']=DBaccess.DbPost(st.session_state.myClient,'uploadTestRunResults', pageDict['testSchema'])
        #         st.write("### **Successful Upload**:",pageDict['upVal']['componentTestRun']['date'])
        #         st.balloons()
        #         st.write(pageDict['upVal'])
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: Update **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        # if 'upVal' in pageDict.keys():
        #     try:
        #         st.write("last successful _upload_:",pageDict['upVal']['testRun']['id'])
        #         if st.button("Delete testrun"):
        #             ### delete data
        #             try:
        #                 pageDict['delVal'] = DBaccess.DbPost(st.session_state.myClient,'deleteTestRun', {'testRun':pageDict['upVal']['testRun']['id']})
        #                 st.write("### **Successful Deletion**:",pageDict['delVal']['testRun']['id'])
        #                 st.balloons()
        #                 st.write(pageDict['delVal'])
        #             except itkX.BadRequest as b:
        #                 st.write("### :no_entry_sign: Deletion unsuccessful")
        #                 st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     except KeyError:
        #         pass
        # if 'delVal' in pageDict.keys():
        #     try:
        #         st.write("last successful _deletion_:",pageDict['delVal']['testRun']['id'])
        #     except KeyError:
        #         pass
