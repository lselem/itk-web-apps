### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
from PIL import Image
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

### return component children with childTypeCode
def GetChildren(compCode,childTypeCode):
    myComp= DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':compCode})
    try:
        st.write("found children:",len([x for x in myComp['children'] if x['component']!=None]))
        # childList=[x['component']['code'] for x in myComp['children'] if x['componentType']['code']==childTypeCode and x['component']!=None]
        childList=[x for x in myComp['children'] if x['componentType']['code']==childTypeCode and x['component']!=None]
    except TypeError:
        st.write("type error")
        childList=None
    except KeyError:
        st.write("key error")
        childList=None
    return childList
### return component parent with parentTypeCode
def GetParents(compCode,parentTypeCode):
    myComp= DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':compCode})
    try:
        st.write("found parents:",len([x for x in myComp['parents'] if x['component']!=None]))
        # parentList=[x['component']['code'] for x in myComp['parents'] if x['componentType']['code']==parentTypeCode and x['component']!=None]
        parentList=[x for x in myComp['parents'] if x['componentType']['code']==parentTypeCode and x['component']!=None]
    except TypeError:
        st.write("type error")
        parentList=None
    except KeyError:
        st.write("key error")
        parentList=None
    return parentList

### return bit dictionary
def bitfield(n):
    return [{str(2**e):int(digit)} for e,digit in enumerate(reversed(format(n, '#06b')[2:]))] # [2:] to chop off the "0b" part

infoList=[" * Input identifier for MODULE, BARE_MODULE, MODULE_CARRIER, PCB",
        " * Get parent MODULE (if necessary)",
        " * Get MODULE ORIENTATION property",
        " * Get FE_CHIP children of BARE_MODULE child",
        " * Get TRIM information from FE_CHIP tests",
        " * Visualise bond padd connections"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Trim Check", ":microscope: Check Module Front End TRIM values", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("## Select component")

        infra.Radio(pageDict,'inputType',["Enter identifier","Select from inventory"],"Select input method:")
        st.write("### ",pageDict['inputType'])

        if "identifier" in pageDict['inputType']:

            infra.TextBox(pageDict,'inComp',"Enter component identifier")
            infra.Radio(pageDict,'altID',['serialNumber','alternativeIdentifier'],"Type of identifier")

            if pageDict['inComp']==None or len(pageDict['inComp'])<1:
                st.write("Enter identifier to get component information")
                st.stop()

            if 'comp' not in pageDict.keys() or st.button("Get component"):
                compQueryJson={'component':pageDict['inComp'],'type':"SN"}
                if pageDict['altID']=='alternativeIdentifier':
                    compQueryJson['type']='altID'
                stTrx.DebugOutput("queryJson:",compQueryJson)

                pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

        else:
            ### select components by hand
            if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
                st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
                pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
            else:
                st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            df_compList=pd.json_normalize(pageDict['compList'], sep = "_")

            df_subComps=pdbTrx.SelectManyDf(pageDict,df_compList,'componentType_code')
            stTrx.Stringify(df_subComps)
            infra.SelectBox(pageDict,'selSN',df_subComps['serialNumber'].to_list(),'Select serialNumbers:')
            stTrx.DebugOutput('Selected serialnumber: ',pageDict['selSN'])
            pageDict['comp'] = next(item for item in pageDict['compList'] if item['serialNumber'] == pageDict['selSN'])

        if 'comp' not in pageDict.keys():
            st.write("Enter identifier to get component information")
            st.stop()

        stTrx.DebugOutput("ComponentJson:",pageDict['comp'])
        if pageDict['comp']==None:
            st.write("No component information found. Check inputs")
            st.stop()

        st.write("---")

        myComp= DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pageDict['comp']['code']})


        ########################
        ### Extract data from parents & children
        ########################
        try:
            if myComp['componentType']['code'] not in ['MODULE','BARE_MODULE','MODULE_CARRIER','PCB']:
                st.write("This ain\'t no module. It's a "+myComp['componentType']['code']+". **HCF**")
                st.stop()
        except KeyError:
            st.write("no componentType code key")
            st.stop()
        except TypeError:
            st.write("unexpected object")
            st.stop()

        modChiSNs=None
        oriFlag=True
        modType=None
        if myComp['componentType']['code']=="MODULE":
            parType, chiType= "MODULE", "BARE_MODULE"
            st.write("checking "+parType+" component for "+chiType+" children")
            modType=myComp['type']['code']
            modComp=myComp
            try:
                oriFlag=next((item for item in myComp['properties'] if item['code'] == "ORIENTATION"), None)['value']
                st.write(f"Extracted MODULE ORIENTATION")
            except TypeError:
                st.write("Cannot extract MODULE ORIENTATION")
            modChis=GetChildren(myComp['code'],chiType)
            if modChis==None:
                st.write("No children found")
                st.stop()
        if myComp['componentType']['code'] in ["MODULE_CARRIER","BARE_MODULE","PCB"]:
            parType, chiType= "MODULE", myComp['componentType']['code']
            st.write("checking "+chiType+" component for "+parType+" parents")
            pars=GetParents(myComp['code'],parType)
            if pars==None:
                st.write("No parents found")
                st.stop()
            else:
                stTrx.DebugOutput("parents:",pars)
                try:
                    modComp= DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pars[0]['component']['code']})
                    oriFlag=next((item for item in modComp['properties'] if item['code'] == "ORIENTATION"), None)['value']
                    modType=modComp['type']['code']
                    st.write(f"Extracted MODULE ORIENTATION: {oriFlag}")
                except TypeError:
                    st.write(f"Cannot extract Orientation for {pars[0]['component']['code']}")
                st.write("checking MODULE for BARE_MODULE")
                modChis=GetChildren(pars[0]['component']['code'],"BARE_MODULE")
                if modChis==None:
                    st.write("No children found")
                    st.stop()

        if modChis==None:
            st.write(f"no BARE_MODULEs found. Check input component ({myComp['componentType']['code']})")
            st.stop()
        modChiSNs=[x['component']['code'] for x in modChis]
        stTrx.DebugOutput("found BARE_MODULEs:",modChiSNs)

        st.write("### MODULE Summary")
        st.write(f"__Serial number: {modComp['serialNumber']}__")
        st.write(f"__module type: {modType}__")
        st.write(f"__Extracted MODULE ORIENTATION: {oriFlag}__")

        ########################
        ### Quad version
        ########################
        if "QUAD" in modType:
            st.write("Quad analysis")

            bmSN=None
            if len(modChiSNs)!=1:
                st.write("Unexpected number of BARE_MODULEs. Check components")
                st.write(modChiSNs)
            else:
                bmSN=modChiSNs[0]
            st.write("found BARE_MODULE:",bmSN)

            if bmSN==None:
                st.stop()

            ########################
            ### get FE_CHIP children
            ########################
            bmComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':bmSN})

            feSNs=[x['component']['code'] for x in bmComp['children'] if x['componentType']['code']=="FE_CHIP" and x['component']!=None]
            stTrx.DebugOutput("Child codes",feSNs)
            if feSNs==None:
                st.write("No FE_CHIP children found")
                st.stop()

            childMap={c['component']['code']:str(c['order']) for c in bmComp['children'] if c['componentType']['code']=="FE_CHIP" and c['component']!=None}
            # normal orientation <-- oriFlag=True
            nameMap={'0':"GA1",'1':"GA2",'2':"GA3",'3':"GA4"}
            ### (single) alternative = 180º rotation <-- oriFlag=False
            if oriFlag==False:
                nameMap={'0':"GA3",'1':"GA4",'2':"GA1",'3':"GA2"}

            st.write("Get FE_CHIP data")
            feComps=DBaccess.DbGet(st.session_state.myClient,'getComponentBulk', {'component':feSNs})
            ### optional check
            if st.checkbox("See retrieved quad FE information?"):
                st.write("### Child Components")
                st.write("retrieved FE_CHIP data:",len(feComps))
                st.dataframe(feComps)
            else:
                st.write(" - retrieved FE_CHIP data:",len(feComps))

        ########################
        ### Triplet version
        ########################
        elif "TRIPLET" in modType:
            st.write("Triplet analysis")

            bmSNs=None
            if len(modChiSNs)!=3:
                st.write("Unexpected number of BARE_MODULEs. Check components")
                st.write(modChiSNs)
            else:
                bmSNs=modChiSNs
            # st.write("found BARE_MODULEs:",bmSNs)

            if bmSNs==None:
                st.stop()

            childMap={c['component']['code']:str(c['order']) for c in modComp['children'] if c['component']!=None}

            nameMap={'0':"Position1",'1':"Position2",'2':"Position3",'3':"Position4"}
            if len(list(childMap.keys()))<1:
                print("No children found")
            for c in modComp['children']:
                if c['componentType']['code']!="BARE_MODULE":
                    continue
                cc=c['component']['code']

            bmComps=DBaccess.DbGet(st.session_state.myClient,'getComponentBulk', {'component':bmSNs})
            if st.session_state.debug:
                st.write("Found BARE_MODULEs")
                st.dataframe(pd.DataFrame(bmComps))

            chiType="FE_CHIP"
            feComps=DBaccess.DbGet(st.session_state.myClient,'getComponentBulk', {'component':[y['component']['code'] for x in bmComps for y in x['children'] if y['component']!=None and y['componentType']['code']==chiType]})
            febmMap={x['code']:y['component']['code'] for x in feComps for y in x['parents'] if y['componentType']['code']=="BARE_MODULE"}
            ### optional check
            if st.checkbox("See retrieved triplet FE information?"):
                st.write("### Child Components")
                st.write("retrieved FE_CHIP data:",len(feComps))
                st.dataframe(feComps)
            else:
                st.write(" - retrieved FE_CHIP data:",len(feComps))

        else:
            st.write("### Module type not recognised:",modType)
            st.stop()

        ########################
        ### Get Trim info.
        ########################
        testIDs=[]
        for fe in feComps:
            try:
                #st.write("test count:",len(fe['tests']))
                for fet in fe['tests']:
                    #st.write(fet['code'])
                    for fetr in fet['testRuns']:
                        # try:
                        testIDs.append({'serialNumber':fe['serialNumber'],'code':fet['code'],'id':fetr['id']})
                        # except KeyError:
                        #     testIDs[fet['code']]=[fetr['id']]
            except KeyError:
                st.write("key error")

        st.write("### FE_CHIP Tests")
        df_feIDs=pd.DataFrame(testIDs)
        # st.dataframe(df_feIDs)
        feTestRuns=DBaccess.DbGet(st.session_state.myClient,'getTestRunBulk', {'testRun':df_feIDs.query('code=="FECHIP_TEST"')['id'].to_list()})
        ### optional check
        if st.checkbox("See retrieved test information?"):
            st.write("### Test data")
            st.write("retrieved test data:",len(feTestRuns))
            st.dataframe(feTestRuns)
        else:
            st.write(" - retrieved testRuns:",len(feTestRuns))
        # st.dataframe(pd.DataFrame(feTestRuns))

        ### formatting data
        df_testRuns=pd.DataFrame(feTestRuns)
        df_testRuns=df_testRuns.query('state=="ready"')[['components','institution','date','properties','results','passed']]

        st.write(f" - extract testRun components (normal orientaion flag: {oriFlag})")
        df_testRuns=df_testRuns.explode('components')
        for k in ['serialNumber','alternativeIdentifier', 'code']:
            df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k])
        st.write("   - map chips to module positions")
        if "QUAD" in modType:
            df_testRuns['nickName']=df_testRuns['code'].apply(lambda x: nameMap[childMap[x]])
        elif "TRIPLET" in modType:
            df_testRuns['nickName']=df_testRuns['code'].apply(lambda x: nameMap[childMap[febmMap[x]]])
        else:
            df_testRuns['nickName']=None

        df_testRuns=df_testRuns.explode('results')
        df_testRuns['IREF_TRIM']=df_testRuns['results'].apply(lambda x: x['value'] if x['code']=="IREF_TRIM" else None)
        df_testRuns=df_testRuns[df_testRuns['IREF_TRIM'].notna()]
        df_testRuns['IREF_bit']=df_testRuns['IREF_TRIM'].apply(lambda x: bitfield(int(x)))
        df_testRuns['IREF_bin']=df_testRuns['IREF_bit'].apply(lambda x: [{k:1} if v==0 else {k:0} for b in x for k,v in b.items()])

        ########################
        ### visualise data
        ########################
        st.write("### Visualise")
        filePath=os.path.realpath(__file__)
        image = Image.open(filePath[:filePath.rfind('/')]+'/vista.jpg')
        st.image(image, caption='Not bond pads')

        bars = alt.Chart(df_testRuns).mark_bar().encode(
                    y=alt.Y('nickName:N', title="FE position"),
                    x=alt.X('IREF_TRIM:Q', scale=alt.Scale(domain=[0, 20])),
                    color=alt.Color('passed:N'),
                    tooltip=['serialNumber:N','nickName:N','IREF_TRIM:Q','passed:N']
                ).properties(
                    width=600,
                    title="FE_CHIP IREF_TRIM values for MODULE: "+myComp['serialNumber']
                )
        text = alt.Chart(df_testRuns).mark_text(
                fontSize=17,
                font='utopia-std, serif',
                dx=10,
                align='left'
            ).encode(
                y=alt.Y('nickName:N'),
                x=alt.X('IREF_TRIM:Q'),
                color=alt.Color('passed:N'),
                text=alt.Text("IREF_TRIM")
            )
        st.altair_chart(bars+text)

        bondMap={'1':"IREF_TRIM0",'2':"IREF_TRIM1",'4':"IREF_TRIM2",'8':"IREF_TRIM3"}
        for nn in sorted(df_testRuns['nickName'].unique()):
            df_nn=df_testRuns.query('nickName=="'+nn+'"')
            df_nn=df_nn.explode('IREF_bin')
            df_nn['IREF_bin_key']=df_nn['IREF_bin'].apply(lambda x: bondMap[(list(x.keys())[0])])
            df_nn['IREF_bin_val']=df_nn['IREF_bin'].apply(lambda x: list(x.values())[0])
            maxVal=3
            df_nn['long']=df_nn['IREF_bin_val'].apply(lambda x: [int(x)*0.5,int(x)*maxVal])
            df_nn=df_nn.explode('long')

        #     st.dataframe(df_nn)

            bar=alt.Chart(df_nn).mark_bar().encode(
                            x=alt.X('IREF_bin_key:N', sort=None, title=None, axis=alt.Axis(labelAngle=-45)),
                            y=alt.Y('IREF_bin_val:Q', axis=alt.Axis(ticks=False, domain=False, labels=False, values=[0,1]), scale=alt.Scale(domain=[0,maxVal]), title=None),
        #                     opacity=alt.Opacity('IREF_bin_val:N')
                        ).properties(
                            width=600,
                            height=200,
                            title="IREF_TRIM="+str(int(df_nn['IREF_TRIM'].values[0]))+" map for "+df_nn['nickName'].values[0]+", "+df_nn['serialNumber'].values[0]+" (passed="+str(df_nn['passed'].values[0])+")"
                        )
            long=alt.Chart(df_nn).mark_line(height=5, color='red').encode(
                        x=alt.X('IREF_bin_key:N', sort=None, title=None),
                        y=alt.Y('long:Q', axis=alt.Axis(ticks=False, domain=False, labels=False, values=[0,1]), scale=alt.Scale(domain=[0,maxVal]), title=None),
                        detail=alt.Detail('IREF_bin_key')
                    )
            text=alt.Chart(pd.DataFrame([{'text':"50",'y':"IREF_TRIM3",'x':0.25}])).mark_text(color='orange', size=15).encode(
                        x=alt.X('y:N', sort=None, title=None),
                        y=alt.Y('x:Q', axis=alt.Axis(ticks=False, domain=False, labels=False, values=[0,1]), scale=alt.Scale(domain=[0,maxVal]), title=None),
                        text=alt.Text('text:N')
                    )
            st.altair_chart(bar+long+text)
