### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * review and edit test schema",
        " * upload image",
        " * upload test schema to PDB with image added as attachment",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Optical Upload", ":microscope: Upload Optical Inspection Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="BARE_MODULE"
        if "code" not in pageDict.keys():
            pageDict['testType']="OPTICAL_INSPECTION"
        if "stage" not in pageDict.keys():
            pageDict['stage']="BAREMODULERECEPTION"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_P.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testType'])
        st.dataframe(df_stageTest)

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")

        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testType']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':True})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["png","jpg","jpeg"])
        if st.session_state.debug: st.write(pageDict['file'])

        if pageDict['file'] is not None:
            st.image(pageDict['file'])
        else:
            st.write(" No image file set")
            st.stop()

        st.write("### :construction: Page under construction :construction:")
        st.stop()

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")

        # if st.button("Upload Test"):
        #     ### set stage
        #     try:
        #         pageDict['setVal']=DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':pageDict['testSchema']['component'], 'stage':pageDict['stage']})
        #         st.write("### **Successful stage set** (",pageDict['stage'],") for:",pageDict['setVal']['id'])
        #         # df_set=pd.json_normalize(pageDict['setVal'], sep = "_")
        #         # st.dataframe(df_set)
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: Stage (",pageDict['stage'],")setting **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     ### upload data
        #     try:
        #         pageDict['upVal']=DBaccess.DbPost(st.session_state.myClient,'uploadTestRunResults', pageDict['testSchema'])
        #         st.write("### **Successful Upload**:",pageDict['upVal']['componentTestRun']['date'])
        #         st.balloons()
        #         st.write(pageDict['upVal'])
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: Update **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     ### upload file
        #     try:
        #         ### faff to creat file for binary upload - reading dierect from buffer loses name(?!)
        #         tmpPath="/tmp"
        #         fnew=open(tmpPath+"/"+pageDict['file'].name, "w")
        #         fnew.write(pageDict['file'].getvalue().decode("utf-8"))
        #         fnew.close()
        #         if st.session_state.debug:
        #             st.write([f for f in os.listdir(tmpPath) if os.path.isfile(os.path.join(tmpPath, f))])
        #             st.write("use:",tmpPath+"/"+pageDict['file'].name)
        #         f = open(tmpPath+"/"+pageDict['file'].name, "rb")
        #         pageDict['attVal']=st.session_state.myClient.post('createTestRunAttachment', data=dict(testRun=pageDict['upVal']['testRun']['id'],title="Pull test results",type="file"), files=dict(data=f))
        #         st.write("### **Successful File Upload**:",pageDict['attVal']['filename'])
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: File Upload **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #
        # if 'upVal' in pageDict.keys():
        #     try:
        #         st.write("last successful _upload_:",pageDict['upVal']['testRun']['id'])
        #         if st.button("Delete testrun"):
        #             ### delete data
        #             try:
        #                 pageDict['delVal'] = DBaccess.DbPost(st.session_state.myClient,'deleteTestRun', {'testRun':pageDict['upVal']['testRun']['id']})
        #                 st.write("### **Successful Deletion**:",pageDict['delVal']['testRun']['id'])
        #                 st.balloons()
        #                 st.write(pageDict['delVal'])
        #             except itkX.BadRequest as b:
        #                 st.write("### :no_entry_sign: Deletion unsuccessful")
        #                 st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     except KeyError:
        #         pass
        # if 'delVal' in pageDict.keys():
        #     try:
        #         st.write("last successful _deletion_:",pageDict['delVal']['testRun']['id'])
        #     except KeyError:
        #         pass
