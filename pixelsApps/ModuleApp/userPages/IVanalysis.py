### based on: https://gitlab.cern.ch/huye/itk_pdb_testapp_pix_reception

import numpy as np
import streamlit as st

### Scale factor to normalise for temperature
def scaleF(Tbare, Tmod):
    Eg = 1.22
    k = 8.617333262e-5

    def kelvin(T):
        return 273.15 + T

    return (
        np.power(kelvin(Tmod) / kelvin(Tbare), 2)
        * np.exp(Eg * (Tmod - Tbare) / 2 / k / kelvin(Tmod) / kelvin(Tbare))
        - 1
    )

### Checking if the component is 3D by using YY-identifiers
def Check3D(serialNum):
    is3D = False
    if any(serialNum[6] == x for x in ["G", "H", "I", "J", "V", "W", "0", "1"]):
        is3D = True
    return is3D

### Finding Leakage current and breakdown voltage
def calVbdIlc(serialNum, Vdepl, xdata, ydata):
    Vbd = 0
    Ilc = 0

    is3D= Check3D(serialNum)

    # 3D sensor criterias
    if is3D:
        st.write("Recognise __3D__ device")
        if Vdepl == 0:
            Vdepl = 5
        I_voltage_point = Vdepl + 20
        # break_threshold = Vdepl + 20
        # I_treshold = 2.5  # current in uA

    # Planar sensor criterias
    else:
        st.write("Recognise __planar__ device")
        if Vdepl == 0:
            Vdepl = 50
        I_voltage_point = Vdepl + 50
        # break_threshold = Vdepl + 70
        # I_treshold = 0.75  # current in uA

    # Finding leakage current at threshold voltage
    for idx, V in enumerate(xdata):
        # if V<0:
        #     V=V*-1
        if V < Vdepl:
            continue
        elif V <= I_voltage_point:
            # Vlc = V
            Ilc = ydata[idx]

        # Finding breakdown voltage for 3D
        if is3D:
            if ydata[idx] > ydata[idx - 5] * 2 and xdata[idx - 5] > Vdepl:
                Vbd = xdata[idx - 5]
                break

        # Finding breakdown voltage for Planar
        else:
            if ydata[idx] > ydata[idx - 1] * 1.2 and xdata[idx - 1] != 0:
                Vbd = V
                break
    if Vbd == 0:
        Vbd = ">%s" % round(xdata[-1], 1)

    # st.write("return: ",[Vbd, Ilc])
    return Vbd, Ilc

### Get pass/fail for analysis
def GetNewToOldComp(Vnew,Vold,Vmax_old=None):

    ## 2. check breakdown comparison
    # check old and new values
    oldCheck, newCheck = True, True
    if type(Vnew)==type("str") or Vnew==-999:
        st.write("No _new_ breakdown value for comparison.")
        newCheck=False
    if type(Vold)==type("str"):
        st.write("No _old_ breakdown value for comparison.")
        oldCheck=False
    
    # specified difference permitted
    specDiff=10
    # if old and new Vdep values can be compared
    if oldCheck and newCheck:
        st.write(f"Compare breakdown values, permitted difference: {specDiff}")
        measDiff=abs(Vnew - Vold)
        # fail: >specDiff
        if measDiff > specDiff:
            st.write(f"### :broken_heart: **Analysis failed** BREAKDOWN_VOLTAGE difference > {specDiff}: {measDiff} ")
            return False
        # pass: <specDiff
        else:
            st.write(f"### :green_heart: **Analysis passed** BREAKDOWN_VOLTAGE difference OK: {measDiff}")
            return True
    # if both values indicate no Vdep recorded
    elif not oldCheck and not newCheck:
        st.write("Both _old_ and _new_ breakdown values missing.")
        st.write(f"### :green_heart: **Analysis passed**")
        return True
    # if Vdep only avalable for one
    else:
        # st.write(f"Can't compare values (old,new): {Vold}, {Vnew}")
        ### "temporary" hack for missing sensor breakdown
        if Vold==0:
            st.write("__Temprary hack__: _old_ value ==0, __assume no breakdown found__")
            if not newCheck:
                st.write("Both _old_ and _new_ breakdown values missing.")
                st.write(f"### :green_heart: **Analysis passed**")
                return True
            else:
                # we have breakdown now but not before
                st.write(f"Can't compare values (old,new): {Vold}, {Vnew}")
                if Vmax_old==None:
                    st.write(f"Old Vmax missing. Assume passed.")
                    st.write(f"### :green_heart: **Analysis passed**")
                    return True
                else:
                    st.write(f"Use old Vmax...")
                    if Vnew > Vmax_old:
                        st.write(f"{Vnew} _more_ than {Vmax_old}")
                        st.write(f"### :green_heart: **Analysis passed**")
                        return True
                    else:
                        st.write(f"{Vnew} _less_ than {Vmax_old}")
                        st.write(f"### :broken_heart: **Analysis failed** ")
                        return False
        else:
            if not newCheck:
                # we have (non-zero) old value but no new value
                st.write(f"Can't compare values (old,new): {Vold}, {Vnew} - new missing. Assume passed.")
                st.write(f"### :green_heart: **Analysis passed**")
                return True
            else:
                st.write(f"Shouldn't get here with values (old,new): {Vold}, {Vnew}. Please contact Kenny. Assume passed.")
                st.write(f"### :green_heart: **Analysis passed**")

    
    # st.write("Did I miss something?")

def GetVmaxCheck(Vmax, Vdep, Vbk):
    
    VmaxCheck=True
    if Vmax < Vdep + 70:
        st.write(f"Vmax ({Vmax}) _less_ than Vdep + 70 ({Vdep+70})")
        st.write(f"### :broken_heart: **Analysis failed** ")
        VmaxCheck=False
    else:
        st.write(f"Vmax ({Vmax}) _more_ than Vdep + 70 ({Vdep+70})")

    # if Vbk < Vmax:
    #     st.write(f"Vbk ({Vbk}) _less_ than Vmax ({Vmax})")
    # else:
    #     st.write(f"Vbk ({Vbk}) _more_ than Vmax ({Vmax})")
    #     st.write(f"### :broken_heart: **Analysis failed** ")
    #     VmaxCheck=False

    if VmaxCheck:
        st.write(f"### :green_heart: **Analysis passed**")
    else:
        VmaxCheck=False

