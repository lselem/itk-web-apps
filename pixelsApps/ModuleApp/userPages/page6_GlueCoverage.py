### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from . import glueAnalysis

import cv2

#####################
### useful functions
#####################

infoList=[" * input file",
        " * draw trapezoid to mark crop",
        " * save when ready",
        " * set thresholds",
        " * get stats"]
#####################
### main part
#####################

class Page6(Page):
    def __init__(self):
        super().__init__("Glue Coverage", ":microscope: Check Glue Coverage from Image", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # # set up test info.
        # if "componentType" not in pageDict.keys():
        #     pageDict['componentType']="PCB"
        # if "testCode" not in pageDict.keys():
        #     pageDict['testCode']="METROLOGY"
        # if "stageCode" not in pageDict.keys():
        #     pageDict['stageCode']="PCB_RECEPTION_MODULE_SITE"
        # if "project" not in pageDict.keys():
        #     pageDict['project']="P"

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload image file", type=["png","jpg","jpeg"])
        if st.session_state.debug: st.write(pageDict['file'])

        if pageDict['file'] is not None:
            if st.checkbox('check input image?'):
                st.image(pageDict['file'])
        else:
            st.write(" No image file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="glue_cover_example.jpg"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
            with open(filePath[:filePath.rfind('/')]+"/"+exampleFileName, "rb") as file:
                st.download_button(
                        label="Download example",
                        data=file,
                        file_name=exampleFileName,
                        mime="image/jpg"
                    )

            # st.download_button(label="Download example", data=, file_name=exampleFileName, mime="image/jpg")
            for k in ['warp', 'imList', 'analysis', 'coords']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()

        st.write("---")
        st.write("## Image Cropping")
        st.write("Define (red) trapezoid on image to crop and adjust.")
        st.write("Test cropping with image comparison until ready to proceed.")

        st.write("### Trapezoid coordinates")
        st.write("tl = Top Left, tr = Top Right, bl = Bottom Left, br = Bottom Right")
        if "coords" not in pageDict.keys():
            pageDict['coords']={
                            "tl": [
                                615,
                                365
                            ],
                            "tr": [
                                1850,
                                350
                            ],
                            "bl": [
                                535,
                                1400
                            ],
                            "br": [
                                2000,
                                1400
                            ]
                            }
            # # top-left point
            # tl=[100,1]
            # # top-right point
            # tr=[2150,50]
            # # bottom-left point
            # bl=[1,1650]
            # # bottom-right point
            # br=[2250,1700]
            # # cast as ints if required
            # for k,v in {'tl':tl, 'tr':tr, 'bl':bl, 'br':br}.items():
            #     pageDict['coords'][k]=v
            #     for t in range(0,len(v),1):
            #         try:
            #             pageDict['coords'][k][t]=int(str(v[t]).replace(',',''))
            #         except:
            #             st.write(f"issue with point {k}[{t}]: {v[t]}")
            # pageDict['coords']['squareWidth']=1500    

        infra.ToggleButton(pageDict,'toggleCoor','Edit Coordinates?')
        if pageDict['toggleCoor']:
            st.write("### Edit Cropping Coordinates")
            pageDict['coords']=pdbTrx.EditJson(pageDict['coords'])
        else:
            st.write(pageDict['coords'])
        
        infra.Radio(pageDict,'cropReady',['Cropping Test','Run Crop'],'Ready to Crop?')

        if st.button("Run cropping"):
            # We will first manually select the source points 
            # we will select the destination point which will map the source points in
            # original image to destination points in unwarped image
            src = np.float32([(pageDict['coords']['tl'][0], pageDict['coords']['tl'][1]), # TL
                             (pageDict['coords']['tr'][0], pageDict['coords']['tr'][1]), # TR
                             (pageDict['coords']['bl'][0], pageDict['coords']['bl'][1]), # BL
                             (pageDict['coords']['br'][0], pageDict['coords']['br'][1])]) # BR
            # # map original to new points
            # dst = np.float32([(0, 0), # TL
            #                 (pageDict['coords']['squareWidth'], 0), # TR
            #                 (0, pageDict['coords']['squareWidth']), # BL
            #                 (pageDict['coords']['squareWidth'], pageDict['coords']['squareWidth'])]) # BR
            
            # Convert the file to an opencv image.
            file_bytes = np.asarray(bytearray(pageDict['file'].read()), dtype=np.uint8)
            im = cv2.imdecode(file_bytes, 1)
            if "test" in pageDict['cropReady'].lower():
                glueAnalysis.unwarp(im, src, True)
            else:
                pageDict['warp'], M = glueAnalysis.unwarp(im, src, False)

        if "warp" in pageDict.keys():
            st.write("Got cropped image")
        else:
            st.write("Run cropping to continue")
            st.stop()

        st.image(pageDict['warp'])

        st.write("---")
        st.write("## Image Analysis")
        st.write("Analysis from [link](https://gitlab.cern.ch/tiany/image_analysis)")
        st.write("Set black/white array: [ start_value, stop_value, increments]")

        # inputFolder='/Users/ytian/hardware/2023-2_siteQualification/flex0306_glass7'
        # imgNames=[ "IMG_1122_cut_painted.jpg" ]
        # outputFolder='./'

        if "analysis" not in pageDict.keys():
            pageDict['analysis']={
                'thresholdArray':[120,127,2],
                'optGreen':"", #"green"
                'optCorr':"correct black"
            }
        
        infra.ToggleButton(pageDict,'toggleAna','Edit Analysis?')
        if pageDict['toggleAna']:
            st.write("### Edit Analysis settings")
            pageDict['analysis']=pdbTrx.EditJson(pageDict['analysis'])
        else:
            st.write(pageDict['analysis'])

        if st.button("Run analysis"):
            pageDict['imList']=glueAnalysis.analyseImage(pageDict['warp'],pageDict['analysis']['thresholdArray'],pageDict['analysis']['optGreen'],pageDict['analysis']['optCorr'])

        if "imList" not in pageDict.keys():
            st.write("Run analysis to see results.")
            st.stop()

        st.write("---")
        st.write("## Results")
        st.write("See caption for analysis stats")
        st.write(f"Analyses images: {len(pageDict['imList'])}")
        for e,il in enumerate(pageDict['imList'], 1):
            st.image(il['image'], caption=f"{e}. {il['comment']}")
        