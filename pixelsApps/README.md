# Pixels Applications

A collection of applications intended for ITk Pixels project

Currently hosted on [Cern OKD](https://itk-pdb-webapps-pixels.web.cern.ch).

---

## Examples

Apps are mainly built around componentTypes, rather than testTypes. Hence, an app may provide functionality for testRun uploads for various testTypes.

Non-comprehensive list of included apps.

### Module App

Pixel Module specific tools, e.g.:

#### Translation

The translation page lets users choose between Front End, PCB, and Sensor components.  On the Front End option, inputting wafer ID and tile position yields the ATLAS Serial Number (ASN).  The PCB option is under construction.  The sensor option gives the ASN when given the alternative identifier for the sensor.

#### Trim Check

The trim check is related to wirebonding of FE chips to the PCB: this page is not currently 100% functional, but the performance issues are under review.

### SQ App

Site Qualification tool.

The site qualification app is used by sites to upload their qualification slides on the "Update SQ" page, and the "Review SQ" page is used to check the slides uploaded by other sites in order to approve or disapprove of them.

- upload SQ steps
- review institution uploads

### Vendor App

Tool for vendors to register and assemble Bare Modules in the PDB.

- Sensor wafer disassembly
- Bare module registration and assembly
- Gel pack registration, assembly and shipping

### wirebondingApp

This app currently has one unique page, useful for uploading the results of the wirebonding pull tests.  There are plans in the works to add a dedicated wirebonding information upload page, and possibly move both pages from this app to the ModuleApp.

To upload pull testing results, ensure first that your .xls or .csv file matches the formatting of the example given on the Pull Test Upload page.  You may drag and drop or browse your files to select it.  If the component serial number is in the title of the file, then the webapp should automatically pull that. **Known bug:** currently, the option to set the institution in the .xls or .csv file does not work properly- you will therefore need to edit the schema manually to change from the default value of GL to the appropriate institution.  You should also edit the run number as appropriate.  Before Uploading, make sure that the box to "Convert values to text?" is checked.  If you've done everything right, you should see the line "**\*\*Successful uploadTestRunResults\*\*:**" followed by the run ID.
