# Assembly App

Tools for Bare Module and Module assembly.

---

## Pages

### Bare Module Assembly
  * Assemble BARE_MODULE component

### Module Assembly
  * Assemble MODULE component
