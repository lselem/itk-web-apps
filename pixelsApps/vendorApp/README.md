# Hybridisation Vendor WebApp

[Hybridisation_Vendor_Streamlit_WebApp](http://itk-vendor-upload.web.cern.ch/) is an interface for hybridisation vendor.
Through this interface, the vendor can review or make changes to the components as this WebApp is connected to the ITk production database.

In order to use the WebApp, it is necessary to use the two passwords to log in to the ITk production database via the link ([http://itk-vendor-upload.web.cern.ch/](http://itk-vendor-upload.web.cern.ch/)).

<img src="/images/pixelsApps/vendor_webapp/Token_1.png" alt="Token" width="400"/>


In the left side there is a list of pages that could be checked on them after having token.

__WebApp pages__
- [Inventory](#inventory).
- [Shipping Informatuon](#shipping-information-and-creation).
- [Disassembly of Sensor Tiles](#disassembly-of-the-sensor-tiles).
- [Registration and Assembly of Bare Module](#registration-and-assembly-of-bare-modules).
- [GelPak Assembly](#gelpak-assembly).
- [GelPak Registration](#gelpak-registration).
- [Front-End Chips](#front-end-chips).

From the left sidepage the institution and the project could be chosen.


---

## Pages


### Inventory

The page shows all the components at an institute. The institution could be chosen from the dropdown menu on the left side.

<img src="/images/pixelsApps/vendor_webapp/inventory_1.png" alt="inventory 1" width="400"/>

The components are listed by the component type which could chosen fron **Select Component Type**.
The list of components are coloured by the current stage-code for the components. The content of the list is (the serial number, alterntive identifier, component type code, type code, current stage, assemble) for each component.
This list will be downloaded as a CSV file if the **Write CSV** button has been clicked.

<img src="/images/pixelsApps/vendor_webapp/inventory_2.png" alt="inventory 2" width="400"/>

A simple stats displays the number of components that are ready, reworked and trashed.
A specific component could be selected by its serial number and a list of this component information by its stage history.

<img src="/images/pixelsApps/vendor_webapp/inventory_3.png" alt="inventory 3" width="400"/>

At the end of the page there is an option to delete a selected component and other option to list the stages of an appointed type of component.


### Shipping Information and Creation

The shippments information can be checked on  for a spacific date which could be selected from a range of 100 days. The table of shipments information is showen with different colour depend on the shipment status. 

<img src="/images/pixelsApps/vendor_webapp/Shipping_1.png" alt="Shipping 1" width="400"/>

By selected shipment name a list of the shipment components appears and the details of the shipment.

<img src="/images/pixelsApps/vendor_webapp/Shipping_2.png" alt="Shipping 2" width="400"/>

To creat a new shipment, the sender and recipient institution should be selected and the type of the shipping. There are two options to make the shipment. The first option is uploading the list of component with .CSV file format: 

<img src="/images/pixelsApps/vendor_webapp/Shipping_3.png" alt="Shipping 3" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/Shipping_4.png" alt="Shipping 4" width="400"/>

The other option is choosing the components from the list of the institution component after select the type of the compnents:

<img src="/images/pixelsApps/vendor_webapp/Shipping_5.png" alt="Shipping 5" width="400"/>


### Disassembly of the Sensor Tiles

Sensor tiles should be disassembled from the sensor wafer in the ITk production databse when the wafer being diced in the real world.
The format file of the list of the wafers could be downloaded by pressing on **the example file** button. 

<img src="/images/pixelsApps/vendor_webapp/ST_Disassembly_1.png" alt="ST Disassembly 1" width="400"/>

The dicing state could be chosen from the downloaded file and comments on the wafer state could be added.

<img src="/images/pixelsApps/vendor_webapp/ST_Disassembly_5.png" alt="ST Disassembly 5" width="400"/>

A table of the uploading file data and the extracted collection of these data would be shown.
The process of converting the alternative identifier and checking on each sensor wafer in the ITk production database would be automatically.

<img src="/images/pixelsApps/vendor_webapp/ST_Disassembly_2.png" alt="ST Disassembly 2" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/ST_Disassembly_3.png" alt="ST Disassembly 3" width="400"/>


If the sensor wafers are found in the ITk production database, a table of the wafers information would be display coloured rely on the type code of the component.
The wafers of sensor tiles which are not disassemled will be tabulated and by click on the **Disassembly** button, a list of dicing wafers and the message of the disassembly confirmation will appear.  

<img src="/images/pixelsApps/vendor_webapp/ST_Disassembly_4.png" alt="ST Disassembly 4" width="400"/>



### Registration and Assembly of Bare Modules

This page is used to register the bare module and the first step is uploading the excel file containing the sensors and the front-end chips manufacturer as in the example (could download it by clicking on **download example**).

<img src="/images/pixelsApps/vendor_webapp/BM_1.png" alt="BM 1" width="400"/>

The inserted data table appears and is sorted to ensure that they are found in the ITk production database. A table of all the components’ information appears with different colors to distinguish the type of component. 

<img src="/images/pixelsApps/vendor_webapp/BM_3.png" alt="BM 3" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/BM_4.png" alt="BM 4" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/BM_5.png" alt="BM 5" width="400"/>


As in the following picture, a set of bare module properties could be chosen by selecting the front-end chips version (RD53A-ITkpix_v1- ITkpix_v1.1- ITkpix_v1.2-No chip) and the sensor type (L0 inner pixel 3D sensor tile - L0 inner pixel planar sensor tile – L1 inner pixel quad sensor tile- Outer pixel quad sensor tile – Dummy sensor tile) and vendor (Thin-Thick-IZM) and the last option set is the type of the bare module.

<img src="/images/pixelsApps/vendor_webapp/BM_6.png" alt="BM 6" width="400"/>


Setting the bare module serial number (SN) table is showing and there is an option for editing it.
To ensure the existence of the selected bade module serial number in the database, click on **check BARE_MODULE SerialNumbers**.
By clicking on **Register**, the chosen bare module would be registered into ITk production database.

<img src="/images/pixelsApps/vendor_webapp/BM_7.png" alt="BM 7" width="400"/>

At the end of the page, the bare model assembly where the sensor tiles and front-end chips assemble to the bare module in the database and there is an option of disassembling them will appear at the end of the assembly process. 
  

### GelPak Assembly

Through this page, it could make the gelpack assembly and at the beginning of the page there are three options for the chosen institute bare module and gelpack components  - the institute can be changed from the side page at the bottom left - and a table will appear showing all the components information and the table can be enlarged and the rest of the information is shown by clicking on the sidelines at the bottom right of the table.

<img src="/images/pixelsApps/vendor_webapp/GP_1.png" alt="GP 1" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/GP_2.png" alt="GP 2" width="400"/>

To assemble the bare module into a gelpack the type of the bare module should be chosen (single-quad).
A number of the available bare module will be shown and the position of the bare module and the orientation could be set. 

<img src="/images/pixelsApps/vendor_webapp/GP_3.png" alt="GP 3" width="400"/>

The gelpack could be selected from a list of the gelpack of the institute and by clicking on **Assembly** the chosen gelpack will be assembled in the database. 

<img src="/images/pixelsApps/vendor_webapp/GP_4.png" alt="GP 4" width="400"/>


### GelPak Registration

To register gelpack, an excel file of all the gelpack information should be uploaded (as the one in the **download example** button).
The uploaded data will be read and the preliminary stat would be shown.
Gelpack subtype and sub-project could be chosen from the selection options for each one and the serial number for the gelpack components would be worked on.

<img src="/images/pixelsApps/vendor_webapp/GPR_1.png" alt="GPR 1" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/GPR_2.png" alt="GPR 2" width="400"/>

A table for the gelpack components test information will appear and the serial numbers exciting in the ITk production database could be checked by clicking on **Check GELPACK SerialNumber** button.
If the chosen serial numbers are not found, then the gelpack could be registered by clicking on **Register Components** and a message will appear confirming that the registration was successful.   

<img src="/images/pixelsApps/vendor_webapp/GPR_3.png" alt="GPR 3" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/GPR_4.png" alt="GPR 4" width="400"/>


### Front End Chips

This page aimed to mark the damaged front-end chips in the ITk production database as UNUSED for production and add a flag to them indicating the cause of the damage.
A table of the damaged front-end chips should be uploaded as in the excel file example which includes the front-end wafer number (manufacturer identifier) and the position of the damaged chip and choose the reason that cause the damage for the list and a comment could be added (add a dash between the comment words instead of the space).


<img src="/images/pixelsApps/vendor_webapp/FE_1.png" alt="FE 1" width="400"/>

<img src="/images/pixelsApps/vendor_webapp/FE_2.png" alt="FE 2" width="400"/>

The processing of sorting the uploading components and checking their existence in the database would appear.
If the front-end chips are found in the database, their information details would be shown in a table and the Adding flag to them button will appear, and click on **Add Flag** a message of changing the stage of the damaged front-end chips and adding the chosen flag to them would be shown.

<img src="/images/pixelsApps/vendor_webapp/FE_3.png" alt="FE 3" width="400"/>

