### standard
from re import X
from sys import flags
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
import numpy as np
import os
from io import StringIO
import io
import xlsxwriter
import xlrd
import openpyxl
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from .SharedCode import SelectCheck
from .SharedCode import EditJson
from .SharedCode import to_excel
from .SharedCode import DFsFromExcel
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
### get FE_WAFER ASN from 'ID' property
def FindWafer(compList,waferID):
    for feComp in compList:
        ### Find matching altID
        if feComp['alternativeIdentifier']==waferID:
            return feComp['serialNumber']
    return None

### get ASN for FE_CHIP
def GetFE_SN(compList,waferID,pos,prefix="20UPGFC"):
    waferSN=FindWafer(compList,waferID)
    if waferSN==None:
        return None
    if st.session_state.debug:
        st.write('serialNumber:',waferSN,"-->",waferSN[-3:])
        st.write(int(waferSN[-3:]),"-->",hex(int(waferSN[-3:])))
    combStr=str(hex(int(waferSN[-3:])))[-3:]+pos.replace('-','')
    if st.session_state.debug:
        st.write(hex(int(waferSN[-3:])),"+",pos,"-->",combStr)
    finalStr=prefix+str(int(combStr, 16)).zfill(7)
    if st.session_state.debug:
        st.write(combStr,"-->",int(combStr, 16),"-->",finalStr)
    return finalStr#####################

infoList=[" * Input formatted xlsx",
            " * Extract data",
            "  - FE Ids + slots",
            " * Convert Ids -> ASNs",
            " * Check in PDB",
            " * Add Flag to damage FE chips",
            "  - Change damage FE chips to UNUSABLE",
            "  -Add comments,"]
#####################
### main part
#####################

class Page7(Page):
    def __init__(self):
        super().__init__("FE Chips", ":microscope: Front-End Chips", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['file']= st.file_uploader("Upload data file", type=["xls","xlsx"])
        stTrx.DebugOutput("Input file:",pageDict['file'])

        sheetName='Sheet1'
        if pageDict['file'] is None:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="Damage_FE_Chips.xlsx"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
                st.write(os.listdir(filePath[:filePath.rfind('/')]))
            df_test=pd.read_excel(filePath[:filePath.rfind('/')]+"/"+exampleFileName, sheet_name=sheetName)
            df_xlsx = to_excel(df_test,sheetName)
            st.download_button(label="Download example", data=df_xlsx, file_name=exampleFileName)
            for k in pageDict.keys()-['file']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()


        st.write("## Read Data")
        df_head, df_data= DFsFromExcel(pageDict['file'], sheetName)
        st.dataframe(df_data)
        st.write("Number of Damage FE Chips:",len(df_data['Position'].unique()))
        stTrx.DebugOutput("Columns in data: ",df_data.columns.to_list())

        if "regList" not in pageDict.keys() or st.button("Read Data"):
            pageDict['regList']=[]
            for sensor in df_data['Wafer_Number'].unique():
                pageDict['regList'].append({'comb':None,'parts':[],})
                for index,feRow in df_data.query('Wafer_Number=="'+sensor+'"').iterrows():
                    st.write("FE Chips:",feRow['Wafer_Number'],feRow['Position'])
                    pageDict['regList'][-1]['parts'].append({'SN':None,'compType':"FE_CHIP",'wafer':feRow['Wafer_Number'],'pos':feRow['Position'],'flag':feRow['Damage_Reason']})
        st.write("### Extracted collections:")
        st.write(pageDict['regList'])

        if st.session_state.debug:
            if "FEprefix" not in pageDict.keys():
                pageDict['FEprefix']="20UPGFC"
            infra.TextBox(pageDict,'FEprefix',"Enter FE prefix:")

        st.write("### Converted identifiers:")
        if "converted" not in pageDict.keys() or st.button("convert IDs"):
            pageDict['converted']=True
            # FEs for identification
            pageDict['feCompList']=DBaccess.DbGet(st.session_state.myClient,'listComponents', {'componentType':"FE_WAFER"},True)
            status_bar_con=st.progress(0.0)
            status_text_con=st.empty()
            result_text_con=st.empty()
            checks_text_con=st.empty()
            listLen=len(pageDict['regList'])
            ### ASN Conversion
            for e,rl in enumerate(pageDict['regList']):
                for part in rl['parts']:
                    status_bar_con.progress(1.0*e/listLen)

                    if part['compType']=="FE_CHIP":
                        status_text_con.text("working on "+part['compType']+": "+part['wafer']+", "+part['pos'])
                        ### translate to ASN
                        if "FEprefix" in pageDict.keys():
                            feSN=GetFE_SN(pageDict['feCompList'],part['wafer'],part['pos'],pageDict['FEprefix'])
                        else:
                            feSN=GetFE_SN(pageDict['feCompList'],part['wafer'],part['pos'])
                        if feSN==None:
                            result_text_con.text("- no ASN found.")
                            part['SN']=None
                            continue
                        ### checks!
                        try:
                            checkVal=pdbTrx.CheckComponent(feSN, part['compType'], st.session_state.Authenticate['inst']['code'], st.session_state.debug)
                            if "False" in checkVal:
                              checks_text_con.text(feSN+" failed checks: "+checkVal)
                              # continue
                            part['SN']=feSN
                            result_text_con.text("- Found ASN: "+feSN)
                        except TypeError:
                            checks_text_con.text("cannot check component: "+feSN)
                            part['SN']=None
                            pass
        else:
            st.write("Done. Re-run if required")
        if "ExtractList" not in pageDict.keys() or st.button("reset list"):
            status_bar_ids=st.progress(0.0)
            status_text_ids=st.empty()
            listLen=len(pageDict['regList'])
            pageDict['ExtractList']=[]
            for e,c in enumerate(pageDict['regList']):
                for d in c['parts']:
                    loopText="checking SN: "+ d['SN']
                    status_text_ids.text(loopText)
                    status_bar_ids.progress(1.0*e/listLen)
                    if d['SN']==None:
                        continue
                    try:
                        pageDict['ExtractList'].append(DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':d['SN']} ))
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: get component **Unsuccessful** for",d)
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        else:
            st.write("Done. Re-run if required")

        try:
            df_extractList=pd.json_normalize(pageDict['ExtractList'], sep = "_")
        except AttributeError:
            st.write("No matching registered components found in PDB.")
            st.stop()
        st.write("### Components found in Production Database")
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code']
        stTrx.ColourDF(df_extractList[colz],'currentStage_code')

        stTrx.DebugOutput("Augmented collections: ",pageDict['regList'])

        st.write("---")

        st.write("## Add Flag to the damage FE Chips :")
        if st.button("Add Flag"):
           for d in enumerate(df_extractList['serialNumber']):
               try:
                  DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':d[1],'stage':"UNUSABLE"})

               except itkX.BadRequest:
                    st.write(":no_entry_sign: Changing **Unsuccessful**")
               s=(hex(int(d[1][-7:])))[-2:].upper()
               for k in enumerate(df_data[df_data['Position']==s]['Damage_Reason']):
                   try:
                      DBaccess.DbPost(st.session_state.myClient,'addComponentFlag', {'component':d[1], 'flag':k[1]})
                   except itkX.BadRequest:
                       st.write(":no_entry_sign: Add Flag**Unsuccessful**")
                   if df_data[df_data['Position']==s]['Comment']!="nan": 
                     try:
                        DBaccess.DbPost(st.session_state.myClient,'createComponentComment',{'component':d[1],'comments':[n for n in df_data[df_data['Position']==s]['Comment']]})
                     except itkX.BadRequest:
                        st.write(":no_entry_sign: Add Comment **Unsuccessful**")        
        

