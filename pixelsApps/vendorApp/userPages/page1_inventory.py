### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

infoList=[  " * Get components at institute",
            " * View componentTypes",
            "  - Select componentTypes",
            "  - View components",
            "  - Remove Assembled optional",
            " * Select Component",
            "  - View stage history",
            " * Compare to componentType",
            "  - View stages",
            "  - select stage",
            "  - View tests",
            "* Optional delete component"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Inventory", ":microscope: Components at Institute", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        if st.checkbox("Quick Component Check?"):
            if 'SCC' not in pageDict.keys():
                pageDict['SCC']={}
            pdbTrx.SelectComponentChunk(pageDict['SCC'])
            stTrx.DebugOutput("SelectComponentChunk info.",pageDict['SCC'])
            try:
                st.dataframe(pd.json_normalize(pageDict['SCC']['comp'], sep = "_"))
            except TypeError:
                st.write("no compnent info.")

        st.write("### Check componentTypes in PDB")
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
        else:
            st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        df_compTypes=df_compList[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compTypes)


        infra.MultiSelect(pageDict, 'selComps', df_compTypes['componentType_code'].astype(str).unique().tolist(), 'Select componentTypes:')
        queryStr=""
        for e,c in enumerate(pageDict['selComps']):
            if e>0:
                queryStr+=' | '
            queryStr+='componentType_code=="'+c+'"'
        stTrx.DebugOutput("queryStr",queryStr)

        # infra.SelectBoxDf(pageDict,'compType',df_compTypes,'Select a componentType', 'componentType_code')
        # stTrx.DebugOutput("*componentType* id",pageDict['compType'])

        st.write("### List of *"+", ".join(pageDict['selComps'])+"* components")
        st.write("(colour--> _",'currentStage_code',"_)")
        df_comps=df_compList.sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        if len(pageDict['selComps'])>0:
            df_comps=df_compList.query(queryStr).sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        # df_comps=df_compList.query('componentType_code=="'+pageDict['compType']['componentType_code'].values[0]+'"')
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','assembled','cts']

        ### make all columns strings
        for c in colz:
            df_comps[c]=df_comps[c].astype(str)

        infra.ToggleButton(pageDict,'removeAss',"Remove assembled components list?")
        if pageDict['removeAss']:
            df_comps=df_comps.query('assembled=="False"')

        try:
            st.dataframe(df_comps[colz].style.apply(stTrx.ColourCells, df=df_comps[colz], colName='currentStage_code', flip=True, axis=1))
        except IndexError:
            st.dataframe(df_comps[colz])
        ### download dataframe
        st.download_button(label="Write csv", data=df_comps[colz].to_csv(index=False),file_name="components_"+st.session_state.Authenticate['inst']['code']+"_"+st.session_state.Authenticate['proj']['code']+".csv")


        st.write("### Simple Stats")
        if len(pageDict['selComps'])>0:
            for compType in pageDict['selComps']:
                st.write("**",compType,"**")
                for compState in ["ready","reworked","trashed"]:
                    st.write("-",compState,":",df_comps.query('componentType_code=="'+compType+'" & state=="'+compState+'"')['serialNumber'].count())
        else:
            st.write("**all components**")
            for compState in ["ready","reworked","trashed"]:
                st.write("-",compState,":",df_comps.query('state=="'+compState+'"')['serialNumber'].count())

        #st.dataframe(df_comps[['serialNumber','componentType_code','currentStage_code']])
        # .groupby(by="currentStage_code")
        # st.write("### Output inventory")
        # if 'outCols' not in pageDict:
        #     pageDict['outCols']=["serialNumber", "componentType_code", "type_code", "currentStage_code", "currentLocation_code"]
        # infra.MultiSelect(pageDict, 'outCols', df_comps.columns.tolist(), 'Select componentTypes:')
        # stTrx.DebugOutput("df_comps for csv",df_comps[pageDict['outCols']])
        # st.download_button(label="Write csv", data=df_comps[pageDict['outCols']].to_csv(index=False),file_name="components_"+st.session_state.Authenticate['inst']['code']+"_"+st.session_state.Authenticate['proj']['code']+".csv")

        if "compKey" not in pageDict.keys():
            pageDict['compKey']='serialNumber'
        stTrx.DebugOutput("using distinguishing key:",pageDict['compKey'])
        if st.session_state.debug:
            infra.SelectBox(pageDict,'compKey',df_comps.columns,'Select key')
            st.write("unique values:",len(df_comps[pageDict['compKey']].unique()))
            st.write("total length:",len(df_comps))
        df_comps[pageDict['compKey']]=df_comps[pageDict['compKey']].astype(str)

        infra.SelectBox(pageDict, 'selCompVal', df_comps[pageDict['compKey']].astype(str).unique().tolist(), 'Select component '+pageDict['compKey']+':')

        df_compSel=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'"')
        st.dataframe(df_compSel[colz])

        if df_compSel.shape[0]>1:
            st.write("Multiple entries with "+pageDict['compKey']+":",pageDict['selCompVal'])
            infra.SelectBox(pageDict, 'selCompDate', df_compSel['stateTs'].astype(str).unique().tolist(), 'Select component creation date:')
            pageDict['selCompID']=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'" & stateTs=="'+pageDict['selCompDate']+'"')['id'].values[0]
        else:
            pageDict['selCompID']=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'"')['id'].values[0]

        pageDict['thisComp']=df_comps.query('id=="'+pageDict['selCompID']+'"')

        # infra.ToggleButton(pageDict,'removeDups',"Remove duplicates from list?")
        # if pageDict['removeDups']:
        #     df_comps=df_comps.drop_duplicates(subset=pageDict['compKey'])
        #
        # st.dataframe(df_comps.sort_values(by=pageDict['compKey'])[pageDict['compKey']])
        # st.write("components:",len(df_comps))
        #
        #
        # if len(df_comps[pageDict['compKey']].unique())==len(df_comps):
        #     try:
        #         infra.SelectBoxDf(pageDict,'thisComp',df_comps,'Select a component', pageDict['compKey'])
        #     except TypeError:
        #         st.write("issue with key, try other...")
        #         infra.SelectBox(pageDict,'compKey',df_comps.columns,'Select key')
        # else:
        #     st.write(pageDict['compKey']," is not distinguishable.")
        #     infra.SelectBox(pageDict,'compKey',df_comps.columns,'Select key')


        st.write("### Component Stage History")
        st.write("(colour--> _",'code',"_)")
        # st.write(pageDict['thisComp']['stages'])
        ### split list of stages to separate rows

        stTrx.DebugOutput("full component information",pageDict['thisComp'].to_dict())
        if "thisComp" not in pageDict.keys():
            st.write("Select component")
            st.stop()

        df_stages=pageDict['thisComp'][['serialNumber','stages']]
        df_stages=df_stages.explode('stages').reset_index(drop=True)
        ### split dictionary into columns
        df_life=pd.concat([df_stages.drop(['stages'], axis=1), df_stages['stages'].apply(pd.Series)], axis=1)
        st.dataframe(df_life.style.apply(stTrx.ColourCells, df=df_life, colName='code', flip=True, axis=1))

        ### delete component?
        pdbTrx.DeleteCoTeSt(pageDict,'delComp','deleteComponent',pageDict['thisComp']['serialNumber'].values[0])


        ### Add comparison to archetype componentType stages
        infra.ToggleButton(pageDict,'allStages',"See all "+pageDict['thisComp']['componentType_code'].values[0]+" stages?")
        if pageDict['allStages']:
            compTypeRet=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['thisComp']['componentType_code'].values[0]})
            df_compStages=pd.json_normalize(compTypeRet['stages'], sep = "_")
            st.dataframe(df_compStages)
            infra.SelectBoxDf(pageDict,'stageCode',df_compStages,'Select stage', 'code')
            try:
                df_stageTests=pd.json_normalize(pageDict['stageCode']['testTypes'].to_list()[0], sep = "_")
                st.dataframe(df_stageTests[['testType_code','order','nextStage']])
            except TypeError:
                st.write("No tests found")
