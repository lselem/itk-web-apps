### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from datetime import datetime
from datetime import timedelta
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

infoList=[  " * Review shipments",
            "  - Check last 100 days",
            "  - Sender/recipient relevant",
            " * Select shipment",
            "  - Check constituents",
            "  - Update shipment status",
            " * Create shipment",
            "  - select sender & recipient",
            "  - Upload components (ASNs) via formatted csv",
            "  - Send shipment",
            "  - Update status to inTransit"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Shipping", ":microscope: Shipping Information and Creation", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("## Check shipments in PDB")
        if st.button("reset list") or "shipList" not in list(pageDict.keys()): # check list
            st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code'])
            pageDict['shipList']=DBaccess.DbGet(st.session_state.myClient,'listShipmentsByInstitution',{'code':st.session_state.Authenticate['inst']['code']},True)
        else:
            st.write("**Got** list for "+st.session_state.Authenticate['inst']['code'])
        df_shipList=pd.json_normalize(pageDict['shipList'], sep = "_")
        df_shipList=df_shipList[['name','status','sender_code','recipient_code','stateTs','id']].sort_values(by=['status','stateTs'], ascending=False).reset_index(drop=True)
        df_shipList['stateTs']=pd.to_datetime(df_shipList['stateTs'], format='%Y-%m-%dT%H:%M:%S.%fZ', errors='coerce')
        if 'startTime' not in pageDict.keys():
            st.write("default 100 days ago")
            # then=(datetime.now() - timedelta(days = 100)).date()
            # df_shipList[~(df_shipList['stateTs'] <  datetime.strptime(str(pageDict['startTime']), '%Y-%m-%d') ) ]
            pageDict['startTime']= (datetime.now() - timedelta(days = 100)).date()

        pageDict['startTime']=st.slider('Select date', min_value=df_shipList['stateTs'].min().date(), value=pageDict['startTime'] ,max_value=df_shipList['stateTs'].max().date(), format="Y-M-D")

        df_shipList = df_shipList[~(df_shipList['stateTs'] <  datetime.strptime(str(pageDict['startTime']), '%Y-%m-%d') ) ]

        st.write("### Shipment List")
        st.write("(colour--> _",'status',"_)")
        st.dataframe(df_shipList.style.apply(stTrx.ColourCells, df=df_shipList, colName='status', flip=True, axis=1))

        infra.SelectBox(pageDict, 'selShipName', df_shipList['name'].astype(str).unique().tolist(), 'Select shipment name:')

        df_shipListSel=df_shipList.query('name=="'+pageDict['selShipName']+'"')
        st.dataframe(df_shipListSel)

        if df_shipListSel.shape[0]>1:
            st.write("Multiple entries with name:",pageDict['selShipName'])
            infra.SelectBox(pageDict, 'selShipDate', df_shipListSel['stateTs'].astype(str).unique().tolist(), 'Select shipment date:')
            pageDict['selShipID']=df_shipList.query('name=="'+pageDict['selShipName']+'" & stateTs=="'+pageDict['selShipDate']+'"')['id'].values[0]
        else:
            pageDict['selShipID']=df_shipList.query('name=="'+pageDict['selShipName']+'"')['id'].values[0]

        stTrx.DebugOutput("selected shipment ID: ",pageDict['selShipID'])

        st.write("**Shipment Components**")
        st.write("(colour--> _",'component_componentType_code',"_)")
        compShipList=DBaccess.DbGet(st.session_state.myClient,'listShipmentItems',{'shipment':pageDict['selShipID']},True)
        try:
            df_compShipList=pd.json_normalize(compShipList, sep = "_")[['component_serialNumber','component_alternativeIdentifier','component_componentType_code']]
            st.dataframe(df_compShipList.style.apply(stTrx.ColourCells, df=df_compShipList, colName='component_componentType_code', flip=True, axis=1))
        except KeyError:
            st.write("**No components found in shipment**")

        ### update existing shipment
        if "selShipID" in pageDict.keys():
            infra.ToggleButton(pageDict,'shipChange','Update shipment?')
            if pageDict['shipChange']:
                # update existing shipment
                st.write("### Update **existing** shipment *status*")
                # input id
                pageDict['existInfo']=DBaccess.DbGet(st.session_state.myClient,'getShipment', {'shipment':pageDict['selShipID']})
                try:
                    if pageDict['existInfo']!=None:
                        st.write("### :white_check_mark: Found!")
                        st.write("Found! \""+pageDict['existInfo']['name']+"\"")
                        infra.ToggleButton(pageDict,'fullShip',"See full shipment information")
                        if pageDict['fullShip']:
                            st.write("Full shipment detail:")
                            st.write(pageDict['existInfo'])
                        # select shipping status
                        pageDict['status']=pageDict['existInfo']['status']
                        infra.Radio(pageDict,'status',["prepared", "inTransit", "delivered", "deliveredIncomplete", "deliveredWithDamage", "undelivered"],"Select shipment *status*:")
                        if st.button("Update!"):
                            try:
                                pageDict['shipVal']=DBaccess.DbPost(st.session_state.myClient,'setShipmentStatus', {'shipment':pageDict['existInfo']['id'],'status':pageDict['status']})
                                st.write("### **Successful Update**:",pageDict['shipVal']['name'])
                                st.balloons()
                                st.write(pageDict['shipVal'])
                            except itkX.BadRequest as b:
                                st.write("### :no_entry_sign: Update **Unsuccessful**")
                                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                        pdbTrx.DeleteCoTeSt(pageDict,'delShip','deleteShipment',pageDict['existInfo']['id'])
                    else:
                        st.write("### :no_entry_sign: search unsuccessful")
                except KeyError:
                    pass


        ### create new shipment
        st.write("---")
        st.write("## Create **new** shipment")
        st.write("### Select *Sender* & *Recipient* Institutions")
        #st.write(st.session_state.Authenticate['inst']['name'])
        if  st.button("reset sender") or "sender" not in pageDict.keys():
            pageDict['sender']=st.session_state.Authenticate['inst']
        stTrx.DebugOutput("Full *institution* list: ",st.session_state.Authenticate['instList'])
        #st.write(pageDict['sender']['name'])
        infra.SelectBox(pageDict,'sender',st.session_state.Authenticate['instList'],'Select a **sender** Institution','name')
        infra.SelectBox(pageDict,'recipient',st.session_state.Authenticate['instList'],'Select a **recipient** Institution','name')

        # select shipping type
        st.write("### Select shipping *type*")
        infra.Radio(pageDict,'type',["domestic", "intraContinental", "continental"],"Select shipment *type*:")

        # upload components
        infra.ToggleButton(pageDict,'tog_upFile',"Upload components by file")
        if pageDict['tog_upFile']:
            st.write("Upload *component codes* (ASNs/codes) from **csv** file")
            st.write("Follow data format:")
            df_example=pd.DataFrame([{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'}])
            st.write(df_example.style.hide_index())
            pageDict['file']= st.file_uploader("Upload a file", type=["csv"])
            stTrx.DebugOutput("file: ",pageDict['file'])

            if pageDict['file'] is not None:
                pageDict['file'].seek(0)
                df_input=pd.read_csv(pageDict['file'])
                if "serialNumber" not in df_input.columns:
                    st.write("adding serialNumber header") # reset input file reader
                    pageDict['file'].seek(0)
                    df_input=pd.read_csv(pageDict['file'], header=None)
                    df_input=df_input.rename(columns={0: "serialNumber"})
                myComps=DBaccess.DbGet(st.session_state.myClient,'getComponentBulk', {'component':list(df_input['serialNumber'].values)})
                st.write("length of myComps:",len(myComps))
                pageDict['df_data']=pd.DataFrame([{'serialNumber':x['serialNumber'],'code':x['code'],'compCode':x['componentType']['code'],'curLoc':x['currentLocation']['code']} for x in myComps])
            else:
                st.write("No data file set")
                st.stop()

        infra.ToggleButton(pageDict,'tog_upSel',"Select components")
        if pageDict['tog_upSel']:
            ### select components by hand
            st.write("### Check componentTypes in PDB")
            if st.button("reset list",key="shipping") or "compList" not in list(pageDict.keys()): # check list
                st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
                pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
            else:
                st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            df_compList=pd.json_normalize(pageDict['compList'], sep = "_")

            df_subComps=pdbTrx.SelectManyDf(pageDict,df_compList,'componentType_code')
            stTrx.Stringify(df_subComps)
            infra.MultiSelect(pageDict,'multiSNs',df_subComps['serialNumber'].to_list(),'Select serialNumbers:')
            if len(pageDict['multiSNs'])<1:
                st.write("No serialNumbers chosen")
                st.stop()
            else:
                stTrx.DebugOutput('Selected serialnumbers: ',pageDict['multiSNs'])
                pageDict['df_data'] = df_subComps[df_subComps['serialNumber'].isin(pageDict['multiSNs'])]

        if "df_data" not in pageDict.keys():
            st.write("No components selected to ship")
            st.stop()
        st.write("Selected components to ship")
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','assembled','cts']
        st.dataframe(pageDict['df_data'][colz])
        st.write("length of dataframe:",len(pageDict['df_data']))


        infra.TextBox(pageDict,'shipName',"Name of shipment:")

        shipObj={'name':pageDict['shipName'],'sender':pageDict['sender']['code'],'recipient':pageDict['recipient']['code'],'type':pageDict['type'],'shipmentItems':list(pageDict['df_data']['code'].values)}

        infra.ToggleButton(pageDict,'moreData',"Add tracking information")
        if pageDict['moreData']:
            infra.TextBox(pageDict,'trackNum',"Tracking number:")
            infra.TextBox(pageDict,'shipService',"Shipping service:")
            shipObj['trackingNumber']=pageDict['trackNum']
            shipObj['shippingService']=pageDict['shipService']

        stTrx.DebugOutput("Shipping schema for upload...",shipObj)

        # access database
        if st.button("Create Shipping!"):
            ### create shipment
            try:
                pageDict['retVal']=DBaccess.DbPost(st.session_state.myClient,'createShipment', shipObj)
                st.write("### **Successful shipment created** for:",pageDict['retVal']['name'])
                st.balloons()
                st.write(pageDict['retVal'])
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Shipment setting **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

        if 'retVal' in pageDict.keys():
            ### update shipment state
            st.write("Set shipment ("+pageDict['retVal']['name']+") to *inTransit*")
            if st.button("Set to inTransit"):
                try:
                    pageDict['upVal']=DBaccess.DbPost(st.session_state.myClient,'setShipmentStatus', {'shipment':pageDict['retVal']['id'],'status':"inTransit"})
                    st.write("### **Successful Update**:",pageDict['upVal']['name'])
                    st.balloons()
                    st.write(pageDict['upVal'])
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Update **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            try:
                st.write("last successful _upload_:",pageDict['retVal']['name'])
                ### delete shipment?
                pdbTrx.DeleteCoTeSt(pageDict,'delShip','deleteShipment',pageDict['retVal']['name'])
            except KeyError:
                pass
        if 'delVal' in pageDict.keys():
            try:
                st.write("last successful _deletion_:",pageDict['delVal']['name'])
            except KeyError:
                pass
