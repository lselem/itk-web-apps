### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

infoList=[  " * Get BARE_MODULEs and GELPACKs at institute",
            " * Select BM type: single/quad",
            "  - Change GP occupancy",
            " * Select BM components",
            "  - Choose slot",
            "  - Choose orientation (TBC)",
            " * Select GELPACK",
            " * Assemble BMs to GP",
            "  - Disassemble optional"]
#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("GP Assembly", ":microscope: GelPak Assembly", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        ### select components at institute
        compTypes=["BARE_MODULE","GELPACK"]
        st.write("## Get components at institute")
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** "+", ".join(compTypes)+" components for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'], 'componentType':compTypes },True)
        else:
            st.write("**Got** "+", ".join(compTypes)+" components for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            #st.write("(colour--> _",'componentType_code',"_)")
        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        df_compList=df_compList.query('trashed==False')
        colz=['serialNumber','code','alternativeIdentifier','componentType_name','componentType_code','assembled']

        ### make all columns strings
        for c in colz:
            df_compList[c]=df_compList[c].astype(str)

        infra.Radio(pageDict,'removeAss',['all','only assembled','not assembled'],"Keep components in list?")
        if pageDict['removeAss']=="not assembled":
            df_compList=df_compList.query('assembled=="False"')
        if pageDict['removeAss']=="only assembled":
            df_compList=df_compList.query('assembled=="True"')

        try:
            stTrx.ColourDF(df_compList[colz].sort_values(by=['serialNumber']),'componentType_code')
            # st.dataframe(df_compList[colz].sort_values(by=['serialNumber']).style.apply(stTrx.ColourCells, df=df_compList[colz], colName='componentType_code', flip=True, axis=1))
        except KeyError:
            st.write("No componentTypes found:",",".join(compTypes))
            st.stop()

        st.write("---")
        st.write("## Select BARE_MODULEs")

        occupancyList=[{'type':"singles",'count':9},{'type':"quads",'count':4}]
        infra.SelectBox(pageDict, 'occupancy', occupancyList, 'Select type of Bare Modules:','type')

        df_BMs=df_compList.query('componentType_code=="'+"BARE_MODULE"+'"').sort_values(by=['serialNumber'])
        st.write("components found at institution:",len(df_BMs))

        st.write("### Up to "+str(pageDict['occupancy']['count'])+" components per GelPack")
        if "parts" not in pageDict.keys() or st.button("reset positions"):
            pageDict['parts']=[]
            for tc in [x for x in pageDict.keys() if "togComp_" in x]:
                pageDict[tc]=False

        for i,row in df_BMs.iterrows():
            sn=str(row['serialNumber'])
            altID=str(row['alternativeIdentifier'])
            infra.ToggleButton(pageDict,'togComp_'+str(row['code']),sn+" ("+altID+")")
            if pageDict['togComp_'+str(row['code'])]:
                selPos=st.selectbox("Position:",[x for x in range(0,pageDict['occupancy']['count'],1)], key=sn+'_pos')
                selOri=st.selectbox("Orientation:",['normal','reversed'], key=sn+'_ori')
                if sn in [x['SN'] for x in pageDict['parts']]:
                    ind=[x['SN'] for x in pageDict['parts']].index(sn)
                    if pageDict['parts'][ind]['slot']==selPos:
                        if pageDict['parts'][ind]['ori']==selOri:
                            continue
                        else:
                            pageDict['parts'][ind]['ori']=selOri
                    else:
                        if selPos in [x['slot'] for x in pageDict['parts']]:
                            st.write("**position already filled** choose again")
                        else:
                            pageDict['parts'][ind]['slot']=selPos
                            pageDict['parts'][ind]['ori']=selOri
                else:
                    if selPos in [x['slot'] for x in pageDict['parts']]:
                        st.write("**position already filled** choose again")
                    else:
                        pageDict['parts'].append({'compType':"BARE_MODULE",'SN':sn,'slot':selPos,'ori':selOri})
            ### remove if exists
            else:
                if sn in [x['SN'] for x in pageDict['parts']]:
                    pageDict['parts'].remove(next(item for item in pageDict['parts'] if item['SN'] == sn))

        st.write("selected components:",len(pageDict['parts']))
        st.dataframe(pageDict['parts'])

        st.write("---")

        if len(pageDict['parts'])<1:
            st.stop()
        elif len(pageDict['parts'])>pageDict['occupancy']['count']:
            st.write("Too many selections. Choose up to",pageDict['occupancy']['count'])
            st.stop()

        st.write("## Select GELPACK")
        df_GPs=df_compList.query('componentType_code=="'+"GELPACK"+'"').sort_values(by=['serialNumber'])
        st.write("components found at institution:",len(df_GPs))
        infra.SelectBox(pageDict,'selectedGP',df_GPs['serialNumber'].to_list(),'Select Gel Pack')
        pageDict['comb']={'compType':"GELPACK",'SN':pageDict['selectedGP']}

        st.write("---")
        st.write("## GELPACK assembly")

        ### ASSEMBLE PARTS
        if "GPass" not in pageDict.keys():
            pageDict['GPass']=False
        if st.button("Assemble"):
            ### assemble function
            pdbTrx.AssembleComponent(pageDict,True)

        infra.ToggleButton(pageDict,'togAss',"Assembled: "+pageDict['selectedGP'])

        if pageDict['togAss']:
            gpComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {"component" : pageDict['selectedGP']} )
            st.write("Children:",[x['component']['serialNumber'] for x in gpComp['children'] if x['component']!=None])
            stTrx.DebugOutput("Children details: ",gpComp['children'])

            positions=[]
            for x in gpComp['children']:
                if x['component']==None: continue
                positions.append({'x':int(x['order']/2),'y':int(x['order'])%2,'text':x['component']['serialNumber']})
            stTrx.DebugOutput("Positions dataframe: ",pd.DataFrame(positions))
            stTrx.VisOrientation(pd.DataFrame(positions))

            if st.button("disassemble"):
                pdbTrx.DisassembleComponent(gpComp)
