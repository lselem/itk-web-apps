### standard
import streamlit as st
### custom
import os
import pandas as pd
import ast
import csv
import json
from datetime import datetime
from io import BytesIO
### PDB stuff
import core.DBaccess as DBaccess
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def CheckComponent(sn, compType=None, curLoc=None, debug=False):
    ### mostly Carl's comp check code

    compCheck=""
    if debug: st.write("getComponent")
    try:
        retVal=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':sn})
        if debug: st.write("found:",sn,"("+retVal['componentType']['code']+")")
    except itkX.BadRequest:
        if debug: st.write("No component found")
        return "False: No component found "

    # Check type of component is as expected
    if debug: st.write("check type")
    try:
        if retVal['componentType']['code'] != compType:
            if debug: st.write("Unexpected component type for "+compType+":", retVal['componentType']['code'])
            compCheck+="False: Wrong componentType "
    except TypeError:
        if debug: st.write("Problem with database return")
        compCheck+="False: return issue "

    # Check at correct institution
    if debug: st.write("check inst.")
    try:
        if retVal['currentLocation']['code'] != curLoc:
            if debug: st.write("Component not "+curLoc+", it\'s:", retVal['currentLocation']['code'])
            compCheck="False: Wrong currentLocation "
    except TypeError:
        st.write("Problem with database return")
        compCheck="False: return issue "

    # Check not already attached to a bare module
    try:
        if retVal["parents"] is None:
                used = []
        else:
            used = [c["componentType"]["id"] for c in retVal["parents"] if c["componentType"]["code"] == "BARE_MODULE"]
        if len(used)>0:
            if debug: st.write("Component already attached to bare module:", used[0])
            compCheck="False: attach issue "
    except TypeError:
        if debug: st.write("Problem with database return")
        compCheck="False: return issue "

    # Check at correct institution
    if debug: st.write("check state")
    try:
        if retVal['state'] != "ready":
            if debug: st.write("Component not ready:", retVal['state'])
            compCheck="False: Wrong state "
    except TypeError:
        st.write("Problem with database return")
        compCheck="False: return issue "

    return compCheck


def ColourCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)


def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)

    if k in ['passed','problems']: # avoid bool trouble
        return bool(val)
    if k in ['runNumber']: # avoid string trouble
        return str(val)

    if "list" in str(type(val)):
        val=list(val)
    else:
        try:
            val=int(val)
        except ValueError:
            try:
                val=float(val)
            except ValueError:
                val=str(val)

    return val


def EditJson(inJson):
    for k,v in inJson.items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                inJson[k][l]=SelectCheck(l,w)
        else:
            inJson[k]=SelectCheck(k,v)
    return inJson


def SlotFinder(parent,compType,pos):
    for child in parent['children']:
        if st.session_state.debug:
            st.write("**DEBUG** checking children:",child['componentType']['code'],child['order'])
        if child['componentType']['code']==compType and str(child['order'])==pos:
            return child['id']
    ### if fail then return None
    return None

def to_excel(df,shtNm):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, index=False, sheet_name=shtNm)
    workbook = writer.book
    worksheet = writer.sheets[shtNm]
    format1 = workbook.add_format({'num_format': '0.00'})
    worksheet.set_column('A:A', None, format1)
    writer.save()
    processed_data = output.getvalue()
    return processed_data

def DFsFromExcel(fileObj, sheetName, colIndex=0):
    ### read in excel file
    df_xl=pd.read_excel(fileObj, sheet_name=sheetName, index_col=colIndex)
    #df_xl.reset_index(level=0, inplace=True)
    df_xl=df_xl.reset_index()

    ### get header
    splitRow=5
    df_head=df_xl[0:splitRow].copy(deep=True)
    df_head.columns = df_head.columns.str.replace(' ', '_')
    df_head=df_head.iloc[:, [0, 1, 2]]
    df_head

    ### get data
    df_data= df_xl[splitRow+1::].copy(deep=True)
    df_data.columns = list(df_xl.iloc[splitRow].values)
    df_data.columns = df_data.columns.astype(str)
    df_data.columns = df_data.columns.str.strip()
    df_data.columns = df_data.columns.str.replace(' ', '_')
    df_data.columns = df_data.columns.str.replace('\s+', '_')
    df_data.columns = df_data.columns.str.replace('__', '_')
    df_data.columns = df_data.columns.str.replace('.', '')
    df_data.columns = df_data.columns.str.replace(' ', '_')
    df_data = df_data.drop(columns=['nan']).reset_index(drop=True)
    for c in df_data.columns:
        df_data[c]=df_data[c].astype(str)
        df_data[c]=df_data[c].str.replace(' ', '')

    return df_head,df_data
