### standard
import streamlit as st
from core.Page import Page
### custom
import os
import pandas as pd
import copy
import plotly.graph_objects as go
from io import BytesIO
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from .SharedCode import to_excel
from .SharedCode import DFsFromExcel

#####################
### useful functions
#####################

### get FE_WAFER ASN from 'ID' property
def FindWafer(compList,waferID):
    # st.write("checking",len(compList),"components")
    for feComp in compList:
        ### Find matching altID
        if feComp['alternativeIdentifier']==waferID:
            # st.write("check:",prop['code'],prop['value'])
            return feComp['serialNumber']
        # ### Find matching property
        # for prop in feComp['properties']:
        #     if prop['code']=="ID" and prop['value']==waferID:
        #         # st.write("check:",prop['code'],prop['value'])
        #         return feComp['serialNumber']
    return None

### get ASN for FE_CHIP
def GetFE_SN(compList,waferID,pos,prefix="20UPGFC"):
#     feComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':waferID,'alternativeIdentifier':True})
#     waferSN=feComp['serialNumber']
    waferSN=FindWafer(compList,waferID)
    if waferSN==None:
        #st.write("no ASN found for",waferID)
        return None
    if st.session_state.debug:
        st.write('serialNumber:',waferSN,"-->",waferSN[-3:])
        st.write(int(waferSN[-3:]),"-->",hex(int(waferSN[-3:])))
    combStr=str(hex(int(waferSN[-3:])))[-3:]+pos.replace('-','')
    if st.session_state.debug:
        st.write(hex(int(waferSN[-3:])),"+",pos,"-->",combStr)
    finalStr=prefix+str(int(combStr, 16)).zfill(7)
    if st.session_state.debug:
        st.write(combStr,"-->",int(combStr, 16),"-->",finalStr)
    return finalStr

### build BARE_MODULE ASN: ['SENSOR_TYPE', 'FECHIP_VERSION', 'THICKNESS', 'VENDOR']
def MakeBM_SN(schemaProps,manSN,YY="B4"):
    # check SN spec.: https://edms.cern.ch/ui/#!master/navigator/document?D:100968451:100968451:subDocs
    serialNumber_BM="20UPG"+YY+schemaProps['FECHIP_VERSION']+schemaProps['VENDOR']+manSN.zfill(5)
    return serialNumber_BM

### Calculate child position
def ModSlot(inVal,mod=4):
    val=int(inVal)%mod
    if val==0:
        val=mod
    return str(val)

### Selections (copied form data-structure)
def GetCodeTable(codeStr):
    
    if "fe" in codeStr.lower() or "chip" in codeStr.lower():
        return [{"code": "0","value": "RD53A"},
                {"code": "1","value": "ITkpix_v1"},
                {"code": "2","value": "ITkpix_v1.1"},
                {"code": "3","value": "ITkpix_v2"},
                {"code": "9","value": "No FE chip"} ]
    elif "sensor" in codeStr.lower():
        return [{"code": "0","value": "forgetme1", "name":"No sensor"},
                {"code": "1","value": "forgetme2", "name": "Market survey sensor tile"},
                {"code": "2","value": "L0_INNER_PIXEL_3D_SENSOR_TILE_50", "name": "L0 inner pixel 3D sensor tile"},
                {"code": "2","value": "L0_INNER_PIXEL_3D_SENSOR_TILE_25", "name": "L0 inner pixel 3D sensor tile"}, ## duplicate
                {"code": "3","value": "forgetme3", "name": "L0 inner pixel planar sensor tile"},
                {"code": "4","value": "L1_INNER_PIXEL_QUAD_SENSOR_TILE", "name": "L1 inner pixel quad sensor tile"},
                {"code": "5","value": "OUTER_PIXEL_QUAD_SENSOR_TILE", "name": "Outer pixel quad sensor tile"},
                {"code": "9","value": "DUMMY_TESTSTRUCTURE", "name": "Dummy sensor tile"} ]
    elif "thick" in codeStr.lower():
        return [{"code": "0", "value": "Thin"},
                {"code": "1", "value": "Thick"}]
    elif "vend" in codeStr.lower():
        ### this code table matches EDMS: HPK=3 & Leonardo=1 (these are flipped in PDB (19/12/23))
        return [{"code": "0", "value": "Advacam", "map":{'1':"1", '2':"2", '3':"3", '4':"4" }},
                {"code": "3", "value": "HPK", "map":{'1':"1", '2':"2", '3':"3", '4':"4" }},
                {"code": "2", "value": "IZM", "map":{'1':"1", '2':"2", '3':"4", '4':"3" }}, ### 
                {"code": "1", "value": "Leonardo", "map":{'1':"1", '2':"2", '3':"4", '4':"3" }},
                {"code": "9", "value": "ITk institute", "map":{'1':"1", '2':"2", '3':"3", '4':"4" }}]
    elif "bm" in codeStr.lower():
        return [{"suff": "B1", "value": "Single", "code":"SINGLE_BARE_MODULE"},
                {"suff": "B2", "value": "Dual", "code":"DUAL_BARE_MODULE"},
                {"suff": "B4", "value": "Quad", "code":"QUAD_BARE_MODULE"},
                {"suff": "BS", "value": "Digital single", "code":"DIGITAL_SINGLE_BARE_MODULE"},
                {"suff": "BQ", "value": "Digital Quad", "code":"DIGITAL_QUAD_BARE_MODULE"},
                {"suff": "BT", "value": "Dummy single", "code":"DUMMY_SINGLE_BARE_MODULE"},
                {"suff": "BR", "value": "Dummy Quad", "code":"DUMMY_QUAD_BARE_MODULE"},
                {"suff": "XB", "value": "Tutorial Quad", "code":"TUTORIAL_BARE_MODULE"}]
    else:
        st.write("❗ don't understand code table?!")
        return None

def CheckBM_SN(bm_sn):
    # check new BM SN already in PDB
    st.write(f"Check BM serialNumber: {bm_sn}")
    # try to get BM info. from PDB (using serialNumber)
    try:
        bmInfo=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':bm_sn})
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: get component **Unsuccessful** for",bm_sn)
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    # if nothing found :)
    if bmInfo==None:
        st.write("✔ __No__ existing bare module found")
        return False
    # if BM found :(
    else:
        st.write("❌ Existing bare module found. Registration will be blocked")
        # st.write(bmInfo)
        return True
    
def CheckFEs_Version(feList):
    # check FE versions match
    feTypes=[fe['type']['name'] for fe in feList if "type" in fe.keys()]
                    
    # should be single version
    if len(set(feTypes))==1:
        st.write(f"✔ FE versions match: {set(feTypes)}")
        return False
    # if multiple FE versions found
    else:
        st.write("❌ FE versions do not match")
        # show FEs info.
        try:
            st.dataframe([{'serialNumber':fe['serialNumber'],'chip version':fe['type']['code']} for fe in feList if "type" in fe.keys()])
        except:
            st.write(feList)
        return True

def CheckFEs_Assembly(feList):
    # check exists and assembly
    for fe in feList:
        st.write(f"component type: {fe['type']['name']}")
        # if FE not found
        if fe==None:
            st.write("❌ __No__ existing component found")
            return False
        # if exists check assembly
        else:
            if fe['assembled']==True:
                st.write(f"❌ Component ({fe['serialNumber']}) already assembled. Assembly will be blocked")
                return True
            else:
                st.write(f"✔ Component ({fe['serialNumber']}) ready for assembly.")
                return False
            

infoList=[  " * Input formatted xlsx",
            " * Extract data",
            "  - Sensor Id",
            "  - FE Ids + slots",
            "  - Rework comment",
            " * Convert Ids -> ASNs",
            " * Check in PDB",
            " * Set BM properties",
            "  - Default from sensor + FE + institute",
            " * Generate BM ASN",
            "  - Properties + vendor SN",
            "  - Optional check in PDB",
            " * Register BARE_MODULEs",
            "  - Delete optional",
            " * Assemble components",
            "  - Each component",
            "  - Add rework comments",
            "  - Disassemble optional"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("Bare Modules", ":microscope: Bare Module Registration and Assembly", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['vendor']=st.session_state.Authenticate['inst']['code']
        infra.SelectBox(pageDict,'vendor',[v['value'] for v in GetCodeTable("vendor")],"Select institution")
        if st.checkbox("See vendor orientation table?"):
            st.dataframe(GetCodeTable("vendor"))

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["xls","xlsx"])
        stTrx.DebugOutput("Input file:",pageDict['file'])

        sheetName='Einzelprojektübersicht'
        if pageDict['file'] is None:
            ### delete existing info (defined below)
            try:
                del pageDict['regList']
            except KeyError:
                pass
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="BARE_MODULE_vendor_test_updateGP.xlsx"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
                st.write(os.listdir(filePath[:filePath.rfind('/')]))
            df_test=pd.read_excel(filePath[:filePath.rfind('/')]+"/"+exampleFileName, sheet_name=sheetName)
            df_xlsx = to_excel(df_test,sheetName)
            #st.dataframe(df_test)
            st.download_button(label="Download example", data=df_xlsx, file_name=exampleFileName)
            #st.write(pageDict.keys()-['file'])
            for k in pageDict.keys()-['file']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()

        ###########################
        ### extract data from input file
        ###########################
        st.write("## Read Data")
        ### separate header info from data
        df_head, df_data= DFsFromExcel(pageDict['file'], sheetName)
        # show data
        st.dataframe(df_data)

        ### input stats
        # get number hybrids === number sensors
        st.write("### Preliminary stats")
        st.write("unique sensor numbers:",len(df_data['Sensor_No'].unique()))
        st.write("unique chip numbers:",len(df_data['ROC_position'].unique()))
        st.write("... from",len(df_data['ROC_wafer'].unique()),"wafers")
        #st.write("number unique chips:",len(df_data['Projekt-_/_Run-Nr'].unique()))

        st.write("---")
        st.write("## Extract Data")

        stTrx.DebugOutput("Columns in data: ",df_data.columns.to_list())

        ### COMPILE COMPONENTS FOR ASSEMBLY
        ### loop over components to make modules
        if "regList" not in pageDict.keys() or st.button("re-read Data"):
            # compile FE_WAFER list to find FE_CHIPs later
            pageDict['regList']=[]
            st.write("### looping over data...")
            for sensor in df_data['Sensor_No'].unique():
                ### set up BM dictionary
                pageDict['regList'].append({'comb':None,'parts':[],'assembly':False,'registered':False, 'reworks':[], 'stopReg':False, 'stopAss':False, 'comments':[]})
                ### check out SENSOR_TILE
                st.write("working on sensor:",sensor)
                pageDict['regList'][-1]['parts'].append({'compType':"SENSOR_TILE",'SN':None,'ID':sensor})
                ### catch manSN for BM ASN definition
                pageDict['regList'][-1]['manSN']=df_data.query('Sensor_No=="'+sensor+'"').iloc[0]['Current_Module_No']
                ### check out FE_CHIPs
                for index,feRow in df_data.query('Sensor_No=="'+sensor+'"').iterrows():
                    ### add project to comb
                    pageDict['regList'][-1]['comb']={'project':feRow['Project']}
                    ### get FE info.
                    st.write("- got FE:",feRow['ROC_wafer'],feRow['ROC_position'])
                    pageDict['regList'][-1]['parts'].append({'SN':None,'compType':"FE_CHIP",'wafer':feRow['ROC_wafer'],'pos':feRow['ROC_position'],'slot':ModSlot(feRow['ROC_slot'])})
                    if feRow["ROC_Number_removed_ROC"] != "nan":
                        st.write("Found rework!", feRow["ROC_Number_removed_ROC"])
                        ### todo: translate to ASN
                        rwSN=feRow['ROC_Number_removed_ROC']
                        pageDict['regList'][-1]['reworks'].append({'info':rwSN,'slot':feRow['ROC_slot']})
                    if feRow["Remarks_for_customer"] != "0":
                        st.write("Found comment:",feRow["Remarks_for_customer"])
                        pageDict['regList'][-1]['comments'].append(feRow["Remarks_for_customer"])

        if st.checkbox("View extracted collections?"):
            st.write(pageDict['regList'])

        if st.session_state.debug:
            if "FEprefix" not in pageDict.keys():
                pageDict['FEprefix']="20UPGFC"
            infra.TextBox(pageDict,'FEprefix',"Enter FE prefix:")

        st.write("### Converted identifiers:")
        if "converted" not in pageDict.keys() or st.button("re-convert IDs"):
            pageDict['converted']=True
            # FEs for identification

            if "feCompList" not in pageDict:
                st.write("grabbing FE component list...")

                ### get indexing
                total=st.session_state.myClient.get('listComponents', json={'project':"P",'componentType':"FE_WAFER",'pageInfo':{ 'pageSize': 1 }}).total
                pageSize=50
                count= int(total/pageSize)
                if total%pageSize>0:
                    count=count+1
                
                ### make list of altIDs and SNs
                st.write("Retrieving FE info.")
                my_bar = st.progress(0)
                pageDict['feCompList']=[]
                for pi in range(0,count,1):
                    # st.write(f"loop:{pi+1}/{count}")
                    my_bar.progress( float(pi+1)/count)
                    compList=st.session_state.myClient.get('listComponents', json={'project':"P",'componentType':"FE_WAFER",'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
                    # st.write(f'length: {len(compList.data)}')
                    pageDict['feCompList'].extend([{'alternativeIdentifier':comp['alternativeIdentifier'], 'serialNumber':comp['serialNumber']} for comp in compList.data])
                else:
                    st.write("Retrieval complete")
            else:
                st.write("got FE component list...")

            # pageDict['feCompList']=DBaccess.DbGet(st.session_state.myClient,'listComponents', {'componentType':"FE_WAFER"},True)
            
            
            
            
            status_bar_con=st.progress(0.0) #st.empty()
            status_text_con=st.empty()
            result_text_con=st.empty()
            checks_text_con=st.empty()
            listLen=len(pageDict['regList'])
            ### ASN Conversion
            for e,rl in enumerate(pageDict['regList']):
                for part in rl['parts']:
                    status_bar_con.progress(1.0*(e+1)/listLen)
                    if part['compType']=="SENSOR_TILE":
                        status_text_con.text("working on "+part['compType']+": "+part['ID'])
                        ### translate to ASN
                        try:
                            senSN=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':part['ID'],'alternativeIdentifier':True})['serialNumber']
                            try:
                                checkVal=pdbTrx.CheckComponent(senSN, part['compType'], st.session_state.Authenticate['inst']['code'], st.session_state.debug)
                                if "False" in checkVal:
                                    checks_text_con.text(senSN+" failed checks: "+checkVal)
                                    # continue
                                part['SN']=senSN
                                result_text_con.text("- Found ASN: "+senSN)
                            except TypeError:
                                checks_text_con.text("cannot check component: "+senSN)
                                part['SN']=None

                        except KeyError:
                            result_text_con.text("- no ASN found.")
                            part['SN']=None
                        except TypeError:
                            result_text_con.text("- no ASN found.")
                            part['SN']=None
                                
                        # else:
                        #     checks_text_con.text("cannot check component: "+senSN)
                        #     part['SN']=None
                    if part['compType']=="FE_CHIP":
                        ### check out FE_CHIPs
                        status_text_con.text("working on "+part['compType']+": "+part['wafer']+", "+part['pos'])
                        ### translate to ASN
                        if "FEprefix" in pageDict.keys():
                            feSN=GetFE_SN(pageDict['feCompList'],part['wafer'],part['pos'],pageDict['FEprefix'])
                        else:
                            feSN=GetFE_SN(pageDict['feCompList'],part['wafer'],part['pos'])
                        if feSN==None:
                            result_text_con.text("- no ASN found.")
                            part['SN']=None
                            continue
                        ### checks!
                        try:
                            checkVal=pdbTrx.CheckComponent(feSN, part['compType'], st.session_state.Authenticate['inst']['code'], st.session_state.debug)
                            if "False" in checkVal:
                              checks_text_con.text(feSN+" failed checks: "+checkVal)
                              # continue
                            part['SN']=feSN
                            result_text_con.text("- Found ASN: "+feSN)
                        except TypeError:
                            checks_text_con.text("cannot check component: "+feSN)
                            part['SN']=None
                            pass
        else:
            st.write("Done. Re-run if required")

        st.write("### Check database for identifiers:")
        if "ExtractList" not in pageDict.keys() or st.button("reset extracted list"):
            status_bar_ids=st.progress(0.0) #st.empty()
            status_text_ids=st.empty()
            listLen=len(pageDict['regList'])
            pageDict['ExtractList']=[]
            for e,c in enumerate(pageDict['regList']):
                for d in c['parts']:
                    loopText=f"checking SN: {d['SN']}" #+", "+str(e+1)+" of "+str(listLen)
                    status_text_ids.text(loopText)
                    status_bar_ids.progress(1.0*(e+1)/listLen)
                    if d['SN']==None:
                        continue
                    try:
                        pageDict['ExtractList'].append(DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':d['SN']} ))
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: get component **Unsuccessful** for",d)
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        
            ### flag extraction complete
            pageDict['extraction']=True
        else:
            st.write("Done. Re-run if required")

        ### show corresponding PDB information
        # try to get info from dictionary
        try:
            df_extractList=pd.json_normalize(pageDict['ExtractList'], sep = "_")
        # stop if nothing found
        except AttributeError:
            st.write("No matching registered components found in PDB.")
            st.stop()

        if df_extractList.empty:
            st.write("Extraction list is empty :(. Please check inputs.")
            st.stop()

        st.write("### Child components found in Production Database")
        #st.write("(colour--> _",'componentType_code',"_)")
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code','assembled']
        try:
            stTrx.ColourDF(df_extractList[colz],'componentType_code')
        except KeyError:
            st.write("Cannot style dataframe :(")
            st.write(df_extractList)
        #st.dataframe(df_extractList[colz].style.apply(stTrx.ColourCells, df=df_extractList[colz], colName='componentType_code', flip=True, axis=1))

        stTrx.DebugOutput("Augmented collections: ",pageDict['regList'])

        ###########################
        ### set-up
        ###########################
        st.write("---")
        st.write("## BARE_MODULE Set-up")

        ### Component checks loop
        if "set-up" not in pageDict.keys() or st.button("re-run set-up"):
            ### unselect any existing toggles (defined below)
            for k in [pdk for pdk in pageDict.keys() if "togSet_" in pdk]:
                pageDict[k]=False

            ### get compSchema from PDB
            compSchema_orig=DBaccess.DbGet(st.session_state.myClient,'generateComponentTypeDtoSample', {'code':"BARE_MODULE",'project':"P",'requiredOnly':True})

            ### loop over extracted collections
            for rl in pageDict['regList']:
                st.write(f"### Working on {rl['manSN']}")

                ### check FEs info
                feSNs=[p['SN'] for p in rl['parts'] if p['compType']=="FE_CHIP"]
                st.write(f"Check chip versions: {feSNs}")
                ### try to get component info from PDB (using FE SNs)
                try:
                    feList=DBaccess.DbGet(st.session_state.myClient,'getComponentBulk', {'component':[p['SN'] for p in rl['parts'] if p['compType']=="FE_CHIP"]}, True)
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: get component **Unsuccessful** for",[p['SN'] for p in rl['parts'] if p['compType']=="FE_CHIP"])
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                # st.write(feList)
                
                if CheckFEs_Version(feList) or CheckFEs_Assembly(feList):
                    rl['stopAss']=True

                # check sensor info (should be only one, i.e. )
                # check first entry
                try:
                    sensorSN=[p['SN'] for p in rl['parts'] if p['compType']=="SENSOR_TILE"][0]
                # if single entry not found
                except IndexError:
                    st.write("Unexpected number of sensors. Can't get sensor version.")
                st.write(f"Check sensor: {sensorSN}")
                # try to get info from PDB (using sensor SN)
                try:
                    sensorInfo=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':sensorSN})
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: get component **Unsuccessful** for",sensorSN)
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                # st.write(sensorInfo)
                # check exists and assembly
                # if sensor not found
                if sensorInfo==None:
                    st.write("❌ __No__ existing component found")
                else:
                    st.write(f"component type: {sensorInfo['type']['name']}")
                    # if exists check assembly
                    if sensorInfo['assembled']==True:
                        st.write(f"❌ Component {sensorInfo['serialNumber']} already assembled. Assembly will be blocked")
                        rl['stopAss']=True
                    else:
                        st.write(f"✔ Component ({sensorInfo['serialNumber']}) ready for assembly.")

                ### Calculate FE and sensor codes
                # sensor type
                # try to compare to reference table (defined above)
                try:
                    # st.write(f"looking for: {sensorInfo['type']['name']}")
                    # st.dataframe(GetCodeTable("sensor"))
                    sensorType = next((item['code'] for item in GetCodeTable("sensor") if item['value'].lower() == sensorInfo['type']['code'].lower()), None)
                    # st.write("I have found:", sensorType, "using:", sensorInfo['type'])
                # if not key then set None
                except KeyError:
                    sensorType=None
                except TypeError:
                    sensorType=None
                # fe chip version
                # try to compare to reference table (defined above)
                try:
                    # st.write(f"looking for: {feTypes[0]}")
                    # st.dataframe(GetCodeTable("chip"))
                    # check FE versions match
                    feTypes=[fe['type']['name'] for fe in feList if "type" in fe.keys()]
                    feType = next((item['code'] for item in GetCodeTable("chip") if item['value'].lower() == feTypes[0].lower()), None)
                # if not key then set None
                except KeyError:
                    feType=None
                # vendor code
                # try to compare to reference table (defined above)
                try:
                    # st.write(f"looking for: {pageDict['vendor']}")
                    # st.dataframe(GetCodeTable("vendor"))
                    vendorCode = next((item['code'] for item in GetCodeTable("vendor") if item['value'].lower() == pageDict['vendor'].lower()), None)
                # if not key then set None
                except KeyError:
                    vendorCode=None
                ### BM type
                if "single" in rl['comb']['project'].lower():
                    bmType = next((item for item in GetCodeTable("bm") if item['suff'] == "B1"), None)
                elif "dual" in rl['comb']['project'].lower():
                    bmType = next((item for item in GetCodeTable("bm") if item['suff'] == "B2"), None)
                elif "quad" in rl['comb']['project'].lower():
                    bmType = next((item for item in GetCodeTable("bm") if item['suff'] == "B4"), None)
                else:
                    st.write("can't get BM type from project. Try counting FEs...")
                    # try to compare to reference table (defined above)
                    try:
                        # counting FE chips (rough!)
                        bmType = next((item for item in GetCodeTable("bm") if item['suff'] == "B"+str(len([p for p in rl['parts'] if p['compType']=="FE_CHIP"])) ), None)
                    # if not key then set None
                    except KeyError:
                        bmType=None

                ### set-up comp schema (do this per collection object to avoid memory referencing issues)
                # compSchema=DBaccess.DbGet(st.session_state.myClient,'generateComponentTypeDtoSample', {'code':"BARE_MODULE",'project':"P",'requiredOnly':True})
                compSchema=copy.deepcopy(compSchema_orig)
                # add info.
                compSchema['subproject']="PG"
                compSchema['institution']=st.session_state.Authenticate['inst']['code']
                compSchema['type']=bmType['code']
                # loop over parts
                matchDict={'SENSOR_TYPE':sensorType, 'FECHIP_VERSION':feType, 'VENDOR':vendorCode}
                for k in matchDict.keys():
                    try:
                        compSchema['properties'][k]=matchDict[k]
                    except TypeError:
                        compSchema['properties'][k]=None

                # ignore thickness
                # compSchema_update['properties']['THICKNESS']=thickness

                # calculate BM SN
                rl['comb']['compType']="BARE_MODULE"
                rl['comb']['SN']=MakeBM_SN(compSchema['properties'],rl['manSN'],bmType['suff'])
                compSchema['serialNumber']=rl['comb']['SN']

                # keep the registration data-structure
                rl['compSchema']=compSchema

                ### debug output
                stTrx.DebugOutput("Component schema: ",rl['compSchema'])

                rl['stopReg']=CheckBM_SN(rl['comb']['SN'])
            
            ### flag set-up complete
            pageDict['set-up']=True
        else:
            st.write("Done. Re-run if required")
        

        st.write("### Settings Summary")
        ### list of property keys
        matchList=['SENSOR_TYPE', 'FECHIP_VERSION', 'VENDOR']
        ### loop over extracted collections
        # for e in range(0,len(pageDict['regList']),1): 
        for e,rl in enumerate(pageDict['regList']):
            ### get existing schema
            # compSchema=rl['compSchema']
            ### check box to edit
            # st.write(e,": ",rl)
            infra.ToggleButton(pageDict,'togSet_'+str(e),f"{e}. edit: {rl['comb']['SN']}")
            stTrx.DebugOutput("Component schema: ",rl['compSchema'])
            # st.write(rl['compSchema'])
            ### if edit selected...
            if pageDict['togSet_'+str(e)]:
                ### existing schema
                ### loop over property keys
                for k in matchList:
                    infra.ToggleButton(pageDict,'togSet_'+str(e)+'_'+k,f" - set {rl['manSN']} {k}") # (current: {compSchema_update['properties'][k]})")
                    if pageDict['togSet_'+str(e)+'_'+k]:
                        ### get list
                        tabList=GetCodeTable(k)
                        ### match existing value to index
                        try:
                            ### try to find match for existing
                            defIdx=[item['code'] for item in tabList].index(rl['compSchema']['properties'][k])
                            ### get selected
                            selVal=st.selectbox(f'Select {e}-{k}:',[item['value'] for item in tabList],index=defIdx)
                        except ValueError:
                            ### if issue with existing
                            ### get selected
                            selVal=st.selectbox(f'Select {e}-{k}:',[item['value'] for item in tabList])
                        ### match value-->code in list
                        matchVal=next((item['code'] for item in tabList if item['value'] == selVal), None)
                        ### update if change
                        if rl['compSchema']['properties'][k]!=matchVal:
                            # st.write(f"I'm updating ({e}) {rl['compSchema']['properties'][k]} with {matchVal}")
                            rl['compSchema']['properties'][k]=matchVal
                            st.write(f"➡ updated to {rl['compSchema']['properties'][k]}")
                            ###
                            # rl['compSchema']=compSchema
                ### update type
                infra.ToggleButton(pageDict,'togSet_'+str(e)+'_BM',f" - set {rl['manSN']} BM type") # (current: {compSchema_update['type']})")
                if pageDict['togSet_'+str(e)+'_BM']:
                    ### get list
                    tabList=GetCodeTable("bm")
                    ### match existing value to index
                    defIdx=[item['code'] for item in tabList].index(rl['compSchema']['type'])
                    ### get selected
                    selVal=st.selectbox(f'Select type:',[item['value'] for item in tabList],index=defIdx)
                    ### match value-->code in list
                    matchVal=next((item['code'] for item in tabList if item['value'] == selVal), None)
                    ### update if change
                    if rl['compSchema']['type']!=matchVal:
                        rl['compSchema']['type']=matchVal
                        st.write(f"➡ updated to {rl['compSchema']['type']}")
                        rl['stopReg']=CheckBM_SN(rl['comb']['SN'])
                        ###
                        # rl['compSchema']=compSchema
                ### re-caclulate BM serialNumber...
                ### get suffix from table
                bmSuff=next((item for item in GetCodeTable("bm") if item['code'] == rl['compSchema']['type']), None)
                ### calculate SN
                rl['comb']['SN']=MakeBM_SN(rl['compSchema']['properties'],rl['manSN'],bmSuff['suff'])
                ### offer manual update
                rl['comb']['SN']=st.text_input("Set serialNumber manually if required:",value=rl['comb']['SN'],key=500+e)
                if rl['compSchema']['serialNumber']!=rl['comb']['SN']:
                    rl['compSchema']['serialNumber']=rl['comb']['SN']
            ### if edit NOT selected...
            else:
                sumList=[]
                for k in matchList:
                    matchVal=next((item['value'] for item in GetCodeTable(k) if item['code'] == rl['compSchema']['properties'][k]), None)
                    # st.write(f" - {k}: {matchVal}")
                    sumList.append({'key':k, 'value':matchVal})
                matchVal=next((item['value'] for item in GetCodeTable("bm") if item['code'] == rl['compSchema']['type']), None)
                # st.write(f" - type: {matchVal}")
                sumList.append({'key':"type", 'value':matchVal})
                st.dataframe(sumList)
                for s in ['stopReg','stopAss']:
                    # sumList.append({'key':s, 'value':rl[s]})
                    if rl[s]:
                        st.write(f"❌ {s}: {rl[s]}")
                    else:
                        st.write(f"✔ {s}: {rl[s]}")


            ### show settings
            

        ###########################
        ### REGISTER BARE_MODULES
        ###########################
        st.write("---")
        count=0
        if st.button("Register"):
            for rl in pageDict['regList']:
                ### skips...
                if "SN" not in rl['comb'].keys():
                    st.write("No serialNumber found in _comb_ object: skip")
                    continue
                if rl['parts']==None:
                    st.write("No _parts_ found: skip")
                    continue
                if "stopReg" in rl.keys() and rl['stopReg']==True:
                    st.write(f"### Problem with {rl['comb']['SN']} check previous set-up output")
                # check parts
                # if len(rl['parts'])!=5:
                #     st.write("Not enough parts("+str(len(rl['parts']))+"): skip")
                #     continue

                ### Register
                st.write("Try register "+rl['comb']['compType']+":",rl['comb']['SN'])
                ## register BARE_MODULE: 'FECHIP_VERSION', 'SENSOR_TYPE', 'THICKNESS', 'VENDOR'
                try:
                    regVal=DBaccess.DbPost(st.session_state.myClient,'registerComponent', rl['compSchema'] )
                    st.write("### **Successful Registration**:",regVal['component']['serialNumber'])
                    #rl['comb']['SN']=regVal['component']['serialNumber']
                    rl['registered']=True
                    count+=1
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: registration **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                except TypeError:
                    st.write("Registration unsuccessful")

        myRegs=[x['comb']['SN'] for x in pageDict['regList'] if x['registered']==True]
        st.write("### Registered components:",len(myRegs))
        for k,rl in enumerate(pageDict['regList']):
            if rl['registered']==False: continue
            infra.ToggleButton(pageDict,'togReg_'+str(k),"R"+str(k)+": "+rl['comb']['SN'])
            #st.write("Component:",rl['comb']['SN'])
            if pageDict['togReg_'+str(k)]:
                if st.button("delete",key=k):
                    try:
                        delVal=DBaccess.DbPost(st.session_state.myClient,'deleteComponent', {'component':rl['comb']['SN']} )
                        st.write("**Successful Deletion**:",rl['comb']['SN'])
                        rl['registered']=False
                    except itkX.BadRequest as b:
                        st.write(":no_entry_sign: deletion **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks


        ###########################
        ### ASSEMBLE PARTS
        ###########################
        st.write("---")
        st.write("## BARE_MODULE Assembly")

        ### assemble button
        if st.button("Assemble"):
            ### loop over collections
            for rl in pageDict['regList']:
                ### map slots
                # get map
                vendObj = next((item for item in GetCodeTable("vendor") if item['code'] == rl['compSchema']['properties']['VENDOR']), None)
                st.write(f"Using vendor slot map for: {vendObj['value']}")
                # st.write(vendObj['map'])
                # loop over FEs
                for fe in [part for part in rl['parts'] if "fe" in part['compType'].lower()]:
                    # try to map slot (need to subtract one for position (PDB child order starts at 0))
                    # st.write(f"slot in: {fe['slot']}")
                    try:
                        fe['slot']=str( int( vendObj['map'][fe['slot']] ) - 1 )
                    except TypeError:
                        st.write(f"No mapping! Problem with type ({type(vendObj['map'][fe['slot']])})")
                    except IndexError:
                        st.write(f"No mapping! Bad index ({fe['slot']})")
                    except KeyError:
                        st.write(f"No mapping! Band key ({fe['slot']})")
                    st.write(f"use child slot: {fe['slot']}")
                ### assemble function
                rl['assembly']=chnx.AssembleChunk(rl,True)

        myAsss=[x['assembly'] for x in pageDict['regList'] if x['assembly']==True]
        st.write("### Assembled components:",len(myAsss))
        for k,rl in enumerate(pageDict['regList']):
            if rl['assembly']==False: continue
            if "stopAss" in rl.keys() and rl['stopAss']==True:
                st.write(f"### Problem with {rl['comb']['SN']} check previous set-up output")
            infra.ToggleButton(pageDict,'togAss_'+str(k),"A"+str(k)+": "+rl['comb']['SN'])
            #st.write("Component:",rl['comb']['SN'])
            if pageDict['togAss_'+str(k)]:
                bmComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {"component" : rl['comb']['SN']} )
                st.write("Children:",[x['component']['serialNumber'] for x in bmComp['children'] if x['component']!=None])
                stTrx.DebugOutput("Children details: ",bmComp['children'])

                positions=[]
                for x in bmComp['children']:
                    if x['component']==None: continue
                    positions.append({'x':int(x['order'])/2,'y':int(x['order'])%2,'text':x['component']['serialNumber']})
                stTrx.VisOrientation(pd.DataFrame(positions))

                if st.button("disassemble",key=k+1000):
                    chnx.DisassembleChunk(bmComp)

        # st.write("---")
        # st.write("## Trash reworks")
        #
        # myTrash=[y for x in pageDict['regList'] for y in x['reworks']]
        # st.write("### Trashable components:",len(myTrash))
        # st.write(myTrash)
        #
        # if 'trashCom' not in pageDict.keys():
        #     pageDict['trashCom']=""
        # infra.ToggleButton(pageDict,'togCom',"Add trash comment?")
        # #st.write("Component:",rl['comb']['SN'])
        # if pageDict['togCom']:
        #     infra.TextBox(pageDict, 'trashCom', "Trash comment:")
        #
        # if st.button("Trash components"):
        #     for rl in pageDict['regList']:
        #         for rw in rl['reworks']:
        #             st.write("part:",rw)
        #             try:
        #                 trashVal=DBaccess.DbPost(st.session_state.myClient,'setComponentTrashed', {"component" : rw, 'reason':pageDict['trashCom']} )
        #                 st.write("### **Successful trashing**:",trashVal['component']['id'])
        #             except itkX.BadRequest as b:
        #                 st.write("### :no_entry_sign: trashing **Unsuccessful**")
        #                 st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
