# base image
FROM image-registry.openshift-image-registry.svc:5000/itk-pdb-webapps/itkdb-multi-base

### set-up packages
USER root
RUN true \ 
    && apt-get update \
    && apt-get install ffmpeg libsm6 libxext6 git -y \
    && rm -rf ./userPages/theme*

### common code 
COPY commonCode/* ./commonCode/
### get latest stage-test info.
RUN true \
    && /bin/bash commonCode/get_stages_and_tests.sh

### copy in apps
COPY pixelsApps/vendorApp/userPages/* ./userPages/vendorApp/
COPY generalApps/commonApp/userPages/page1_inventory.py ./userPages/vendorApp/page1_inventory.py
COPY generalApps/commonApp/userPages/page2_shipping.py ./userPages/vendorApp/page2_shipping.py
# COPY pixelsApps/ringLoadingApp/userPages/* ./userPages/ringLoadingApp/

### set main info.
COPY mainFiles/mainAppVendor.py ./mainApp.py
# add build date and anouncements
COPY .git/refs/heads/master gitRef.txt
RUN true \   
    # && git ls-remote https://gitlab.cern.ch/wraight/itk-web-apps --short HEAD | awk '{ print $1}' > gitRef.txt \ 
    && sed -i 's/COMMITCODE/'$(cat gitRef.txt | head -c7)'/g' mainApp.py \
    && sed -i 's/BUILDDATE/'$(date +%d-%m-%y)'/g' mainApp.py \
    && sed -i 's/announcement_text=None/announcement_text="Recent changes: Now reading login info. from cookies. _Please contact if problems_."/g' corePages/pageA.py
COPY requirements_apps.txt ./requirements.txt
RUN pip3 install -r requirements.txt

# # temp files image
RUN true \ 
    && chown -R appuser:appuser /tmp \
    && chmod 755 /tmp

# # fudge for CERN image
# RUN mkdir /.streamlit
# COPY temp_id  /.streamlit/.stable_random_id
# RUN chmod 777 /.streamlit/.stable_random_id

# # run as not root
# RUN adduser appuser
USER appuser

# match exposed port
CMD ["streamlit", "run", "mainApp.py","--server.port=8501"]
