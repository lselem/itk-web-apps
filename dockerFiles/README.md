# Docker Files

Dockerfiles used to collect apps and present in single streamlit application.

---

## Dockerfiles

See individual files for details.

### Project Specific apps

- Common Electonics
- Strips
- Pixels

### Non-project specific apps

- General: non-project specific apps
- Vendor: App for Pixels Bare Module registration and assembly by vendors
- Test: test app

