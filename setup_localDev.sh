#! /bin/bash

doDevelopment="y"

docker build . -f dockerFiles/DockerfileType1Power -t type1-power-app

if [[ $doDevelopment == "y" ]]
then

  if [[ $# -ne 2 ]]
  then
    docker run -p 8501:8501 -v $(pwd)/Type1PowerApps/QCApp/userPages/:/code/userPages/QCApp type1-power-app
  else
    docker run -p 8501:8501 -v $(pwd)/Type1PowerApps/QCApp/userPages/:/code/userPages/QCApp type1-power-app streamlit run mainApp.py "ac1 ${1}" "ac2 ${2}"
  fi
else
  if [[ $# -ne 2 ]]
  then
    docker run -p 8501:8501 type1-power-app
  else
    docker run -p 8501:8501 type1-power-app streamlit run mainApp.py "ac1 ${1}" "ac2 ${2}"
  fi
fi
