# Reporting Apps

Applications for on-demand generic PDB reporting, _i.e._  non-project specific.

Currently hosted on [Cern OKD](https://itk-pdb-webapps-reporting.web.cern.ch).

---

## Examples

Non-comprehensive list of included apps.

### Population App

- PDB Stats page: overall PDB numbers, e.g. 
    - almost 10M testRuns uploaded
- PDB Objects page: compare number of stages and tests per componentType, e.g. 
    - Pixels FE chip has 35 stages and 126 testTypes
    - Strips AMAC has 10 stags and 260 tests
- Plot Param: a generic tool to plot component properties and testType properties & results (non-dictionary data). This is not full _Data Integrity_ or _Production Quality_ reporting, but perhaps a useful histogram visualisation, with an option to the dataset as csv. 
    - **NB** can be broken by old/strange PDB data.
