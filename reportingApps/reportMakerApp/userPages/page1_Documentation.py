import streamlit as st
import streamlit.components.v1 as components
from PIL import Image
import os

import core.DBaccess as DBaccess
import core.stInfrastructure as infra
from core.Page import Page

import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

class Toc:

    def __init__(self):
        self._items = []
        self._placeholder = None
    
    def title(self, text):
        self._markdown(text, "h1")

    def header(self, text):
        self._markdown(text, "h2", " " * 2)

    def subheader(self, text):
        self._markdown(text, "h3", " " * 4)

    def placeholder(self, sidebar=False):
        self._placeholder = st.sidebar.empty() if sidebar else st.empty()

    def generate(self):
        if self._placeholder:
            self._placeholder.markdown("\n".join(self._items), unsafe_allow_html=True)
    
    def _markdown(self, text, level, space=""):
        import re
        key = re.sub('[^0-9a-zA-Z]+', '-', text).lower()
        # key = "".join(filter(str.isalnum, text)).lower()

        st.markdown(f"<{level} id='{key}'>{text}</{level}>", unsafe_allow_html=True)
        self._items.append(f"{space}* <a href='#{key}'>{text}</a>")

infoList=[" * check test parameters for componentType at institute(s)",
        " * select componentType at institute",
        " * select stage and testType (from test schema)",
        " * select parameter (currently no arrays supported)",
        " * visualise and download csv"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Documentation", ":microscope:  How To... use this app", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        userPagesDir=os.path.realpath(__file__)
        userPagesDir=userPagesDir[:userPagesDir.rfind('/')]+"/"

        toc = Toc()

        st.title("Table of contents")
        toc.placeholder()

        toc.header("Set up report")
        st.write("""
        * The user can check the checkbox to see all the report scripts available
        * Select report type gives the user the option to select a report script depending on what type of report the user need 
        """)
        image = Image.open(userPagesDir+"Images/scr1.png")
        st.image(image)
        st.write("""
        * Currently we have reports for testType_dashboard, componentType_dashboard and institute_Inventory
        * By checking the Upload your spec file checkbox the user will be prompted to upload a spec file instead of creating a new one
        * If the user wants to create their own spec file from scratch just leave the above checkbox unchecked and write the name of the json file in the empty field
        * The user can check templates below the Create json file box 
        * The corresponding template will be automatically selected when choosing a report
        """)
        image = Image.open(userPagesDir+"Images/scr2.png")
        st.image(image)

        st.video(userPagesDir+"Videos/streamlit1.webm", format="webm", start_time=0)

        toc.header("User credentials")
        st.write("""
        * The user can check the Enter credentials via file checkbox to upoad a json file with their credentials
        * If the user checked the box there will be an example how the file should look like
        """)
        image = Image.open(userPagesDir+"Images/scr5.png")
        st.image(image)
        st.write("""
        * Or the user can just fill the two fields in manually
        * After that just click on the Set credentials
        """)
        image = Image.open(userPagesDir+"Images/scr4.png")
        st.image(image)

        st.video(userPagesDir+"Videos/streamlit2.webm", format="webm", start_time=0)

        toc.header("User input")
        st.write("The spec files is made up of four parts:")
        st.write("""
        * population
        * extraction
        * visualisation
        * distribution
        """)
        st.write("Depending on the report script the user is using the spec file will have different fields")
        st.write("Use reset json button at the beginning, to have an empty json file")

        toc.header("General structure of population")
        st.write("""
        * alias - ID for that population
        * spec - specifications of the population
            * projCode - code of the project, e.g. S - strips
            * compTypeCode - code of the component type, e.g. MODULE
            * instCode  - code of the institute, e.g. CUNI - Charles University
            * filters - for filtering through child, parent, grandChild, grandParent and keyValue
        * The user can select the project code from the selectbox 
        """)
        image = Image.open(userPagesDir+"Images/scr6.png")
        st.image(image)
        image = Image.open(userPagesDir+"Images/scr7.png")
        st.image(image)
        st.write("""
        * the user can just ignore filters or enable them by checking the checkbox
        """)
        image = Image.open(userPagesDir+"Images/scr8.png")
        st.image(image)
        st.write("""
        * When the user fills out all the fields click on the Add population to add everything to their json file
        * The user can add additional filters using Add aditional filters (just change the fields for filters and click the Add aditional filters button)
        """)

        st.video(userPagesDir+"Videos/streamlit3.webm", format="webm", start_time=0)

        toc.header("General structure of extraction")
        st.write("""
        * population used - the user can select what population they want to use for the extraction, by selecting the corresponding population alias
        * alias - ID for that exraction
        * component summary, stage order, stage summary, test summary are options that can be added to the spec file
        * The user can add them by checking the checkbox, by default they will be false, the user can turn them on by checking the checkbox below them
        * spec - by default it is just a string, it can be changed by checking Add multiple specs, in that case it becomes a list of dictionaries
        * For specs the user can select testCode, paraCode and subs, however many the user want
        * When the user fills out all the fields click on Add extraction to add everything to their json file
        * The user can add additional specs using Add aditional specs (just change the fields for specs and click the Add aditional specs button)
        """)
        image = Image.open(userPagesDir+"Images/scr9.png")
        st.image(image)

        st.video(userPagesDir+"Videos/streamlit4.webm", format="webm", start_time=0)

        toc.header("General structure of visualisation")
        st.write("""
        * extraction used - select extraction to be used for visualisation, can be multiple
        * alias - ID for that visualisation
        * component summary, stage order, stage summary, test summary, data quality are options that can be added to the spec file
        * The user can add them by checking the checkbox, by default they will be false, the user can turn them on by checking the checkbox below them
        * match parameter code - used in parameter comparison and parameter consistency **(work in progress)**
        * combinations - used in parameter consistency **(work in progress)**
        * correlations - used in parameter comparison **(work in progress)**
        * When the user fills out all the fields click on Add visualisation to add everything to their json file
        * The user can add additional combinations/correlations using Add aditional combinations/correlations buttons respectively (just change the fields for combinations/correlations and click the button)
        """)
        image = Image.open(userPagesDir+"Images/scr10.png")
        st.image(image)

        st.video(userPagesDir+"Videos/streamlit5.webm", format="webm", start_time=0)

        toc.header("General structure of distribution")
        st.write("""
        * alias - ID for that distribution
        * report name - the name of the report that will be shown in Datapane
        * location - where the report will be saved
            * local - the report will be saved locally on the server and the user can download it below
            * remote - save the report directly to Datapane
        * In case of chosing the remote option a field that asks for their datapane key will appear
        * visualisation used - select visualisation to be used for distribution, can be multiple
        * After the user fills out all the fields click on the Add distribution button to add everything to their json file
        """)
        image = Image.open(userPagesDir+"Images/scr11.png")
        st.image(image)

        st.video(userPagesDir+"Videos/streamlit6.webm", format="webm", start_time=0)

        toc.header("Run script")
        st.write("In the field write report name. After filling it out click on the Submit button.")
        image = Image.open(userPagesDir+"Images/scr12.png")
        st.image(image)

        toc.header("Report outputs")
        st.write("You can see the submitted outputs.")

        toc.header("Report htmls")
        st.write("You can select a report that was saved locally and download it as a html file by clicking the Download html button.")
        image = Image.open(userPagesDir+"Images/scr13.png")
        st.image(image)

        st.video(userPagesDir+"Videos/streamlit7.webm", format="webm", start_time=0)

        toc.generate()