from pyexpat.errors import codes
from re import T
import streamlit as st
import streamlit.components.v1 as components

import core.DBaccess as DBaccess
import core.stInfrastructure as infra
from core.Page import Page

import commonCode.StreamlitTricks as stTrx

import pandas as pd
import numpy as np

import json
import subprocess

import os
import itkdb
import itkdb.exceptions as itkX

import ast

#####################
### useful functions
#####################

descDict={
    'inventory': "Produce an inventory of components at a specified institution",
    'testType': "Gather information on a specific testType for specified component population",
    'componentType' : "Gather information on stages and tests for a specific componentType for specified component population",
    'comparison' : "Compare defined test parameters for correlation for specified component population",
    'consistency' : "Compare testRuns of testType over production for specified component population"
}

infoList=[" * check test parameters for componentType at institute(s)",
        " * select componentType at institute",
        " * select stage and testType (from test schema)",
        " * select parameter (currently no arrays supported)",
        " * visualise and download csv"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Submit Report", ":microscope: Submit itk-reports report", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        # st.write("Current dir:", os.getcwd())
        # st.write("comb",os.path.dirname(os.getcwd()))

        ### a list of useful path definitions
        filePath=os.path.realpath(__file__)
        outPath=filePath[:filePath.rfind('/')]+"/outputs/"
        htmlPath=filePath[:filePath.rfind('/')]+"/htmls/"
        scriptPath="/code/itk-reports/reporting_scripts/"
        # specPath="/code/itk-reports/reporting_specs/"
        specPath=filePath[:filePath.rfind('/')]+"/rep_specs/"
        localSpecPath=filePath[:filePath.rfind('/')]+"/"+"rep_specs/"

        ##########################
        ### selecting reporting script
        ##########################
        st.write('## Set up report')
        st.write("Scripts are baseed on [itk-reports](https://itk-reports.docs.cern.ch)")
        ### selection
        st.write('### Select report script')
        # collect scripts
        pyFiles=[f for f in os.listdir(scriptPath) if '.py' in f and not 'common' in f and not '__' in f and not 'testFile' in f and not 'Streamlit' in f] 
        repCheck=st.checkbox('See Report Scripts?')
        if repCheck:
            st.write(pyFiles)
        # select script
        pySel=st.selectbox('Select report type:',pyFiles,format_func=lambda x: x.replace('.py','')) 
        # show short description
        descStr=next((v for k,v in descDict.items() if k in pySel), None)
        if descStr!=None:
            st.write(f"__Report Description__: {descStr}")

        ##########################
        ### selecting specification file
        ##########################
        st.write('### Select specication file')

        # upload file?
        specOption=st.radio("Select input:",["Upload file","Select from examples"])

        # choose template
        if "example" in specOption.lower():
            st.write('__Select example specification file__')
            specFiles=[f for f in os.listdir(specPath) if ".json" in f]
            # try matching script
            try:
                specExample=st.selectbox("Select example:",specFiles, index=specFiles.index(pySel.replace(".py","_example.json")))  
            # if no match just select
            except ValueError:
                specExample=st.selectbox("Select example:",specFiles)  
            # open template
            f = open(specPath+specExample)
            specExampleDict = json.load(f)
            f.close()
                
            # keep dictionary
            inDict=specExampleDict
            inName=specExample

        # upload file
        if "upload" in specOption.lower():
            st.write('__Upload specification file__')
            specUpl = st.file_uploader('Upload spec file', type='json')
            if specUpl==None:
                st.write('Please upload spec file')
                st.stop()
            else:
                data = specUpl.getvalue().decode('utf-8')  
                inDict=json.loads(data)
                inName="userSpec.json"

        ### replace all distribution reportDirs with local htmls 
        for d in range(0,len(inDict['distribution']),1):
            inDict['distribution'][d]['reportDir']=htmlPath

        # download option to edit
        st.write("__Specifications__")
        st.json(inDict)
        st.write("This specification can be downloaded to edit...")
        st.download_button(label='Download json?', data=json.dumps(inDict, indent=2), file_name=inName)


        specSel=localSpecPath+inName
        if 'json' in inName:
            ### write tmp file
            with open(specSel, 'w') as f:
                json.dump(inDict, f)
            ### read file (check)
            if st.session_state.debug:
                st.write('__Input json__')
                with open(specSel,'r') as f: 
                # with open(os.path.dirname(os.getcwd())+'/reporting/specs/'+specSel,'r') as f: 
                    inFile = json.load(f)
                    st.write(inFile)
        else:
            st.write('not recognised. Please input _json_')

        st.write('---')
        ##########################
        ### running script
        ##########################
        st.write('## Run script')

        st.write('Selected report to submit:')

        repName=st.text_input('Set report name (no spaces please)',value='repName')
        repName=repName.strip().replace(' ','_') # replace any spaces

        # current directory check
        # st.write("I am: ",os.getcwd())

        ### set whole array (avoid messing with indiviual arguments)
        cmdArr=['python3',scriptPath+pySel]
        # cmdArr.extend(['--file',os.path.dirname(os.getcwd())+'/reporting/specs/'+specSel])
        cmdArr.extend(['--file', specSel])

        st.write(' '.join(cmdArr))

        ### submit report
        if st.button('Submit '+repName):
            # set environment variables from session state
            os.environ['AC1'] = st.session_state.Authenticate['ac1']
            os.environ['AC2'] = st.session_state.Authenticate['ac2']
            st.write("credentials set ✔")
            # run subprocess
            st.write('Running... (may take a couple of minutes)')
            session = subprocess.Popen(cmdArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = session.communicate()
            # catch outputs
            if stdout:
                with open(outPath+repName+'.out', 'wb') as f: 
                    f.write(stdout)
                st.success('Output written!')
            if stderr:
                st.error('Error '+stderr.decode('utf-8'))

        ### option to view outputs
        outCheck=st.checkbox('Check outputs?')
        if outCheck:
            st.write('### Report outputs')
            outFiles=[f for f in os.listdir(outPath) if '.out' in f] 
            st.write(outFiles)

            outSel=st.selectbox('Select report output file:',outFiles)

            with open(outPath+outSel, 'r') as f:
                st.write(f.readlines())

        st.write('---')
        ##########################
        ### View reports
        ##########################
        st.write('## View Reports')

        outFiles=[f for f in os.listdir(outPath) if '.out' in f and '.out.' not in f] 
        for of in outFiles:
            with open(outPath+of,'r') as f: 
                st.write(of)
                inStr=[r for r in f.readlines() if 'https' in r]
                if len(inStr)<1:
                    f.seek(0)
                    inStr=[r for r in f.readlines() if 'saved' in r]
                if len(inStr)<1:
                    st.write('No local or remote marker found! check output')
                    st.write(f.readlines())
                else:
                    st.write(f' - {inStr[-1]}')

        # st.write(outFiles)

        ### htmls
        st.write('### Report _html_ s')
        htmlFiles=[f for f in os.listdir(htmlPath) if '.html' in f] 
        # st.write(htmlFiles)

        htmlSel=st.selectbox('Select report html file:',htmlFiles)

        ### embed datapane report
        with open(htmlPath+htmlSel, 'r') as f:
            components.html( f.read(), width=800, height=400 )
            # st.markdown(f.read(),unsafe_allow_html=True)

        ### download button
        with open(htmlPath+htmlSel, 'rb') as file:
            st.download_button(
                label='Download html',
                data=file,
                file_name=htmlSel,
                mime='html'
                )

        st.write('---')
