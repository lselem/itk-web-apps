import streamlit as st
from core.Page import Page
import pandas as pd
#import numpy as np
import os
import ast
import datetime

import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio

import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

infoList=["  * Several different summary tables",
        "   * can also choose state of component assembly"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Comp Finder", "↔ Match WBS to components", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")
            if st.session_state.debug:
                st.write("Attribute Error")
                st.write(AttributeError)

        ### gatekeeping
        if not doWork:
            st.stop() 

        ### pick up csv files
        st.write("Loading csvs")
        filePath="/".join(os.path.realpath(__file__).split('/')[0:-1])
        df_wbs= pd.read_csv(filePath+"/wbs_list.csv", sep=';')
        st.write(f"- WBS list: {len(df_wbs.index)}")
        df_abs= pd.read_csv(filePath+"/abs_list.csv", sep=';')
        st.write(f"- ABS list: {len(df_abs.index)}")
        df_pdb= pd.read_csv(filePath+"/pdb_list.csv", sep=';')
        st.write(f"- PDB list: {len(df_pdb.index)}")
        ### format
        # replace all nan with empty string
        df_wbs=df_wbs.fillna('')
        df_abs=df_abs.fillna('')
        df_pdb=df_pdb.fillna('')
        # strip all cells
        df_wbs=df_wbs.applymap(lambda x: x.strip() if type(x)==str else x)
        df_abs=df_abs.applymap(lambda x: x.strip() if type(x)==str else x)
        df_pdb=df_pdb.applymap(lambda x: x.strip() if type(x)==str else x)


        #####################
        ### select project
        #####################
        st.write("---")
        st.write("## Project View")
        
        selMode=st.radio("Select description", ["WBS","ABS","PDB"])

        ### WBS info.
        if "wbs" in selMode.lower():
            st.write("### Work Build Structure information")
            st.write(df_wbs)

            # select object to compare
            selThing=st.selectbox("Select WBS code:",[": ".join(l) for l in df_wbs[['code','name']].values.tolist()])
            selCode=selThing.split(':')[0].strip()
            wbs=df_wbs.query(f'code=="{selCode}"').to_dict("records")[0]
            # st.write(wbs)

            # print(f"  - WBS: {wbs.to_dict('records')}")
            wbs_abs=ast.literal_eval(wbs['abs'])
            st.write(f" - search ABS for: {wbs_abs}")
            abs=df_abs.query(f'name==@wbs_abs')
            # print(f"  - ABS: {abs.to_dict('records')}")
            abs_pdb=list( dict.fromkeys([a['pdb'] for a in abs.to_dict('records')] ) )
            abs_proj=list( dict.fromkeys([a['project'] for a in abs.to_dict('records')] ) )
            abs_name=list( dict.fromkeys([a['name'] for a in abs.to_dict('records')] ) )
            st.write(f" - search PDB for: {abs_pdb} ({abs_proj})")
            pdb=df_pdb.query(f'componentType==@abs_pdb & project==@abs_proj')
            # print(f"  - PDB: {pdb.to_dict('records')}")

            ### output
            st.write(f"\nComponents found for WBS input: {wbs['name']}")
            st.write(pdb)

            # st.write(df_abs.query(f'name==@abs_name')['group'].to_list())


        ### not really ABS info.
        if "abs" in selMode.lower():
            st.write("### _not really_ Assembly Build Structure")
            st.write(df_abs)

            # select object to compare
            selThing=st.selectbox("Select ABS code:",df_abs['group'].unique())
            abs=df_abs.query(f'group=="{selThing}"').to_dict("records")[0]

            abs_proj=abs['project']
            abs_name=abs['name']
            pdb_list=abs['pdb']
            # print(f"  - ABS: {abs.to_dict('records')}")
            st.write(f" - search WBS for: {abs_name} ({abs_proj})")
            wbs=df_wbs.query(f'abs.str.contains("{abs_name}")', engine='python')
            if not wbs.empty:
                wbs_name=wbs.to_dict('records')[0]['name']
                wbs_code=wbs.to_dict('records')[0]['code']
                st.write(f"  - WBS: {wbs.to_dict('records')}")
                st.write(f" - search PDB for: {pdb_list}")
                pdb=df_pdb.query(f'project=="{abs_proj}" & componentType==@pdb_list')
                # print(f"  - PDB: {pdb.to_dict('records')}")

                ### output
                st.write(f"\nComponents found for ABS input: {abs_name}")
                st.write(pdb)
                st.write(f"WBS: __{wbs_code} {wbs_name}__")
            else:
                st.write(" - no WBS found")


        ### PDB info.
        if "pdb" in selMode.lower():
            st.write("### Production Database information")
            st.write(df_pdb)

            # select object to compare
            selThing=st.selectbox("Select PDB code:",[f"{ct} ({p})" for ct,p in zip(df_pdb['componentType'].to_list(),df_pdb['project'].to_list())])
            queryStr='componentType=="'+selThing.split("(")[0].strip()+'" & project=="'+selThing.split("(")[1].strip(')')+'"'
            # st.write(queryStr)
            pdb=df_pdb.query(queryStr).to_dict("records")[0]

            pdb_proj=pdb['project']
            pdb_code=pdb['componentType']
            # print(f"  - ABS: {abs.to_dict('records')}")
            st.write(f" - search ABS for: {pdb_code} ({pdb_proj})")
            abs=df_abs.query(f'project=="{pdb_proj}" & pdb=="{pdb_code}"')
            abs_name=None
            if not abs.empty:
                abs_name=abs.to_dict('records')[0]['name']
                abs_proj=abs.to_dict('records')[0]['project']
                st.write(f"  - ABS: {abs_name} ({abs_proj})")
                st.write(f" - search WBS for: {abs_name} ({abs_proj})")
                wbs=df_wbs.query(f'abs.str.contains("{abs_name}")', engine='python')
                if not wbs.empty:
                    wbs_name=wbs.to_dict('records')[0]['name']
                    wbs_code=wbs.to_dict('records')[0]['code']
                    st.write(f"  - WBS: {wbs_code} ({wbs_name})")
                    # print(f"  - PDB: {pdb.to_dict('records')}")

                    ### output
                    st.write(f"For {pdb['componentType']} ({pdb['project']}):")
                    st.write(f"- ABS: __{abs_name}__")
                    st.write(f"- WBS: __{wbs_code} {wbs_name}__")

                else:
                    st.write(" - no WBS found")
            else:
                st.write(" - no ABS found")


        #####################
        ### visualise
        #####################
        st.write("---")
        st.write("## Visualisation")

        if abs_name==None:
            st.write("No associated assemblies")
            st.stop()
        st.write(f"Information on __{abs_name}__")
        if type(abs_name)==type([]):
            df_list=df_abs.query(f'name==@abs_name')
        else:
            df_list=df_abs.query(f'name=="{abs_name}"')
        # st.write(df_list)
        # st.write(f"ABS list: {len(df_list.index)}")
        if df_list.empty:
            st.write("Nothing to visualise")
            st.stop()
        
        # looop over abs groups (grouped by file)
        for grp in df_list['group'].unique():
            st.write(f"__Structure for: {grp}__")
            df_vis=df_abs.query(f'group=="{grp}"').reset_index(drop=True)
            st.write(f" - entries: {len(df_vis.index)}")
            # get children
            df_vis['children'] = df_vis['children'].str.strip('[]').str.split(',')

            st.write("### List of related objects")
            st.write(df_vis)

            st.write("### Sunburst of related objects")
            ### sunburst
            # get parent component
            def GetMap(name):
                df_map=df_vis.explode("children").query(f'children.str.contains("{name}")', engine='python')
                if not df_map.empty:
                    return df_map.iloc[0]['name']
                else:
                    return "top"

            df_vis['map']=df_vis['name'].apply(lambda x: GetMap(x))
            # display(df_vis.head())

            # plotting
            df_vis['val']=1
            sunburst_fig = px.sunburst(
                df_vis,
                names='name',
                parents='map',
                values='val',
                color='map',
                title=f"Sunburst: {grp}"
            )
            sunburst_fig.update_layout(
                autosize=False,
                width=800,
                height=800,
                margin=dict(
                    l=50,
                    r=50,
                    b=100,
                    t=100,
                    pad=4
                ),
                paper_bgcolor="LightSteelBlue"
            )
            st.write(sunburst_fig)


            st.write("### Sankey of related objects")

            # get parent component
            def GetMapIndex(name):
                df_map=df_vis.explode("children").query(f'children.str.contains("{name}")', engine='python')
                if not df_map.empty:
                    return df_map.iloc[0]['index']
                else:
                    return -1

            if "index" not in df_vis.columns:
                df_vis=df_vis.reset_index()
            df_vis['mapInd']=df_vis['name'].apply(lambda x: GetMapIndex(x))
            # display(df_vis.head())

            # plotting
            df_vis['val']=1
            sankey_fig = go.Figure(data=[go.Sankey(
            node = dict(
            pad = 15,
            thickness = 20,
            line = dict(color = "black", width = 5.0),
            label = df_vis['name'].to_list(),
            color = "blue"
            ),
            link = dict(
            line = dict(width = 1.0),
            color = "red",
            source = df_vis['mapInd'].to_list(), # indices correspond to labels, eg A1, A2, A1, B1, ...
            target = df_vis['index'].to_list(),
            value = df_vis['val'].to_list()
            ))])

            sankey_fig.update_layout(title_text=f"Sankey: {grp}", font_size=10)
            st.write(sankey_fig)
