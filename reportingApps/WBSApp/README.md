# Work Breakdown Structure (WBS) Apps

Tools to help connect ITk project structure (WBS) to information in ITk Production Database (PDB).

---

## Pages

This relies on infromation external to the PDB to map PDB objects to Project effort.

- attempt to begin compilation here: [googleDoc](https://docs.google.com/spreadsheets/d/10HNFWD7FMOy73upILieS48Ipla9Rj6W_cWBL0dWUq3s/edit?gid=0#gid=0)

   - At the moment incomplete, but active.

### Component Finder

Connect WBS to PDB, or vice verse:

- Select PDB componentType and find WBS point
- Select WBS point and find associated PDB componentTypes
