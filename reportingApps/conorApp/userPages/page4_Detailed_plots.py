import streamlit as st
from core.Page import Page
import pandas as pd
#import numpy as np
#import matplotlib
import pycountry
import datetime

import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio

import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

from .page1_Summary_tables import db_table_access

#####################
### useful functions
#####################
def DateFormat(dt): #from Kenny
    return str("{0:02}_{1:02}_{2:04}".format(dt.day,dt.month,dt.year)) 

def convert_df(df):
            # IMPORTANT: Cache the conversion to prevent computation on every rerun
            return df.to_csv().encode('utf-8') 

infoList=["  * Several different summary plots",
        "   * can also choose state of component assembly"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("Detailed Plots", "ITK component overview", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()
 
        st.write('The plots below give an overview of component production worldwide and at country level.')
        st.write('Each plot can be expanded to full screen using a button on the top right of the plot.')
        st.write('By clicking on an item in the legend that item can be toggled on/off.')
        st.write('For documentation and usage instructions please visit: [https://itk.docs.cern.ch/pixels/reporting/reporting_tutorial/](https://itk.docs.cern.ch/pixels/reporting/reporting_tutorial/)')
        st.write('Please note that tutorial and dummy components have now been removed from the tables and plots.')
        st.write("---") 

        nowTime = datetime.datetime.now()
        time_current = DateFormat(nowTime) 
        

        ##########################################################################
        # my code
        ##########################################################################
        project_user= st.session_state.Authenticate['proj']['code']   

        project_type = {'project': project_user} 
        if project_user=='P':
            comp_name= st.selectbox('Please select a component', ["Selection", "Module", "Module carrier", "Bare Module", "Module PCB", "OB Wirebonding Protection Roof", "Sensor Wafers", "Sensor Tiles", "FE chips"])

            if comp_name== "Selection":
               st.stop()
            elif comp_name=='Module':
                comp_type_kw = ['module'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'carrier', 'bare', 'esq'] #lower case 

                chip_type={
                            'RD53A' : '0',
                            'ITkpix_v1' : '1',
                            'ITkpix_v1.1' : '2',
                            'ITkpix_v2' : '3',
                            'No chip' : '9'
                            }

            elif comp_name=='Module carrier':
                comp_type_kw = ['carrier','hybrid'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'board'] #lower case 
            elif comp_name=='Bare Module':
                comp_type_kw = ['bare'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'ob', 'oec', 'is', 'esq'] #lower case 

                chip_type={
                            'RD53A' : '0',
                            'ITkpix_v1' : '1',
                            'ITkpix_v1.1' : '2',
                            'ITkpix_v2' : '3',
                            'No chip' : '9'
                            }

            elif comp_name=='Module PCB':
                comp_type_kw = ['pcb'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case 

                chip_type={
                            'RD53A' : '0',
                            'Prototype (ITkpix_v1)' : '1',
                            'Pre-production OS (ITkpix_v1)' : '2',
                            'Pre-production IS (ITkpix_v1)' : '3',
                            'Production OS (ITkpix_v2)' : '4',
                            'Production IS (ITkpix_v2)' : '5',
                            'No chip' : '9'
                            }
            elif comp_name=='OB Wirebonding Protection Roof':
                comp_type_kw = ['wb'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case 
            elif comp_name=='Sensor Wafers':
                comp_type_kw = ['sensor'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'tile', 'esq'] #lower case 

                production_protype={
                              'Prototype (Market survey)': '0',
                              'Pre-production' : '1',
                              'Production' : '2'
                            } 
                vendor={
                            'V1 (ADVACAM)' : '0',
                            'V2 (HLL)' : '1',
                            'V3 (FBK, planar)' : '2',
                            'V4 (HPK)' : '3',
                            'V5 (LFoundry)' : '4',
                            'V6 (MICRON)' : '5', 
                            'V7 (CNM)' : '6',
                            'V8 (FBK, 3D)' : '7',
                            'V9 (SINTEF)' : '8',
                            'Dummy' : '9'
                            }
            elif comp_name=='Sensor Tiles':
                comp_type_kw = ['sensor'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case 

                production_protype={
                              'Prototype (Market survey)': '0',
                              'Pre-production' : '1',
                              'Production' : '2'
                            } 
                vendor={
                            'V1 (ADVACAM)' : '0',
                            'V2 (HLL)' : '1',
                            'V3 (FBK, planar)' : '2',
                            'V4 (HPK)' : '3',
                            'V5 (LFoundry)' : '4',
                            'V6 (MICRON)' : '5', 
                            'V7 (CNM)' : '6',
                            'V8 (FBK, 3D)' : '7',
                            'V9 (SINTEF)' : '8',
                            'Dummy' : '9'
                            }

                structure={
                            'RD53A test structure' : '0',
                            'Single' : '1',
                            'Double' : '2',
                            'Quad' : '3',
                            'Planar diode test structure' : '4',
                            'Strip test structure' : '5',
                            'Mini-sensor test structure' : '6',
                            'Inter pixel capacitance test structure' : '7',
                            'Biasing test structure (poli-Si resistivity or punch through)' : '8',
                            '3D diode test structure' : '9'
                            } 
            elif comp_name=='FE chips':
                comp_type_kw = ['fe'] #lower case #old has no pcb but front
                comp_veto_kw= ['test', 'june', 'cell', 'wafer', 'esq'] #lower case  
            else:
                st.stop


            project_name='pixels' 
        elif project_user=='S':  
            st.warning('This is a work in progress')
            st.stop() 
            comp_type_kw = st.multiselect('please select', ['module','hybrid_', 'sensor', 'pwb', 'hcc']) #, 'abc' #lower case ##the abc chips are about 90% of all components
            comp_veto_kw= ['sensor_', 'pwb_', 'abc_', 'hcc_', 'fail', 'test', 'flex_', 'jj', 'frame'] #lower c #the abc chips are about 95% of all components
            project_name='strips'   
            check= st.checkbox('I have selected my components')
            if not check:
                st.stop()
        else:
            st.write('Please choose either strips or pixels as your project')
            st.stop()
    

        with st.spinner('Please wait while component information is loaded from the ITK production database'):
            comp_df_func, stage_dict = db_table_access(project_type, comp_type_kw, comp_veto_kw, time_current) 
            if comp_df_func.empty:
                st.warning('No components to display')
                st.stop() 
        comp_df_initial=comp_df_func.copy() #stops streamlit mutation warning
        stage_dict_initial= stage_dict.copy()
        if project_user=='S':
            abc_comp= st.checkbox('Rerun to include ABC chips (Warning: this may take approximately 15 minutes to load)')
            if abc_comp:
                with st.spinner('Please wait while component information is loaded from the ITK production database'):
                    comp_df_func_abc, stage_dict_abc = db_table_access(project_type, ['abc'], comp_veto_kw, time_current)  
                    if comp_df_func_abc.empty:
                        st.warning('No components to display')
                        st.stop() 
                comp_df_abc=comp_df_func_abc.copy() #stops streamlit mutation warning
                comp_df_second= pd.concat([comp_df_initial, comp_df_abc])
                comp_df=comp_df_second
                stage_dict_initial.update(stage_dict_abc) 
                stage_dict= stage_dict_initial.copy()
            else:
                comp_df=comp_df_initial
                stage_dict= stage_dict_initial
        else:
            comp_df=comp_df_initial 
            stage_dict= stage_dict_initial  
        comp_df = comp_df[comp_df["state"] != "deleted"] 

        if project_user=='P':
            comp_df = comp_df.loc[~comp_df['serialNumber'].str[5].isin(['X', 'Y', 'Z'])] #remove dummy and tutorial components  
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
            comp_df = comp_df.loc[~comp_df['serialNumber'].str[5:6].isin(['BR', 'BT'])] #remove dummy and tutorial components
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
        comp_df = comp_df.loc[comp_df['type_name'].str.contains('|'.join(['tutorial', 'dummy']), case=False)==False] #remove dummy and tutorial components
        if comp_df.empty:
            st.warning('No components to display')
            st.stop() 
         
        

        if comp_name in ['Module', 'Bare Module', 'Module PCB']:
            chip_names= list(chip_type.keys())
            chip_name_selection = st.multiselect('Please select chip version', chip_names, chip_names)
            chip_sn_numbers = [chip_type[x] for x in chip_name_selection]

            comp_df = comp_df.loc[comp_df['serialNumber'].str[7].isin(chip_sn_numbers)] #select which elements match array
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
            
        elif comp_name in ['Sensor Wafers', 'Sensor Tiles']:
            prototype_names= list(production_protype.keys())
            prototype_name_selection = st.multiselect('Please select whether to display prototype or production components', prototype_names, prototype_names)
            prototype_ad_numbers = [production_protype[x] for x in prototype_name_selection]

            comp_df = comp_df.loc[comp_df['alternativeIdentifier'].str[3].isin(prototype_ad_numbers)] #select which elements match arrayif comp_df.empty:
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 

            vendor_names= list(vendor.keys())
            vendor_name_selection = st.multiselect('Please select vendor name', vendor_names, vendor_names)
            vendor_id_numbers = [vendor[x] for x in vendor_name_selection]

            comp_df = comp_df.loc[comp_df['serialNumber'].str[7].isin(vendor_id_numbers)] #select which elements match array
            if comp_df.empty:
                st.warning('No components to display')
                st.stop() 
            
            if comp_name == 'Sensor Tiles':
                structure_names= list(structure.keys())
                structure_name_selection = st.multiselect('Please select sensor type or test structure', structure_names, structure_names)
                structure_id_numbers = [structure[x] for x in structure_name_selection]

                comp_df = comp_df.loc[comp_df['serialNumber'].str[8].isin(structure_id_numbers)] #select which elements match array
                if comp_df.empty:
                    st.warning('No components to display')
                    st.stop() 

        with st.expander("Component selection options"):

            assemble= st.selectbox('Would you like to see all components, assembled components or components not yet assembled?',
                                    ['All Components', 'Assembled Only', 'Not Assembled'])
            
            if assemble== 'Assembled Only':
                comp_df_assemble= comp_df.loc[comp_df['assembled'] == True]  
            elif assemble== 'Not Assembled':
                comp_df_assemble= comp_df.loc[comp_df['assembled'] == False]  
            else:
                comp_df_assemble= comp_df

            trashed= st.selectbox('Would you like to see all components, only good components or only trashed components?',
                                    ['All Components', 'Good Components', 'Trashed Components Only'])
            
            if trashed== 'Trashed Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['trashed'] == True]  
            elif trashed== 'Good Components':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['trashed'] == False]  
            else:
                comp_df_assemble= comp_df_assemble

            rework= st.selectbox('Would you like to see all components, only good components or only reworked components?',
                                    ['All Components', 'Good Components Only', 'Reworked Components Only'])
            
            if rework== 'Reworked Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['reworked'] == True]  
            elif rework== 'Good Components Only':
                comp_df_assemble= comp_df_assemble.loc[comp_df_assemble['reworked'] == False]  
            else:
                comp_df_assemble= comp_df_assemble

        if comp_df_assemble.empty:
            st.warning('No components meet this criteria, please change your selection')
            st.stop() 

        comp_df_assemble["currentStage_name"].replace(to_replace=[None], value='Undefined', inplace=True) #replace empty stages
        comp_df_assemble["currentStage_name"].fillna("Undefined", inplace = True)  #replace empty stages  

        comp_df_pivot_inst=pd.pivot_table(comp_df_assemble, index=['type_name', 'currentStage_name'], 
                                        columns=['country', 'currentLocation_code'], values=['code'], 
                                        aggfunc=['count'], margins=True, margins_name = "Total", fill_value=0)#.fillna(0)
        
    
        result = pd.concat([comp_df_pivot_inst, comp_df_pivot_inst.groupby(axis=0, level=[0]).sum().assign(name='Subtotal').
                            set_index('name',append=True)]).sort_index(level=[0], sort_remaining=False)
        
        result = result.droplevel(axis=1, level=0)    
        # Drop subtotal from total, which is last row
        ##result.drop(result.tail(1).index,inplace=True)  

        #Find all countries that have one institute
        full_columns= list(result.columns)
        columns_no_repeat=[]
        for item in full_columns:
            columns_no_repeat.append(item[1]) 
        columns_no_repeat= [x for x in columns_no_repeat if columns_no_repeat.count(x)==1]

        # Add subtotals for columns ...  
        
        # Create data frame for subtotals
        subcol = result.groupby(axis=1, level=[1]).sum()
        # Form indices 
        indices = list(zip(subcol.columns, ["Subtotal"]*len(subcol.columns)))
        ###indices[-1] = ("Total", "") #what does this do?
        subcol.columns = pd.MultiIndex.from_tuples(indices, names = ["country", "currentLocation_code"])
        # Drop total
        subcol = subcol.drop(columns_no_repeat, axis=1)  
        result = result.droplevel(axis=1, level=0) #remove 'count' level
        # Concat to main Table
        result = pd.concat([result, subcol], axis=1).sort_index(axis = 1, level = [0]).astype(int) # convert floats to ints   
        
        index_stage = result.index.to_list()
        subcoms = [x[0] for x in index_stage]
        subcoms =  list(dict.fromkeys(subcoms))
        subcoms = [x for x in subcoms if "Total" not in x]
         
        stage_index=[]
        for item in list(stage_dict.items()):
            for subitem in item[1]:
                for subcomb in subcoms:
                    tuple_type= (subcomb, subitem)
                    stage_index.append(tuple_type)
         
        missing_stages = st.checkbox('Include stages that currently have no components?')
        no_stage = pd.DataFrame()
        if missing_stages:
            for item in stage_index: 
                if result.index.isin([item]).any()==False:     
                    stage_no_comps = pd.DataFrame([[0]*result.shape[1]],columns=result.columns, index= pd.MultiIndex.from_tuples([item]))
                    no_stage = pd.concat([no_stage, stage_no_comps])
            result = pd.concat([result, no_stage])   
        
        def criteria_column(x):  
            if x[0] == "CERN":
                return ("AAAAAA", x[1]) 
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            if x[0] in stage_dict:  
                alphabet= [chr(k) for k in range(len(stage_dict[x[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[x[0]]):
                    if x[1]==item:  
                        return(x[0], alphabet[i])   
                return(x[0], x[1])
            else:   
                return x

        def criteria_row(x):   
            if x[0] == "Total":
                return ("zzzzzz", x[1])  
            if x[1]== "Subtotal":        
                return(x[0], 'zzzzz')#"ZZZZZZZZ")
            if x[1]== "Total":        
                return(x[0], 'zzzzzz')#"ZZZZZZZZ")
            else:
                comp_key = list(stage_dict.keys())
                alphabet= [chr(k) for k in range(len(stage_dict[comp_key[0]]))] #chr gives the ASCII character for the given number  
                for i, item in enumerate(stage_dict[comp_key[0]]):
                    if x[1]==item:  
                        return(x[0], alphabet[i])   
                return(x[0], x[1]) 

        # Sort column order
        columns_orig = result.columns.tolist() 
        columns_sort = sorted(columns_orig, key=criteria_column) 
        result = result[columns_sort]

        # Sort row order 
        index_sorted = sorted(result.index.to_list(), key=criteria_row)  
        result= result.reindex(index_sorted) 
        st.write('## ITK ' + project_name + ' ' + comp_name +' overview plots') 

        result.drop(result.tail(1).index,inplace=True)  

        table_countries= result.columns.levels[0].tolist()
        for item in ['total']:
            table_countries= list(filter(lambda x: (item not in x.lower()), table_countries)) #removes total

        table_institutes= result.columns.levels[1].tolist()
        table_institutes= filter(None, table_institutes)
        #for item in ['']:
        #    table_institutes= list(filter(lambda x: (item not in x.lower()), table_institutes)) #removes test components 

        table_components= result.index.levels[0].tolist()
        for item in ['total']:
            table_components= list(filter(lambda x: (item not in x.lower()), table_components)) #removes total
    


        plots = st.radio('Please select a plot type',
        ('All sub components and countries', 'All countries with particular sub component', 
        'All sub components for particular country', 'Particular sub component and country'))

        if plots== 'All sub components and countries':
            result_small= result.copy() 
            for item in result_small.columns.tolist():
                if 'Subtotal' in item[1]: 
                    result_small= result_small.drop(['Subtotal'], level=1, axis=1)  
            result_small= result_small.drop(['Subtotal'], level=1, axis=0)
            result_small= result_small.drop(['Total'], axis=1)
            result_small= result_small.drop(['Total'], axis=0)
            result_small= result_small.groupby(level=0, axis=1).sum()
            result_small= result_small.groupby(level=0, axis=0).sum()

            result_small_tidy= result_small.copy()   
            result_small_tidy.loc['Total']= result_small_tidy.sum()
            result_small_tidy['Total']= result_small_tidy.sum(axis=1)
            csv_result_small = convert_df(result_small_tidy) 
            result_small_name=  comp_name.replace(' ', '_') + '_sub-components_and_countries'  + '_plot_data_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download plot data", data=csv_result_small, file_name=result_small_name, mime='text/csv' )
            #result_small= result_small[['GB', 'JP' ]] #potential input, reduce the countries
            #result_small= result_small.loc[['Module PCB', 'Sensor Tile' ]] #potential input, reduce the components
            result_small_transpose= result_small.transpose() 
            fig_plots_transpose = px.bar(result_small_transpose, #pattern_shape="componentType_name", 
                        labels={'value':'Number of sub components', 'type_name': 'Sub component type', 'country': 'Country'},
                        title='Number of sub components per country, subdivided by sub component type', barmode="group")   
            st.plotly_chart(fig_plots_transpose) 
    
            #result_small= result_small[['GB', 'JP' ]] #potential input, reduce the countries
            ##result_small= result_small.loc[['Module PCB', 'Bare Module', 'Module', 'Module carrier']]  #potential input, reduce the components
            ##result_small=result_small.transpose()
            fig_plots = px.bar(result_small, #pattern_shape="country", 
                        labels={'value':'Number of sub components', 'typee_name': 'sub component type', 'country': 'Country'},
                        title='Number of sub components per sub component type, subdivided by country')#, hover_name="country")  
            st.plotly_chart(fig_plots) 

        elif plots== 'All countries with particular sub component': 
            list_countries= st.selectbox('Please select sub component', table_components)
            result_small= result.copy()  
            for item in result_small.columns.tolist():
                if 'Subtotal' in item[1]: 
                    result_small= result_small.drop(['Subtotal'], level=1, axis=1)  
            result_small= result_small.groupby(level=0, axis=1).sum()
            result_small= result_small.loc[list_countries] #imput

            result_small_tidy= result_small.copy()  
            result_small_tidy= result_small_tidy.rename(index={'Subtotal': 'Total'})  
            csv_result_small = convert_df(result_small_tidy)  
            result_small_name=  'All_countries_' + list_countries.replace(' ', '_')  + '_plot_data_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download plot data", data=csv_result_small, file_name=result_small_name, mime='text/csv' )

            result_total= result_small.copy() #this is for the total plot  
            result_small= result_small.drop(['Total'], axis=1)
            result_small= result_small.drop(['Subtotal'], axis=0)  
            #result_small= result_small[['GB', 'JP' ]] ##potential input, reduce the countries
            #result_small= result_small.loc[['QC', 'Complete' ]] #potential input, reduce the stages
            result_small_transpose=result_small.transpose()  
            if list_countries=='STAR Hybrid Assembly':
                fig_transpose = px.bar(result_small_transpose, #pattern_shape="currentStage_name", 
                            labels={'value':'Number of sub components', 'country': 'Country', 'currentStage_name': 'Component stage'},
                            title='Number of STAR Hybrid Assemblies per country, subdivided by stage'.format(list_countries), barmode="group" ) 
                st.plotly_chart(fig_transpose)

                fig_plots = px.bar(result_small, #pattern_shape="country", 
                    labels={'value':'Number of sub components', 'country': 'Country', 'currentStage_name': 'Component stage'},
                    title='Number of STAR Hybrid Assemblies per stage, subdivided by country'.format(list_countries))  
                st.plotly_chart(fig_plots)
                
                result_total= result_total.drop(['Subtotal'], axis=0) 
                result_total= result_total['Total']
                result_total= result_total.transpose()
                fig_total = px.bar(result_total,
                            labels={'value':'Number of sub components', 'currentStage_name': 'Component stage'},
                            title='Number of STAR Hybrid Assemblies per stage'.format(list_countries) )  
                st.plotly_chart(fig_total) 
            else:
                fig_transpose = px.bar(result_small_transpose, #pattern_shape="currentStage_name", 
                            labels={'value':'Number of sub components', 'country': 'Country', 'currentStage_name': 'Component stage'},
                            title='Number of {}s per country, subdivided by stage'.format(list_countries), barmode="group" ) 
                st.plotly_chart(fig_transpose)

                fig_plots = px.bar(result_small, #pattern_shape="country", 
                    labels={'value':'Number of sub components', 'country': 'Country', 'currentStage_name': 'Component stage'},
                    title='Number of {}s per stage, subdivided by country'.format(list_countries))  
                st.plotly_chart(fig_plots)
                
                result_total= result_total.drop(['Subtotal'], axis=0) 
                result_total= result_total['Total']
                result_total= result_total.transpose()
                fig_total = px.bar(result_total,
                            labels={'value':'Number of sub components', 'currentStage_name': 'Component stage'},
                            title='Number of {}s per stage'.format(list_countries) )  
                st.plotly_chart(fig_total) 

        elif plots== 'All sub components for particular country':
            if 'CERN' in table_countries:
                list_countries= st.multiselect('Please select countries (for best results please choose a single country)', table_countries, default='CERN') #non-broadcastable output operand with shape (2,1) doesn't match the broadcast shape (2,2)
            else:
                list_countries= st.multiselect('Please select countries (for best results please choose a single country)', table_countries)#= st.selectbox('Please select countries', table_countries)
            if len(list_countries)==0:
                st.warning('Please input at least one country')
                st.stop() 
            result_small= result.copy()
            result_small= result_small.drop(['Subtotal'], level=1, axis=0)
            result_small= result_small.groupby(level=0, axis=0).sum()
            result_small= result_small.drop(['Total'], axis=1)
            result_small= result_small.drop(['Total'], axis=0)
            result_small= result_small[list_countries]#, 'JP' ]] #input, remove to list for every institute
            result_small = result_small.droplevel(axis=1, level=0) 
            if 'Subtotal' in result_small.columns.tolist():
                result_small= result_small.drop(['Subtotal'], axis=1)  

            result_small_tidy= result_small.copy()   
            result_small_tidy.loc['Total']= result_small_tidy.sum()
            result_small_tidy['Total']= result_small_tidy.sum(axis=1)
            csv_result_small = convert_df(result_small_tidy) 
            list_countries_join ='_'.join(list_countries)
            result_small_name=  comp_name.replace(' ', '_') + '_sub_components_' + list_countries_join +  '_plot_data_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download plot data", data=csv_result_small, file_name=result_small_name, mime='text/csv' )

            ## this was active #result_small= result_small.loc[['Module PCB', 'Bare Module', 'Module', 'Module carrier']] #potential input, reduce the components 
            #result_small= result_small[['LIV', 'GL' ]] #potential input, reduce the institutes
            result_small_transpose=result_small.transpose() 
            fig_transpose = px.bar(result_small_transpose, #pattern_shape="componentType_name", 
                        labels={'value':'Number of csub omponents', 'type_name': 'Sub component type', 'currentLocation_code': 'Institute'},
                        title='Number of sub components per institute, subdivided sub component type', barmode="group")  
            st.plotly_chart(fig_transpose) 

            fig = px.bar(result_small, #pattern_shape="currentLocation_code", 
                labels={'value':'Number of sub components', 'type_name': 'Component type', 'currentLocation_code': 'Institute'},
                title='Number of components per sub component type, subdivided by institute' )
            st.plotly_chart(fig) 


        elif plots== 'Particular sub component and country':
            list_components= st.selectbox('Please select sub components', table_components)
            result_small= result.copy()   
            result_small= result_small.loc[list_components] #input 
            result_small = result_small.loc[:, (result_small != 0).any(axis=0)]  
            table_countries= result_small.columns.remove_unused_levels().levels[0].tolist() 
            table_countries = list(filter(None, table_countries)) 
            for item in ['total']:
                table_countries= list(filter(lambda x: (item not in x.lower()), table_countries)) #removes test components 



            if 'CERN' in table_countries:
                list_countries= st.multiselect('Please select countries (for best results please choose a single country)', table_countries, default='CERN') #non-broadcastable output operand with shape (2,1) doesn't match the broadcast shape (2,2)
            else:
                list_countries= st.multiselect('Please select countries (for best results please choose a single country)', table_countries)
            #list_countries= st.selectbox('Please select countries', table_countries)
            if len(list_countries)==0:
                st.warning('Please input at least one country')
                st.stop() 
            #list_components= st.selectbox('Please select sub components', table_components)
            #result_small= result.copy()  
            result_small= result_small[list_countries]#, 'JP' ]] #input, remove to use every institute 
            #result_small=result_small.loc[list_components] #input
            result_small = result_small.droplevel(axis=1, level=0)
            if 'Subtotal' in result_small.columns.tolist():
                result_small= result_small.drop(['Subtotal'], axis=1)

            result_small_tidy= result_small.copy()  
            result_small_tidy= result_small_tidy.rename(index={'Subtotal': 'Total'}) 
            result_small_tidy['Total']= result_small_tidy.sum(axis=1)
            csv_result_small = convert_df(result_small_tidy) 
            list_countries_join ='_'.join(list_countries)
            result_small_name=  list_components.replace(' ', '_') + '_' + list_countries_join +  '_plot_data_' + time_current + '_' + project_name +'.csv'
            st.download_button(label="Download plot data", data=csv_result_small, file_name=result_small_name, mime='text/csv' )

            result_small= result_small.drop(['Subtotal'], axis=0)
            #result_small= result_small[['GL', 'LIV']] #potential input, reduce the institutes 
            #result_small= result_small.loc[['QC', 'Complete' ]] #potential input, reduce the stages
            result_small_transpose=result_small.transpose() 
            if list_components=='STAR Hybrid Assembly':
                fig_transpose = px.bar(result_small_transpose, #pattern_shape="currentStage_name", 
                            labels={'value':'Number of components', 'currentLocation_code': 'Institute', 'currentStage_name': 'Component stage'},
                            title='Number of STAR Hybrid Assemblies per institute, subdivided by stage'.format(list_components), barmode="group") #log_y=True
                st.plotly_chart(fig_transpose) 

                fig = px.bar(result_small, #pattern_shape="currentLocation_code", 
                            labels={'value':'Number of components', 'currentLocation_code': 'Institute', 'currentStage_name': 'Component stage'},
                            title='Number of STAR Hybrid Assemblies per institute, subdivided by stage'.format(list_components) ) #log_y=True
                st.plotly_chart(fig) 
            else:
                fig_transpose = px.bar(result_small_transpose, #pattern_shape="currentStage_name", 
                            labels={'value':'Number of components', 'currentLocation_code': 'Institute', 'currentStage_name': 'Component stage'},
                            title='Number of {}s per institute, subdivided by stage'.format(list_components), barmode="group") #log_y=True
                st.plotly_chart(fig_transpose) 

                fig = px.bar(result_small, #pattern_shape="currentLocation_code", 
                            labels={'value':'Number of components', 'currentLocation_code': 'Institute', 'currentStage_name': 'Component stage'},
                            title='Number of {}s per institute, subdivided by stage'.format(list_components) ) #log_y=True
                st.plotly_chart(fig) 

    