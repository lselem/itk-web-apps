import streamlit as st
from core.Page import Page
import pandas as pd
import altair as alt
import numpy as np

import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX

from commonCode.codeChunks import MakeStatDict

#####################
### useful functions
#####################

### extract component information
def GetComponentInfo(myClient, projCode, compTypeCode, propCodes):

    ### get indexing
    total=myClient.get('listComponents', json={'project':projCode,'componentType':compTypeCode, 'pageInfo':{'pageSize':1}}).total
    pageSize=50
    count= int(total/pageSize)
    if total%pageSize>0:
        count=count+1

    ### make list of altIDs and SNs
    st.write(f"Retrieving population info. ({total})")
    my_bar = st.progress(0)
    my_text=st.empty()
    popList=[]
    keyList=["alternativeIdentifier","serialNumber"]
    codeList=["type", "currentLocation", "institution"]
    for pi in range(0,count,1):
        my_bar.progress( float(pi+1)/count)
        compList=myClient.get('listComponents', json={'project':projCode,'componentType':compTypeCode,'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
        popList.extend( #compList.data)
            [ {k:comp[k] for k in keyList if k in comp.keys()} | 
             {k:comp[k]['code'] for k in codeList if k in comp.keys() and type(comp[k])==type({}) and "code" in comp[k].keys()} |
             {prop['code']:prop['value'] for prop in comp['properties'] if type(prop)==type({}) and "code" in prop.keys() and "value" in prop.keys() and prop['code'] in propCodes}
             for comp in compList.data if type(comp)==type({}) and "properties" in comp.keys() and type(comp['properties'])==type([])] )
        my_text.markdown(f' - total length: {len(popList)}')
    else:
        ### at end
        my_text.markdown(f"Population retrieval complete. found {len(popList)}")
    
    # st.dataframe(popList)
    return popList

### extract testRun information
def GetTestRunInfo(myClient, projCode, compTypeCode, testTypeCode):

    ### listTestRuns doesn't work --> use listTestRunsByTestType
    retAlpha= myClient.get('listTestRunsByTestType', json={'project':projCode,'componentType':compTypeCode, 'testType':testTypeCode, 'pageInfo':{'pageSize':1}})

    ### get indexing
    try:
        total= retAlpha.total
    except AttributeError:
        total=len(retAlpha)
    pageSize=50
    count= int(total/pageSize)
    if total%pageSize>0:
        count=count+1

    ### make list of altIDs and SNs
    st.write(f"Retrieving population info. ({total})")
    my_bar = st.progress(0)
    my_text=st.empty()
    popList=[]
    keyList=["id","state"]
    codeList=["stage","institution","testType"]
    for pi in range(0,count,1):
        my_bar.progress( float(pi+1)/count)
        testList=myClient.get('listTestRunsByTestType', json={'project':projCode,'componentType':compTypeCode, 'testType':testTypeCode,'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
        popList.extend( #testList.data)
            [ {k:test[k] for k in keyList if k in test.keys()} | 
             {k:test[k]['code'] for k in codeList if k in test.keys() and type(test[k])==type({}) and "code" in test[k].keys()}
             for test in testList.data if type(test)==type({}) ] )
        my_text.markdown(f' - total length: {len(popList)}')
    else:
        ### at end
        my_text.markdown(f"Population retrieval complete. found {len(popList)}")
    
    # st.dataframe(popList)
    return popList

def GetTestRunData(myClient, idList, propCodes):

    ### get indexing
    total=len(idList)
    pageSize=100
    count= int(total/pageSize)
    if total%pageSize>0:
        count=count+1

    ### make list of altIDs and SNs
    st.write(f"Retrieving testRun data ({total})")
    my_bar = st.progress(0)
    my_text=st.empty()
    popList=[]
    keyList=["id","date"]
    codeList=["institution","testType"]
    compList=["serialNumber"]
    for pi in range(0,count,1):
        my_bar.progress( float(pi+1)/count)
        testList=myClient.get('getTestRunBulk', json={'testRun':idList[pi*pageSize:(pi+1)*pageSize]})
        # popList.extend( #testList)
        for test in testList:
            ### check is dictionary
            if type(test)!=type({}):
                continue
            ### add top-level stuff
            popList.append( { k:test[k] for k in keyList if k in test.keys()} )
            # st.write("top-level",popList[-1])
            popList[-1].update( { k:test[k]['code'] for k in codeList if k in test.keys() and type(test[k])==type({}) and "code" in test[k].keys()} )
            # st.write("code-level",popList[-1])
            popList[-1].update( { k:test['components'][0][k] for k in compList if "components" in test.keys() and len(test['components'])>0 and type(test['components'][0])==type({}) and k in test['components'][0].keys()} )
            # st.write("comp-level",popList[-1])
            ### try properties and results
            for pr in ['properties','results']:
                if pr in test.keys():
                    if type(test[pr])!=type([]):
                        # st.write("skipping missing:",pr)
                        continue
                    popList[-1].update( { para['code']:para['value'] for para in test[pr] if type(para)==type({}) and "code" in para.keys() and "value" in para.keys() and para['code'] in propCodes } )
                    # st.write(f"{pr}-level",popList[-1])

        my_text.markdown(f' - total length: {len(popList)}')
    else:
        ### at end
        my_text.markdown(f"Data retrieval complete. found {len(popList)}")
    
    # st.dataframe(popList)
    return popList


infoList=["  * Get components at selected institution",
        "   * filter based on populations"]
#####################
### main part
#####################

class Page6(Page):
    def __init__(self):
        super().__init__("Plot Param", "Plot TestType Parameter Distribution", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Select Project and Component Type")

        ### Vdep from sensor wafers
        PROJECT_CODE= st.selectbox("Select project code:",["S","P","CE","CM"])

        ### retrieving componentType information...
        compTypeList=st.session_state.myClient.get('listComponentTypes', json={'project':PROJECT_CODE})                        
        ### report

        ### select componentType
        sel_ct=st.selectbox("Select componentType:",sorted([x['code'] for x in compTypeList if type(x)==type({}) and "code" in x.keys()]) )

        compTypeObj=st.session_state.myClient.get('getComponentTypeByCode', json={'project':PROJECT_CODE, 'code':sel_ct})          
        
        ### select mode
        sel_mode=st.radio("Get parameter:", ["from componentType", "from testType"])


        ### componentType mode
        if "componentType" in sel_mode:
            st.write("### Get Component Type parameters")              

            df_paras=pd.DataFrame()
            for k in ["properties","results"]:
                try:
                    if df_paras.empty:
                        df_paras=pd.DataFrame(compTypeObj[k])
                    else: 
                        df_paras=pd.concat(df_paras, pd.DataFrame(compTypeObj[k]))
                except KeyError:
                    st.write(f"No _{k}_ info. found.")

            if df_paras.empty:
                st.write("No parameter information found. Please select another")
                st.stop()

            if st.checkbox("remove non numeric?"):
                filter_list=['integer','int','float','boolean','bool']
                df_paras=df_paras.query('dataType==@filter_list')
            st.write(df_paras)

            if df_paras.empty:
                st.write("__no parameters to retrieve__")
                st.stop()

            
            if not st.checkbox("Ready to get component data?"):
                st.stop()

            st.write("---")
            st.write(f"### Getting information for {sel_ct}s ({PROJECT_CODE})")

            if "sel_ct" not in pageDict.keys() or pageDict['sel_ct']!=sel_ct or pageDict['sel_mode']!=sel_mode or st.button("Get component info."):
                pageDict['sel_ct']=sel_ct
                pageDict['sel_mode']=sel_mode
                pageDict['popList']=GetComponentInfo(st.session_state.myClient, PROJECT_CODE, sel_ct, df_paras['code'].unique())


        ### testType mode
        if "testType" in sel_mode:
            st.write("### Get Test Type parameters")   

            stageTests=[stg['testTypes'] for stg in compTypeObj['stages'] if type(stg)==type({}) and "testTypes" in stg.keys() and type(stg['testTypes'])==type([])]
            # st.write(stageTests)
            testTypes=[tt['testType']['code'] for stgts in stageTests for tt in stgts if type(tt)==type({}) and "testType" in tt.keys()]
            # st.write(testTypes)
            sel_tt=st.selectbox("Select testType:",sorted(set(testTypes)) )


            ### get testRun data
            testTypeObj=st.session_state.myClient.get('getTestTypeByCode', json={'project':PROJECT_CODE, 'componentType':sel_ct, 'code':sel_tt})          

            df_paras=pd.DataFrame()
            for k in ["properties","parameters"]:
                try:
                    if df_paras.empty:
                        df_paras=pd.DataFrame(testTypeObj[k])
                    else: 
                        df_paras=pd.concat([df_paras, pd.DataFrame(testTypeObj[k])])
                except KeyError:
                    st.write(f"No _{k}_ info. found.")

            if df_paras.empty:
                st.write("No parameter information found. Please select another")
                st.stop()

            if st.checkbox("remove non numeric?"):
                filter_list=['integer','int','float','boolean','bool']
                df_paras=df_paras.query('dataType==@filter_list')
            st.write(df_paras)

            if df_paras.empty:
                st.write("__no parameters to retrieve__")
                st.stop()

            if not st.checkbox("Ready to get test run data?"):
                st.stop()
            
            st.write("---")
            st.write(f"### Getting information for {sel_ct}s ({PROJECT_CODE}) {sel_tt}")

            if "sel_tt" not in pageDict.keys() or pageDict['sel_ct']!=sel_ct or pageDict['sel_tt']!=sel_tt or pageDict['sel_mode']!=sel_mode or st.button("Get testRun info."):
                pageDict['sel_tt']=sel_tt
                pageDict['sel_ct']=sel_ct
                pageDict['sel_mode']=sel_mode
                pageDict['idList']=GetTestRunInfo(st.session_state.myClient, PROJECT_CODE, sel_ct, sel_tt) 

            if len(pageDict['idList'])<1:
                st.write("__No testRuns found__")
                st.stop()
            
            df_testIds=pd.DataFrame(pageDict['idList'])
            if st.checkbox("See testRun _id_ list?"):
                st.write(df_testIds)
            
            st.write("Using only _ready_ testRuns")
            df_testIds=df_testIds.query('state=="ready"')
            st.write(f" - {len(df_testIds)} testRuns")

            ### need additional query to get testRun data
            if "idLen" not in pageDict.keys() or pageDict['idLen']!=len(df_testIds) or "idCols" not in pageDict.keys() or set(pageDict['idCols'])!=set(df_testIds.columns) or st.button("Get testRun data."):
                pageDict['idCols']=df_testIds.columns
                pageDict['idLen']=len(df_testIds)
                pageDict['popList']=GetTestRunData(st.session_state.myClient,df_testIds['id'].to_list(), df_paras['code'].unique())
            
            # if st.checkbox("See testRun _data_ list?"):
            #     st.dataframe(pageDict['popList'])

        ### format dataframe
        df_prop=pd.DataFrame(pageDict['popList'])
        if st.checkbox("See retrieved data"):
            st.write(df_prop)
            if st.checkbox("see statistics"):
                st.write(df_prop.describe())
        
        ####################
        ### visualisation
        ####################

        sel_pc=st.selectbox("Select parameter:",sorted(df_paras['code'].unique()) )
        # check data type
        dataType=df_paras.query(f'code=="{sel_pc}"')['dataType'].values[0]
        st.write(f" - expected dataType is {dataType}")
        # check value type
        valueType=df_paras.query(f'code=="{sel_pc}"')['valueType'].values[0]
        st.write(f" - expected valueType is {valueType}")
        
        if sel_pc not in df_prop.columns:
            st.write("Not parameter included in these tests. Please select another")
            st.stop()

        ### select legend parameter
        sel_col=st.selectbox('select legend:',sorted(df_prop.columns))

        ### copy data for plotting
        df_plot=df_prop.copy(deep=True)

        # default make histo
        unpackt=False
        if valueType=="array":
            if st.checkbox("Unpack array values (for plotting)?"):
                st.write(f"   - unpacking values...")
                df_plot=df_plot.explode(sel_pc).reset_index(drop=True)
                unpackt=True

        cCol=sel_col
        # make selection
        selection_cCol = alt.selection_multi(fields=[cCol])
        # make condition
        condition_cCol=alt.condition(selection_cCol,
                        alt.Color(cCol+':N', legend=None),
                        alt.value('lightgray'))
        
        ### make stat dict for plot dressing
        statDict=MakeStatDict(df_plot, sel_pc, dataType)
        if st.checkbox("show calculated stats?"):
            st.dataframe([{'key':k, 'value':v} for k,v in statDict.items()])

        if statDict['lo']==statDict['hi']:
            st.write("__Warning: plot range strange: is this value numeric?__")

        ### basic plot
        if st.checkbox("apply statistics?"):

            if st.checkbox("Manual limits?"):

                statDict['lo']=st.number_input("Insert lower limit", value=statDict['lo'])
                statDict['hi']= st.number_input("Insert upper limit", value=statDict['hi'])
                lim_count=len(df_plot.query(f"{sel_pc}>{statDict['lo']} & {sel_pc}<{statDict['hi']}").reset_index().index)
                st.write(f"Points inside limits({statDict['lo']}:{statDict['hi']}): __{lim_count}__")

            st.write("_let's see if this works..._")
            chart=alt.Chart(df_plot).mark_bar().encode(
                x=alt.X(sel_pc+':Q', bin=alt.Bin(extent=[statDict['lo'], statDict['hi']], step=(statDict['hi']-statDict['lo'])/25), scale=alt.Scale(domain=[statDict['lo'], statDict['hi']]), title=sel_pc),
                y=alt.Y('count():Q'),
                color=condition_cCol,
                tooltip=[sel_pc+':Q',cCol+':N','count():Q']
            ).properties(
            title={
                'text':[f"{sel_pc} distribution"], 
                'subtitle':[f"stats version, total: {statDict['total']}", f"2σ underflow: {statDict['under_range']}, overflow: {statDict['over_range']}"] 
                },           
            width=600)
        else:
            chart=alt.Chart(df_plot).mark_bar().encode(
                x=alt.X(sel_pc+':Q', bin=True, title=sel_pc),
                y=alt.Y('count():Q'),
                color=condition_cCol,
                tooltip=[sel_pc+':Q',cCol+':N','count():Q']
            ).properties(
            title={
                'text':[f"{sel_pc} distribution"], 
                'subtitle':[f"raw version, total: {statDict['total']}"] 
                },           
            width=600)

        # make legend
        legend= alt.Chart(df_plot).mark_point(size=100, filled=True).encode(
            y=alt.Y(cCol+':N', axis=alt.Axis(orient='right')),
            color=condition_cCol
        ).add_selection(
            selection_cCol
        )
        chart=chart.add_selection( selection_cCol )
        chart= chart|legend
        st.write(chart)

        ### See upload timeline
        if st.checkbox("See upload dates"):
            # st.write(df_plot)
            
            ### do histo plot
            if valueType=="array" and unpackt==True:
                timeline=alt.Chart(df_plot).mark_boxplot(extent='min-max').encode(
                                    x=alt.X('date:T', axis=alt.Axis(
                                        format='%Y-%m-%dT%H:%M:%SZ',
                                        labelAngle=-45,
                                        title='Date') ),
                                    y=alt.Y(sel_pc+':Q', title=sel_pc),
                                    color=condition_cCol,
                                    tooltip=[sel_pc+':Q',cCol+':N','serialNumber:N','date:T']
                                ).properties(
                                    title={
                                        'text':[f"{sel_pc} uploads"]
                                        },    
                                    width=600
                                )
            else:
                timeline=alt.Chart(df_plot).mark_circle().encode(
                                    x=alt.X('date:T', axis=alt.Axis(
                                        format='%Y-%m-%dT%H:%M:%SZ',
                                        labelAngle=-45,
                                        title='Date') ),
                                    y=alt.Y(sel_pc+':Q', title=sel_pc),
                                    color=condition_cCol,
                                    tooltip=[sel_pc+':Q',cCol+':N','serialNumber:N','date:T']
                                ).properties(
                                    title={
                                        'text':[f"{sel_pc} uploads"]
                                        },    
                                    width=600
                                )

            timeline=timeline.add_selection( selection_cCol )
            timeline= timeline|legend
            st.write(timeline)

        st.write("__Grouped Info__")
        st.write(df_prop.groupby(by=[cCol]).count())
        st.download_button(label="Download full info.", data=df_prop.to_csv(),file_name=f"{pageDict['sel_ct']}_{pageDict['sel_tt']}.csv")

    