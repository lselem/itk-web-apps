import streamlit as st
from core.Page import Page
import pandas as pd
import altair as alt

import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
projMap={'P':"Pixels",'S':"Strips",'CE':"Common Electronics",'CM':"Common Mechanics"}

def QuickStats(myClient):

    retDict={}
    cmdList=["listInstitutions","listUsers",
            "listComponentTypes","listComponents",
            "listTestTypes","listTestRuns",
            "listShipments", "listClusters",
            "listBatchTypes"
            ]

    qs_bar = st.progress(0)
    # retStr="## Quick Stats for PDB...\n"
    count=0
    for cmdStr in cmdList:
        count+=1
        qs_bar.progress( float(count)/(len(cmdList)+1))
        bigList=myClient.get(cmdStr, json={})
        # print(f"### Total {cmdStr}: {bigList.total}\n-----------")
        # retStr+=f"\n### Total {cmdStr.replace('list','')}: {bigList.total}\n-----------"
        retDict[cmdStr.replace('list','')]=bigList.total

    ### list projects
    projList=myClient.get('listProjects', json={})
    total=0
    for pc in pd.DataFrame(projList)['code'].unique():
        total+=myClient.get('listBatches', json={'project':pc}).total
    # print(f"### Total listBatches: {total}")
    count+=1
    qs_bar.progress( float(count)/(len(cmdList)+1))
    # retStr+=f"\n### Total Batches: {total}"
    retDict['Batches']=total

    return retDict

scale = alt.Scale(
    domain=['Pixels', 'Strips', 'Common Electronics', 'Common Mechanics', 'Not Known'],
    range=['blue', 'red', 'green', 'purple', 'grey']
)

### Users - beware double entries!
def MakePerProjectDict(myClient, cmdStr, colStr, subStr, grpStr, exFlag=False):
    ### total bit
    bigList=myClient.get(cmdStr, json={})
    titleStr= f"Total {cmdStr}: {bigList.total}"
    st.write(titleStr)

    popList=[]
    for pc in projMap.keys():
        try:
            popList.append({'projCode':pc, 'population':myClient.get(cmdStr, json={'filterMap':{'project':pc},'pageInfo':{'pageSize':1}} ).total }) #, 'state':"ready"
        except AttributeError:
            popList.append({'projCode':pc, 'population':0})

    df_pop=pd.DataFrame(popList)
    df_pop['project']=df_pop['projCode'].apply(lambda x: projMap[x] if x in projMap.keys() else "Unknown")
    # st.write(df_pop)
    ### mapping projects
    df_bigList=pd.DataFrame(bigList.data)

    # st.write(cmdStr, df_bigList.columns)
    # print(df_bigList.keys())
    if exFlag:
        df_bigList=df_bigList.explode(colStr)
    # st.write(df_bigList.columns)
    # st.write(bigList.data[0])
    df_bigList['projCode']=df_bigList[colStr].apply(lambda x: x[subStr] if type(x)==type({}) and subStr in x.keys() else "Not known")
    df_bigList=df_bigList.groupby([grpStr,'projCode']).last().reset_index()
    
    # print(f"projCodes: {df_bigList['projCode'].unique()}")
    totalDict={}
    # loop over projects
    for pc in df_bigList['projCode'].unique():
        count=len(df_bigList.query(f'projCode=="{pc}"'))
        # print(f"\t- \"{pc}\" number {cmdStr}: {count}")
        try:
            totalDict[pc]+=count
        except KeyError:
            totalDict[pc]=count

    proj_plot=alt.Chart(pd.DataFrame([{'projCode':k,'count':v} for k,v in totalDict.items()])).mark_bar().encode(
        x='count:Q',
        y=alt.Y('project:N', sort='-x'),
        color=alt.Color('project:N', scale=scale, legend=None),
        tooltip=['project:N','count:Q']
    ).transform_calculate(
        project= 'datum.projCode=="P" ? "Pixels" : datum.projCode=="S" ? "Strips" : datum.projCode=="CE" ? "Common Electronics" : datum.projCode=="CM" ? "Common Mechanics" : "Not known"'
    ).properties( title=titleStr, width=600)

    # st.write(proj_plot)

    return proj_plot, df_pop


infoList=["  * Standard PDB statistics",
        "   * plots for per project comparisons"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("PDB Stats", "Quick PDB Overview stats", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Overall Stats for PDB")
        if "statDict" not in pageDict.keys() or st.button("Refresh stats"):
            st.write("Grabbing data...")

            pageDict['statDict']=QuickStats(st.session_state.myClient)
        

        st.dataframe([{'name':k,'number':v} for k,v in pageDict['statDict'].items()])

        st.write("---")
        st.write("## Per Project Information")

        if "projPlots" not in pageDict.keys():
            pageDict['projPlots']=[]

        pp_bar = st.progress(0)
        count=1
        if len(pageDict['projPlots'])<1 or st.button("Refresh project stats"):
            pageDict['projPlots']=[]
        
            proj_plot, df_pop = MakePerProjectDict(st.session_state.myClient, "listInstitutions", "componentTypes", "code", "code", True)
            pageDict['projPlots'].append({'name':"listInstitutions", 'plot':proj_plot, 'df':df_pop})
            pp_bar.progress(1/5.0)

            proj_plot, df_pop = MakePerProjectDict(st.session_state.myClient, "listUsers", "preferences", "defaultProject", "id", False)
            pageDict['projPlots'].append({'name':"listUsers", 'plot':proj_plot, 'df':df_pop})
            pp_bar.progress(2/5.0)

            proj_plot, df_pop = MakePerProjectDict(st.session_state.myClient, "listComponentTypes", "project", "code", "code", False)
            pageDict['projPlots'].append({'name':"listComponentTypes", 'plot':proj_plot, 'df':df_pop})
            pp_bar.progress(3/5.0)

            proj_plot, df_pop = MakePerProjectDict(st.session_state.myClient, "listComponents", "project", "code", "code", False)
            pageDict['projPlots'].append({'name':"listComponents", 'plot':proj_plot, 'df':df_pop})
            pp_bar.progress(4/5.0)

            proj_plot, df_pop = MakePerProjectDict(st.session_state.myClient, "listTestTypes", "componentType", "project", "code", False)
            pageDict['projPlots'].append({'name':"listTestTypes", 'plot':proj_plot, 'df':df_pop})
            pp_bar.progress(5/5.0)

            proj_plot, df_pop = MakePerProjectDict(st.session_state.myClient, "listTestRuns", "testType", "code", "cts", False)
            pageDict['projPlots'].append({'name':"listTestRuns", 'plot':proj_plot, 'df':df_pop})
            pp_bar.progress(4/5.0)

        
        for pp in pageDict['projPlots']:

            st.write(f"### {pp['name']}")
            st.write(f"__Table__ (using API filterMap)")
            st.write(pp['df'])
            if len(pp['df']['population'].unique())==1:
                st.write(" - something is not right with these values. Go with the plot.")
            else:
                st.write(f" - total: {pp['df']['population'].sum()}")
            st.write(f"__Plot__ (no API filterMap)")
            st.write(pp['plot'])