import streamlit as st
from core.Page import Page
import pandas as pd
import altair as alt

import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
projMap={'P':"Pixels", 'S':"Strips", 'CE':"Common Electronics", 'CM':"Common Mechanics"}

def GetStatsFromCsv(projCode):

    ### read data from file
    csv_url=f"https://wraight.web.cern.ch/itk-pdb-structures/stageTestList_{projCode}.csv"
    df_data= pd.read_csv(csv_url)

    return df_data

infoList=["  * test types and stages per project component type",
        "   * using CSVs on EOS"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("PDB Objects", "Quick PDB stats per componentType", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        st.write("## Select Project")
        
        proj=st.selectbox("Select project:",projMap.values())
        projKey=list(projMap.keys())[list(projMap.values()).index(proj)]
        
        st.write("---")

        df_data=GetStatsFromCsv(projKey)

        st.write(f"### Tests and stages for {proj} componentTypes")
        st.write(df_data)

        st.write("### Plots")
        df_data['altStage']=df_data['altStage'].apply(lambda x: True if x==True or x=="True" else False)

        ### make sure stage defined (not so for CE)
        if "stage" in list(df_data.columns):
            ### stages plot
            # group tests
            df_stages=df_data.groupby(by=['compType','stage']).first().reset_index()
            # get total stages
            df_stages['stages_in_total']=df_stages['compType'].apply(lambda x: len(df_stages[ (df_stages['compType']==x) ].index) )

            stage_chart=alt.Chart(df_stages).mark_bar().encode(
                y=alt.Y('compType:N'),
                x=alt.X('count(stage):Q'),
                color=alt.Color('stage:N', legend=None),
                opacity=alt.condition(alt.datum.altStage == True, alt.value(0.3), alt.value(1)),
                tooltip=['compType:N','stage:N','altStage:N','stages_in_total:Q']
            ).properties( title=f"{proj} stages per componentType" )
            
            ### tests plot
            # drop no tests
            df_plot=df_data.dropna().reset_index(drop=True)
            # get total tests
            df_plot['tests_in_total']=df_plot['compType'].apply(lambda x: len(df_plot[ (df_plot['compType']==x) ].index))
            df_plot['tests_in_stage']=df_plot.apply(lambda x: len(df_plot[ (df_plot['compType']==x['compType']) & (df_plot['stage']==x['stage']) ].index), axis=1)
            test_chart=alt.Chart(df_plot).mark_bar().encode(
                y=alt.Y('compType:N'),
                x=alt.X('count(testType):Q'),
                color=alt.Color('stage:N', legend=None),
                detail=alt.Detail('testType:N'),
                tooltip=['compType:N','testType:N','stage:N','altStage:N','tests_in_stage:Q','tests_in_total:Q']
            ).properties( title={'text': f"{proj} tests per componentType", 'subtitle':"components & stages without tests removed"} )

            ### display
            st.write(stage_chart | test_chart)
        
        ### if no stage column defined
        else:
            st.write("__no stages defined__")
            ### tests plot
            # drop no tests
            df_plot=df_data.dropna().reset_index(drop=True)
            # get total tests
            df_plot['tests_in_total']=df_plot['compType'].apply(lambda x: len(df_plot[ (df_plot['compType']==x) ].index))
            test_chart=alt.Chart(df_plot).mark_bar().encode(
                y=alt.Y('compType:N'),
                x=alt.X('count(testType):Q'),
                color=alt.Detail('Color:N', legend=None),
                tooltip=['compType:N','testType:N','tests_in_total:Q']
            ).properties( title={'text': f"{proj} tests per componentType", 'subtitle':"components without tests removed"} )

            ### display
            st.write(test_chart)