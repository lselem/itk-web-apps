### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from .CommonCode import EditJson
from .CommonCode import GetGleanList
from .CommonCode import GetIdentifier
from .CommonCode import GetCurrentDetails
from .CommonCode import GetStageFromFileName
### analyses from p_d_s (03/02/22)
from . import SENSOR_IV_Analysis_module
from .CommonConfig import datafields
import commonCode.codeChunks as chnx
import commonCode.PDBTricks as pdbTrx

#####################
### useful functions
#####################


infoList=[" * check test type",
        " * upload _dat_ IV file",
        " * review retrieved data & visualisation",
        " * review analysis checks (from [here](https://gitlab.cern.ch/cambridge-ITK/production_database_scripts/-/blob/sensor_updates/strips/sensors/sensor_tests_interfacing_functions))",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]

#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Module IV Upload", ":microscope: Upload Module IV Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()
        
        #####################
        ### select testType: AMAC or not
        ##################### 
        st.write(f"## Select test type")
        pageDict['testType']=st.radio("Select testType:",["MODULE_IV_PS_V1","MODULE_IV_AMAC"])

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)

        st.write("---")
        #####################
        ### Read-in raw data from file
        ##################### 
        st.write(f"## Read Data {pageDict['testType']}")

        ### AMAC IV json bit (optional)
        if "AMAC" in pageDict['testType']:

            pageDict['file']= st.file_uploader("Upload AMAC json:", type=["json"])
            st.write(pageDict['file'])

            if pageDict['file'] is not None:
                retDict = json.load(pageDict['file'])
                # st.write(retDict)
            else:
                for k in ['testSchema', 'toggleEdit', 'toggleText', 'file', 'project','componentType','stage']:
                    try:
                        pageDict.pop(k)
                    except KeyError:
                        pass
                st.write("Please uploads _json_ file.")
                st.stop()

            st.write("### Visualisation")
            st.write("**IV info.**")
            IVchart=alt.Chart(pd.DataFrame(retDict['results'])).mark_line().encode(
                    x='VOLTAGE:Q',
                    y='CURRENT:Q',
                    tooltip=['VOLTAGE:Q','CURRENT:Q']
            ).properties(width=600).interactive()
            st.altair_chart(IVchart)

            pageDict['testSchema']=retDict

            # identify details from inputs
            # component identifier
            compId=GetIdentifier(retDict)
            if compId==None:
                st.write("Cannot extract component identifier. Please cheeck inputs")
                st.stop()
            if "compId" not in pageDict.keys():
                pageDict['compId']=compId
            # get project, componentType and currentStage (if not set)
            if "componentType" not in pageDict.keys() or compId!=pageDict['compId']:
                pageDict['compId']=compId
                GetCurrentDetails(compId,pageDict)
                if pageDict['currentStage']==None:
                    pageDict['stage']=GetStageFromFileName(pageDict, df_stageTest)
                else:
                    pageDict['stage']=pageDict['currentStage']
            # option to change stage if required
            infra.ToggleButton(pageDict,'toggleChanger',f"Change current test values ({pageDict['testType']}@{str(pageDict['stage'])})?")
            if pageDict['toggleChanger']:
                st.write("### componentTypes and stages for testType=="+pageDict['testType'])
                st.dataframe(df_stageTest)
                infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
                infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")
            # check stage 
            if pageDict['stage']==None:
                st.write("Stage is not set please select")
                st.stop()

        ### non-AMAC IV bit - txt or dat file
        else:
            ## drag and drop method
            pageDict['file']= st.file_uploader("Upload data file", type=["csv","dat"])
            if st.session_state.debug: st.write(pageDict['file'])

            if pageDict['file'] is not None:
                headerList, dataList = GetGleanList(pageDict['file'])
            else:
                st.write("No data file set")
                filePath=os.path.realpath(__file__)
                exampleFileName="20USBML1234636_HV_TAB_ATTACHED_IV_002.dat"
                if st.session_state.debug:
                    st.write("looking in:",filePath[:filePath.rfind('/')])
                st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
                st.write("See p.8&9 [EDMS document](https://edms.cern.ch/ui/file/2647669/2.2/QC_Sensor-and-Module-IV.pdf) for details on format")

                for k in ['origSchema', 'testSchema', 'toggleEdit', 'toggleText', 'file', 'project','componentType','stage']:
                    try:
                        pageDict.pop(k)
                    except KeyError:
                        pass
                st.stop()

            ### Extract data
            st.write("---")
            st.write("## Extract Data")

            st.write("### header info.")
            df_header = pd.DataFrame(headerList, columns=['key','value'])
            df_header['key']=df_header['key'].str.replace(':','')
            df_header.fillna('', inplace=True)
            st.dataframe(df_header)
            st.write("### data info.")
            df_data = pd.DataFrame(dataList, columns=['voltage', 'current', 'shunt'])
            st.dataframe(df_data)

            if st.session_state.debug:
                infra.ToggleButton(pageDict,'togBart',"Are you Bart?")
                if pageDict['togBart']:
                    st.write("Hello Bart!")
                    df_data['current']=-1*df_data['current'].astype(float)
                    df_data['voltage']=-1*df_data['voltage'].astype(float)
                    df_data['shunt']=-1*df_data['shunt'].astype(float)
            st.write("### Visualisation")
            st.write("**IV info.**")
            IVchart=alt.Chart(df_data).mark_line().transform_fold(
                ['current', 'shunt']
            ).encode(
                    x='voltage:Q',
                    y='value:Q',
                    color='key:N',
                    tooltip=['key:N','voltage:Q','value:Q']
            ).properties(width=600).interactive()
            st.altair_chart(IVchart)

            st.write("---")

            ### fill schema
            st.write("## Filling "+pageDict['testType']+" schema")

            # identify details from inputs
            # component identifier
            head_dict={x:y for x,y in zip([k for k in df_header.to_dict(orient="list")['key']],[v for v in df_header.to_dict(orient="list")['value']]) }
            compId=GetIdentifier(head_dict)
            if compId==None:
                st.write("Cannot extract component identifier. Please cheeck inputs")
                st.stop()
            if "compId" not in pageDict.keys():
                pageDict['compId']=compId
            # get project, componentType and currentStage (if not set)
            if "componentType" not in pageDict.keys() or compId!=pageDict['compId']:
                pageDict['compId']=compId
                GetCurrentDetails(compId,pageDict)
                if pageDict['currentStage']==None:
                    pageDict['stage']=GetStageFromFileName(pageDict, df_stageTest)
                else:
                    pageDict['stage']=pageDict['currentStage']
            # option to change stage if required
            infra.ToggleButton(pageDict,'toggleChanger',f"Change current test values ({pageDict['testType']}@{str(pageDict['stage'])})?")
            if pageDict['toggleChanger']:
                st.write("### componentTypes and stages for testType=="+pageDict['testType'])
                st.dataframe(df_stageTest)
                infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
                infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")
            # check stage 
            if pageDict['stage']==None:
                st.write("Stage is not set please select")
                st.stop()
            
            # get test schema
            if "origSchema" not in pageDict.keys() or pageDict['testType']!=pageDict['origSchema']['testType'] or st.button("Reset Schema: "+pageDict['testType']+" for "+pageDict['componentType']):
                pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':True})
                #pageDict['origSchema']={'properties':{}, 'results':{}, 'passed':False, 'problems':False}

            if st.session_state.debug:
                st.write("**DEBUG** Original *schema*")
                st.write(pageDict['origSchema'])

            # set schema from input data
            if "testSchema" not in pageDict.keys() or st.button("re-read file"):
                pageDict['testSchema']=pageDict['origSchema']
                ### general
                for tc in ['Component','Module_SN']:
                    try:
                        pageDict['testSchema']['component']=df_header.query('key=="'+str(tc)+'"')['value'].values[0]
                        break
                    except IndexError:
                        pass
                #pageDict['testSchema']['component']= df_header.query('key=="Component"')['value'].values[0]
                # make date
                dateStr=df_header.query('key=="Date"')['value'].values[0]+"@"+df_header.query('key=="Time"')['value'].values[0]
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr, "%d %b %Y@%H:%M:%S")
                pageDict['testSchema']['institution']= df_header.query('key=="Institute"')['value'].values[0]

                # properties
                pageDict['testSchema']['properties']['COMMENTS']=df_header.query('key=="Comments"')['value'].values[0]
                pageDict['testSchema']['properties']['RSERIES']=df_header.query('key=="Rseries"')['value'].values[0]
                pageDict['testSchema']['properties']['RSHUNT']=df_header.query('key=="Rshunt"')['value'].values[0]
                pageDict['testSchema']['properties']['TEST_DMM']=df_header.query('key=="Test_DMM"')['value'].values[0]
                pageDict['testSchema']['properties']['VBIAS_SMU']=df_header.query('key=="Vbias_SMU"')['value'].values[0]
                pageDict['testSchema']['properties']['RUNNUMBER']=df_header.query('key=="RunNumber"')['value'].values[0]
                pageDict['testSchema']['runNumber']=df_header.query('key=="RunNumber"')['value'].values[0]

                ### comma in key name fudge
                try:
                    pageDict['testSchema']['properties']['SOFTWARE_TYPE_VERSION']=df_header.query('key=="Software type and version, fw version"')['value'].values[0]
                except:
                    pageDict['testSchema']['properties']['SOFTWARE_TYPE_VERSION']=df_header.query('key=="Software type and version/ fw version"')['value'].values[0]

                # results
                pageDict['testSchema']['results']['HUMIDITY']=float(df_header.query('key=="Humidity"')['value'].values[0])
                pageDict['testSchema']['results']['TEMPERATURE']=float(df_header.query('key=="Temperature"')['value'].values[0])
                pageDict['testSchema']['results']['VOLTAGE']=df_data['voltage'].astype(float).to_list()
                #pageDict['testSchema']['results']['CURRENT']=df_data['current'].astype(float).to_list()
                pageDict['testSchema']['results']['SHUNT_VOLTAGE']=df_data['shunt'].astype(float).to_list()

                ### load up input dictionary
                anaDict={k:None for k in datafields}
                for c in df_header['key'].to_list():
                    for k in anaDict.keys():
                        if c.lower() in k.lower():
                            anaDict[k]=df_header.query('key=="'+c+'"')['value'].values[0]
                anaDict['Filepath']="someString"
                anaDict['Filename']="someString"
                anaDict['TABLE_Current[nA]']=df_data['current'].astype(float).to_list()
                # fudge to use Sensor test analysis
                anaDict['TestType']="ATLAS18_IV_TEST_V1"

                # st.write(df_data.dtypes)
                def massage(x):
                    if '-' in x:
                        return round(float(x.strip('-').rstrip('0')))
                    else:
                        return 0

                anaDict['TABLE_Voltage[V]']=[round(float(x)) for x in df_data['voltage'].astype(float).to_list()]

                ### find the child sensor info.
                myComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pageDict['testSchema']['component']})
                anaDict['Type']=None
                if "SENSOR" not in [x['componentType']['code'] for x in myComp['children'] if x['component']!=None]:
                    st.write("no SENSOR found for",pageDict['testSchema']['component'])
                elif len([x for x in myComp['children'] if x['componentType']['code']=="SENSOR" and x['component']!=None])>1:
                    st.write("too many SENSORs found"+str(len([x for x in myComp['children'] if x['componentType']['code']=="SENSOR" and x['component']!=None])))
                else:
                    sensorComp=[x for x in myComp['children'] if x['componentType']['code']=="SENSOR" and x['component']!=None][0]
                    st.write("found sensor:",sensorComp['component']['serialNumber'])
                    anaDict['Type']=sensorComp['type']['code']
                #anaDict['Type']="ATLAS18SS"

                ### get test info.
                resDict=SENSOR_IV_Analysis_module.Analyze_IV(anaDict)
                pageDict['resDict']=resDict

                if resDict['PassIV']!="Yes":
                    st.write("### :broken_heart: **Analysis failed**")
                    st.write("flags:",resDict['Flags'])
                    pageDict['testSchema']['passed']=False
                else:
                    st.write("### :green_heart: **Analysis passed**")
                    pageDict['testSchema']['passed']=True

                # update schema with derived results
                pageDict['testSchema']['problems']=False # what to do?
                # properties
                pageDict['testSchema']['properties']['ALGORITHM_VERSION']=resDict['AlgorithmVersion']
                # results (input)
                pageDict['testSchema']['results']['HUMIDITY']=float(df_header.query('key=="Humidity"')['value'].values[0])
                pageDict['testSchema']['results']['TEMPERATURE']=float(df_header.query('key=="Temperature"')['value'].values[0])
                pageDict['testSchema']['results']['VOLTAGE']=df_data['voltage'].astype(float).to_list()
                pageDict['testSchema']['results']['SHUNT_VOLTAGE']=df_data['shunt'].astype(float).to_list()
                # results (derived)
                pageDict['testSchema']['results']['CURRENT']=df_data['current'].astype(float).to_list()
                pageDict['testSchema']['results']['RMS_STABILITY']=resDict['RMS_Stability[nA]']
                pageDict['testSchema']['results']['VBD']=float(resDict['MicroDischargeV'])
                pageDict['testSchema']['results']['I_500V']=resDict['IDataAtTestV']

            # derived values
            st.write("### Derived values")
            st.write("Analysis output (based on [here](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/blob/master/strips/sensors/sensor_tests_interfacing_functions_itkdb/SENSOR_IV_Analysis.py))")
            st.write('passed',pageDict['testSchema']['passed'])
            for x in ['RMS_STABILITY','VBD','I_500V']:
                st.write(x,":",pageDict['testSchema']['results'][x])

            if st.checkbox("Show fill analysis results?"):
                st.write(pageDict['resDict'])

        st.write("---")
        #####################
        ### Review and edit
        ##################### 
        
        st.write("### Review and edit schema:")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write("### Review Schema")
            # st.write(pageDict['testSchema'])
            st.json(pageDict['testSchema'], expanded=False)

        infra.ToggleButton(pageDict,'toggleText','Convert all values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        #####################
        ### Registration
        ##################### 
        st.write("## Test Registration")
        
        st.write(f"Uploading {pageDict['testSchema']['testType']} to {pageDict['testSchema']['component']} @ {pageDict['stage']}")
        # check existing component tests
        chnx.StageCheck(pageDict)

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")
            ### set stage
 