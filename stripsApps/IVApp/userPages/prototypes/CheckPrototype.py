#!/usr/bin/env python
# CheckPrototype.py - Functions to authenticate and get the template JSON for upload for metrology
# Usage: From cmd line with -s flag will refresh the templates to current guidelines. Option to use in analysis package to get up to date prototype

import itkdb
import json
import os
import getpass
import time
import sys

# Function to authenticate, input of accessCodes not mandatory. 
# To prevent putting in access codes everytime, put the access codes in the environment variables below
def authentication(accessCode1 = None, accessCode2 = None):
  print("Authenticating...")

  if accessCode1 is None or accessCode2 is None:
    # Check for token in environment    
    ITKDB_ACCESS_CODE1 = os.getenv('ITKDB_ACCESS_CODE1', None)
    ITKDB_ACCESS_CODE2 = os.getenv('ITKDB_ACCESS_CODE2', None)

    # If not in environment ask for password
    if ITKDB_ACCESS_CODE1 is None or  ITKDB_ACCESS_CODE2 is None:
      auth_data = {}
      auth_data['accessCode1'] = getpass.getpass("AccessCode1: ")
      auth_data['accessCode2'] = getpass.getpass("AccessCode2: ")
      user = itkdb.core.User(auth_data['accessCode1'], auth_data['accessCode2'])
      client = itkdb.Client(user=user)

    else:
      client = itkdb.Client()
      
  # if access codes passed directly 
  else:
    user = itkdb.core.User(accessCode1, accessCode2)
    client = itkdb.Client(user=user)
  
  client.user.authenticate()

  print("Authentication Finished")  
  return client


# Fetch the template JSON as a dictionary and give option to save if needed
def fetchDto(test_type, verbose = None, save = None):
  # Get the client
  client = authentication()

  supported_tt = ["MODULE_METROLOGY", "HYBRID_METROLOGY", "MODULE_BOW"]

  if test_type == "ALL":
    for tc in supported_tt: 
      dto = client.get("generateTestTypeDtoSample", json={'code': '{}'.format(tc), 'project':'S', 'componentType':"{}".format(tc.split("_")[0]), 'requiredOnly': 0})
      if verbose:
        print("Test type: {0} \n {1}".format(tc, j_dump))
 
      if save:
        with open("prototype_{0}_{1}.txt".format(tc, time.strftime("%Y%m%d-%H%M%S")), "w") as file:
          j_dump = json.dumps(dto, indent = 4)
          file.write(j_dump)
        file.close()

  else:
    if test_type not in supported_tt:
      print("test type requested not supported in Dto fetcher")
      sys.exit()

    dto = client.get("generateTestTypeDtoSample", json={'code': '{}'.format(test_type), 'project':'S', 'componentType':"{}".format(test_type.split("_")[0]), 'requiredOnly': 0})
    j_dump = json.dumps(dto, indent = 4)
    if verbose:
        print("Test type: {0} \n {1}".format(test_type, j_dump))
    if save:
        with open("prototype_{0}_{1}.txt".format(test_type, time.strftime("%Y%m%d-%H%M%S")), "w") as file:
          j_dump = json.dumps(dto, indent = 4)
          file.write(j_dump)
        file.close()
     
  return dto

if __name__ == "__main__":

  import argparse
  #For CMD line interface
  parser = argparse.ArgumentParser(description = "Script to update prototype JSONS using DB and also to authenticate if needed")
  parser._action_groups.pop()  
  optional = parser.add_argument_group("optional arguments")
  optional.add_argument("-t", "--type", dest = "test_type", type = str, choices = ["MODULE_METROLOGY", "HYBRID_METROLOGY", "MODULE_BOW", "ALL"], default = "ALL", help = "Test type you want to update")
  optional.add_argument("-v", "--verbose", dest = "verbose", action = "store_true", help = "Print fetched JSONs")
  optional.add_argument("-s", "--save", dest = "save", action = "store_true", help = "Save JSON as file")
  
  args = parser.parse_args()

  fetchDto(args.test_type, args.verbose, args.save)
  
    

