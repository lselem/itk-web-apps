### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import altair as alt
import os
import copy
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from .CommonCode import GetGleanListWB
from .CommonCode import testerOptions
import commonCode.codeChunks as chnx
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * Select bond index",
        " * Select defect",
		"  - Standard list or add new",
	    " * Add to list"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("WB Upload", ":microscope: Wirebond Upload", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="HYBRID_ASSEMBLY"
        if "testType" not in pageDict.keys():
            pageDict['testType']="WIRE_BONDING"
        if "stage" not in pageDict.keys():
            pageDict['stage']="WIRE_BONDING"
        if "project" not in pageDict.keys():
            pageDict['project']="S"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testType'])
        st.dataframe(df_stageTest)

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")

        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testType']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':False})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ### preset failure modes
        failList=[{'code':"1",'value':"Wire bond failure"},
                    {'code':"2",'value':"ASIC pad failure"},
                    {'code':"3",'value':"Substrate failure"}]
        

        st.write("---")
        st.write("## Select component")
        
        chnx.SelectComponentChunk(pageDict)

        if 'comp' not in pageDict.keys():
            st.write("Enter identifier to get component information")
            st.stop()

        ### set-up test schema
        if "testSchema" not in pageDict.keys() or st.button("re-set schema"):
            pageDict['testSchema']=copy.deepcopy(pageDict['origSchema'])
            pageDict['testSchema']['component']=pageDict['comp']['serialNumber']
            pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
            # update json
            ### general
            pageDict['testSchema']['passed']=True
            pageDict['testSchema']['problems']=False
            st.write(pageDict['testSchema']['testType']+" schema reset")

        # st.write(pageDict['comp'])


        st.write("---")
        st.write("## Input bad bonds information")

        inputOpt= st.radio('Select data input method:',["File input","Manual input"])

        if "file" in inputOpt.lower():

            ## drag and drop method
            pageDict['file']= st.file_uploader("Upload data file", type=["csv"])
            if st.session_state.debug: st.write(pageDict['file'])

            if pageDict['file'] is not None:
                headerList, dataList = GetGleanListWB(pageDict['file']) #,names=list('abcd'))
                df_header = pd.DataFrame(headerList, columns=['key','value','alt1','alt2'])
                df_data = pd.DataFrame(dataList, columns=['section','channel','code','alt2'])
            else:
                st.write("No data file set")
                ### from EDMS document (p.29): https://edms.cern.ch/ui/file/2520891/2.2/QC_Bond_Pulling.pdf

                filePath=os.path.realpath(__file__)
                exampleFileName="MODULE_WIRE_BONDING_example.csv"
                if st.session_state.debug:
                    st.write("looking in:",filePath[:filePath.rfind('/')])
                st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
                #st.write("See p.2 [EDMS document](https://edms.cern.ch/ui/file/2520890/3.2/Bonding-Procedures_HV-tab.pdf) for details on format")
                for k in ['testSchema', 'toggleEdit', 'toggleText']:
                    try:
                        pageDict.pop(k)
                    except KeyError:
                        pass
                st.stop()

            if st.session_state.debug:
                st.write("Gleaned lists")
                st.write(headerList)
                st.write(dataList)

            for i, col in enumerate(df_header.columns):
                df_header.iloc[:, i] = df_header.iloc[:, i].str.replace('"', '')
            for i, col in enumerate(df_data.columns):
                df_data.iloc[:, i] = df_data.iloc[:, i].str.replace('"', '')
            stTrx.Stringify(df_data)

            # show gleaned information in dataframes
            st.write("### header info.")
            st.dataframe(df_header)
            st.write("### data info.")
            st.dataframe(df_data)

            st.write("### :wrench: Data extraction")
            ### standard
            for sk in ["component","runNumber"]:
                try:
                    # st.write(df_header.query('key=="'+pk+'"')['value'])
                    pageDict['testSchema'][sk]=df_header.query('key=="'+sk+'"')['value'].values[0]
                    st.write("got:",sk)
                except:
                    st.write("Cannot find "+sk+" key")

            ### date
            dateStr=df_header.query('key=="date"')['value'].values[0]
            dateStr+="_"+df_header.query('key=="time"')['value'].values[0]
            try:                                                                    
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr,"%d/%m/%Y_%H:%M:%S")
                st.write("got:",date)
            except:
                st.write("Issue extracting date from:",dateStr)

            ### get properties
            for pk in pageDict['testSchema']['properties'].keys():
                try:
                    # st.write(df_header.query('key=="'+pk+'"')['value'])
                    pageDict['testSchema']['properties'][pk]=df_header.query('key=="'+pk+'"')['value'].values[0]
                    st.write("got",pk)
                except:
                    st.write("Cannot find "+pk+" property key")

            st.write("### :bar_chart: Bond fail visualisation")
            visChart=alt.Chart(df_data).mark_bar().encode(
                    x='code:N',
                    y=alt.Y('count(code):Q',scale=alt.Scale(zero=False)),
                    color='section:N',
                    tooltip=['code:N','count(code):Q','section:N']
            ).interactive()
            st.altair_chart(visChart, use_container_width=True)

            for rk in [rk for rk in pageDict['testSchema']['results'].keys() if "total" not in rk.lower()]:
                df_sect=df_data.query('section=="'+rk+'"')
                if df_sect.empty:
                    st.write(rk+" section not found")
                    continue
                if pageDict['testSchema']['results'][rk]==None:
                    pageDict['testSchema']['results'][rk]={}
                for i,row in df_sect.iterrows():
                    pageDict['testSchema']['results'][rk][row['channel']]=next((item['value'] for item in failList if item['code']==str(row['code'])),None)
                

        else:

            ### result dictionaries
            failDictKeys=[rk for rk in pageDict['testSchema']['results'].keys() if "total" not in rk.lower()]
            resDict= st.radio('Select results dictionary',failDictKeys)

            ### set failure dictionary
            if type(pageDict['testSchema']['results'][resDict])!=type({}) or st.button("reset failures"):
                pageDict['testSchema']['results'][resDict]={}


            bnVal=st.text_input('Bond number:')
            # add other failure mode
            infra.ToggleButton(pageDict,'tog_other',"Use other failure mode")
            if pageDict['tog_other']:
                bnMode=st.text_input('Failure mode:')
            else:
                bnMode=st.selectbox('Select failure mode:',[fl['value'] for fl in failList])

            if st.button("Add failed bond"):
                if bnVal in pageDict['testSchema']['results'][resDict].keys():
                    st.write("Over written.")
                else:
                    st.write("Added!")
                pageDict['testSchema']['results'][resDict][bnVal]=bnMode
            

            st.write("Current list:",pageDict['testSchema']['results'][resDict])
            infra.ToggleButton(pageDict,'tog_remove',"remove item from list?")
            if pageDict['tog_remove']:
                remVal=st.selectbox('Select item to remove:',pageDict['testSchema']['results'][resDict].keys())
                if st.button("remove "+remVal):
                    del pageDict['testSchema']['results'][resDict][remVal]
                    st.write("done. uncheck box")

            # if st.button("Add list to "+resDict):
            #     pageDict['testSchema']['results'][resDict]=pageDict['failDict']
            #     st.write("Added list to",resDict)

            pageDict['testSchema']['results']["TOTAL_FAILED_ASIC_BACKEND"]=0
            for fk in failDictKeys:
                if type(pageDict['testSchema']['results'][fk])==type({}):
                    pageDict['testSchema']['results']["TOTAL_FAILED_ASIC_BACKEND"]+=len(pageDict['testSchema']['results'][fk].keys())
            st.write("Current total fails (only added lists counted):",pageDict['testSchema']['results']["TOTAL_FAILED_ASIC_BACKEND"])


        st.write("---")

        chnx.ReviewAndEditChunk(pageDict, 'testSchema')

        infra.ToggleButton(pageDict,'tog_file',"Add file?")
        if pageDict['tog_file']:
            st.write("### File Attachment")
            st.write("Please limit file size < 1 Mb")
            pageDict['addFile']= st.file_uploader("Add file", type=["pdf","png","jpg"])
            
            if pageDict['addFile']==None:
                st.write("Please upload file")
                st.stop()

        st.write("---")

        st.write("### Upload to PDB")
        st.write("Add comment after registration if required")

        # check existing component tests
        chnx.StageCheck(pageDict)

        # upload(!)
        if pageDict['tog_file']:
            chnx.RegChunk(pageDict, "TestRun", "testSchema", "addFile")
        else:
            chnx.RegChunk(pageDict, "TestRun", "testSchema")