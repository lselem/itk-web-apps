### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from .CommonCode import EditJson, SelectCheck
from .CommonCode import GetGleanListEDMS, GetGleanListSCIPP
from .CommonCode import testerOptions
import commonCode.codeChunks as chnx
import commonCode.PDBTricks as pdbTrx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ pull test file",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Pull Test Upload", ":microscope: Upload Pull Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="CICOREL_CARD"
        if "testType" not in pageDict.keys():
            pageDict['testType']="PULL_TEST"
        if "stage" not in pageDict.keys():
            pageDict['stage']="NEW"
        if "project" not in pageDict.keys():
            pageDict['project']="S"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testType'])
        st.dataframe(df_stageTest)

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")

        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testType']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':True})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["csv"])
        if st.session_state.debug: st.write(pageDict['file'])

        if pageDict['file'] is not None:
            headerList, dataList = GetGleanListSCIPP(pageDict['file']) #,names=list('abcd'))
            try: # try SCIPP version
                df_header = pd.DataFrame(headerList, columns=['key','value','unit'])
                df_data = pd.DataFrame(dataList, columns=['test','grade','force'])
            except ValueError: # try EDMS version
                st.write("alternative reader go!")
                pageDict['file'].seek(0)
                headerList, dataList = GetGleanListEDMS(pageDict['file'])
                df_header = pd.DataFrame(headerList, columns=['key','value','unit','other'])
                df_data = pd.DataFrame(dataList, columns=['other','test','grade','force'])
        else:
            st.write("No data file set")
            ### from EDMS document (p.29): https://edms.cern.ch/ui/file/2520891/2.2/QC_Bond_Pulling.pdf


            filePath=os.path.realpath(__file__)
            exampleFileName="pullTestExample_EDMS_edit.csv"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
            st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
            st.write("See p.29 [EDMS document](https://edms.cern.ch/ui/file/2520891/2.2/QC_Bond_Pulling.pdf) for details on format")
            for k in ['testSchema', 'toggleEdit', 'toggleText']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()

        ### US date format toggle
        infra.ToggleButton(pageDict,'toggleUS','adapt US date format? (%m/%d/%Y->%d/%m/%Y)')

        if st.session_state.debug:
            st.write("Gleaned lists")
            st.write(headerList)
            st.write(dataList)

        for i, col in enumerate(df_header.columns):
            df_header.iloc[:, i] = df_header.iloc[:, i].str.replace('"', '')
        for i, col in enumerate(df_data.columns):
            df_data.iloc[:, i] = df_data.iloc[:, i].str.replace('"', '')

        # show gleaned information in dataframes
        st.write("### header info.")
        st.dataframe(df_header)
        st.write("### data info.")
        st.dataframe(df_data)

        st.write("### Pull Test visualisation")
        visChart=alt.Chart(df_data).mark_boxplot().encode(
                x='grade:N',
                y=alt.Y('force:Q',scale=alt.Scale(zero=False)),
                color='grade:N',
                tooltip=['type:Q','force:Q']
        ).interactive()
        st.altair_chart(visChart, use_container_width=True)


        if "testSchema" not in pageDict.keys() or st.button("re-read file"):
            pageDict['testSchema']=pageDict['origSchema']
            pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
            # update json
            ### general
            pageDict['testSchema']['passed']=True
            pageDict['testSchema']['problems']=False
            dateStr=df_header.query('key=="DATE"')['value'].values[0]+"@"+df_header.query('key=="TIME"')['value'].values[0]
            if pageDict['toggleUS']:
                try:
                    dateStr= datetime.strptime(dateStr, "%m/%d/%y@%H:%M:%S").strftime("%d/%m/%y@%H:%M:%S")
                except ValueError:
                    dateStr= datetime.strptime(dateStr, "%m/%d/%Y@%H:%M:%S").strftime("%d/%m/%Y@%H:%M:%S")
            try:
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr,"%d/%m/%y@%H:%M:%S")
            except ValueError:
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr,"%d/%m/%Y@%H:%M:%S")
            try:
                pageDict['testSchema']['institution']= df_header.query('key=="INSTITUTE"')['value'].values[0]
            except IndexError:
                pass

        # properties
        pageDict['testSchema']['properties']['NUMBER_WIRES']=int(df_data['test'].count()-df_data[(df_data['grade']=="6")]['grade'].count())
        pageDict['testSchema']['properties']['OPERATOR']=df_header.query('key=="OPERATOR"')['value'].values[0]

        # results
        pageDict['testSchema']['results']['PULL_GRADE']=df_data['grade'].astype(int).tolist()
        pageDict['testSchema']['results']['PULL_STRENGTH']=df_data['force'].astype(float).tolist()

        ### derived
        st.write("### Derived values")
        pageDict['testSchema']['results']['HEEL_BREAKS']=100*df_data[(df_data['grade']=="1") | (df_data['grade']=="2") | (df_data['grade']=="3")]['grade'].count()/df_data['grade'].count()
        pageDict['testSchema']['results']['MAXIMUM_STRENGTH']=df_data['force'].astype(float).max()
        pageDict['testSchema']['results']['MEAN']=df_data['force'].astype(float).mean()
        pageDict['testSchema']['results']['MINIMUM_STRENGTH']=df_data['force'].astype(float).min()
        pageDict['testSchema']['results']['STANDARD_DEVIATION']=df_data['force'].astype(float).std()
        pageDict['testSchema']['results']['TOO_LOW']=len([x for x in df_data['force'].astype(float).tolist() if x<5.0])
        for x in ["HEEL_BREAKS","STANDARD_DEVIATION","TOO_LOW"]:
            st.write(x,"check:",pageDict['testSchema']['results'][x])

        ### add PULL_TEST_MACHINE (optional test parameter)
        if "PULL_TEST_MACHINE" not in pageDict['testSchema']['properties'].keys():
            try:
                st.write("Found Pull test Machine:",df_header.query('key=="TESTER"')['value'].values[0])
                pageDict['testSchema']['properties']['PULL_TEST_MACHINE']=df_header.query('key=="TESTER"')['value'].values[0]
            except KeyError:
                pass
        infra.ToggleButton(pageDict,'toggleTester','Add test machine information? (not required for upload)')
        try:
            st.write("Current test machine set:",pageDict['testSchema']['properties']['PULL_TEST_MACHINE'])
        except KeyError:
            st.write("Current test machine is not set.")
        if pageDict['toggleTester']:
            infra.SelectBox(pageDict,'testMachine',testerOptions,'Select pull test machine type:')
            pageDict['testSchema']['properties']['PULL_TEST_MACHINE']=pageDict['testMachine']

        # st.write("### NB User Beware!")
        # st.write("**Analysis is not yet implemented so please set the pass/fail value (_default pass_)**")

        ### Analysis checks
        st.write("### Analysis checks")
        failAna=False
        ### Minimum sample size which is defined for each test surface
        # not implemented
        ### Minimum mean pull strength of 8g
        if float(pageDict['testSchema']['results']['MEAN'])< 8.0:
            st.write("**Failed** *mean strength* :no_entry_sign:")
            failAna=True
        else: st.write("**Passed** *mean strength* :heavy_check_mark:")
        ### Minimum 90% heel breaks
        if float(pageDict['testSchema']['results']['HEEL_BREAKS'])< 90:
            st.write("**Failed** *heel breaks (inc. midspan)* :no_entry_sign:")
            failAna=True
        else: st.write("**Passed** *heel breaks (inc. midspan)* :heavy_check_mark:")
        ### Maximum standard deviation of 1.5g
        if float(pageDict['testSchema']['results']['STANDARD_DEVIATION'])> 1.5:
            st.write("**Failed** *standard deviation* :no_entry_sign:")
            failAna=True
        else: st.write("**Passed** *standard deviation* :heavy_check_mark:")
        ### Minimum single wire pull strength of 5g
        if int(pageDict['testSchema']['results']['TOO_LOW'])> 0:
            st.write("**Failed** *minimum single pull (5g)* :no_entry_sign:")
            failAna=True
        else: st.write("**Passed** *minimum single pull* :heavy_check_mark:")

        if failAna:
            st.write("### :broken_heart: **Analysis failed**")
            pageDict['testSchema']['passed']=False
        else:
            st.write("### :green_heart: **Analysis passed**")

        ### Review and edit
        st.write("### Review and edit schema:")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write("### Review Schema")
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        st.write("## Test Registration")

        # check existing component tests
        chnx.StageCheck(pageDict)


        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")

        # if st.button("Upload Test"):
        #     ### set stage
        #     try:
        #         pageDict['setVal']=DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':pageDict['testSchema']['component'], 'stage':pageDict['stage']})
        #         st.write("### **Successful stage set** (",pageDict['stage'],") for:",pageDict['setVal']['id'])
        #         # df_set=pd.json_normalize(pageDict['setVal'], sep = "_")
        #         # st.dataframe(df_set)
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: Stage (",pageDict['stage'],")setting **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     ### upload data
        #     try:
        #         pageDict['upVal']=DBaccess.DbPost(st.session_state.myClient,'uploadTestRunResults', pageDict['testSchema'])
        #         st.write("### **Successful Upload**:",pageDict['upVal']['componentTestRun']['date'])
        #         st.balloons()
        #         st.write(pageDict['upVal'])
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: Update **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     ### upload file
        #     try:
        #         ### faff to creat file for binary upload - reading dierect from buffer loses name(?!)
        #         tmpPath="/tmp"
        #         fnew=open(tmpPath+"/"+pageDict['file'].name, "w")
        #         fnew.write(pageDict['file'].getvalue().decode("utf-8"))
        #         fnew.close()
        #         if st.session_state.debug:
        #             st.write([f for f in os.listdir(tmpPath) if os.path.isfile(os.path.join(tmpPath, f))])
        #             st.write("use:",tmpPath+"/"+pageDict['file'].name)
        #         f = open(tmpPath+"/"+pageDict['file'].name, "rb")
        #         pageDict['attVal']=st.session_state.myClient.post('createTestRunAttachment', data=dict(testRun=pageDict['upVal']['testRun']['id'],title="Pull test results",type="file"), files=dict(data=f))
        #         st.write("### **Successful File Upload**:",pageDict['attVal']['filename'])
        #     except itkX.BadRequest as b:
        #         st.write("### :no_entry_sign: File Upload **Unsuccessful**")
        #         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #
        # if 'upVal' in pageDict.keys():
        #     try:
        #         st.write("last successful _upload_:",pageDict['upVal']['testRun']['id'])
        #         if st.button("Delete testrun"):
        #             ### delete data
        #             try:
        #                 pageDict['delVal'] = DBaccess.DbPost(st.session_state.myClient,'deleteTestRun', {'testRun':pageDict['upVal']['testRun']['id']})
        #                 st.write("### **Successful Deletion**:",pageDict['delVal']['testRun']['id'])
        #                 st.balloons()
        #                 st.write(pageDict['delVal'])
        #             except itkX.BadRequest as b:
        #                 st.write("### :no_entry_sign: Deletion unsuccessful")
        #                 st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        #     except KeyError:
        #         pass
        # if 'delVal' in pageDict.keys():
        #     try:
        #         st.write("last successful _deletion_:",pageDict['delVal']['testRun']['id'])
        #     except KeyError:
        #         pass
