### Author: Dengfeng Zhang (dengfeng.zhang@cern.ch)
import os
import json
import itkdb
import core.DBaccess as DBaccess
import itkdb.exceptions as itkX
import csv
from datetime import *

def check_wire_conflict(FailedWires, RepairedWires):
  if FailedWires !=None and RepairedWires != None and len(FailedWires)!=0 and len(RepairedWires) !=0:
    for failed_wire in FailedWires.keys():
      for repair_wire in RepairedWires.keys():
        if repair_wire == failed_wire:
          return True
  return False

def convert_wirenumber_to_stripnumber(Wires1, Wires2):
  Wires = {}
  if Wires1==None and Wires2==None: return Wires

  if Wires1!=None:
    for key in list(Wires1.keys()):
      name = str((int(key)-1)*2)
      description = Wires1[key]
      Wires[name] = description
  if Wires2!=None:
    for key in list(Wires2.keys()):
      name = str(int(key)*2-1)
      description = Wires2[key]
      Wires[name] = description

  return Wires

def getMAXCONTUNCONROW(Wires):
  if len(Wires)==0 or Wires==None:
    return 0

  MAXCONTUNCONROW = 1

  WireIndexs = list(Wires.keys())
  WireIndexs.sort()
  CONTUNCONROW = 1
  n = 0
  while n< len(WireIndexs)-1:
    m = n+1
    # 641 is the last wire of each segment
    if int(WireIndexs[m])-int(WireIndexs[n]) == 1:
      if int(WireIndexs[n]) == 641 and int(WireIndexs[m]) ==642:
        if CONTUNCONROW>MAXCONTUNCONROW:
          MAXCONTUNCONROW=CONTUNCONROW
        CONTUNCONROW = 1
      else:
        CONTUNCONROW = CONTUNCONROW+1
        if CONTUNCONROW>MAXCONTUNCONROW:
          MAXCONTUNCONROW=CONTUNCONROW
    else:
      CONTUNCONROW = 1
    n = n+1

  return MAXCONTUNCONROW

def getMAXCONTUNCONSTRIP(Strips):
  if len(Strips)==0:
    return 0

  MAXCONTUNCONSTRIP = 1

  StripIndexs = list(Strips.keys())
  StripIndexs.sort()
  CONTUNCONSTRIP = 1
  n = 0
  while n< len(StripIndexs)-1:
    m = n+1
    # 1282 is the last wire of each segment
    if int(StripIndexs[m])-int(StripIndexs[n]) == 1:
      if int(StripIndexs[n]) == 1282 and int(StripIndexs[m]) ==1283:
        if CONTUNCONSTRIP>MAXCONTUNCONSTRIP:
          MAXCONTUNCONSTRIP=CONTUNCONSTRIP
        CONTUNCONSTRIP = CONTUNCONSTRIP+1
      else:
        CONTUNCONSTRIP = CONTUNCONSTRIP+1
        if CONTUNCONSTRIP>MAXCONTUNCONSTRIP:
          MAXCONTUNCONSTRIP=CONTUNCONSTRIP
    else:
      CONTUNCONSTRIP = 1
    n = n+1

  return MAXCONTUNCONSTRIP

def checkwire(wiredata):
  if len(wiredata)<3:
    print("Wire Number or comment is missing!!!!!  Check your input file")
    print(wiredata)
    return False
  try:
    int(wiredata[1])
  except ValueError:
    print("Can not get the channel number!!!! Check your input file")
    print(wiredata)
    return False
  return True

def getdata(csvdata, basicInfo, FAILED_FRONTEND_ROW1, FAILED_FRONTEND_ROW2, FAILED_FRONTEND_ROW3, FAILED_FRONTEND_ROW4, REPAIRED_FRONTEND_ROW1, REPAIRED_FRONTEND_ROW2, REPAIRED_FRONTEND_ROW3, REPAIRED_FRONTEND_ROW4, FAILED_HYBRID_TO_PB, REPAIRED_HYBRID_TO_PB, FAILED_MODULE_TO_FRAME, REPAIRED_MODULE_TO_FRAME):

  getstatus = True
  for row in csvdata:
    if row[0].startswith("#"):
      continue
    elif row[0].startswith("component"):
      basicInfo["component"] = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
    elif row[0].startswith("institution"):
      basicInfo["institution"] = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
    elif row[0].startswith("runNumber"):
      basicInfo["runNumber"] = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
    elif row[0].startswith("date"):
      basicInfo["date"] = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
    elif row[0].startswith("time"):
      basicInfo["time"] = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
    elif row[0].startswith("OPERATOR"):
      basicInfo["OPERATOR"] = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
    elif row[0].startswith("BOND_MACHINE"):
      basicInfo["BOND_MACHINE"] = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
    elif row[0].startswith("FAILED_FRONTEND_ROW1"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in FAILED_FRONTEND_ROW1.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      FAILED_FRONTEND_ROW1[key] = comment
    elif row[0].startswith("FAILED_FRONTEND_ROW2"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in FAILED_FRONTEND_ROW2.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      FAILED_FRONTEND_ROW2[key] = comment
    elif row[0].startswith("FAILED_FRONTEND_ROW3"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in FAILED_FRONTEND_ROW3.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      FAILED_FRONTEND_ROW3[key] = comment
    elif row[0].startswith("FAILED_FRONTEND_ROW4"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in FAILED_FRONTEND_ROW4.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      FAILED_FRONTEND_ROW4[key] = comment
    elif row[0].startswith("REPAIRED_FRONTEND_ROW1"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in REPAIRED_FRONTEND_ROW1.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      REPAIRED_FRONTEND_ROW1[key] = comment
    elif row[0].startswith("REPAIRED_FRONTEND_ROW2"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in REPAIRED_FRONTEND_ROW2.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      REPAIRED_FRONTEND_ROW2[key] = comment
    elif row[0].startswith("REPAIRED_FRONTEND_ROW3"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in REPAIRED_FRONTEND_ROW3.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      REPAIRED_FRONTEND_ROW3[key] = comment
    elif row[0].startswith("REPAIRED_FRONTEND_ROW4"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in REPAIRED_FRONTEND_ROW4.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      REPAIRED_FRONTEND_ROW4[key] = comment
    elif row[0].startswith("FAILED_HYBRID_TO_PB"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in FAILED_HYBRID_TO_PB.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      FAILED_HYBRID_TO_PB[key] = comment
    elif row[0].startswith("REPAIRED_HYBRID_TO_PB"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in REPAIRED_HYBRID_TO_PB.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      REPAIRED_HYBRID_TO_PB[key] = comment
    elif row[0].startswith("FAILED_MODULE_TO_FRAME"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in FAILED_MODULE_TO_FRAME.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      FAILED_MODULE_TO_FRAME[key] = comment
    elif row[0].startswith("REPAIRED_MODULE_TO_FRAME"):
      if not checkwire(row):
        getstatus = False
        break
      if row[1] in REPAIRED_MODULE_TO_FRAME.keys():
        getstatus = False
        print("Failed/Repaired Channel has been reginstered!!! Check your input file.")
        break
      key = row[1].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      comment = row[2].strip(" ").strip("    ").rstrip(" ").rstrip("    ")
      REPAIRED_MODULE_TO_FRAME[key] = comment
  return getstatus

def module_wirebond(client, csvdata):

  if not client:
    print("No ITkDB Client!!!!")
    return None
  if not csvdata or len(csvdata)==0:
    print("No cvs data input!!!!")
    return None

  user = DBaccess.DbGet(client,'getUser', {'userIdentity': client.user.identity})
  UserName = user.get("firstName")+" "+user.get("lastName")

  #jsondata = json.dumps({'component': '...', 'testType': 'MODULE_WIRE_BONDING', 'institution': '...', 'runNumber': '...', 'date': '2022-02-04T16:40:18.021Z', 'passed': True, 'problems': False, 'properties': {'OPERATOR': '', 'BOND_MACHINE': ''}, 'results': {'FAILED_FRONTEND_ROW1': None, 'TOTAL_PERC_UNCON_SENSOR_CHAN': None, 'REPAIRED_FRONTEND_ROW1': None, 'MAX_CONT_UNCON_ROW1': None, 'FAILED_FRONTEND_ROW2': None, 'REPAIRED_FRONTEND_ROW2': None, 'MAX_CONT_UNCON_ROW2': None, 'FAILED_FRONTEND_ROW3': None, 'REPAIRED_FRONTEND_ROW3': None, 'MAX_CONT_UNCON_ROW3': None, 'FAILED_FRONTEND_ROW4': None, 'REPAIRED_FRONTEND_ROW4': None, 'MAX_CONT_UNCON_ROW4': None, 'FAILED_HYBRID_TO_PB': None, 'REPAIRED_HYBRID_TO_PB': None, 'FAILED_MODULE_TO_FRAME': None, 'REPAIRED_MODULE_TO_FRAME': None, 'MAX_UNCON_SENSOR_CHAN': None}})
  jsondata = {'component': '...', 'testType': 'MODULE_WIRE_BONDING', 'institution': '...', 'runNumber': '...', 'date': '2022-02-04T16:40:18.021Z', 'passed': True, 'problems': False, 'properties': {'OPERATOR': '', 'BOND_MACHINE': ''}, 'results': {'FAILED_FRONTEND_ROW1': None, 'TOTAL_PERC_UNCON_SENSOR_CHAN': None, 'REPAIRED_FRONTEND_ROW1': None, 'MAX_CONT_UNCON_ROW1': None, 'FAILED_FRONTEND_ROW2': None, 'REPAIRED_FRONTEND_ROW2': None, 'MAX_CONT_UNCON_ROW2': None, 'FAILED_FRONTEND_ROW3': None, 'REPAIRED_FRONTEND_ROW3': None, 'MAX_CONT_UNCON_ROW3': None, 'FAILED_FRONTEND_ROW4': None, 'REPAIRED_FRONTEND_ROW4': None, 'MAX_CONT_UNCON_ROW4': None, 'FAILED_HYBRID_TO_PB': None, 'REPAIRED_HYBRID_TO_PB': None, 'FAILED_MODULE_TO_FRAME': None, 'REPAIRED_MODULE_TO_FRAME': None, 'MAX_UNCON_SENSOR_CHAN': None}}

  basicInfo = {}
  FAILED_FRONTEND_ROW1 = {}
  FAILED_FRONTEND_ROW2 = {}
  FAILED_FRONTEND_ROW3 = {}
  FAILED_FRONTEND_ROW4 = {}
  REPAIRED_FRONTEND_ROW1 = {}
  REPAIRED_FRONTEND_ROW2 = {}
  REPAIRED_FRONTEND_ROW3 = {}
  REPAIRED_FRONTEND_ROW4 = {}
  FAILED_HYBRID_TO_PB = {}
  REPAIRED_HYBRID_TO_PB = {}
  FAILED_MODULE_TO_FRAME = {}
  REPAIRED_MODULE_TO_FRAME = {}

  getdatastatus = getdata(csvdata, basicInfo, FAILED_FRONTEND_ROW1, FAILED_FRONTEND_ROW2, FAILED_FRONTEND_ROW3, FAILED_FRONTEND_ROW4, REPAIRED_FRONTEND_ROW1, REPAIRED_FRONTEND_ROW2, REPAIRED_FRONTEND_ROW3, REPAIRED_FRONTEND_ROW4, FAILED_HYBRID_TO_PB, REPAIRED_HYBRID_TO_PB, FAILED_MODULE_TO_FRAME, REPAIRED_MODULE_TO_FRAME)
  if not getdatastatus:
    print("Failed the get the wire bonding data!!!!!")
    return None

  # check if there is overlap between failed and repaired
  if check_wire_conflict(FAILED_FRONTEND_ROW1, REPAIRED_FRONTEND_ROW1):
    print("Failed/Repaired wires are registered multi times!!!!!")
    return None
  if check_wire_conflict(FAILED_FRONTEND_ROW2, REPAIRED_FRONTEND_ROW2):
    print("Failed/Repaired wires are registered multi times!!!!!")
    return None
  if check_wire_conflict(FAILED_FRONTEND_ROW3, REPAIRED_FRONTEND_ROW3):
    print("Failed/Repaired wires are registered multi times!!!!!")
    return None
  if check_wire_conflict(FAILED_FRONTEND_ROW4, REPAIRED_FRONTEND_ROW4):
    print("Failed/Repaired wires are registered multi times!!!!!")
    return None

  component = basicInfo["component"]
  institution = basicInfo["institution"]
  runNumber = basicInfo["runNumber"]
  date = basicInfo["date"]
  time = basicInfo["time"]
  OPERATOR = basicInfo["OPERATOR"]
  BOND_MACHINE = basicInfo["BOND_MACHINE"]

  if not component or component == "":
    print("component Serial Number is missing!!!!")
    return None
  if not institution or institution == "":
    print("institution is missing!!!!")
    return None

  try:
    module = DBaccess.DbGet(client,'getComponent', {'component': component})
  except Exception:
    print('Failed to get component!!!')
    return None

  currentStage = module['currentStage']['code']
  moduleType = module['type']['code']
  testType = "MODULE_WIRE_BONDING"

  if institution != module['currentLocation']['code']:
    print("This module is not at your institution!!!!")
    return None
  if module['currentStage']['code'] != "BONDED":
    print('This module is not at BONDED stage!!! Update the its stage!!!!')
    return None

  if not date or not time or date == "" or time == "":
    date = datetime.now(timezone.utc).strftime("%Y-%m-%d") 
    time = datetime.now(timezone.utc).strftime("%H:%M:%S") 

  if not OPERATOR or OPERATOR == "":
    OPERATOR = UserName
  if not BOND_MACHINE or BOND_MACHINE == "":
    BOND_MACHINE = "HESSE BONDJET BJ820"
  if not runNumber or runNumber == "":
    runNumber = 1

  pass_test = True
  # FE wires
  nfailedwires_row1 = 0
  nfailedwires_row2 = 0
  nfailedwires_row3 = 0
  nfailedwires_row4 = 0
  MAXCONTUNCONSTRIP = 0
  MAXCONTUNCONROW1 = None
  MAXCONTUNCONROW2 = None
  MAXCONTUNCONROW3 = None
  MAXCONTUNCONROW4 = None
  TOTALPERCUNCONSENSORCHAN = None
  #PERCUNCONSENSORCHAN_Row1 = None
  #PERCUNCONSENSORCHAN_Row2 = None
  #PERCUNCONSENSORCHAN_Row3 = None
  #PERCUNCONSENSORCHAN_Row4 = None
  # write wire bonding data into json file
  jsondata["component"] = component
  jsondata["testType"] = testType
  jsondata["institution"] = institution
  jsondata["runNumber"] = runNumber
  #jsondata["date"] = datetime.strptime(date+" "+time, '%Y-%m-%d %H:%M:%S').isoformat()
  jsondata["date"] = datetime.strptime(date+" "+time, '%Y-%m-%d %H:%M:%S').strftime("%Y-%m-%dT%H:%M:%SZ")
  jsondata['properties']['BOND_MACHINE'] = BOND_MACHINE
  jsondata['properties']['OPERATOR'] = OPERATOR
  if FAILED_FRONTEND_ROW1 and len(FAILED_FRONTEND_ROW1)>0:
    jsondata['results']['FAILED_FRONTEND_ROW1'] = FAILED_FRONTEND_ROW1
    MAXCONTUNCONROW1 = getMAXCONTUNCONROW(FAILED_FRONTEND_ROW1)
    nfailedwires_row1 = len(FAILED_FRONTEND_ROW1)
    jsondata['results']['MAX_CONT_UNCON_ROW1'] = MAXCONTUNCONROW1
  else:
    del jsondata['results']['FAILED_FRONTEND_ROW1']
    del jsondata['results']['MAX_CONT_UNCON_ROW1']
  if FAILED_FRONTEND_ROW2 and len(FAILED_FRONTEND_ROW2)>0:
    jsondata['results']['FAILED_FRONTEND_ROW2'] = FAILED_FRONTEND_ROW2
    MAXCONTUNCONROW2 = getMAXCONTUNCONROW(FAILED_FRONTEND_ROW2)
    nfailedwires_row2 = len(FAILED_FRONTEND_ROW2)
    jsondata['results']['MAX_CONT_UNCON_ROW2'] = MAXCONTUNCONROW2
  else:
    del jsondata['results']['FAILED_FRONTEND_ROW2']
    del jsondata['results']['MAX_CONT_UNCON_ROW2']
  if FAILED_FRONTEND_ROW3 and len(FAILED_FRONTEND_ROW3)>0:
    jsondata['results']['FAILED_FRONTEND_ROW3'] = FAILED_FRONTEND_ROW3
    MAXCONTUNCONROW3 = getMAXCONTUNCONROW(FAILED_FRONTEND_ROW3)
    nfailedwires_row3 = len(FAILED_FRONTEND_ROW3)
    jsondata['results']['MAX_CONT_UNCON_ROW3'] = MAXCONTUNCONROW3
  else:
    del jsondata['results']['FAILED_FRONTEND_ROW3']
    del jsondata['results']['MAX_CONT_UNCON_ROW3']
  if FAILED_FRONTEND_ROW4 and len(FAILED_FRONTEND_ROW4)>0:
    jsondata['results']['FAILED_FRONTEND_ROW4'] = FAILED_FRONTEND_ROW4
    MAXCONTUNCONROW4 = getMAXCONTUNCONROW(FAILED_FRONTEND_ROW4)
    nfailedwires_row4 = len(FAILED_FRONTEND_ROW4)
    jsondata['results']['MAX_CONT_UNCON_ROW4'] = MAXCONTUNCONROW4
  else:
    del jsondata['results']['FAILED_FRONTEND_ROW4']
    del jsondata['results']['MAX_CONT_UNCON_ROW4']
  if REPAIRED_FRONTEND_ROW1 and len(REPAIRED_FRONTEND_ROW1)>0:
    jsondata['results']['REPAIRED_FRONTEND_ROW1'] = REPAIRED_FRONTEND_ROW1
  else:
    del jsondata['results']['REPAIRED_FRONTEND_ROW1']
  if REPAIRED_FRONTEND_ROW2 and len(REPAIRED_FRONTEND_ROW2)>0:
    jsondata['results']['REPAIRED_FRONTEND_ROW2'] = REPAIRED_FRONTEND_ROW2
  else:
    del jsondata['results']['REPAIRED_FRONTEND_ROW2']
  if REPAIRED_FRONTEND_ROW3 and len(REPAIRED_FRONTEND_ROW3)>0:
    jsondata['results']['REPAIRED_FRONTEND_ROW3'] = REPAIRED_FRONTEND_ROW3
  else:
    del jsondata['results']['REPAIRED_FRONTEND_ROW3']
  if REPAIRED_FRONTEND_ROW4 and len(REPAIRED_FRONTEND_ROW4)>0:
    jsondata['results']['REPAIRED_FRONTEND_ROW4'] = REPAIRED_FRONTEND_ROW4
  else:
    del jsondata['results']['REPAIRED_FRONTEND_ROW4']
  if FAILED_HYBRID_TO_PB and len(FAILED_HYBRID_TO_PB)>0:
    jsondata['results']['FAILED_HYBRID_TO_PB'] = FAILED_HYBRID_TO_PB
  else:
    del jsondata['results']['FAILED_HYBRID_TO_PB']
  if REPAIRED_HYBRID_TO_PB and len(REPAIRED_HYBRID_TO_PB)>0:
    jsondata['results']['REPAIRED_HYBRID_TO_PB'] = REPAIRED_HYBRID_TO_PB
  else:
    del jsondata['results']['REPAIRED_HYBRID_TO_PB']
  if FAILED_MODULE_TO_FRAME and len(FAILED_MODULE_TO_FRAME)>0:
    jsondata['results']['FAILED_MODULE_TO_FRAME'] = FAILED_MODULE_TO_FRAME
  else:
    del jsondata['results']['FAILED_MODULE_TO_FRAME']
  if REPAIRED_MODULE_TO_FRAME and len(REPAIRED_MODULE_TO_FRAME)>0:
    jsondata['results']['REPAIRED_MODULE_TO_FRAME'] = REPAIRED_MODULE_TO_FRAME
  else:
    del jsondata['results']['REPAIRED_MODULE_TO_FRAME']

  FailedStrips_S0 = convert_wirenumber_to_stripnumber(FAILED_FRONTEND_ROW1, FAILED_FRONTEND_ROW2)
  FailedStrips_S1 = convert_wirenumber_to_stripnumber(FAILED_FRONTEND_ROW3, FAILED_FRONTEND_ROW4)

  TOTALPERCUNCONSENSORCHAN = 0
  if moduleType=='BARREL_LS_MODULE':
    TOTALPERCUNCONSENSORCHAN = float(nfailedwires_row1+nfailedwires_row2+nfailedwires_row3+nfailedwires_row4)/2564.
  elif moduleType=='BARREL_SS_MODULE':
    TOTALPERCUNCONSENSORCHAN = float(nfailedwires_row1+nfailedwires_row2+nfailedwires_row3+nfailedwires_row4)/5128.

  MAXCONTUNCONSTRIP_S0 = getMAXCONTUNCONSTRIP(FailedStrips_S0)
  MAXCONTUNCONSTRIP_S1 = getMAXCONTUNCONSTRIP(FailedStrips_S1)
  MAXCONTUNCONSTRIP = max([MAXCONTUNCONSTRIP_S0, MAXCONTUNCONSTRIP_S1])

  FailedStrips_X_S0 = {}
  FailedStrips_X_S1 = {}
  FailedStrips_Y_S0 = {}
  FailedStrips_Y_S1 = {}
  for Wire in FailedStrips_S0.keys():
    if int(Wire)<=1282:
      FailedStrips_X_S0[Wire] = FailedStrips_S0[Wire]
    else:
      FailedStrips_Y_S0[Wire] = FailedStrips_S0[Wire]
  for Wire in FailedStrips_S1.keys():
    if int(Wire)<=1282:
      FailedStrips_X_S1[Wire] = FailedStrips_S1[Wire]
    else:
      FailedStrips_Y_S1[Wire] = FailedStrips_S1[Wire]

  PERCUNCONSENSORCHAN_X_S0 = len(FailedStrips_X_S0)/1282.
  PERCUNCONSENSORCHAN_X_S1 = len(FailedStrips_X_S1)/1282.
  PERCUNCONSENSORCHAN_Y_S0 = len(FailedStrips_Y_S0)/1282.
  PERCUNCONSENSORCHAN_Y_S1 = len(FailedStrips_Y_S1)/1282.

  jsondata['results']['MAX_UNCON_SENSOR_CHAN'] = MAXCONTUNCONSTRIP

  jsondata['results']['TOTAL_PERC_UNCON_SENSOR_CHAN'] = TOTALPERCUNCONSENSORCHAN

  if TOTALPERCUNCONSENSORCHAN>0.01 or PERCUNCONSENSORCHAN_X_S0>0.01 or PERCUNCONSENSORCHAN_X_S1>0.01 or PERCUNCONSENSORCHAN_Y_S0>0.01 or PERCUNCONSENSORCHAN_Y_S1>0.01 or MAXCONTUNCONSTRIP>8:
    pass_test = False

  jsondata['passed'] = pass_test
  jsondata['problems'] = not pass_test

  '''
  testRuns = []
  try:
    #testRuns = client.get('listTestRunsByComponent', json={'component': jsondata['component']})
    testRuns = DBaccess.DbGet(client,'listTestRunsByComponent', {'component': component})
  except Exception:
    print('Can not get testruns!!!!')
  maxrunNumber = 0
  for testrun in testRuns:
    if testrun['testType']['code']==jsondata['testType'] and testrun['stage']['code']==currentStage:
      if int(testrun['runNumber'])>maxrunNumber:
        maxrunNumber = int(testrun['runNumber'])
  runNumber = int(jsondata["runNumber"])
  if maxrunNumber>=runNumber:
    runNumber = maxrunNumber+1
  jsondata["runNumber"] = str(runNumber)
  '''

  return jsondata
