# Strips Applications

A collection of applications intended for ITk Strips project

Currently hosted on [Cern OKD](https://itk-pdb-webapps-strips.web.cern.ch).

---

## Examples

Apps are mainly built around testTypes, rather than componentTypes. Hence, an app may provide functionality for testRun uploads for various componentTypes.

Non-comprehensive list of included apps.


### IV App

Upload IV measurements

- Sensors
- Modules
- Powerboards


### Metrology App

Upload metrology measurements for Modules and Hybrids

- includes analysis from [here](https://gitlab.cern.ch/gdyckes/metrologyanalysis/-/tree/master/)
