from copy import deepcopy

### Define type identifiers for hybrids and modules, for both Barrel and Endcap
typeHybrids_Barrel = ["HX", "HY"]
typeHybrids_Endcap = ["H0", "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9", "HA", "HB", "HC"]
typeHybrids = typeHybrids_Barrel + typeHybrids_Endcap

typeModules_Barrel = ["ML", "MS"]
typeModules_Endcap = ["M0", "M1", "M2", "MA", "MB", "MC", "MD", "ME", "MF"]
#typeModules_Endcap = ["M0", "M1", "M2", "3L", "3R", "4L", "4R", "5L", "5R"]
typeModules = typeModules_Barrel + typeModules_Endcap

### Define specifications such as thicknesses of ASICs/hybrids/powerboards, or (x,y) positions of ASICs on hybrids, and hybrids/powerboards on modules
### Each are defined for every campaign of Pre-Production and Production
### All values are written in mm
specifications = {
    "PPA" : { # Pre-Production A
        "thickness" : { # TODO: Reference?
            "ASIC" : 0.300,
            "hybrid" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.oxfijslipv1w
                "X" : 0.380, # GPC1938
                "Y" : 0.360, # GPC1940
                "Endcap" : 0.250 # L001 (250+/-20%)
            },
            "powerboard" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.exxk0bi2u3zi
                "Barrel" : 0.390, # V3.a (390-400)
                "Endcap" : 0.270 # L001 (270+/-20%)
            }
        },
        "position" : {
            "hybrid" : {
                "HX" : { # X = TODO: Reference?
                    "H_X_P1" :     [ 0.,    0.],
                    "H_X_P2" :     [96.4,   0.],
                    "ABC_X_9_P1" : [ 0.899, 1.198],
                    "ABC_X_9_P2" : [ 8.501, 1.198],
                    "ABC_X_8_P1" : [10.563, 1.198],
                    "ABC_X_8_P2" : [18.165, 1.198],
                    "ABC_X_7_P1" : [20.227, 1.198],
                    "ABC_X_7_P2" : [27.829, 1.198],
                    "ABC_X_6_P1" : [29.891, 1.198],
                    "ABC_X_6_P2" : [37.493, 1.198],
                    "ABC_X_5_P1" : [39.555, 1.198],
                    "ABC_X_5_P2" : [47.157, 1.198],
                    "ABC_X_4_P1" : [49.219, 1.198],
                    "ABC_X_4_P2" : [56.821, 1.198],
                    "ABC_X_3_P1" : [58.883, 1.198],
                    "ABC_X_3_P2" : [66.485, 1.198],
                    "ABC_X_2_P1" : [68.547, 1.198],
                    "ABC_X_2_P2" : [76.149, 1.198],
                    "ABC_X_1_P1" : [78.211, 1.198],
                    "ABC_X_1_P2" : [85.813, 1.198],
                    "ABC_X_0_P1" : [87.875, 1.198],
                    "ABC_X_0_P2" : [95.477, 1.198],
                    "HCC_X_0_P1" : [ 6.721, 7.319],
                    "HCC_X_0_P2" : [11.653, 7.319]
                },
                "HY" : { # Y = TODO: Reference? TODO: Not sure of ABC numbering - metrology drawing is opposite of hybrid metrology procedures document
                    "H_Y_P1" :     [ 0.,    0.],
                    "H_Y_P2" :     [96.376, 0.],
                    "ABC_Y_0_P1" : [ 0.875, 1.193],
                    "ABC_Y_0_P2" : [ 8.478, 1.193],
                    "ABC_Y_1_P1" : [10.539, 1.193],
                    "ABC_Y_1_P2" : [18.142, 1.193],
                    "ABC_Y_2_P1" : [20.203, 1.193],
                    "ABC_Y_2_P2" : [27.806, 1.193],
                    "ABC_Y_3_P1" : [29.867, 1.193],
                    "ABC_Y_3_P2" : [37.470, 1.193],
                    "ABC_Y_4_P1" : [39.531, 1.193],
                    "ABC_Y_4_P2" : [47.134, 1.193],
                    "ABC_Y_5_P1" : [49.195, 1.193],
                    "ABC_Y_5_P2" : [56.798, 1.193],
                    "ABC_Y_6_P1" : [58.859, 1.193],
                    "ABC_Y_6_P2" : [66.462, 1.193],
                    "ABC_Y_7_P1" : [68.523, 1.193],
                    "ABC_Y_7_P2" : [76.126, 1.193],
                    "ABC_Y_8_P1" : [78.187, 1.193],
                    "ABC_Y_8_P2" : [85.790, 1.193],
                    "ABC_Y_9_P1" : [87.851, 1.193],
                    "ABC_Y_9_P2" : [95.454, 1.193],
                    "HCC_Y_0_P1" : [ 6.698, 7.315],
                    "HCC_Y_0_P2" : [11.629, 7.315]
                },
                # TODO: Endcap...
            },
            "module" : {
                "ML" : { # LS = https://edms.cern.ch/ui/file/2380225/1/atlas-ls-module_metrology.pdf
                    "H_X_P1" : [ 0.788, 39.673],
                    "H_X_P2" : [97.188, 39.673],
                    "PB_P1" :  [93.201, 24.611],
                    "PB_P2" :  [24.951, 24.611]
                },
                "ML_edge" : { # LS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 45.639],
                    "H_X_P2" : [97.700, 45.639],
                    "PB_P1" :  [97.700, 19.111],
                    "PB_P2" :  [25.701, 19.111]
                },
                "MS" : { # SS = https://edms.cern.ch/ui/file/2380225/1/atlas-ss-module_metrology.pdf
                    "H_X_P1" : [ 0.788, 63.873],
                    "H_X_P2" : [97.188, 63.873],
                    "PB_P1" :  [93.201, 48.811],
                    "PB_P2" :  [24.951, 48.811],
                    "H_Y_P1" : [ 0.812, 33.753],
                    "H_Y_P2" : [97.188, 33.753]
                },
                "MS_edge" : { # SS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 69.8105],
                    "H_X_P2" : [97.700, 69.8105],
                    "PB_P1" :  [97.701, 43.311], #from https://edms.cern.ch/ui/file/2380225/1/atlas-ss-module_metrology.pdf
                    "PB_P2" :  [25.701, 43.311], #from https://edms.cern.ch/ui/file/2380225/1/atlas-ss-module_metrology.pdf
                    "H_Y_P1" : [ 0.250, 27.8105],
                    "H_Y_P2" : [97.700, 27.8105]
                },
                "M0" : { # R0 = TODO: Reference?
                    "H_R0H0_P1" : [10.55, 76.97],
                    "H_R0H0_P2" : [90.11, 79.79],
                    "PB_P1" :     [52.41, 62.45],
                    "PB_P2" :     [88.81, 51.44],
                    "H_R0H1_P1" : [ 5.61, 37.31],
                    "H_R0H1_P2" : [93.33, 39.96]
                },
                "M1" : { # R1 = TODO: Reference?
                    "H_R1H0_P1" : [  8.085, 58.421],
                    "H_R1H0_P2" : [108.671, 61.185],
                    "PB_P1" :     [ 70.939, 44.617],
                    "PB_P2" :     [107.332, 33.586],
                    "H_R1H1_P1" : [  3.633, 20.527],
                    "H_R1H1_P2" : [111.731, 23.126]
                },
                "M2" : { # R2 = TODO: Reference?
                    "H_R2H0_P1" : [  4.244, 23.221],
                    "H_R2H0_P2" : [124.211, 25.858],
                    "PB_P1" :     [ 81.879, 10.595],
                    "PB_P2" :     [122.388,  1.135]
                },
                "MA" : { # R3M0 = TODO: Reference?
                    "H_R3H0_P1" : [ 6.758, 83.128],
                    "H_R3H0_P2" : [71.861, 85.822],
                    "PB_P1" :     [29.315, 70.491],
                    "PB_P2" :     [72.688, 53.162],
                    "H_R3H2_P1" : [ 3.492, 31.822],
                    "H_R3H2_P2" : [73.01,  34.417]
                },
                "MB" : { # R3M1 = TODO: Reference?
                    "H_R3H1_P1" : [ 6.757, 83.121],
                    "H_R3H1_P2" : [71.861, 85.818],
                    "H_R3H3_P1" : [ 3.489, 31.784],
                    "H_R3H3_P2" : [73.007, 34.374]
                },
                "MC" : { # R4M0 = TODO: Reference?
                    "H_R4H0_P1" : [ 4.62, 47.09],
                    "H_R4H0_P2" : [83.9,  49.72],
                    "PB_P1" :     [41.64, 32.09],
                    "PB_P2" :     [85.3,  15.57]
                },
                "MD" : { # R4M1 = TODO: Reference?
                    "H_R4H1_P1" : [ 4.62, 47.09],
                    "H_R4H1_P2" : [83.9,  49.72]
                },
                "ME" : { # R5M0 = TODO: Reference?
                    "H_R5H0_P1" : [ 5.,   52.66],
                    "H_R5H0_P2" : [94.06, 55.31],
                    "PB_P1" :     [51.79, 37.67],
                    "PB_P2" :     [95.44, 21.13]
                },
                "MF" : { # R5M1 = TODO: Reference?
                    "H_R5H1_P1" : [ 5.,   52.66],
                    "H_R5H1_P2" : [94.06, 55.31]
                }
            }
        }
    },
    "PPB" : { # Pre-Production B
        "thickness" : { # TODO: Reference?
            "ASIC" : 0.300,
            "hybrid" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.oxfijslipv1w
                "X" : 0.400, # 398-415 depending on batch 
                "Y" : 0.400, # 398-403 depending on batch
                "Endcap" : 0.250 # L001 (250+/-20%)
            },
            "powerboard" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.exxk0bi2u3zi
                "Barrel" : 0.385, # To be finalized!
                "Endcap" : 0.270 # L001 (270+/-20%)
            }
        },
        "position" : {
            "hybrid" : {
                "HX" : { # X = https://edms.cern.ch/ui/file/2376015/2/abcstar_hybrid_x_v3_metrology.pdf
                    "H_X_P1" :     [ 0.,    0.],
                    "H_X_P2" :     [96.408, 0.],
                    "ABC_X_9_P1" : [ 0.907, 1.186], # note the slight slope in y position.  Fiducials moved -> sloping x-axis.
                    "ABC_X_9_P2" : [ 8.509, 1.186],
                    "ABC_X_8_P1" : [10.571, 1.187],
                    "ABC_X_8_P2" : [18.173, 1.187],
                    "ABC_X_7_P1" : [20.235, 1.188],
                    "ABC_X_7_P2" : [27.837, 1.188],
                    "ABC_X_6_P1" : [29.899, 1.190],
                    "ABC_X_6_P2" : [37.501, 1.190],
                    "ABC_X_5_P1" : [39.563, 1.191],
                    "ABC_X_5_P2" : [47.165, 1.191],
                    "ABC_X_4_P1" : [49.227, 1.192],
                    "ABC_X_4_P2" : [56.829, 1.192],
                    "ABC_X_3_P1" : [58.891, 1.193],
                    "ABC_X_3_P2" : [66.493, 1.193],
                    "ABC_X_2_P1" : [68.555, 1.194],
                    "ABC_X_2_P2" : [76.157, 1.194],
                    "ABC_X_1_P1" : [78.219, 1.196],
                    "ABC_X_1_P2" : [85.821, 1.196],
                    "ABC_X_0_P1" : [87.883, 1.197],
                    "ABC_X_0_P2" : [95.485, 1.197],
                    "HCC_X_0_P1" : [ 6.727, 7.188],
                    "HCC_X_0_P2" : [11.658, 7.188]
                },
                "HY" : { # Y = https://edms.cern.ch/ui/file/2376015/2/abcstar_hybrid_y_v3_metrology.pdf
                    "H_Y_P1" :     [ 0.,    0.],
                    "H_Y_P2" :     [96.376, 0.],
                    "ABC_Y_0_P1" : [ 0.876, 1.193],
                    "ABC_Y_0_P2" : [ 8.478, 1.193],
                    "ABC_Y_1_P1" : [10.540, 1.193],
                    "ABC_Y_1_P2" : [18.142, 1.193],
                    "ABC_Y_2_P1" : [20.204, 1.193],
                    "ABC_Y_2_P2" : [27.806, 1.193],
                    "ABC_Y_3_P1" : [29.868, 1.193],
                    "ABC_Y_3_P2" : [37.470, 1.193],
                    "ABC_Y_4_P1" : [39.532, 1.193],
                    "ABC_Y_4_P2" : [47.134, 1.193],
                    "ABC_Y_5_P1" : [49.196, 1.193],
                    "ABC_Y_5_P2" : [56.798, 1.193],
                    "ABC_Y_6_P1" : [58.860, 1.193],
                    "ABC_Y_6_P2" : [66.462, 1.193],
                    "ABC_Y_7_P1" : [68.524, 1.193],
                    "ABC_Y_7_P2" : [76.126, 1.193],
                    "ABC_Y_8_P1" : [78.188, 1.193],
                    "ABC_Y_8_P2" : [85.790, 1.193],
                    "ABC_Y_9_P1" : [87.852, 1.193],
                    "ABC_Y_9_P2" : [95.454, 1.193],
                    "HCC_Y_0_P1" : [ 6.696, 7.203],
                    "HCC_Y_0_P2" : [11.628, 7.203]
                }
            },
            "module" : {
                "ML" : { # LS = https://edms.cern.ch/ui/file/2380225/2/atlas-ls-module_metrology_b.pdf
                    "H_X_P1" : [ 0.780, 39.661],
                    "H_X_P2" : [97.188, 39.673],
                    "PB_P1" :  [93.273, 23.474],
                    "PB_P2" :  [23.200, 24.649]
                },
                "ML_edge" : { # LS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 45.639],
                    "H_X_P2" : [97.700, 45.639],
                    "PB_P1" :  [97.700, 19.111],
                    "PB_P2" :  [25.701, 19.111]
                },
                "MS" : { # SS = https://edms.cern.ch/file/2380225/2/atlas-ss-module_metrology_b.pdf 
                    "H_X_P1" : [ 0.780, 63.861],
                    "H_X_P2" : [97.188, 63.873],
                    "PB_P1" :  [93.273, 47.674],
                    "PB_P2" :  [23.200, 48.849],
                    "H_Y_P1" : [ 0.812, 33.753],
                    "H_Y_P2" : [97.188, 33.753]
                },
                "MS_edge" : { # SS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 69.8105],
                    "H_X_P2" : [97.700, 69.8105],
                    "PB_P1" :  [97.700, 43.311],
                    "PB_P2" :  [25.701, 43.311],
                    "H_Y_P1" : [ 0.250, 27.8105],
                    "H_Y_P2" : [97.700, 27.8105]
                },
                "M0" : { # R0 = TODO: Reference?
                    "H_R0H0_P1" : [10.55, 76.97],
                    "H_R0H0_P2" : [90.11, 79.79],
                    "PB_P1" :     [52.41, 62.45],
                    "PB_P2" :     [88.81, 51.44],
                    "H_R0H1_P1" : [ 5.61, 37.31],
                    "H_R0H1_P2" : [93.33, 39.96]
                },
                "M1" : { # R1 = TODO: Reference?
                    "H_R1H0_P1" : [  8.085, 58.421],
                    "H_R1H0_P2" : [108.671, 61.185],
                    "PB_P1" :     [ 70.939, 44.617],
                    "PB_P2" :     [107.332, 33.586],
                    "H_R1H1_P1" : [  3.633, 20.527],
                    "H_R1H1_P2" : [111.731, 23.126]
                },
                "M2" : { # R2 = TODO: Reference?
                    "H_R2H0_P1" : [  4.244, 23.221],
                    "H_R2H0_P2" : [124.211, 25.858],
                    "PB_P1" :     [ 81.879, 10.595],
                    "PB_P2" :     [122.388,  1.135]
                },
                "MA" : { # R3M0 = TODO: Reference?
                    "H_R3H0_P1" : [ 6.758, 83.128],
                    "H_R3H0_P2" : [71.861, 85.822],
                    "PB_P1" :     [29.315, 70.491],
                    "PB_P2" :     [72.688, 53.162],
                    "H_R3H2_P1" : [ 3.492, 31.822],
                    "H_R3H2_P2" : [73.01,  34.417]
                },
                "MB" : { # R3M1 = TODO: Reference?
                    "H_R3H1_P1" : [ 6.757, 83.121],
                    "H_R3H1_P2" : [71.861, 85.818],
                    "H_R3H3_P1" : [ 3.489, 31.784],
                    "H_R3H3_P2" : [73.007, 34.374]
                },
                "MC" : { # R4M0 = TODO: Reference?
                    "H_R4H0_P1" : [ 4.62, 47.09],
                    "H_R4H0_P2" : [83.9,  49.72],
                    "PB_P1" :     [41.64, 32.09],
                    "PB_P2" :     [85.3,  15.57]
                },
                "MD" : { # R4M1 = TODO: Reference?
                    "H_R4H1_P1" : [ 4.62, 47.09],
                    "H_R4H1_P2" : [83.9,  49.72]
                },
                "ME" : { # R5M0 = TODO: Reference?
                    "H_R5H0_P1" : [ 5.,   52.66],
                    "H_R5H0_P2" : [94.06, 55.31],
                    "PB_P1" :     [51.79, 37.67],
                    "PB_P2" :     [95.44, 21.13]
                },
                "MF" : { # R5M1 = TODO: Reference?
                    "H_R5H1_P1" : [ 5.,   52.66],
                    "H_R5H1_P2" : [94.06, 55.31]
                }
            }
        }
    },
    "PROD_THIN" : { # Thin barrel production hybrids, old tools
        "thickness" : { # TODO: Reference?
            "ASIC" : 0.300,
            "hybrid" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.oxfijslipv1w
                "X" : 0.370, # 
                "Y" : 0.370, # 
                "Endcap" : 0.250 # L001 (250+/-20%)
            },
            "powerboard" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.exxk0bi2u3zi
                "Barrel" : 0.385, # To be finalized!
                "Endcap" : 0.270 # L001 (270+/-20%)
            }
        },
        "position" : {
            "hybrid" : {
                "HX" : { # X = https://edms.cern.ch/ui/file/2376015/2/abcstar_hybrid_x_v3_metrology.pdf
                    "H_X_P1" :     [ 0.,    0.],
                    "H_X_P2" :     [96.408, 0.],
                    "ABC_X_9_P1" : [ 0.907, 1.186], # note the slight slope in y position.  Fiducials moved -> sloping x-axis.
                    "ABC_X_9_P2" : [ 8.509, 1.186],
                    "ABC_X_8_P1" : [10.571, 1.187],
                    "ABC_X_8_P2" : [18.173, 1.187],
                    "ABC_X_7_P1" : [20.235, 1.188],
                    "ABC_X_7_P2" : [27.837, 1.188],
                    "ABC_X_6_P1" : [29.899, 1.190],
                    "ABC_X_6_P2" : [37.501, 1.190],
                    "ABC_X_5_P1" : [39.563, 1.191],
                    "ABC_X_5_P2" : [47.165, 1.191],
                    "ABC_X_4_P1" : [49.227, 1.192],
                    "ABC_X_4_P2" : [56.829, 1.192],
                    "ABC_X_3_P1" : [58.891, 1.193],
                    "ABC_X_3_P2" : [66.493, 1.193],
                    "ABC_X_2_P1" : [68.555, 1.194],
                    "ABC_X_2_P2" : [76.157, 1.194],
                    "ABC_X_1_P1" : [78.219, 1.196],
                    "ABC_X_1_P2" : [85.821, 1.196],
                    "ABC_X_0_P1" : [87.883, 1.197],
                    "ABC_X_0_P2" : [95.485, 1.197],
                    "HCC_X_0_P1" : [ 6.727, 7.188],
                    "HCC_X_0_P2" : [11.658, 7.188]
                },
                "HY" : { # Y = https://edms.cern.ch/ui/file/2376015/2/abcstar_hybrid_y_v3_metrology.pdf
                    "H_Y_P1" :     [ 0.,    0.],
                    "H_Y_P2" :     [96.376, 0.],
                    "ABC_Y_0_P1" : [ 0.876, 1.193],
                    "ABC_Y_0_P2" : [ 8.478, 1.193],
                    "ABC_Y_1_P1" : [10.540, 1.193],
                    "ABC_Y_1_P2" : [18.142, 1.193],
                    "ABC_Y_2_P1" : [20.204, 1.193],
                    "ABC_Y_2_P2" : [27.806, 1.193],
                    "ABC_Y_3_P1" : [29.868, 1.193],
                    "ABC_Y_3_P2" : [37.470, 1.193],
                    "ABC_Y_4_P1" : [39.532, 1.193],
                    "ABC_Y_4_P2" : [47.134, 1.193],
                    "ABC_Y_5_P1" : [49.196, 1.193],
                    "ABC_Y_5_P2" : [56.798, 1.193],
                    "ABC_Y_6_P1" : [58.860, 1.193],
                    "ABC_Y_6_P2" : [66.462, 1.193],
                    "ABC_Y_7_P1" : [68.524, 1.193],
                    "ABC_Y_7_P2" : [76.126, 1.193],
                    "ABC_Y_8_P1" : [78.188, 1.193],
                    "ABC_Y_8_P2" : [85.790, 1.193],
                    "ABC_Y_9_P1" : [87.852, 1.193],
                    "ABC_Y_9_P2" : [95.454, 1.193],
                    "HCC_Y_0_P1" : [ 6.696, 7.203],
                    "HCC_Y_0_P2" : [11.628, 7.203]
                }
            },
            "module" : {
                "ML" : { # LS = https://edms.cern.ch/ui/file/2380225/2/atlas-ls-module_metrology_b.pdf
                    "H_X_P1" : [ 0.780, 39.661],
                    "H_X_P2" : [97.188, 39.673],
                    "PB_P1" :  [93.273, 23.474],
                    "PB_P2" :  [23.200, 24.649]
                },
                "ML_edge" : { # LS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 45.639],
                    "H_X_P2" : [97.700, 45.639],
                    "PB_P1" :  [97.700, 19.111],
                    "PB_P2" :  [25.701, 19.111]
                },
                "MS" : { # SS = https://edms.cern.ch/file/2380225/2/atlas-ss-module_metrology_b.pdf 
                    "H_X_P1" : [ 0.780, 63.861],
                    "H_X_P2" : [97.188, 63.873],
                    "PB_P1" :  [93.273, 47.674],
                    "PB_P2" :  [23.200, 48.849],
                    "H_Y_P1" : [ 0.812, 33.753],
                    "H_Y_P2" : [97.188, 33.753]
                },
                "MS_edge" : { # SS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 69.8105],
                    "H_X_P2" : [97.700, 69.8105],
                    "PB_P1" :  [97.700, 43.311],
                    "PB_P2" :  [25.701, 43.311],
                    "H_Y_P1" : [ 0.250, 27.8105],
                    "H_Y_P2" : [97.700, 27.8105]
                },
                "M0" : { # R0 = TODO: Reference?
                    "H_R0H0_P1" : [10.55, 76.97],
                    "H_R0H0_P2" : [90.11, 79.79],
                    "PB_P1" :     [52.41, 62.45],
                    "PB_P2" :     [88.81, 51.44],
                    "H_R0H1_P1" : [ 5.61, 37.31],
                    "H_R0H1_P2" : [93.33, 39.96]
                },
                "M1" : { # R1 = TODO: Reference?
                    "H_R1H0_P1" : [  8.085, 58.421],
                    "H_R1H0_P2" : [108.671, 61.185],
                    "PB_P1" :     [ 70.939, 44.617],
                    "PB_P2" :     [107.332, 33.586],
                    "H_R1H1_P1" : [  3.633, 20.527],
                    "H_R1H1_P2" : [111.731, 23.126]
                },
                "M2" : { # R2 = TODO: Reference?
                    "H_R2H0_P1" : [  4.244, 23.221],
                    "H_R2H0_P2" : [124.211, 25.858],
                    "PB_P1" :     [ 81.879, 10.595],
                    "PB_P2" :     [122.388,  1.135]
                },
                "MA" : { # R3M0 = TODO: Reference?
                    "H_R3H0_P1" : [ 6.758, 83.128],
                    "H_R3H0_P2" : [71.861, 85.822],
                    "PB_P1" :     [29.315, 70.491],
                    "PB_P2" :     [72.688, 53.162],
                    "H_R3H2_P1" : [ 3.492, 31.822],
                    "H_R3H2_P2" : [73.01,  34.417]
                },
                "MB" : { # R3M1 = TODO: Reference?
                    "H_R3H1_P1" : [ 6.757, 83.121],
                    "H_R3H1_P2" : [71.861, 85.818],
                    "H_R3H3_P1" : [ 3.489, 31.784],
                    "H_R3H3_P2" : [73.007, 34.374]
                },
                "MC" : { # R4M0 = TODO: Reference?
                    "H_R4H0_P1" : [ 4.62, 47.09],
                    "H_R4H0_P2" : [83.9,  49.72],
                    "PB_P1" :     [41.64, 32.09],
                    "PB_P2" :     [85.3,  15.57]
                },
                "MD" : { # R4M1 = TODO: Reference?
                    "H_R4H1_P1" : [ 4.62, 47.09],
                    "H_R4H1_P2" : [83.9,  49.72]
                },
                "ME" : { # R5M0 = TODO: Reference?
                    "H_R5H0_P1" : [ 5.,   52.66],
                    "H_R5H0_P2" : [94.06, 55.31],
                    "PB_P1" :     [51.79, 37.67],
                    "PB_P2" :     [95.44, 21.13]
                },
                "MF" : { # R5M1 = TODO: Reference?
                    "H_R5H1_P1" : [ 5.,   52.66],
                    "H_R5H1_P2" : [94.06, 55.31]
                }
            }
        }
    },
    "PROD_THICK" : { # Thick barrel production hyrids
        "thickness" : { # TODO: Reference?
            "ASIC" : 0.300,
            "hybrid" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.oxfijslipv1w
                "X" : 0.420, # 
                "Y" : 0.420, # 
                "Endcap" : 0.250 # L001 (250+/-20%)
            },
            "powerboard" : { # https://docs.google.com/document/d/1yDC_Mntuf_88RSODtNslXhEW01uCtfw-MHPLhRAc_T8/edit#heading=h.exxk0bi2u3zi
                "Barrel" : 0.385, # To be finalized!
                "Endcap" : 0.270 # L001 (270+/-20%)
            }
        },
        "position" : {
            "hybrid" : {
                "HX" : { # X = https://edms.cern.ch/ui/file/2376015/2/abcstar_hybrid_x_v3_metrology.pdf
                    "H_X_P1" :     [ 0.,    0.],
                    "H_X_P2" :     [96.408, 0.],
                    "ABC_X_9_P1" : [ 0.907, 1.186], # note the slight slope in y position.  Fiducials moved -> sloping x-axis.
                    "ABC_X_9_P2" : [ 8.509, 1.186],
                    "ABC_X_8_P1" : [10.571, 1.187],
                    "ABC_X_8_P2" : [18.173, 1.187],
                    "ABC_X_7_P1" : [20.235, 1.188],
                    "ABC_X_7_P2" : [27.837, 1.188],
                    "ABC_X_6_P1" : [29.899, 1.190],
                    "ABC_X_6_P2" : [37.501, 1.190],
                    "ABC_X_5_P1" : [39.563, 1.191],
                    "ABC_X_5_P2" : [47.165, 1.191],
                    "ABC_X_4_P1" : [49.227, 1.192],
                    "ABC_X_4_P2" : [56.829, 1.192],
                    "ABC_X_3_P1" : [58.891, 1.193],
                    "ABC_X_3_P2" : [66.493, 1.193],
                    "ABC_X_2_P1" : [68.555, 1.194],
                    "ABC_X_2_P2" : [76.157, 1.194],
                    "ABC_X_1_P1" : [78.219, 1.196],
                    "ABC_X_1_P2" : [85.821, 1.196],
                    "ABC_X_0_P1" : [87.883, 1.197],
                    "ABC_X_0_P2" : [95.485, 1.197],
                    "HCC_X_0_P1" : [ 6.727, 7.188],
                    "HCC_X_0_P2" : [11.658, 7.188]
                },
                "HY" : { # Y = https://edms.cern.ch/ui/file/2376015/2/abcstar_hybrid_y_v3_metrology.pdf
                    "H_Y_P1" :     [ 0.,    0.],
                    "H_Y_P2" :     [96.376, 0.],
                    "ABC_Y_0_P1" : [ 0.876, 1.193],
                    "ABC_Y_0_P2" : [ 8.478, 1.193],
                    "ABC_Y_1_P1" : [10.540, 1.193],
                    "ABC_Y_1_P2" : [18.142, 1.193],
                    "ABC_Y_2_P1" : [20.204, 1.193],
                    "ABC_Y_2_P2" : [27.806, 1.193],
                    "ABC_Y_3_P1" : [29.868, 1.193],
                    "ABC_Y_3_P2" : [37.470, 1.193],
                    "ABC_Y_4_P1" : [39.532, 1.193],
                    "ABC_Y_4_P2" : [47.134, 1.193],
                    "ABC_Y_5_P1" : [49.196, 1.193],
                    "ABC_Y_5_P2" : [56.798, 1.193],
                    "ABC_Y_6_P1" : [58.860, 1.193],
                    "ABC_Y_6_P2" : [66.462, 1.193],
                    "ABC_Y_7_P1" : [68.524, 1.193],
                    "ABC_Y_7_P2" : [76.126, 1.193],
                    "ABC_Y_8_P1" : [78.188, 1.193],
                    "ABC_Y_8_P2" : [85.790, 1.193],
                    "ABC_Y_9_P1" : [87.852, 1.193],
                    "ABC_Y_9_P2" : [95.454, 1.193],
                    "HCC_Y_0_P1" : [ 6.696, 7.203],
                    "HCC_Y_0_P2" : [11.628, 7.203]
                }
            },
            "module" : {
                "ML" : { # LS = https://edms.cern.ch/ui/file/2380225/2/atlas-ls-module_metrology_b.pdf
                    "H_X_P1" : [ 0.780, 39.661],
                    "H_X_P2" : [97.188, 39.673],
                    "PB_P1" :  [93.273, 23.474],
                    "PB_P2" :  [23.200, 24.649]
                },
                "ML_edge" : { # LS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 45.639],
                    "H_X_P2" : [97.700, 45.639],
                    "PB_P1" :  [97.700, 19.111],
                    "PB_P2" :  [25.701, 19.111]
                },
                "MS" : { # SS = https://edms.cern.ch/file/2380225/2/atlas-ss-module_metrology_b.pdf 
                    "H_X_P1" : [ 0.780, 63.861],
                    "H_X_P2" : [97.188, 63.873],
                    "PB_P1" :  [93.273, 47.674],
                    "PB_P2" :  [23.200, 48.849],
                    "H_Y_P1" : [ 0.812, 33.753],
                    "H_Y_P2" : [97.188, 33.753]
                },
                "MS_edge" : { # SS (edge) = TODO: Numbers from Bart - cross-check with metrology drawing!!!
                    "H_X_P1" : [ 0.250, 69.8105],
                    "H_X_P2" : [97.700, 69.8105],
                    "PB_P1" :  [97.700, 43.311],
                    "PB_P2" :  [25.701, 43.311],
                    "H_Y_P1" : [ 0.250, 27.8105],
                    "H_Y_P2" : [97.700, 27.8105]
                },
                "M0" : { # R0 = TODO: Reference?
                    "H_R0H0_P1" : [10.55, 76.97],
                    "H_R0H0_P2" : [90.11, 79.79],
                    "PB_P1" :     [52.41, 62.45],
                    "PB_P2" :     [88.81, 51.44],
                    "H_R0H1_P1" : [ 5.61, 37.31],
                    "H_R0H1_P2" : [93.33, 39.96]
                },
                "M1" : { # R1 = TODO: Reference?
                    "H_R1H0_P1" : [  8.085, 58.421],
                    "H_R1H0_P2" : [108.671, 61.185],
                    "PB_P1" :     [ 70.939, 44.617],
                    "PB_P2" :     [107.332, 33.586],
                    "H_R1H1_P1" : [  3.633, 20.527],
                    "H_R1H1_P2" : [111.731, 23.126]
                },
                "M2" : { # R2 = TODO: Reference?
                    "H_R2H0_P1" : [  4.244, 23.221],
                    "H_R2H0_P2" : [124.211, 25.858],
                    "PB_P1" :     [ 81.879, 10.595],
                    "PB_P2" :     [122.388,  1.135]
                },
                "MA" : { # R3M0 = TODO: Reference?
                    "H_R3H0_P1" : [ 6.758, 83.128],
                    "H_R3H0_P2" : [71.861, 85.822],
                    "PB_P1" :     [29.315, 70.491],
                    "PB_P2" :     [72.688, 53.162],
                    "H_R3H2_P1" : [ 3.492, 31.822],
                    "H_R3H2_P2" : [73.01,  34.417]
                },
                "MB" : { # R3M1 = TODO: Reference?
                    "H_R3H1_P1" : [ 6.757, 83.121],
                    "H_R3H1_P2" : [71.861, 85.818],
                    "H_R3H3_P1" : [ 3.489, 31.784],
                    "H_R3H3_P2" : [73.007, 34.374]
                },
                "MC" : { # R4M0 = TODO: Reference?
                    "H_R4H0_P1" : [ 4.62, 47.09],
                    "H_R4H0_P2" : [83.9,  49.72],
                    "PB_P1" :     [41.64, 32.09],
                    "PB_P2" :     [85.3,  15.57]
                },
                "MD" : { # R4M1 = TODO: Reference?
                    "H_R4H1_P1" : [ 4.62, 47.09],
                    "H_R4H1_P2" : [83.9,  49.72]
                },
                "ME" : { # R5M0 = TODO: Reference?
                    "H_R5H0_P1" : [ 5.,   52.66],
                    "H_R5H0_P2" : [94.06, 55.31],
                    "PB_P1" :     [51.79, 37.67],
                    "PB_P2" :     [95.44, 21.13]
                },
                "MF" : { # R5M1 = TODO: Reference?
                    "H_R5H1_P1" : [ 5.,   52.66],
                    "H_R5H1_P2" : [94.06, 55.31]
                }
            }
        }
    }
}

specifications["benchmark"] = deepcopy(specifications["PPA"]) # For benchmarking using SCIPP hybrid/module - same as PPA but with different assumed thicknesses
specifications["benchmark"]["thickness"]["hybrid"]["X"] = 0.390 # Assumed for SCIPP LS benchmark
specifications["benchmark"]["thickness"]["hybrid"]["Y"] = 0.390 # Assumed for SCIPP LS benchmark (don't think there was a SS benchmark)
specifications["benchmark"]["thickness"]["powerboard"]["Barrel"] = 0.390 # Assumed for SCIPP LS benchmark

### Get list of production campaigns from the dictionary above.
campaigns = list(specifications.keys())

### Tolerances
tolerances = {
    "hybrid" : {
        "packageHeight" : {
            "nominal" : 0.80,
            "tolerance" : 0.04,
            "target" : 0.02
        },
        "glueHeight" : {
            "nominal" : 0.120,
            "tolerance_plus" : 0.040,
            "tolerance_minus" : 0.060
        },
        "tilt_FE" : {
            "nominal" : 0.,
            "tolerance" : 0.025
        },
        "tilt_BE" : {
            "nominal" : 0.,
            "tolerance" : 0.025
        },
        "tilt_y1" : {
            "nominal" : 0.,
            "tolerance" : 0.025
        },
        "tilt_y2" : {
            "nominal" : 0.,
            "tolerance" : 0.025
        },
        "asicRelPosDX" : {
            "nominal" : 0.,
            "tolerance" : 0.2
        },
        "asicRelPosDY" : {
            "nominal" : 0.,
            "tolerance" : 0.2
        }
    },
    "module" : {
        "glueHeight" : {
            "nominal" : 0.120,
            "tolerance_plus" : 0.050,
            "tolerance_minus" : 0.080,
            "target_minus" : 0.050
        },
        "totalHeight" : {
            "nominal" : 5.71,
            "tolerance" : 0.
        },
        "relPosDX" : {
            "nominal" : 0.,
            "tolerance" : 0.250
        },
        "relPosDY" : {
            "nominal" : 0.,
            "tolerance" : 0.250
        }
    },
    "bow" : {
        "bowing" : {
            "nominal" : 0,
            "tolerance_plus" : 150,
            "tolerance_minus" : 50
        }
    }
}

def getToleranceNominal(mode, value, campaign = None):
    if campaign == "PROD_THICK":
        tolerances["hybrid"]["packageHeight"]["nominal"] = 0.84
    return tolerances[mode][value]["nominal"]

def getToleranceLow(mode, value, campaign = None):
    if campaign == "PROD_THICK":
        tolerances["hybrid"]["packageHeight"]["nominal"] = 0.84
    tolerance = tolerances[mode][value]["tolerance_minus"] if "tolerance_minus" in tolerances[mode][value] else tolerances[mode][value]["tolerance"]
    return tolerances[mode][value]["nominal"] - tolerance

def getToleranceHigh(mode, value, campaign = None):
    if campaign == "PROD_THICK":
        tolerances["hybrid"]["packageHeight"]["nominal"] = 0.84
    tolerance = tolerances[mode][value]["tolerance_plus"] if "tolerance_plus" in tolerances[mode][value] else tolerances[mode][value]["tolerance"]
    return tolerances[mode][value]["nominal"] + tolerance

def getToleranceTargetLow(mode, value, campaign = None):
    if campaign == "PROD_THICK":
        tolerances["hybrid"]["packageHeight"]["nominal"] = 0.84
    target = tolerances[mode][value]["target_minus"] if "target_minus" in tolerances[mode][value] else tolerances[mode][value]["target"]
    return tolerances[mode][value]["nominal"] - target

def getToleranceTargetHigh(mode, value, campaign = None):
    if campaign == "PROD_THICK":
        tolerances["hybrid"]["packageHeight"]["nominal"] = 0.84
    target = tolerances[mode][value]["target_plus"] if "target_plus" in tolerances[mode][value] else tolerances[mode][value]["target"]
    return tolerances[mode][value]["nominal"] + target
