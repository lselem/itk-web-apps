#!/usr/bin/env python3

### Author: Ian Dyckes, Nathan Readioff
### Notes:
### Focused on X-type hybrids and long-strip modules when writing.
### Need to generalize to Y-type hybrids and short-strip sensors.

import numpy as np
import pandas as pd
import os, sys, argparse, math, glob, shutil
import matplotlib.pyplot as plt
from skspatial.objects import Plane, Point, Points, Vector
from skspatial.plotting import plot_3d
import json
import pickle
import fnmatch

### Get specifications
from .spec import *

# get plotting library
from .plotter import *
from .plotlibrary import *

# ### Arguments
# parser = argparse.ArgumentParser(description='For analyzing raw data in the agreed "Common Data Format".')
# parser.add_argument('-f', '--inputFile', required=True, help='Formatted raw data file in the "Common Data Format".')
# parser.add_argument('-o', '--outputDir', default="results", help='Path prefix for directory to send output.')
# parser.add_argument('--type', choices=typeHybrids+typeModules, help='Hybrid [H*] or Module [M*] type')
# parser.add_argument('-c', '--campaign', choices=campaigns, default='PPB', help='Production campaign.  Important for pulling correct spec.')
# parser.add_argument('--tag', default="", help='Tag to add to end of output directory.')
# parser.add_argument("--useEdges", action='store_true', default=False, help="Metrology was performed using the edges, not the fiducials.")
# parser.add_argument("--isBowTest", action='store_true', default=False, help="This is a module bow test.")
# parser.add_argument('-t', '--temperature', help='Temperature during module bow test (in degrees Celcius).')
# parser.add_argument("--skipJSON", action='store_true', default=False, help="Don't make the JSON (e.g. if your metadata is not exactly correct).")
# parser.add_argument("--passed", action='store_true', default=False, help="Tell database JSON that component passes.") # may want this to be an automated check eventually.
# parser.add_argument("--problems", action='store_true', default=False, help="Tell database JSON there was a problem.")
# parser.add_argument("--downloadProto", dest = 'download_prototype', action='store_true', default=False, help="Download the template JSON from DB rather than use potentially outdated ones (Requires DB credentials)")
# args = parser.parse_args()

def main():    
    inputFile = args.inputFile
    outputDir = args.outputDir
    
    print(f"Using campaign {args.campaign}!")

    # Configure JSON output to wrap all floats to 6dp
    class RoundingFloat(float):
        __repr__ = staticmethod(lambda x: format(x, '.6f'))
    json.encoder.c_make_encoder  = None
    json.encoder.float = RoundingFloat

    # Verify directory exists
    inPath = os.path.normpath(inputFile)
    outPath = os.path.normpath(outputDir)
    if not os.path.exists(outPath):
        print(f"'{outPath}' directory does not exist.  Creating.")
        os.makedirs(outPath)

    # Determine and create the output directory.
    fName = os.path.basename(inPath)
    fNameDir = os.path.splitext(fName)[0]
    if args.tag:
        fNameDir += f"_{args.tag}"

    outDir = os.path.join(outPath, fNameDir)
    if not os.path.exists(outDir):
        print(f"'{outDir}' directory does not exist.  Creating.")
        os.makedirs(outDir)

    # Copy input file (CDF) to results dir for bookkeeping
    print("Copying input file to results directory.")
    print(inPath)
    print(os.path.join(outDir, fName))
    shutil.copyfile(inPath, os.path.join(outDir, fName))

    #Read file into list.  Each element is a line (string)
    with open(inPath) as f:
        content = f.readlines()
    # You may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content] 
    # Remove empty lines
    content = [x for x in content if not len(x)==0]

    # Get Metadata
    metaDict = {}
    isBowTest = False
    for line in content:
        if line.startswith("#") and "HEADER" in line.upper(): #skip the #---Header line.  Try to be robust to e.g. '# header' vs. '#---Header:'
            continue
        if line.startswith("#"): # start of position data.
            if 'BOW' in line.upper():
                isBowTest = True
            break
        field = line.split(":")[0]
        value = ":".join(line.split(":")[1:]).lstrip().rstrip()
        metaDict[field]=value

    print("Metadata for file:")
    print(json.dumps(metaDict, indent=4))

    # Identify component and test type
    componentType = metaDict.get('Hybrid Type') or metaDict.get('Module type')
    if args.type is not None:
        componentType = args.type

    testType = None
    if componentType in typeHybrids:
        testType = "hybrid"
    elif componentType in typeModules:
        testType = "module"

    doBowTest = isBowTest or args.isBowTest

    analysisType = None
    if doBowTest:
        analysisType = "bow"
    else:
        analysisType = "position and height"

    generatePlotLibrary()

    # Call appropriate function
    print(f"\nRunning {analysisType} analysis on a {componentType}-type {testType}.\n")
    if testType == "hybrid":
        hybridPositionAndHeightAnalysis(content, outDir, metaDict, componentType, args.campaign)
    elif testType == "module":
        if doBowTest:
            moduleBowAnalysis(content, outDir, metaDict, componentType, args.campaign)
        else:
            modulePositionAndHeightAnalysis(content, outDir, metaDict, componentType, args.campaign, args.useEdges)
    else:
        sys.exit("Should not get here.  Exiting.")



#####################################################
#####################################################
#####################################################

def getPositionData(content):
    start = [i for i, x in enumerate(content) if x.startswith('#') and 'POSITION' in x.upper()][0] + 2 # skip this line and the column titles.
    end = [i for i, x in enumerate(content) if x.startswith('#') and 'HEIGHT' in x.upper()][0] # previous line is last data line
    dictList = []
    for line in content[start:end]:
        comment = line.split()[0]
        x = float(line.split()[1])
        y = float(line.split()[2])
        d = {"comment":comment, "x":x, "y":y}
        dictList.append(d)
    return dictList

def getHeightData(content):
    start = [i for i, x in enumerate(content) if x.startswith('#') and 'HEIGHT' in x.upper()][0] + 2 # skip this line and the column titles.
    dictList = []
    for line in content[start:]: # Go to the end.  Don't care if glue height or other.
        if line.startswith('#'): # skip '#---Other heights' header and column titles.  Putting all height data in same dataframe.
            continue
        comment = line.split()[0]
        type  = int(line.split()[1])
        x = float(line.split()[2])
        y = float(line.split()[3])
        z = float(line.split()[4])
        d = {"comment":comment, "type":type, "x":x, "y":y, "z":z}
        dictList.append(d)
    return dictList

def getPointsXYZ(inputData):
    #list of [x,y,z] points
    pointsList = [ list(t) for t in zip(list(inputData["x"]), list(inputData["y"]), list(inputData["z"])) ]
    return Points(pointsList)

def getPlane(inputData):
    return Plane.best_fit(getPointsXYZ(inputData))
    

def calcTilt(df1, df2):
    # df1 and df2 are dataframes with only 1 row.
    # The projX, projY, projZ columns are the coordinates of the ASIC point projected on the local hybrid plane.
    # The height column is the height of the ASIC point relative to the local hybrid plane.

    # First convert to dictionaries
    df1 = df1.to_dict('records')[0]
    df2 = df2.to_dict('records')[0]

    # Get necessary values and the vector between the projected points
    point1 = Point([df1['projX'], df1['projY'], df1['projZ'] ])
    point2 = Point([df2['projX'], df2['projY'], df2['projZ'] ])
    vec = Vector.from_points(point1, point2)
    height1 = df1['height']
    height2 = df2['height']

    # Return the tilt, maybe also the vector between the two points projected on the plane (for plotting)
    tilt = math.fabs(height1 - height2) / vec.norm()
    return tilt


    
#######################################
# Hybrid Position and Height Analysis #
#######################################

def hybridPositionAndHeightAnalysis(content, outDir, metaDict, componentType, campaign):
    # Get correct spec info
    asicThickness = specifications[campaign]["thickness"]["ASIC"]
    dictPosition = specifications[campaign]["position"]["hybrid"]

    hybridSpecType = componentType

    # Get Position and Height Data
    pDictList = getPositionData(content)
    hDictList = getHeightData(content)

    # Make Dataframes
    pdf = pd.DataFrame(pDictList) # position dataframe.
    hdf = pd.DataFrame(hDictList) # height dataframe.

    # Calculate position deviations from spec
    xSpecColumn, ySpecColumn, xDiffColumn, yDiffColumn = [], [], [], []
    for index, row in pdf.iterrows():
        label = row.comment
        specDict = dictPosition[hybridSpecType] # dict with keys = measurement name, values = [specx, specy]
        xSpecColumn.append(specDict[label][0])
        ySpecColumn.append(specDict[label][1])
        xDiffColumn.append(row.x - specDict[label][0])
        yDiffColumn.append(row.y - specDict[label][1])
    
    ### Add columns to dataframe
    pdf['xSpec'] = xSpecColumn
    pdf['ySpec'] = ySpecColumn
    pdf['xDiff'] = xDiffColumn
    pdf['yDiff'] = yDiffColumn
    # print(pdf.to_string())

    # Construct jig plane using just the jig data from height dataframe
    jigPlane = getPlane( hdf[hdf["comment"]=="JIG"].copy() )

    # Get local hybrid planes
    hybridPlaneDict = {}
    for label in pd.unique(hdf.comment):
        # Skip the jig, only go over the ASICs
        if label=="JIG":
            continue

        # Get just the rows corresponding to this ASIC
        thisDF = hdf[hdf.comment==label].copy()

        # Get just the rows corresponding to the hybrid points around this ASIC.
        hybridData = thisDF[thisDF.type==1].copy() 
        
        # Get the plane from fitting these hybrid points.
        hybridPlaneDict[label]=getPlane(hybridData)

    # Calculate heights relative to local hybrid planes and/or jig plane
    heightColumn, glueHeightColumn, packageHeightColumn = [], [], []
    projXColumn, projYColumn, projZColumn = [], [], []

    for index, row in hdf.iterrows():
        label = row['comment']
        # Initialize column entries
        height, glueHeight, packageHeight = None, None, None
        projX, projY, projZ = None, None, None

        # Skip the jig.  These columns will be Nan for jig rows.
        if "JIG" not in label.upper():
            # Get local hybrid plane from dictionary.
            hybridPlane = hybridPlaneDict[label]
            # Calculate the point in frame defined by setting local hybrid plane as z=0.  Do for ASIC and hybrid points.
            point = Point([row.x, row.y, row.z])
            proj = hybridPlane.project_point(point)
            projX, projY, projZ = proj[0], proj[1], proj[2]
            height = hybridPlane.distance_point_signed(point)
 
            # For points on ASICs, get glue height and package height relative to jig plane
            if row.type==2:
                glueHeight = height - asicThickness
                packageHeight = jigPlane.distance_point_signed(point)
            
        # Append to the column lists
        heightColumn.append(height)
        glueHeightColumn.append(glueHeight)
        projXColumn.append(projX)
        projYColumn.append(projY)
        projZColumn.append(projZ)
        packageHeightColumn.append(packageHeight)

    ### Write columns to height dataframe
    hdf['height'] = heightColumn
    hdf['glueHeight'] = glueHeightColumn
    hdf['projX'] = projXColumn
    hdf['projY'] = projYColumn
    hdf['projZ'] = projZColumn
    hdf['packageHeight'] = packageHeightColumn
    # print(hdf.to_string())


    # Create dataframe for average heights
    dictList = []
    for label in pd.unique(hdf.comment):
        ### Skip jig rows.
        if "JIG" in label.upper():
            continue
        ### Get the rows corresponding to this ASIC, and also just the rows corresponding to the points ON the ASIC.
        thisDF = hdf[hdf.comment==label].copy() # Get just the rows corresponding to this ASIC
        asicData = thisDF[thisDF.type==2].copy() # Get just the rows corresponding to the points on the ASIC only.        
        ### Calculate average, min, and max of heights (height w.r.t local hybrid plane, glue height, and package height (w.r.t. jig plane).
        d = {'asic':label}
        variables  = ["height",'glueHeight','packageHeight']
        for var in variables:
            d['ave'+var[0].upper()+var[1:]] = asicData[var].mean()
            d['max'+var[0].upper()+var[1:]] = asicData[var].max()
            d['min'+var[0].upper()+var[1:]] = asicData[var].min()
            
        ### Calculate the tilts ###
        ### First find corners
        ### Note: somewhat assumes only the four corners of the ASIC are measured.
        byX = asicData.copy().sort_values(by=['x']) #sort by x.
        lowX = byX.copy()[:int(len(byX)/2)] #get half of points on the low X
        lowXbyY = lowX.copy().sort_values(by=['y'])
        lowXlowY = lowXbyY[:1] # low X, lowest Y point.
        lowXhighY = lowXbyY[-1:] # low X, highest Y point.
        highX = byX.copy()[int(len(byX)/2):] #get half of points at high X 
        highXbyY = highX.copy().sort_values(by=['y'])
        highXlowY = highXbyY[:1] # high X, lowest Y point.
        highXhighY = highXbyY[-1:] # high X, highest Y point.

        # Now calculate appropriate tilts.
        # For both X and Y hybrids, Y-axis goes towards the HCC, so the backend side is at high Y.  Frontend side is at low Y.
        d["tilt_FE"] = calcTilt(lowXlowY, highXlowY)   # tilt in x-direction along front end of chip.
        d["tilt_BE"] = calcTilt(lowXhighY, highXhighY) # tilt in x-direction along back end of chip.
        d["tilt_y1"] = calcTilt(lowXlowY, lowXhighY)   # tilt in y-direction along lowX side of chip.
        d["tilt_y2"] = calcTilt(highXlowY, highXhighY) # tilt in y-directino along lowY side of chip.
            
        # Append this dictionary
        dictList.append(d)
        
    calcDF = pd.DataFrame(dictList)
    calcDF.sort_values(by=['asic'],inplace=True)
    # print(calcDF.to_string())

    # Create plots
    plotDir = os.path.join(outDir, "plots")
    print(f"Saving plots in: {plotDir}")
    if not os.path.exists(plotDir):
        os.makedirs(plotDir)
    
    # Average glue height of each ASIC
    create2DPlot('hyb_asicGlueHeight', calcDF, plotDir)

    # Average package height of each ASIC
    create2DPlot('hyb_asicPackageHeight', calcDF, plotDir)
    
    # ASIC tilts
    create2DPlot('hyb_asicTilt', calcDF, plotDir)

    # ASIC position deviations - drop H_X_ and H_Y_ reference points, then sort by ASIC name
    pdf_asic_rel_pos = pdf[pdf.comment.str.contains('H_')==False].copy().sort_values(by=['comment'])
    create2DPlot('hyb_asicRelativePosition', pdf_asic_rel_pos, plotDir)

    ### Debug plot: all ASIC and hybrid points and all hybrid planes.
    ### Try to get each plane to draw
    listDataFrames = [hdf[hdf["type"]==2].copy(), # get points from just the asic data from the height dataframe
                      hdf[hdf["type"]==1].copy() ]# get points from just the hybrid data from the height dataframe

    listPlanes = [i for i in hybridPlaneDict.values()]
    create3DPlot('hyb_3DMap', listDataFrames, listPlanes, plotDir)
    
    
    # Save dataframes as pickles
    print("\nSaving DataFrames as pickle files in:", outDir)
    pdf.to_pickle(os.path.join(outDir, "position_dataframe.pkl"))
    hdf.to_pickle(os.path.join(outDir, "height_dataframe_raw.pkl"))
    calcDF.to_pickle(os.path.join(outDir, "height_dataframe.pkl"))

    # Check for points out of specification
    # TODO: convert testFailed/testWithProblems into a list/dict, which records which tests had failed, then dump this list into the comments on the database
    testFailed = False
    testWithProblems = False

    ### average package height of each asic
    testFailed_avePackageHeight = False
    if (calcDF.avePackageHeight < getToleranceLow("hybrid", "packageHeight")).any() or (calcDF.avePackageHeight > getToleranceHigh("hybrid", "packageHeight")).any():
        testFailed = True
        testFailed_avePackageHeight = True

    ### average glue height of each asic
    if (calcDF.aveGlueHeight < getToleranceLow('hybrid', 'glueHeight')).any():
        testFailed = True
    if (calcDF.aveGlueHeight > getToleranceHigh('hybrid', 'glueHeight')).any():
        if testFailed_avePackageHeight:
            testFailed = True
        else:
            testWithProblems = True

    ### ASIC tilts
    if (calcDF.tilt_FE > getToleranceHigh('hybrid', 'tilt_FE')).any():
        testFailed = True
    if (calcDF.tilt_BE > getToleranceHigh('hybrid', 'tilt_BE')).any():
        testFailed = True
    if (calcDF.tilt_y1 > getToleranceHigh('hybrid', 'tilt_y1')).any():
        testFailed = True
    if (calcDF.tilt_y2 > getToleranceHigh('hybrid', 'tilt_y2')).any():
        testFailed = True

    ### ASIC position deviations
    if (abs(pdf_asic_rel_pos.xDiff) > getToleranceHigh('hybrid','asicRelPosDX')).any():
        testFailed = True
    if (abs(pdf_asic_rel_pos.yDiff) > getToleranceHigh('hybrid','asicRelPosDY')).any():
        testFailed = True

    #############################
    # Create JSON for DB upload #
    #############################

    # if args.skipJSON:
    #     return

    # if args.download_prototype:
    #     from prototypes.CheckPrototype import fetchDto
    #     prototype = fetchDto("HYBRID_METROLOGY")
    # else:
    prototype_filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "prototypes", "prototype_ASIC_METROLOGY.json")
    print("\nLooking for database JSON prototype:",  prototype_filename)
    with open(prototype_filename) as json_file:
        prototype = json.load(json_file)

    ### Metadata
    # find SN hack: add expected key to dictionary
    if "Hybrid Ref Number" not in metaDict.keys():
        val=None
        for v in metaDict.values():
            if "20US" in v:
                val=v
                break
        metaDict['Hybrid Ref Number']=val
    prototype['component'] = metaDict['Hybrid Ref Number'] # JSON wants a serial or component code, not a local name or alt. id.
    prototype['institution'] = metaDict['Institute'] 
    prototype['runNumber'] = metaDict['Test Run Number'] 
    prototype['passed'] = not testFailed or False #args.passed
    prototype['problems'] = testWithProblems or False #args.problems
    prototype['date'] = metaDict['Measurement Date/Time']
    prototype['properties']['USER'] = metaDict['Operator'] 
    prototype['properties']['SETUP'] = metaDict['Instrument Used'] 
    prototype['properties']['SCRIPT_VERSION'] = metaDict['Measurement Program Name']

    # Add comment with useful info
    testComment = f'Campaign: {campaign}; Thicknesses: ASIC = {asicThickness} mm.'
    prototype['comments'] = [testComment]

    # Placeholders
    #   prototype['properties']['H_MET_SERIAL'] = 
    #   prototype['properties']['CHIP_ORDER'] =

    #To Do:  CONVERT TO MICRONS!
    # Drop H_X_ and H_Y_ reference points, then sort by ASIC name.
    pdf_json =  pdf[pdf.comment.str.contains('H_')==False].copy().sort_values(by=['comment'])
    
    # Merge both fiducial measurements, so [ [x,y], [x,y] ].  One dict entry per asic.
    prototype['results']['POSITION'] = {row["comment"].split('_P')[0]:[[row['xDiff']*1000.,row['yDiff']*1000.], [pdf_json.iloc[index+1]['xDiff']*1000.,pdf_json.iloc[index+1]['yDiff']*1000.]] for index, row in pdf_json.reset_index().iterrows() if index%2==0}

    prototype['results']['HEIGHT'] =  {row["asic"]:row["aveGlueHeight"]*1000. for index,row in calcDF.iterrows()} #sorted    
    prototype['results']['TOTAL_HEIGHT'] = {row["asic"]:row["avePackageHeight"]*1000. for index,row in calcDF.iterrows()} #sorted
    abcTiltDict = {row["asic"]:[row["tilt_BE"],row["tilt_FE"]] for index,row in calcDF.iterrows() if "ABC" in row["asic"]}
    hccTiltDict = {row["asic"]:[row["tilt_FE"],row["tilt_y1"]] for index,row in calcDF.iterrows() if "HCC" in row["asic"]}
    prototype['results']['TILT'] = abcTiltDict | hccTiltDict

    ### Write out
    print("\nSaving JSON for database upload in:", outDir)
    with open(os.path.join(outDir, "results.json"), 'w') as f:
        json.dump(prototype, f, indent=4)
    




def modulePositionAndHeightAnalysis(content, outDir, metaDict, componentType, campaign, useEdges):
    # Get correct spec info
    dictThickness = specifications[campaign]["thickness"]
    dictPosition = specifications[campaign]["position"]["module"]

    moduleSpecType = componentType
    if useEdges:
        print("Using edge-based analysis...")
        moduleSpecType += "_edge"

    # Check if Barrel or Endcap
    isBarrel = False
    isEndcap = False
    if componentType in typeModules_Barrel:
        isBarrel = True
    if componentType in typeModules_Endcap:
        isEndcap = True
        
    # Get Position and Height Data
    pDictList = getPositionData(content)
    hDictList = getHeightData(content)

    # Hack to allow CDF to have PB_1 and PB_P1 in Location data 
    for line in pDictList:
        comment = line.get('comment')
        if fnmatch.fnmatch(comment, 'PB_?'):
            comment = comment.split('_')[0] + '_P' + comment.split('_')[1]
            line['comment'] = comment    

    # Make Dataframes
    pdf = pd.DataFrame(pDictList) # position dataframe.
    hdf = pd.DataFrame(hDictList) # height dataframe.

    # Get sensor plane - start with just the sensor data from the height dataframe
    sensorPlane = getPlane(hdf[hdf["comment"]=="Sensor"].copy())

    # Calculate heights relative to sensor plane + the projection of points on sensor plane
    heightColumn = []
    glueHeightColumn = []
    projXColumn, projYColumn, projZColumn = [], [], []
    for index, row in hdf.iterrows():
        # Get height
        point = Point([row['x'], row['y'], row['z']])
        height = sensorPlane.distance_point_signed(point)
        heightColumn.append(height)

        # Get glue height (does not apply to sensor, capacitor, or shield box measurements)
        glueHeight = None
        if "ABC" in row['comment'] or "HCC" in row['comment'] or "H_" in row['comment'] or "PB" in row['comment']:
            thickness = None
            if "ABC" in row['comment'] or "HCC" in row['comment'] or "H_" in row['comment']:
                hybridThickness = dictThickness["hybrid"]
                if "ABC_X" in row['comment'] or "HCC_X" in row['comment'] or "H_X" in row['comment']:
                    thickness = hybridThickness["X"]
                if "ABC_Y" in row['comment'] or "HCC_Y" in row['comment'] or "H_Y" in row['comment']:
                    thickness = hybridThickness["Y"]
                if "ABC_R" in row['comment'] or "HCC_R" in row['comment'] or "H_R" in row['comment']:
                    thickness = hybridThickness["Endcap"]
            elif "PB" in row['comment']:
                powerboardThickness = dictThickness["powerboard"]
                if isBarrel:
                    thickness = powerboardThickness["Barrel"]
                if isEndcap:
                    thickness = powerboardThickness["Endcap"]
            glueHeight = height - thickness
        glueHeightColumn.append(glueHeight)

        # Get coordinates of point projected on the sensor plane
        proj = sensorPlane.project_point(point)
        projXColumn.append(proj[0])
        projYColumn.append(proj[1])
        projZColumn.append(proj[2])

    ### Add columns to dataframe
    hdf['height'] = heightColumn
    hdf['glueHeight'] = glueHeightColumn
    hdf['projX'] = projXColumn
    hdf['projY'] = projYColumn
    hdf['projZ'] = projZColumn
    # print(hdf.to_string())


    # Make another DF for the averages - these are the main results
    aveDictList = []
    checkAveXDict = {}
    for label in pd.unique(hdf.comment):
        thisDF = hdf[hdf.comment==label].copy()

        # Get the average height, max, and min
        thisAve = thisDF.height.mean()
        thisMax = thisDF.height.max()
        thisMin = thisDF.height.min()
        
        # if applicable, Get the average glue height (and the alternative for ABCs on the edge of the hybrid).
        thisGlueAve,    thisGlueMax,    thisGlueMin    = None, None, None
        thisGlueAveAlt, thisGlueMaxAlt, thisGlueMinAlt = None, None, None
        
        if any(s in label for s in ["ABC", "HCC", "H_", "PB"]):
            thisGlueAve = thisDF.glueHeight.mean()
            thisGlueMax = thisDF.glueHeight.max()
            thisGlueMin = thisDF.glueHeight.min()

            # Barrel only: check if ABC_Y_0 and ABC_Y_9 are in the correct orientation
            if any(s in label for s in ["ABC_Y_0", "ABC_Y_9"]):
                checkAveX = thisDF.groupby("comment")["x"].mean().values[0]
                checkAveXDict[label] = checkAveX

            # Get alternative glue height for ABC_?_edge where the points on the edge of the hybrid are dropped from the average.
            xABClow = ["ABC_X_9", "ABC_Y_0", "ABC_R0H0_10", "ABC_R0H1_0", "ABC_R1H0_10", "ABC_R1H1_0", "ABC_R2H0_11", "ABC_R3H0_10", "ABC_R3H1_10", "ABC_R3H2_0", "ABC_R3H3_0", "ABC_R4H0_10", "ABC_R4H1_10", "ABC_R5H0_10", "ABC_R5H1_10"]
            xABChigh = ["ABC_X_0", "ABC_Y_9", "ABC_R0H0_3", "ABC_R0H1_8", "ABC_R1H0_1", "ABC_R1H1_10", "ABC_R2H0_0", "ABC_R3H0_4", "ABC_R3H1_4", "ABC_R3H2_6", "ABC_R3H3_6", "ABC_R4H0_3", "ABC_R4H1_3", "ABC_R5H0_2", "ABC_R5H1_2"]
            if any(s in label for s in xABClow):
                # drop points at lower x
                sortedDF = thisDF.copy().sort_values('x').reset_index(drop=True)
                subDF = sortedDF.copy()[int(len(sortedDF)/2):] # just keep the half of points at higher x.  Drop points at the edge of the hybrid.
                thisGlueAveAlt = subDF.glueHeight.mean()
                thisGlueMaxAlt = subDF.glueHeight.max()
                thisGlueMinAlt = subDF.glueHeight.min()
            elif any(s in label for s in xABChigh):
                # drop points at higher x.
                sortedDF = thisDF.copy().sort_values('x').reset_index(drop=True)
                subDF = sortedDF.copy()[:int(len(sortedDF)/2)] # just keep the half of points at lower x.  Drop points at the edge of the hybrid.
                thisGlueAveAlt = subDF.glueHeight.mean()
                thisGlueMaxAlt = subDF.glueHeight.max()
                thisGlueMinAlt = subDF.glueHeight.min()
                        
        thisDict = {'measurement':label, 
                    'aveHeight':thisAve, 
                    'maxHeight':thisMax, 
                    'minHeight':thisMin, 
                    'aveGlueHeight': thisGlueAve, 
                    'maxGlueHeight': thisGlueMax, 
                    'minGlueHeight': thisGlueMin,
                    'aveGlueHeightAlt': thisGlueAveAlt,
                    'maxGlueHeightAlt': thisGlueMaxAlt,
                    'minGlueHeightAlt': thisGlueMinAlt}
        aveDictList.append(thisDict)

    # Store in dataframe
    calcDF = pd.DataFrame(aveDictList)
    #print(calcDF.to_string())

    # Barrel only: check if ABC_Y_0 and ABC_Y_9 are in the correct orientation
    if componentType=="MS":
        if not checkAveXDict["ABC_Y_0"] < checkAveXDict["ABC_Y_9"]:
            sys.exit("ABC_Y_0 to 9 are the wrong way round, please reverse them.  Exiting.")

    # Calculate position deviations from spec
    xSpecColumn, ySpecColumn, xDiffColumn, yDiffColumn = [], [], [], []
    for index, row in pdf.iterrows():
        label = row.comment
        specDict = dictPosition[moduleSpecType] # dict with keys = measurement name, values = [specx, specy]
        xSpecColumn.append(specDict[label][0])
        ySpecColumn.append(specDict[label][1])
        xDiffColumn.append(row.x - specDict[label][0])
        yDiffColumn.append(row.y - specDict[label][1])
    
    ### Add columns to dataframe
    pdf['xSpec'] = xSpecColumn
    pdf['ySpec'] = ySpecColumn
    pdf['xDiff'] = xDiffColumn
    pdf['yDiff'] = yDiffColumn
    # print(pdf)


    #  Make plots
    plotDir = os.path.join(outDir, "plots")
    print(f"Saving plots in: {plotDir}")
    if not os.path.exists(plotDir):
        os.makedirs(plotDir)

    # Create 3D plot of sensor points in frame defined by setting the sensor plane to z=0 (i.e. plotting residuals).
    #   Have to get sensor points in this frame.
    #   Will also fit a plane to these transformed points, but it should just be z=0 in this coordinate system.
    sensorSubDF = hdf[hdf.comment=='Sensor'].copy()
    transPoints = Points([ Point([row['projX'],row['projY'],row['height']]) for index, row in sensorSubDF.iterrows()])
    transPlane = Plane.best_fit(transPoints) # should just be z=0 plane (XY plane)
    create3DPlot('mod_3DMap', sensorSubDF, transPlane, plotDir)

    # Plot the average glue height, as well as the alternative glue heights for the outside ABCs
    # Drop rows corresponding to the sensor, capacitors, and shield box since no ave glue height calculated.
    calcDF_plot_glue = calcDF.dropna(subset=['aveGlueHeight'])
    create2DPlot("mod_glueHeight", calcDF_plot_glue, plotDir)

    ### Plot the max height of all measurements (not glue height),
    create2DPlot("mod_maxHeight", calcDF, plotDir)

    ### Plot the relative position (e.g. DeltaX = Spec X - measured X)
    create2DPlot('mod_positionMeasurements', pdf, plotDir)

    # Save dataframes as pickles
    print("\nSaving DataFrames as pickle files in:", outDir)
    pdf.to_pickle(os.path.join(outDir, "position_dataframe.pkl"))
    hdf.to_pickle(os.path.join(outDir, "height_dataframe_raw.pkl"))
    calcDF.to_pickle(os.path.join(outDir, "height_dataframe.pkl"))

    # Check for points out of specification
    testFailed = False
    testWithProblems = False

    # Average glue height
    # Keep only the rows which have aveGlueHeightAlt (ABC_*_edge)
    calcDF_Alt = calcDF.dropna(subset=['aveGlueHeightAlt'])

    # Check if alternative locations are out of spec individually
    for index, row in calcDF_Alt.iterrows():
        if (row.aveGlueHeight < getToleranceLow("module", "glueHeight")) or (row.aveGlueHeight > getToleranceHigh("module", "glueHeight")):
            # If so, then instead report the alternative average, by swapping the two values
            calcDF.at[index, "aveGlueHeight"] = row.aveGlueHeightAlt
            calcDF.at[index, "aveGlueHeightAlt"] = row.aveGlueHeight

    # Keep only the rows which start with ABC
    calcDF_ABC = calcDF[calcDF['measurement'].str.startswith("ABC")]

    # Divide the rows of ABCs for each hybrid type present
    calcDF_ABC_hybrids = calcDF_ABC.groupby(calcDF_ABC['measurement'].str.split('_').str[1])

    # Keep only the rows which start with PB but exclude PB_5
    calcDF_PB = calcDF[calcDF['measurement'].str.startswith("PB") & ~calcDF['measurement'].str.contains("PB_5")]

    # Load each divided ABC hybrid type and PB into a list of locations to check
    locations = [calcDF_PB]
    for key, item in calcDF_ABC_hybrids:
        locations.append(calcDF_ABC_hybrids.get_group(key))

    # Only the averages of each location that start with ABC (per hybrid type) or PB (if they exist) must not be out of spec
    for location in locations:
        if not location.empty:
            if (location.aveGlueHeight.mean() < getToleranceLow("module", "glueHeight")) or (location.aveGlueHeight.mean() > getToleranceHigh("module", "glueHeight")):
                testFailed = True
            else:
                # But may pass with problems if below the target
                if location.aveGlueHeight.mean() < getToleranceTargetLow("module", "glueHeight"):
                    testWithProblems = True

    # Max height of all measurements
    if (calcDF.maxHeight > getToleranceHigh("module", "totalHeight")).any():
        testFailed = True

    # Relative position
    if (abs(pdf.xDiff) > getToleranceHigh("module", "relPosDX")).any():
        testFailed = True
    if (abs(pdf.yDiff) > getToleranceHigh("module", "relPosDY")).any():
        testFailed = True
    
    # Create JSON for DB upload
    # if args.skipJSON:
    #     return

    # if args.download_prototype:
    #     from prototypes.CheckPrototype import fetchDto                
    #     prototype = fetchDto("MODULE_METROLOGY")
    # else:
    prototype_filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "prototypes", "prototype_MODULE_METROLOGY.json")
    print("\nLooking for database JSON prototype:", prototype_filename)
    with open(prototype_filename) as json_file:
        prototype = json.load(json_file)

    ### Metadata
    # find SN hack: add expected key to dictionary
    if "Module ref. Number" not in metaDict.keys():
        val=None
        for v in metaDict.values():
            if "20US" in v:
                val=v
                break
        metaDict['Module ref. Number']=val
    prototype['component'] = metaDict['Module ref. Number'] # JSON wants serial or component code, not a local name or alt. ID.
    prototype['institution'] = metaDict['Institute'] 
    prototype['runNumber'] = metaDict['Run Number']
    prototype['passed'] = not testFailed or False #args.passed
    prototype['problems'] = testWithProblems or False #args.problems
    prototype['date'] = metaDict['Date'] 
    prototype['properties']['MACHINE'] = metaDict['Instrument type']
    prototype['properties']['OPERATOR'] = metaDict['Operator']
    prototype['properties']['SCRIPT_VERSION'] = metaDict['Measurement program version']
    
    ### Add comment with useful info.
    testComment = f'Campaign: {campaign}; Thicknesses: hybrid X = {dictThickness["hybrid"]["X"]} mm, Y = {dictThickness["hybrid"]["Y"]} mm, EC = {dictThickness["hybrid"]["Endcap"]} mm, powerboard Barrel = {dictThickness["powerboard"]["Barrel"]} mm, Endcap = {dictThickness["powerboard"]["Endcap"]} mm.'
    prototype['comments'] = [testComment]
    
    ### Results
    prototype['results']['HYBRID_POSITION'] = {row['comment']:[row['xDiff']*1000., row['yDiff']*1000.] for index,row in pdf.iterrows() if row['comment'].startswith('H_')}
    prototype['results']['HYBRID_GLUE_THICKNESS'] = {row['measurement']:row['aveGlueHeight']*1000. for index,row in calcDF.iterrows() if row['measurement'].startswith('ABC') or row['measurement'].startswith('HCC') or row['measurement'].startswith('H_')}
    prototype['results']['PB_POSITION'] = {row['comment']:[row['xDiff']*1000., row['yDiff']*1000.] for index,row in  pdf.iterrows() if row['comment'].startswith('PB')}
    prototype['results']['PB_GLUE_THICKNESS'] = {row['measurement']:row['aveGlueHeight']*1000. for index,row in calcDF.iterrows() if row['measurement'].startswith('PB')}
    prototype['results']['CAP_HEIGHT'] = {row['measurement']:row['maxHeight']*1000. for index,row in calcDF.iterrows() if row['measurement'].startswith('C')}
    prototype['results']['SHIELDBOX_HEIGHT'] = max([row['maxHeight']*1000. for index,row in calcDF.iterrows() if row['measurement'].startswith('Shield')], default=None)

    ### Write out
    print("\nSaving JSON for database upload in:", outDir)
    with open(os.path.join(outDir, "results.json"), 'w') as f:
        json.dump(prototype, f, indent=4)
    



def moduleBowAnalysis(content, outDir, metaDict, componentType, campaign, temperature):
    # Get temperature
    # requiredTemperature = True
    # if requiredTemperature:
    #     if args.temperature is None:
    #         parser.error("Module bow test requires a --temperature argument.")

    # temperature = -999.9
    # if args.temperature:
    #     temperature = float(args.temperature)

    # Get Sensor  Data
    start = [i for i, x in enumerate(content) if x.startswith('#') and 'BOW' in x.upper()][0] + 2 # skip this line and the column titles.
    dictList = []
    for line in content[start:]:
        comment = line.split()[0]
        x = float(line.split()[1])
        y = float(line.split()[2])
        z = float(line.split()[3])
        d = {"comment":comment, "x":x, "y":y, "z":z}
        dictList.append(d)
    sdf = pd.DataFrame(dictList)

    # Get sensor plane
    sensorPoints = getPointsXYZ(sdf)
    sensorPlane = Plane.best_fit(sensorPoints)


    # Calculate sensor heights w.r.t. to sensor plane
    # Essentially transforming to frame where the sensor plane is z=0 - the heights (z in this frame) are residuals
    heightColumn = []
    heightColumn_um = []
    projXColumn, projYColumn, projZColumn = [], [], []
    for point in sensorPoints:
        height = sensorPlane.distance_point_signed(point)
        height_um = height*1000.0
        proj = sensorPlane.project_point(point)
        heightColumn.append(height)
        heightColumn_um.append(height_um)
        projXColumn.append(proj[0])
        projYColumn.append(proj[1])
        projZColumn.append(proj[2])

    # Add columns to dataframe
    sdf['height'] = heightColumn
    sdf['height_um'] = heightColumn_um
    sdf['projX'] = projXColumn
    sdf['projY'] = projYColumn
    sdf['projZ'] = projZColumn
    # print(sdf.to_string())
        
    # Calculate bow and try to develop algorithm to consistently get sign correct
    ### Algorithm:
    ### First calculate |bow| = (max height - min hieght).
    ###    Where the height of each point is relative to the plane fit to all sensor points.
    ### Then determine the sign.
    ###    Divide the sensor into N sections in X and Y --> N*N grid imposed over sensor points.
    ###    Take all points in the outter ring of the grid as the edge (requires N>2).
    ###    Take all other points as the center.
    ###    Compare the mean of the edge points to the mean of the center points.
    ###    Convention: if center is above the edge, the bow is negative.
    ### Alternative:
    ###    Same as above, but just take sign of the average of the center points.
    ###       If it's negative relative to plane, the center is lower and the bow is positive.
    ###       And vice versa.
    
    # Divide into this number of sectors.  So a (div) x (div) grid.  Outter ring is the edge.
    div = 4

    print("\nCalculating the bow with divisor {}:".format(div))
    print("max:", sdf.height.max())
    print("min:", sdf.height.min())
    print("max - min =", sdf.height.max() - sdf.height.min())

    # Calculate |bow| in microns.
    BOW = 1000*(sdf.height.max() - sdf.height.min()) #always positive.

    # Get sign
    xmax = sdf.x.max()
    xmin = sdf.x.min()
    ymax = sdf.y.max()
    ymin = sdf.y.min()
    xstep = (xmax - xmin)/div
    ystep = (ymax - ymin)/div
    sdf_edge = sdf.loc[ (sdf['x']<xmin+1*xstep) | (sdf['x']>xmin+(div-1)*xstep) | (sdf['y']<ymin+1*ystep) | (sdf['y']>ymin+(div-1)*ystep) ]
    sdf_center = sdf.merge(sdf_edge,indicator = True, how='left').loc[lambda x : x['_merge']!='both'] # get all points from sdf not in sdf_edge.
    print('\nEdge average:', sdf_edge.height.mean())
    print('Center average:', sdf_center.height.mean())

    if sdf_center.height.mean() > sdf_edge.height.mean(): # make BOW negative if center is above the edge.
        print("\nThe bow is negative since the center is above the edges. ")
        BOW = -1*BOW
    print("Final bow in microns:", BOW)

    # Make plots sub dir
    plotDir = os.path.join(outDir, "plots")
    print(f"Saving plots in: {plotDir}")
    if not os.path.exists(plotDir):
        os.makedirs(plotDir)

    #  2D plot of sensor points in frame where fitted plane defines z'=0
    create2DPlot('bow_SensorHeightRelPlane', sdf, plotDir)
   
    # 3D plot of sensor points in original frame
    create3DPlot('bow_3DMap', sdf, sensorPlane, plotDir)
    
    # 3D plot of sensor points in frame where fitted plane defines z'=0
    z0plane = Plane(point=[0, 0, 0], normal=[0, 0, 1])
    create3DPlot('bow_3DMapPlaneFrame', sdf, z0plane, plotDir)

    # 3D plot of sensor points in frame where fitted plane defines z'=0 (as a surface)
    create3DPlot('bow_3DMapPlaneFrameSurf', sdf, z0plane, plotDir)
    
    # Debug plot: Not included in plot library, as it has no mechanism for plotting multiple data frames on one axes
    fig, ax1 = plt.subplots()
    sdf_edge.plot.scatter(x='x', y='y', s=10, c="k", label='edge',ax=ax1)
    sdf_center.plot.scatter(x='x', y='y', s=10, c="firebrick", label='center',ax=ax1)
    plt.xlabel("X [mm]")
    plt.ylabel("Y [mm]")
    plt.title("Edge vs. Center Points for Bow Calculation")
    # invert drawing of x and y axes to match real world coordinate system.
    ax_inv = plt.gca()
    # have to define origin in measuremind at bottom right corner of sensor, w/ x-axis extending to left.
    ax_inv.set_xlim(ax_inv.get_xlim()[::-1])
    plt.savefig(os.path.join(plotDir, 'sensor_points_edgeVsCenter.pdf'))

    
    # Save dataframes as pickles
    print("\nSaving DataFrames as pickle files in:", outDir)
    sdf.to_pickle(os.path.join(outDir, "sensor_dataframe.pkl"))

    # Check for points out of specification
    testFailed = False
    testWithProblems = False

    if BOW < getToleranceLow("bow", "bowing") or BOW > getToleranceHigh("bow", "bowing"):
        testFailed = True

    # # Create JSON for DB upload
    # if args.skipJSON:
    #     return

    # if args.download_prototype:
    #     from prototypes.CheckPrototype import fetchDto                
    #     prototype = fetchDto("MODULE_BOW")
    # else:
    prototype_filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "prototypes", "prototype_MODULE_BOW.json")
    print("\nLooking for database JSON prototype:", prototype_filename)
    with open(prototype_filename) as json_file:
        prototype = json.load(json_file)

    # Metadata
    # find SN hack: add expected key to dictionary
    if "Module ref. Number" not in metaDict.keys():
        val=None
        for v in metaDict.values():
            if "20US" in v:
                val=v
                break
        metaDict['Module ref. Number']=val
    prototype['component'] = metaDict['Module ref. Number'] # JSON wants a serial or component code, not a local name or alt. ID.
    prototype['institution'] = metaDict['Institute'] 
    prototype['runNumber'] = metaDict['Run Number'] 
    prototype['date'] = metaDict['Date'] 
    prototype['passed'] = not testFailed or False #args.passed
    prototype['problems'] = testWithProblems or False #args.problems
    prototype['properties']['JIG'] = 'Module Assembly Jig' #hard coded.  Not sure if just checking if it's on a test panel, or if they want to track specific jig (flatness).
    prototype['properties']['OPERATOR'] = metaDict['Operator'] 
    prototype['properties']['USED_SETUP'] = metaDict['Instrument type'] 
    prototype['properties']['SCRIPT_VERSION'] = metaDict['Measurement program version']

    ### CONVERT TO MICRONS!
    prototype['results']['BOW'] = BOW
    prototype['results']['TEMPERATURE'] = temperature

    ### Write out
    print("\nSaving JSON for database upload in:", outDir)
    with open(os.path.join(outDir, "results.json"), 'w') as f:
        json.dump(prototype, f, indent=4)
    print ("Finished!")


# Entry point
if __name__ == "__main__":
    main()
