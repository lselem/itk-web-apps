import numpy as np
import matplotlib as mpl

from .spec import *

plotLibrary = {}

def generatePlotLibrary(moduleName="", campaign=None):
    plotLibrary.clear()
    plotLibrary.update({
        'hyb_asicGlueHeight':{
            'xvar':'asic',
            'yvar':'aveGlueHeight',
            'yerr':'GlueHeight',
            'xlabel':"ASIC",
            'ylabel':"Average Glue Height [mm]",
            'legend':"average glue height",
            'title':"Average Glue Height per ASIC",
            'hlines':[(getToleranceNominal("hybrid", "glueHeight"),'black'),
                      (getToleranceLow ("hybrid", "glueHeight"), 'firebrick'), 
                      (getToleranceHigh("hybrid", "glueHeight"), 'firebrick')],
            'ymin':0.050,
            'ymax':0.200,
            'ymajor':0.050,
            'yminor':0.010,
            'output':'asic_glue_height.pdf',
        },
        'hyb_asicPackageHeight':{
            'xvar':'asic',
            'yvar':'avePackageHeight',
            'yerr':'PackageHeight',
            'xlabel':"ASIC",
            'ylabel':"Average Package Height [mm]",
            'legend':"average package height",
            'title':"Average Package Height per ASIC",
            'hlines':[(getToleranceNominal("hybrid", "packageHeight", campaign),'black'),
                      (getToleranceTargetLow ("hybrid", "packageHeight", campaign), 'firebrick'), 
                      (getToleranceTargetHigh("hybrid", "packageHeight", campaign),'firebrick'),
                      (getToleranceLow ("hybrid", "packageHeight", campaign), 'mediumblue'), 
                      (getToleranceHigh("hybrid", "packageHeight", campaign), 'mediumblue')],
            'ymin':0.725,
            'ymax':0.925,
            'ymajor':0.050,
            'yminor':0.010,
            'output':'asic_package_height.pdf',
        },
        'hyb_asicTilt':{
            'xvar':'asic',
            'yvar':['tilt_FE', 'tilt_BE', 'tilt_y1', 'tilt_y2'],
            'color':['blue', 'none', 'firebrick', 'none'], # None = grey-blue, 'none' = white
            'edgecolor':[None, 'blue', None, 'firebrick'], # None = same as 'color', 'none' = white
            'marker':["^", "^", "s", "s"],
            'legend':["Front-end X-tilt", "Back-end X-tilt", "Left Y-tilt", "Right Y-tilt"],
            'markersize':20,
            'ylabel':'Tilt',
            'title':"ASIC Tilt",
            'ymin':0.00,
            'ymax':0.01,
            'ymajor':0.0010,
            'yminor':0.0002,
            'tightlayout':True,
            'annotate':True,
            'output':'asic_tilt.pdf',
        },
        'hyb_asicRelativePosition':{
            'xvar':'comment',
            'yvar':['xDiff', 'yDiff'],
            'color':['blue', 'red'], # None = grey-blue, 'none' = white
            'edgecolor':[None, None], # None = same as 'color', 'none' = white
            'marker':["^", "v"],
            'legend':["X-coordinate", "Y-coordinate"],
            'markersize':20,
            'ylabel':'Relative Position (Ref. - Meas.) [mm]',
            'hlines':[(getToleranceNominal("hybrid", "asicRelPosDX"),'black'), 
                      (getToleranceLow ("hybrid", "asicRelPosDX"), 'firebrick'), 
                      (getToleranceHigh("hybrid", "asicRelPosDX"), 'firebrick')],
            'ymin':-0.250,
            'ymax':0.250,
            'ymajor':0.05,
            'yminor':0.01,
            'tightlayout':True,
            'output':'asic_relative_position.pdf',
        },
        'hyb_3DMap':{
            'is3D':True,
            'xvar':'x',
            'yvar':'y',
            'zvar':'z',
            'xlabel': "X [mm]",
            'ylabel': "Y [mm]",
            'zlabel': "Z [mm]",
            'planelimitsx': (-5, 5),
            'planelimitsy': (-5, 5),
            'planealpha': 0.2,
            'output':'full_hybrid.pdf'
        },
        'mod_glueHeight':{
            'xvar':'measurement',
            'yvar':['aveGlueHeight', 'aveGlueHeightAlt'],
            'yerr':['GlueHeight', 'GlueHeightAlt'],
            'color':['k', 'green'], # None = grey-blue, 'none' = white
            'edgecolor':[None, None], # None = same as 'color', 'none' = white
            'legend':["Glue Height", "Alt. Glue Height"],
            'legendloc':'upper left',
            'markersize':10,
            'xlabel':"Measurement",
            'ylabel':'Glue Height [mm]',
            'hlines':[(getToleranceNominal("module", "glueHeight"),'black'),
                      (getToleranceTargetLow("module", "glueHeight"), 'mediumblue'),
                      (getToleranceLow ("module", "glueHeight"), 'firebrick'), 
                      (getToleranceHigh("module", "glueHeight"), 'firebrick')],
            'ymin':0.00,
            'ymax':0.25,
            'ymajor':0.05,
            'yminor':0.01,
            'tightlayout':True,
            'output':'glue_height_measurements.pdf',
        },
        'mod_maxHeight':{
            'xvar':'measurement',
            'yvar':'maxHeight',
            'color':'mediumblue',
            'legend':"Height",
            'legendloc':'upper left',
            'xlabel':"Measurement",
            'ylabel':'Max height w.r.t. Sensor Plane [mm]',
            'hlines':[(getToleranceHigh("module", "totalHeight"), 'black')],
            'ymin':0.00,
            'ymax':6.00,
            'ymajor':1.00,
            'yminor':0.20,
            'output':'height_measurements.pdf'
        },
        'mod_positionMeasurements':{
            'xvar':'comment',
            'yvar':['xDiff', 'yDiff'],
            'color':['mediumblue', 'firebrick'],
            'marker':["^", "v"],
            'markersize':10,
            'legend':["X-coordinate" , "Y-coordinate"],
            'legendloc':'upper left',
            'xlabel':"Measurement",
            'ylabel':"Relative Position (ref-meas) [mm]",
            'hlines':[(getToleranceNominal("module", "relPosDX"),'black'), 
                      (getToleranceLow ("module", "relPosDX"), 'mediumblue'), 
                      (getToleranceHigh("module", "relPosDX"), 'mediumblue'),
                      (getToleranceLow ("module", "relPosDY"), 'firebrick'), 
                      (getToleranceHigh("module", "relPosDY"), 'firebrick')],
            'ymin':-0.350,
            'ymax':0.350,
            'ymajor':0.10,
            'yminor':0.02,
            'xtickrot':0,
            'output':'position_measurements.pdf'
        },
        'mod_3DMap':{
            'is3D':True,
            'xvar':'projX',
            'yvar':'projY',
            'zvar':'height',
            'xlabel': "X [mm]",
            'ylabel': "Y [mm]",
            'zlabel': "Z [mm]",
            'pointscolor': 'z',
            'planelimitsx': (-50, 50),
            'planelimitsy': (-50, 50),
            'planealpha': 0.2,
            'output':'sensor_points_heights.pdf'
        },
        'bow_3DMap':{
            'is3D':True,
            'xvar':'x',
            'yvar':'y',
            'zvar':'z',
            'xlabel': "X [mm]",
            'ylabel': "Y [mm]",
            'zlabel': "Z [mm]",
            'pointscolor': 'z',
            'planelimitsx': (-50, 50),
            'planelimitsy': (-50, 50),
            'planealpha': 0.2,
            'output':'sensor_points_3D.pdf'
        },
        'bow_3DMapPlaneFrame':{
            'is3D':True,
            'xvar':'projX',
            'yvar':'projY',
            'zvar':'height_um',
            'xlabel': "X [mm]",
            'ylabel': "Y [mm]",
            'zlabel': "Z " +r'[$\mu$m]',
            'pointscolor': 'z',
            'planelimitsx': (0, 100),
            'planelimitsy': (0, 100),
            'planealpha': 0.2,
            'altviews' : [(15,-90), (0,90)],
            'output':'sensor_points_planeFrame_3D.pdf',
            'animation': False,
            'animationstart': 0,
            'animationstop': 24*2*np.pi, #24 full rotations
            'animationframes': 64,
            'animationoutput':'sensor_points_planeFrame_3D.gif'
        },
        'bow_SensorHeightRelPlane':{
            'xvar':'projX',
            'yvar':'projY',
            'color':'height_um', #Not sure why this works, but color = variable results in color map
            'marker':"s",
            'markersize':10,
            'xlabel':"X [mm]",
            'ylabel':"Y [mm]",
            'title':"Sensor Point Height Relative to Plane " +r'[$\mu$m]',
            'ymin':-100,
            'ymax':0,
            'ymajor': 20,
            'yminor': 5,
            'showlegend': False,
            'annotatepoints': True,
            'invertxaxis': True,
            'output':'sensor_points_planeFrame_2D.pdf'
        },
        'bow_3DMapPlaneFrameSurf':{
            'is3D':True,
            'xvar':'projX',
            'yvar':'projY',
            'zvar':'height_um',
            'color':'height_um',
            'xlabel': "X [mm]",
            'ylabel': "Y [mm]",
            'zlabel': "Z " +r'[$\mu$m]',
            'pointscolor': 'z',
            'planelimitsx': (0, 100),
            'planelimitsy': (0, 100),
            'planealpha': 0.2,
            'pointstrisurf': True,
            'cmap': mpl.cm.coolwarm,
            'output':'sensor_points_planeFrame_3D_surface.pdf',
        },
    })
