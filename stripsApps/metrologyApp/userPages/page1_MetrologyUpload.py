### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from .UsefulCode import GetGleanList
from .UsefulCode import GetAnaObjs
from .UsefulCode import DisplayPDF
from .UsefulCode import fetchDto
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses from p_d_s (03/02/22)
from .analysis import hybridPositionAndHeightAnalysis
from .analysis import modulePositionAndHeightAnalysis
from .analysis import moduleBowAnalysis
### Get specifications
from .spec import specifications, typeHybrids, typeModules

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _txt_ metrology file",
        " * review retrieved data & visualisation",
        " * select analysis type and run analysis",
        " * review analysis checks (from [here](https://gitlab.cern.ch/gdyckes/metrologyanalysis/-/tree/master/))",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]

#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Metrology Upload", ":microscope: Upload Metrology Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "testType" not in pageDict.keys():
            pageDict['testType']="MODULE_METROLOGY"
        if "stage" not in pageDict.keys():
            pageDict['stage']="GLUED"
        if "project" not in pageDict.keys():
            pageDict['project']="S"

        st.write("## Read Data")

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName)
        # st.write("__ALL__", df_stageTest)
        df_stageTest['testType']=df_stageTest['testType'].astype(str) #.str.strip()
        compList=['MODULE','HYBRID_FLEX','SENSOR']
        df_stageTest=df_stageTest.query('compType in @compList').reset_index(drop=True)# & testType.str.contains("'+pageDict['testType']+'")', engine='python').reset_index(drop=True)
        # st.write("__query__", df_stageTest)
        df_stageTest=df_stageTest.query('testType.str.contains("METROLOGY")', engine='python').reset_index(drop=True)
        # st.write("__query__", df_stageTest)
        st.write(f"### componentType in {compList} and stages for METROLOGY testTypes")
        st.dataframe(df_stageTest)

        st.write("__NB__ Sensor data cannot be uploaded to PDB here - but analysis can be run as a _pseudo_ module")

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',compList,"Select componentType code:")
            infra.SelectBox(pageDict,'testType',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['testType'].to_list(),"Select test code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'" & testType=="'+pageDict['testType']+'"')['stage'].to_list(),"Select stage code:")


        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testType']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':True})


        stTrx.DebugOutput("Original *schema*:",pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["txt","dat"])

        stTrx.DebugOutput("Uploaded file:",pageDict['file'])

        if pageDict['file'] is not None:
            headerList, dataList = GetGleanList(pageDict['file'], pageDict['componentType'])
            # hack for sensor CDF
            if "sens" in pageDict['componentType'].lower():
                dataList = dataList[1::]
        else:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="module_positionHeight_ML.txt"
            stTrx.DebugOutput("looking for file in:",filePath[:filePath.rfind('/')])
            st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
            st.write("See p.16 [EDMS document](https://edms.cern.ch/ui/file/2228446/3.2/QC-Metrology-Procedures_Modules.pdf) for details on format")
            st.write("More examples [here](https://gitlab.cern.ch/gdyckes/metrologyanalysis/-/tree/master/test_files)")
            
            keepList=['componentType', 'testType', 'stage', 'project', 'file']
            loseList=[key for key in pageDict.keys() if key not in keepList]
            for k in loseList:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass

            st.stop()

        st.write("---")

        st.write("## Gleaned Data")

        st.write("### header info.")
        df_header = pd.DataFrame(headerList, columns=list('abcdef'))
        df_header=df_header.dropna(axis=1, how='all')
        st.dataframe(df_header)
        st.write("### data info.")
        df_data = pd.DataFrame(dataList, columns=list('abcdef'))
        df_data=df_data.dropna(axis=1, how='all')
        st.dataframe(df_data)


        # check if bow test
        if "isBow" not in pageDict.keys():
            pageDict['isBow']=False
            if "bow" in pageDict['file'].name.lower():
                st.write("Bow detected in file name")
                pageDict['isBow']=True
        infra.ToggleButton(pageDict,'isBow',"This is a module bow test?")

        if pageDict['isBow']:
            pageDict['bowTemp']=st.slider('Temperature during module bow test (in degrees Celcius).', min_value=5.0, max_value=35.0, value=20.0, step=0.1)

        st.write("### Visualisation")

        if pageDict['isBow']:

            if "sens" in pageDict['componentType'].lower():
                df_bow=df_data.rename(columns = {'a':"x",'b':"y",'c':"z",'d':"z_bow"})
                df_bow['name']="BOW"
            else:
                df_bow=df_data.query('a.str.contains("BOW")')
                df_bow=df_bow.rename(columns = {'a':"name",'b':"x",'c':"y",'d':"z"})
            df_bow=df_bow.dropna()
            stTrx.DebugOutput("Bow info.:",df_bow)

            st.write("**Bow visualisation**")
            st.altair_chart(
                alt.Chart(df_bow).mark_rect().encode(
                        x='x:Q',
                        y='y:Q',
                        color='z:Q',
                        tooltip=['x:Q','y:Q','z:Q']
                ).properties(width=600).interactive()
            )

        else:
            infra.Radio(pageDict,'visSel',["Sensor","ABC","Positions"],"Select visualisation")

            if pageDict['visSel']=="Sensor":

                df_sensor=df_data.query('a=="Sensor"')
                df_sensor.rename(columns = {'a':"name",'b':"type",'c':"x",'d':"y",'e':"z"}, inplace = True)
                stTrx.DebugOutput("Sensor info.:",df_sensor)

                st.write("**Sensor visualisation**")
                st.altair_chart(
                    alt.Chart(df_sensor).mark_rect().encode(
                            x='x:Q',
                            y='y:Q',
                            color='z:Q',
                            tooltip=['x:Q','y:Q','z:Q']
                    ).properties(width=600).interactive()
                )


            elif pageDict['visSel']=="ABC":

                df_abc=df_data.query('a.str.contains("ABC|PB_|H_")')
                df_abc.rename(columns = {'a':"name",'b':"type",'c':"x",'d':"y",'e':"z"}, inplace = True)
                df_abc=df_abc.dropna()
                stTrx.DebugOutput("ABC info.:",df_abc)

                st.write("**ABC visualisation**")
                st.altair_chart(
                    alt.Chart(df_abc).mark_circle().encode(
                                x='name:O',
                                y='z:Q',
                                color='type:O',
                                tooltip=['name:O','z:Q','type:O']
                    ).properties(width=600).interactive()
                )

            elif pageDict['visSel']=="Positions":
                df_pos=df_data.query('a.str.contains("ABC|PB_|H_")')
                df_pos=df_pos[df_pos.isnull().any(1)]
                df_pos=df_pos.dropna(axis=1, how='all')
                df_pos.rename(columns = {'a':"name",'b':"x",'c':"y"}, inplace = True)
                stTrx.DebugOutput("Position info.:",df_pos)

                st.write("**Positions visualisation**")
                st.altair_chart(
                    alt.Chart(df_pos).mark_circle().transform_fold(
                                    ['x', 'y']
                                ).encode(
                                x='name:O',
                                y='value:Q',
                                color='key:N',
                                tooltip=['name:O','value:Q','key:N']
                    ).properties(width=600).interactive()
                )

            else:
                st.write("No visualisation selected.")


        st.write("---")
        st.write("## Analysis")
        st.write("Thanks to _Ian Dyckes_, _Theo Zorbas_ and _Mitch Norfolk_. Code [here](https://gitlab.cern.ch/gdyckes/metrologyanalysis/-/tree/master/) - last update April 2024")

        if "anaDone" not in pageDict.keys():
            pageDict['anaDone']=False

        ### extract data in format for analysis
        content, metaDict = GetAnaObjs(pageDict['file'])

        ### hack to turn sensor data into pseudo-module
        if "sens" in pageDict['componentType'].lower():
            st.write("__This is a sensor__")
            st.write(" - _hacking_ to fit _module_ format")
            # add in flag line
            dataIdx=[i for i, x in enumerate(content) if "[mm]" in x][0]
            st.write(f" - insert lines: {dataIdx}")
            # reverse fill
            content.insert(dataIdx+1, "# Location      X [mm]     Y [mm]     Z [mm]")
            content.insert(dataIdx+1, "#---Bow Scan:")
            st.write(f" - insert add 'BOW'")
            content = ["BOW "+x if i>dataIdx+2 else x for i, x in enumerate(content) ] # skip this line and the column titles.
            st.write(f" - add module meta data")
            metaDict['Module type']="ML"
            metaDict['Module ref. Number']=None
            metaDict['Date']="2024-03-07T14:02:20.1234Z"
            metaDict['Institute']="GL"
            metaDict['Operator']="erovevor"
            metaDict['Instrument type']="sdferffg"
            metaDict['Run Number']="1"
            metaDict['Measurement program version']="sffsg"

            pageDict['anaType']=metaDict['Module type']
        # st.write(metaDict)
        # st.write(content)

        stTrx.DebugOutput("content:",content)
        stTrx.DebugOutput("metaDict:",metaDict)

        filePath=os.path.realpath(__file__)
        outDir=filePath[:filePath.rfind('/')]+"/results/"
        stTrx.DebugOutput("outDir:",outDir)
        plotDir=outDir+"plots/"
        stTrx.DebugOutput("plotDir:",plotDir)

        if st.session_state.debug:
            protoFiles=[f for f in os.listdir(outDir.replace('results','prototypes')) if os.path.isfile(os.path.join(outDir.replace('results','prototypes'), f))]
            st.write("proto files:",protoFiles)

        if "protoUpdate" not in pageDict.keys():
            pageDict['protoUpdate']=pdbTrx.TimeStampConverter("now",None)
            fetchDto(st.session_state.myClient, outDir.replace('results','prototypes'), "ALL", False, True)

        if st.session_state.debug:
            st.write("You can update prototype test jsons. Only required once per session.")
            if st.button("Update prototypes"):
                pageDict['protoUpdate']=pdbTrx.TimeStampConverter("now",None)
                fetchDto(st.session_state.myClient, outDir.replace('results','prototypes'), "ALL", True, True)
        st.write("Last prototype update:",pageDict['protoUpdate'])

        # detect analysis code if possible
        ### hack to deal with capitalisation variation
        lowerList=[i.lower() for i in df_header['a'].to_list()]
        valStr="Module type"
        if valStr.lower() in lowerList:
            val=df_header['a'].to_list()[lowerList.index(valStr.lower())]
            st.write(f"Found {val} in header info.")
            try:
                pageDict['anaType']=df_header.query('a=="'+val+'"')['b'].values[0]
                st.write(f"using analysis type: {pageDict['anaType']} from header info.")
            except IndexError:
                st.write("Could not identify Module type from header info.")
        else:
            st.write("no Module type in header info.")
        # select analysis code
        anaCodes= typeHybrids + typeModules
        if "anaType" not in pageDict.keys():
            for ac in anaCodes:
                if ac in pageDict['file'].name:
                    pageDict['anaType']=ac
        infra.SelectBox(pageDict,'anaType',anaCodes,"Select analysis type")
        # select campaign code
        ### hack to deal with capitalisation variation
        campList=list(specifications.keys())
        lowerList=[i.lower() for i in campList]
        if "campType" not in pageDict.keys():
            for cc in lowerList:
                if cc in pageDict['file'].name.lower():
                    val=campList[lowerList.index(cc)]
                    st.write(f"Found {val} in filename")
                    pageDict['campType']=val
                    break
        infra.SelectBox(pageDict,'campType',campList,"Production campaign. Important for pulling correct spec.")

        # select edges
        if "useEdges" not in pageDict.keys():
            if "edges" in pageDict['file'].name:
                pageDict['useEdges']=False
        infra.ToggleButton(pageDict,'useEdges',"Metrology was performed using the edges, not the fiducials? (modules only)")


        ### Get serialNumber from file name if possible - input if not
        # check file name
        if "compSN" not in pageDict.keys():
            serialNumber=None
            st.write(f"Try extracting serialNumber from fileName: {pageDict['file'].name}")
            for x in pageDict['file'].name.replace('.xls','').replace('-','_').split('_'):
                if "20U" in x:
                    serialNumber=x.replace('SN','').replace('sn','')
            if serialNumber!=None:
                st.write(f" - extracted serialNumber: {serialNumber}")
                pageDict['compSN']=serialNumber
            else:
                st.write(" - cannot find suitable string")
        else:
            st.write(f"Using component serial number: {pageDict['compSN']}")
        # manually set
        if "compSN" not in pageDict.keys() or st.checkbox(f"Set serialNumber?"):
            infra.TextBox(pageDict,'compSN',f"Enter module serialNumber:")
            st.write(f" - {pageDict['compSN']} updated")
        metaDict['Module ref. Number']=pageDict['compSN']
        

        # no DB
        if "noDB" not in pageDict.keys():
            if "noDB" in pageDict['file'].name:
                pageDict['noDB']=False
        infra.ToggleButton(pageDict,'noDB',"Don't use the DB to fetch hybrid flex QC thicknesses? (modules only)")

        if st.button("analyse!"):
            pageDict['anaDone']=True
            # clean results directory
            files=[f for f in os.listdir(plotDir) if os.path.isfile(os.path.join(plotDir, f))]
            for f in files:
                os.remove(os.path.join(plotDir, f))
            # run analysis
            if "H" in pageDict['anaType']:
                st.write(f"Running position and height analysis on a {pageDict['anaType']}-type hybrid and {pageDict['campType']} campaign")
                hybridPositionAndHeightAnalysis(content, outDir, metaDict, pageDict['anaType'], pageDict['campType'])
            elif "M" in pageDict['anaType']:
                if pageDict['isBow']:
                    st.write("Running bow analysis on a module.")
                    moduleBowAnalysis(content, outDir, metaDict, pageDict['anaType'], pageDict['campType'], pageDict['bowTemp'])
                else:
                    st.write(f"Running position and height analysis on a {pageDict['anaType']}-strip module and {pageDict['campType']} campaign")
                    modulePositionAndHeightAnalysis(content, outDir, metaDict, pageDict['anaType'], pageDict['campType'], pageDict['useEdges'], pageDict['noDB'], st.session_state.myClient)
            else:
                sys.exit("Should not get here. Exiting.")
            
            ### delete any existing testSchema 
            try:
                del pageDict['testSchema']
            except KeyError:
                pass


        if not pageDict['anaDone']:
            st.write("No analysis yet.")
            st.stop()

        files=[f for f in os.listdir(plotDir) if os.path.isfile(os.path.join(plotDir, f))]
        if len(files)>0:
            st.write("### Check Analysis Plots")
            infra.SelectBox(pageDict,'plotFile',files,'Check plot:')
            DisplayPDF(os.path.join(plotDir, pageDict['plotFile']))
        else:
            st.write("No analysis plots found.")

        ### stop if sensor
        if "sens" in pageDict['componentType'].lower():
            st.write("__No more to be done for SENSOR componentType__")
            st.stop()

        st.write("---")
        st.write("## Upload test data")

        if not os.path.isfile(outDir+"results.json"):
            st.write("no results file found.")
            st.stop()


        if "testSchema" not in pageDict.keys() or st.button("re-read json"):
            with open(outDir+"results.json") as json_file:
                pageDict['testSchema'] = json.load(json_file)
                # pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']


        ### date-time hack
        st.write(pageDict['testSchema']['date'])
        try:
            pageDict['testSchema']['date']=pdbTrx.TimeStampConverter(pageDict['testSchema']['date'],"%Y-%m-%dT%H:%M:%SZ")
        except ValueError:
            st.write("issue with datetime format. Trying to fix")
            st.write(f" - original datetime format: {pageDict['testSchema']['date']}")
            if "altDate" not in pageDict.keys():
                pageDict['altDate']=None
                for dtPat in ["%Y-%m-%dT%H:%M:%SZ","%Y-%m-%dT%H:%M:%S.%fZ","%Y-%m-%dT%H:%M:%S.%f%z"]:
                    try:
                        pageDict['altDate']=pdbTrx.TimeStampConverter(pageDict['testSchema']['date'],dtPat)
                        st.write(f" - found datetime pattern: {dtPat}")
                        break
                    except ValueError:
                        pass
            if pageDict['altDate']!=None:
                pageDict['testSchema']['date']=pageDict['altDate']
                st.write(f" - new datetime format: {pageDict['testSchema']['date']}")
            else:
                st.write("🛑 Could not match dateTime pattern. Please try: \"%Y-%m-%dT%H:%M:%SZ\"")
                st.stop()

        # set testType component SN
        pageDict['testSchema']['component']=metaDict['Module ref. Number']
        st.write("---")

        ### Review and edit
        st.write("### Review and edit schema:")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=pdbTrx.EditJson(pageDict['testSchema'])
        else:
            st.write("### Review Schema")
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert all values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        st.write("## Test Registration")

        # check existing component tests
        chnx.StageCheck(pageDict)

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        chnx.RegChunk(pageDict, "TestRun", "testSchema", "file")
