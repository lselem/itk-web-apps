#!/usr/bin/env python3

### Author: Nathan Readioff
### Notes:
### Wrapper for matplotib pyplots to allow the creation of 2D and 3D
### plots using a dictionary for configurable options

from .plotlibrary import plotLibrary
import os
import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
from matplotlib.animation import FuncAnimation, PillowWriter  # for gif making of rotating 3D plots.
from skspatial.objects import Plane, Point, Points, Vector
from skspatial.plotting import plot_3d


# Internal helper methods
def _makeList(obj):
    return obj if type(obj) in [list, tuple] else [obj]

def _readPlotParameter(plotKey, paramKey, default):
    return plotLibrary[plotKey][paramKey] if paramKey in plotLibrary[plotKey] else default

def _readPlotParameterList(plotKey, paramKey, default):
    return _makeList(plotLibrary[plotKey][paramKey]) if paramKey in plotLibrary[plotKey] else default





def create2DPlot(plotKey, dataFrame, plotDir, multi_plot = False, return_plot = False):
    if plotKey not in plotLibrary:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as this is not defined in plotLibrary!")
        return

    # Verify this is a 2D plot
    if _readPlotParameter(plotKey, 'is3D', default=False):
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as this is a 3D plot!")
        return

    # Retrieve values from dictionary - required values:
    xvar = _readPlotParameter(plotKey, 'xvar', default=None)
    if xvar is None:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as no x variable defined!")
        return

    ymin = _readPlotParameter(plotKey, 'ymin', default=None)
    if ymin is None:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as ymin not defined!")
        return

    ymax = _readPlotParameter(plotKey, 'ymax', default=None)
    if ymax is None:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as ymax not defined!")
        return

    yvarList = _readPlotParameterList(plotKey, 'yvar', default=None)
    if yvarList is None:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as no y variable(s) defined")
        return

    # Optional parameters
    yerrList   = _readPlotParameterList(plotKey, 'yerr',   default=[None]*len(yvarList))
    legendList = _readPlotParameterList(plotKey, 'legend', default=[""]  *len(yvarList))
    markerList = _readPlotParameterList(plotKey, 'marker', default=["."] *len(yvarList))

    colorList     = _readPlotParameterList(plotKey, 'color',     default=['black']*len(yvarList))
    edgecolorList = _readPlotParameterList(plotKey, 'edgecolor', default=[None]*len(yvarList))

    markerSize = _readPlotParameter(plotKey, 'markersize', default=80)

    annotate       = _readPlotParameter(plotKey, 'annotate', default=False)
    annotatePoints = _readPlotParameter(plotKey, 'annotatepoints', default=False)
    annotateOffset = 0.00

    showLegend = _readPlotParameter(plotKey, 'showlegend', default=True)

    # Create plot
    if not multi_plot:
        fig, axis = plt.subplots() 
    else:
        fig = multi_plot[0]
        axis = multi_plot[1]

    for yvarIndex in range(len(yvarList)):
        yvar = yvarList[yvarIndex]
        yerr = yerrList[yvarIndex]
        legend = legendList[yvarIndex]
        color  = colorList[yvarIndex]
        edgecolor = edgecolorList[yvarIndex]
        marker = markerList[yvarIndex]

        if yerr is not None:
            errorLow  = [row['ave'+yerr]-row['min'+yerr] for idx, row in dataFrame.iterrows()]
            errorHigh = [row['max'+yerr]-row['ave'+yerr] for idx, row in dataFrame.iterrows()]
            yerr = [errorLow, errorHigh]

            plt.errorbar(dataFrame[xvar], dataFrame[yvar], yerr=yerr, color=color,
                         fmt='o', elinewidth=1, capthick=1, errorevery=1, alpha=1, ms=0, capsize=3, zorder=12)

        dataFrame.plot.scatter(x=xvar, y=yvar, c=np.array([color]) if multi_plot else color, edgecolor=edgecolor, marker=marker, s=markerSize, label=legend, ax=axis, legend=showLegend)

        # Annotations for points outside of plot edges
        if annotate:
            for dfIndex, dfRow in dataFrame.iterrows():
                # Annotate points above the y-axis limit, or near the top-right legend
                if (dfRow[yvar] > ymax) or ((dfIndex > int(len(dataFrame)/2)) and (dfRow[yvar] > 0.75*ymax)):
                    annotateColor = color
                    if annotateColor is None or annotateColor == 'none':
                        annotateColor = edgecolor
                    # Constrain the annotation to indexes between 1/5th and 1/2th across the plot space
                    plt.annotate("{} {}={:.3f}".format(dfRow[xvar], legend, dfRow[yvar]),
                                 (max(min(dfIndex,int(len(dataFrame)/2)),int(len(dataFrame)/5)), 0.99*ymax - annotateOffset),
                                 size=8,
                                 color=annotateColor,
                                 va="top",
                                 ha="center")
                    annotateOffset += 0.035*ymax

        # Add numbers to points showing actual z value
        # In this case, 'color' is assumed to denote the 3D height variable
        if annotatePoints:
            for dfIndex, dfRow in dataFrame.iterrows():
                plt.annotate("{:.0f}".format(dfRow[color]), (dfRow[xvar], dfRow[yvar]+3), size=5, va="top", ha="center" )



    # Add horizontal lines
    hlines = _readPlotParameter(plotKey, 'hlines', default=None)    
    if hlines is not None:
        xmin = axis.xaxis.get_majorticklocs()[0]
        xmax = axis.xaxis.get_majorticklocs()[-1]
        for line in hlines:
            value = line[0]
            color = line[1]
            plt.plot([xmin, xmax],[value,value], color=color, linestyle='dashed', lw=2)

    # Set misc plot parameters
    plt.title(_readPlotParameter(plotKey, 'title', default=""))
    plt.xticks(fontsize=6, rotation=_readPlotParameter(plotKey, 'xtickrot', 90))
    axis.set_xlabel(_readPlotParameter(plotKey, 'xlabel', default=""))
    axis.set_ylabel(_readPlotParameter(plotKey, 'ylabel', default=""))
    axis.tick_params(axis='x', which='major', labelsize=7)
    axis.tick_params(axis='y', which='both', direction='in', right=True)
    axis.set_ylim(ymin, ymax)

    yminor = _readPlotParameter(plotKey, 'yminor', default=None)
    if yminor is not None:
        axis.yaxis.set_minor_locator(MultipleLocator(yminor))
    
    ymajor = _readPlotParameter(plotKey, 'ymajor', default=None)
    if ymajor is not None:
        axis.yaxis.set_major_locator(MultipleLocator(ymajor))

    if showLegend and not multi_plot:
        legendloc = _readPlotParameter(plotKey, 'legendloc', default='upper right')
        plt.legend(loc=legendloc, framealpha=0.9)
    
    if _readPlotParameter(plotKey, 'tightlayout', default=False) and not multi_plot:
        plt.tight_layout(rect=(0,0,1,1))
    
    if _readPlotParameter(plotKey, 'invertxaxis', default=False):
        axis.set_xlim(axis.get_xlim()[::-1])
        
    outputName = _readPlotParameter(plotKey, 'output', default=None)
    if outputName is None:
        print (f"ERROR: create2DPlot: Cannot plot {plotKey}. Invalid value for 'output'")
        return

    if return_plot:
        return fig, axis
    else:
        plt.savefig(os.path.join(plotDir, outputName))
    





def create3DPlot(plotKey, listDataFrames, listPlanes, plotDir, multi_plot = False, return_plot = False):
    # Verify plotkey exists
    if plotKey not in plotLibrary:
        print(f"ERROR: create3DPlot: Cannot plot {plotKey} as this is not defined in plotLibrary!")
        return

    # Verify this is a 3D plot
    if not _readPlotParameter(plotKey, 'is3D', default=False):
        print(f"ERROR: create3DPlot: Cannot plot {plotKey} as this is a 2D plot!")
        return

    # Read x,y,z variables
    xvar = _readPlotParameter(plotKey, 'xvar', default=None)
    if xvar is None:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as no x variable defined!")
        return
    yvar = _readPlotParameter(plotKey, 'yvar', default=None)
    if yvar is None:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as no y variable defined!")
        return
    zvar = _readPlotParameter(plotKey, 'zvar', default=None)
    if zvar is None:
        print(f"ERROR: create2DPlot: Cannot plot {plotKey} as no z variable defined!")
        return

    # put some logic in place for the comparison plot, everything needed for the multi_plot is passed as a list
    if not multi_plot:
        figure = plt.figure()
        axis = figure.add_subplot(1,1,1, projection='3d')
    if type(multi_plot) == list:
        # if its false it needs initialisation
        if multi_plot[0] == False:
            figure = plt.figure()
            axis = figure.add_subplot(multi_plot[1],2,1, projection='3d')
            plt.subplots_adjust(wspace=0.3,hspace=0.3)  
        elif  multi_plot[0] == True:
            figure = multi_plot[2]
            axis = figure.add_subplot(multi_plot[1],2,multi_plot[3], projection='3d')

    # Plot the points set
    listDataFrames = _makeList(listDataFrames)
    markerSize  = _readPlotParameter(plotKey, 'markersize', default=50)
    pointsColor = _readPlotParameter(plotKey, 'pointscolor', default=None)
    cmap        = _readPlotParameter(plotKey, 'cmap', default='viridis')
    plotTriSurf = _readPlotParameter(plotKey, 'pointstrisurf', default=False)
    legend = _readPlotParameter(plotKey, 'legend', default = None)
    showLegend = _readPlotParameter(plotKey, 'showlegend', default = False)
    axisTitleSize = _readPlotParameter(plotKey, 'axisfontsize', default = 11)
    labelPad = _readPlotParameter(plotKey, 'labelpad', default = 4)

    for dataFrame in listDataFrames:
        pointsColorToUse = dataFrame[zvar]
        if pointsColor != 'z':
            pointsColorToUse = pointsColor
        if plotTriSurf:
            surf = axis.plot_trisurf(dataFrame[xvar], dataFrame[yvar], dataFrame[zvar], cmap=cmap, linewidth=0.5, edgecolors='black')
            pointsColorToUse = None
        axis.scatter(dataFrame[xvar], dataFrame[yvar], dataFrame[zvar], c=pointsColorToUse, s=markerSize, label = legend)
    
    # Plot the planes
    listPlanes = _makeList(listPlanes)
    for plane in listPlanes:
        alpha  = _readPlotParameter(plotKey, 'planealpha',   default=1.0)
        lims_x = _readPlotParameter(plotKey, 'planelimitsx', default=(-1,1))
        lims_y = _readPlotParameter(plotKey, 'planelimitsy', default=(-1,1))
        plane.plot_3d(axis, alpha=alpha, lims_x=lims_x, lims_y=lims_y)

    if plotTriSurf:
        if type(multi_plot) == list:
            cbar = figure.colorbar(surf, shrink=0.5, pad=0.01, location = 'left')
            cbar.ax.tick_params(labelsize = 4)
        else:
            figure.colorbar(surf, shrink=0.5, pad=0.1)

    # Set misc plot parameters
    if type(multi_plot) != list:
        figure.tight_layout()
    else:
        axis.tick_params(axis='both', labelsize = 6, pad = 0)

    axis.set_xlabel(_readPlotParameter(plotKey, 'xlabel', default=""), fontsize = axisTitleSize, labelpad = labelPad)
    axis.set_ylabel(_readPlotParameter(plotKey, 'ylabel', default=""), fontsize = axisTitleSize, labelpad = labelPad)
    axis.set_zlabel(_readPlotParameter(plotKey, 'zlabel', default=""), fontsize = axisTitleSize, labelpad = labelPad)
    
    # Save figure
    outputName = _readPlotParameter(plotKey, 'output', default=None)
    if outputName is None:
        print (f"ERROR: create3DPlot: Cannot plot {plotKey}. Invalid value for 'output'")
        return

    if showLegend:
        legendloc = _readPlotParameter(plotKey, 'legendloc', default='upper left')
        legendFontSize = _readPlotParameter(plotKey, 'legendfontsize', default=6)
        plt.legend(loc=legendloc, framealpha=0.3, prop = {'size': legendFontSize})

    if return_plot:
        return figure
    else:
        plt.savefig(os.path.join(plotDir, outputName))
    
    # Create alternative views of same 3D plot
    listAltViews = _readPlotParameterList(plotKey, 'altviews', default=None)
    if listAltViews is not None:
        for altView in listAltViews:
            axis.view_init(altView[0], altView[1])
            name,extension = os.path.splitext(outputName)
            plt.savefig(os.path.join(plotDir, f'{name}_{altView[0]}_{altView[1]}{extension}'))
    
    # Create animation of rotating 3D plot
    if _readPlotParameterList(plotKey, 'animation', default=False):
        def rotate(i):
            axis.view_init(0, i)
        start  = _readPlotParameter(plotKey, 'animationstart',  default=0)
        stop   = _readPlotParameter(plotKey, 'animationstop',   default=2*np.pi)
        frames = _readPlotParameter(plotKey, 'animationframes', default=50)
        animation = FuncAnimation(figure, rotate, np.linspace(start, stop, frames))
        
        animationOutputName = _readPlotParameter(plotKey, 'animationoutput', default=None)
        if animationOutputName is None:
            print (f"ERROR: create3DPlot: Cannot plot {plotKey}. Invalid value for 'animationOutput'")
            return
        animation.save(os.path.join(plotDir, animationOutputName), writer=PillowWriter(fps=10))
