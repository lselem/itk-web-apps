### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### local stuff
from .CommonCode import EditJson, SelectCheck
from .CommonCode import GetGleanListEDMS, GetGleanListSCIPP
from .CommonCode import testerOptions
import commonCode.codeChunks as chnx
import commonCode.PDBTricks as pdbTrx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ pull test file",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Shear Test", ":microscope: Upload Shear Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="HV_TAB_SHEET"
        if "testType" not in pageDict.keys():
            pageDict['testType']="SHEAR_TEST"
        if "stage" not in pageDict.keys():
            pageDict['stage']="SHEET_WITH_TABS"
        if "project" not in pageDict.keys():
            pageDict['project']="S"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testType'])
        st.dataframe(df_stageTest)

        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")

        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testType']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType'], 'requiredOnly':True})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["csv"])
        if st.session_state.debug: st.write(pageDict['file'])

        if pageDict['file'] is not None:
            headerList, dataList = GetGleanListSCIPP(pageDict['file']) #,names=list('abcd'))
            try: # try SCIPP version
                df_header = pd.DataFrame(headerList, columns=['key','value','unit'])
                df_data = pd.DataFrame(dataList, columns=['test','grade','force'])
            except ValueError: # try EDMS version
                st.write("alternative reader go!")
                pageDict['file'].seek(0)
                headerList, dataList = GetGleanListEDMS(pageDict['file'])
                df_header = pd.DataFrame(headerList, columns=['key','value','unit','other'])
                df_data = pd.DataFrame(dataList, columns=['other','test','grade','force'])
        else:
            st.write("No data file set")
            ### from EDMS document (p.29): https://edms.cern.ch/ui/file/2520891/2.2/QC_Bond_Pulling.pdf


            filePath=os.path.realpath(__file__)
            exampleFileName="shearTestExample_EDMS.csv"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
            st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
            st.write("See p.2 [EDMS document](https://edms.cern.ch/ui/file/2520890/3.2/Bonding-Procedures_HV-tab.pdf) for details on format")
            for k in ['testSchema', 'toggleEdit', 'toggleText']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()

        ### US date format toggle
        infra.ToggleButton(pageDict,'toggleUS','adapt US date format? (%m/%d/%Y->%d/%m/%Y)')

        if st.session_state.debug:
            st.write("Gleaned lists")
            st.write(headerList)
            st.write(dataList)

        for i, col in enumerate(df_header.columns):
            df_header.iloc[:, i] = df_header.iloc[:, i].str.replace('"', '')
        for i, col in enumerate(df_data.columns):
            df_data.iloc[:, i] = df_data.iloc[:, i].str.replace('"', '')

        # show gleaned information in dataframes
        st.write("### header info.")
        st.dataframe(df_header)
        st.write("### data info.")
        st.dataframe(df_data)

        # check for user field ID
        compId=next((v for v in df_header['value'].to_list() if "20US" in v and "20USBVB91" not in v), None)
        

        st.write("### Shear Test visualisation")
        visChart=alt.Chart(df_data).mark_boxplot().encode(
                x='grade:N',
                y=alt.Y('force:Q',scale=alt.Scale(zero=False)),
                color='grade:N',
                tooltip=['type:Q','force:Q']
        ).interactive()
        st.altair_chart(visChart, use_container_width=True)

        infra.Radio(pageDict,'testSubstrate',["Cicorel","Sensor half moon"],'Test substrate?')

        if "testSchema" not in pageDict.keys() or st.button("re-read file"):
            pageDict['testSchema']=pageDict['origSchema']
            pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
            # update json
            ### general
            pageDict['testSchema']['passed']=True
            pageDict['testSchema']['problems']=False
            dateStr=df_header.query('key=="DATE"')['value'].values[0]+"@"+df_header.query('key=="TIME"')['value'].values[0]
            if pageDict['toggleUS']:
                try:
                    dateStr= datetime.strptime(dateStr, "%m/%d/%y@%H:%M:%S").strftime("%d/%m/%y@%H:%M:%S")
                except ValueError:
                    dateStr= datetime.strptime(dateStr, "%m/%d/%Y@%H:%M:%S").strftime("%d/%m/%Y@%H:%M:%S")
            try:
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr,"%d/%m/%y@%H:%M:%S")
            except ValueError:
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr,"%d/%m/%Y@%H:%M:%S")
            try:
                pageDict['testSchema']['institution']= df_header.query('key=="INSTITUTE"')['value'].values[0]
            except IndexError:
                pass

            # properties
            pageDict['testSchema']['properties']['OPERATOR']=df_header.query('key=="OPERATOR"')['value'].values[0]
            pageDict['testSchema']['properties']['TEST_SUBSTRATE']=pageDict['testSubstrate']

            # results
            pageDict['testSchema']['results']['SHEAR_STRENGTH']=df_data['force'].astype(float).tolist()[0]

        # st.write("### NB User Beware!")
        # st.write("**Analysis is not yet implemented so please set the pass/fail value (_default pass_)**")

        ### Analysis checks
        st.write("### Analysis checks")
        failAna=False

        shearTHL=-1
        if pageDict['testSubstrate']=="Sensor half moon":
            shearTHL=300
        if pageDict['testSubstrate']=="Cicorel":
            shearTHL=200

        if shearTHL<0:
            st.write("missing shear threshold")
            st.stop()

        ### Minimum sample size which is defined for each test surface
        # not implemented
        ### Minimum shear strength
        if float(pageDict['testSchema']['results']['SHEAR_STRENGTH'])<= shearTHL:
            st.write("**Failed** *"+pageDict['testSubstrate']+" shear strength* :no_entry_sign:")
            failAna=True
        else: st.write("**Passed** *"+pageDict['testSubstrate']+" shear strength* :heavy_check_mark:")


        if failAna:
            st.write("### :broken_heart: **Analysis failed**")
            pageDict['testSchema']['passed']=False
        else:
            st.write("### :green_heart: **Analysis passed**")

        ### Review and edit
        st.write("### Review and edit schema:")
        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write("### Review Schema")
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        st.write("---")
        st.write("## Registration")

        # check existing component tests
        chnx.StageCheck(pageDict)


        ####################
        ### HV tabs part
        ####################

        st.write("---")
        st.write("### HV tabs information")

        # st.write(df_header['value'].to_list())
        
        compId=next((v for v in df_header['value'].to_list() if "20USBVB91" in v), None)

        if compId==None:
            st.write("Could not find serialNumber. Please check input (looking for _20USBVB91_)")
        else:
            st.write(f"Found serialNumber: {compId}")

            if st.button("Add HV tab info."):
                st.write("working on HV tab:",compId)
                compHVT=DBaccess.DbGet(st.session_state.myClient,'getComponent', {"component":compId} )
                #st.write(compBM)
                st.write(f"{compId} properties:")
                st.dataframe(compHVT['properties'])
                tabCode="NUMBER_TABS_FOR_OTHER"
                modTabs=next((p['value'] for p in compHVT['properties'] if p['code']==tabCode), "None")
                st.write("Module tabs read (before update):", modTabs )

                # hack for unset
                if modTabs==None:
                    modTabs=0
                
                prop_json={
                    'component': compHVT['code'],
                    'code': tabCode,
                    'value': modTabs+1,
                }
                
                try:
                    upVal=DBaccess.DbPost(st.session_state.myClient,'setComponentProperty', prop_json )
                    st.write("### **Successful update**:",upVal)
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: update **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

