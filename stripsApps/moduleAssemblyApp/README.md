# Strips Module Assembly webApp

Tools for Strips Module assembly.

---

## Pages

### Module Assembly
  * Read input file, register modules and assemble components
  * thanks to Paul Miyagawa for useful pointers

### Shear Test Upload
  * Read input file and upload tests
