### standard
import streamlit as st
### custom
import os
import pandas as pd
import ast
import csv
import json
from datetime import datetime
### PDB stuff
import core.DBaccess as DBaccess
import itkdb.exceptions as itkX

#####################
### useful functions specificallhy for this app
#####################

def GetGleanList(fileObj):
    header=[]
    data=[]
    change = False
    splitters=['\t',' ']

    for raw_line in fileObj.readlines():
        #st.write("raw line:",line)
        line=raw_line.decode("ISO-8859-1")
        #st.write(line)
        if not line:
            break

        if change==False:
            if "comment" in line:
                change=True
            header.append([x.strip() for x in line.split(': ')])
        elif change==True:
            for sp in splitters:
                splitline=[x.strip() for x in line.split(sp) if len(x)>0]
                if len(splitline)>1:
                    data.append(splitline)
                    break
        #st.write("Line{}: {}".format(count, line.strip()))
    return header, data
