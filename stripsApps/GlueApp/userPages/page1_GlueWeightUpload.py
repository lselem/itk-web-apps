### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import altair as alt
import ast
import csv
import os
from datetime import datetime
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
from .LocalCode import GetGleanList

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _csv_ pull test file or enter manually",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * review and edit test schema",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Glue Weight Upload", ":microscope: Upload Glue Weight Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "code" not in pageDict.keys():
            pageDict['testType']="GLUE_WEIGHT"
        if "stage" not in pageDict.keys():
            pageDict['stage']="GLUED"
        if "project" not in pageDict.keys():
            pageDict['project']="S"

        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('testType=="'+pageDict['testType']+'"').reset_index(drop=True)
        st.write("### componentTypes and stages for testType=="+pageDict['testType'])
        st.dataframe(df_stageTest)


        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.SelectBox(pageDict,'componentType',df_stageTest['compType'].unique(),"Select componentType code:")
            infra.SelectBox(pageDict,'stage',df_stageTest.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list(),"Select stage code:")

        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['testType']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['testType']}) #, 'requiredOnly':True})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])

        ## drag and drop method
        pageDict['file']= st.file_uploader("Upload data file", type=["dat"])
        if st.session_state.debug: st.write(pageDict['file'])

        if pageDict['file'] is not None:
            headerList, dataList = GetGleanList(pageDict['file'])
        else:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="glueWeightExample.dat"
            if st.session_state.debug:
                st.write("looking in:",filePath[:filePath.rfind('/')])
            st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)
            st.write("See [EDMS document](https://edms.cern.ch/ui/file/2477019/3.1/Weighing_QC_Document.pdf) for test details")
            for k in ['testSchema', 'toggleEdit', 'toggleText']:
                try:
                    pageDict.pop(k)
                except KeyError:
                    pass
            st.stop()

        ### US date format toggle
        infra.ToggleButton(pageDict,'toggleUS','adapt US date format? (%m/%d/%Y->%d/%m/%Y)')

        st.write("---")
        st.write("## Glean Data")

        st.write("### header info.")
        df_header = pd.DataFrame(headerList, columns=['key','value'])
        df_header['key']=df_header['key'].str.replace(':','')
        df_header.fillna('', inplace=True)
        st.dataframe(df_header)
        st.write("### data info.")
        df_data = pd.DataFrame(dataList, columns=['code', 'value'])
        df_data['code']=df_data['code'].str.replace(':','')
        st.dataframe(df_data)

        st.write("### Highlights")
        for k in ['GW_GLUE_PB','GW_GLUE_H1','GW_GLUE_H1H2']:
            try:
                st.write(k+" : "+df_data.query('code=="'+k+'"')['value'].values[0])
            except IndexError:
                pass

        st.write("### Glue Weight Visualisation")
        visChart=alt.Chart(df_data.query('code.str.contains("GW_")')).mark_bar().encode(
                x='code:N',
                y=alt.Y('value:Q',scale=alt.Scale(zero=False)),
                color='code:N',
                tooltip=['code:N','value:Q']
        ).interactive()
        st.altair_chart(visChart, use_container_width=True)


        if "testSchema" not in pageDict.keys() or st.button("re-read file"):
            # update json
            ### general
            pageDict['testSchema']=pageDict['origSchema']

            for k in pageDict['testSchema'].keys():
                if k in ['properties','results','testType']: continue
                gotPara=False
                for c in df_header['key'].to_list():
                    if k.lower()==c.lower():
                        #st.write(f"header: got {k}=={c}")
                        pageDict['testSchema'][k]=df_header.query('key=="'+c+'"')['value'].values[0]
                        gotPara=True
                if not gotPara:
                    pageDict['testSchema'][k]=""

            # update json
            ### general
            pageDict['testSchema']['passed']=True
            pageDict['testSchema']['problems']=False
            dateStr=df_header.query('key=="date"')['value'].values[0]+"@"+df_header.query('key=="time"')['value'].values[0]
            if pageDict['toggleUS']:
                try:
                    dateStr= datetime.strptime(dateStr, "%m/%d/%y@%H:%M:%S").strftime("%d/%m/%y@%H:%M:%S")
                except ValueError:
                    dateStr= datetime.strptime(dateStr, "%m/%d/%Y@%H:%M:%S").strftime("%d/%m/%Y@%H:%M:%S")
            try:
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr,"%d/%m/%y@%H:%M:%S")
            except ValueError:
                pageDict['testSchema']['date']= pdbTrx.TimeStampConverter(dateStr,"%d/%m/%Y@%H:%M:%S")
            try:
                pageDict['testSchema']['institution']= df_header.query('key=="institution"')['value'].values[0]
            except IndexError:
                pass

            for p in ['properties','results']:
                for k in pageDict['testSchema'][p].keys():
                    gotPara=False
                    for c in df_data['code'].to_list():
                        if k.lower()==c.lower():
                            #st.write(f"data: got {k}=={c}")
                            if p=="properties":
                                pageDict['testSchema'][p][k]=df_data.query('code=="'+c+'"')['value'].values[0]
                            elif p=="results":
                                pageDict['testSchema'][p][k]=df_data.query('code=="'+c+'"')['value'].astype(float).values[0]
                            else:
                                st.write("don't know:",p)
                            gotPara=True
                    if not gotPara:
                        if p=="properties":
                            pageDict['testSchema'][p][k]=""
                        elif p=="results":
                            pageDict['testSchema'][p][k]=None
                        else:
                            st.write("don't know:",p)


        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict,'testSchema')

        st.write("---")
        st.write("## Test Registration")

        # check existing component tests
        chnx.StageCheck(pageDict)

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        chnx.RegChunk(pageDict, "TestRun", "testSchema")
