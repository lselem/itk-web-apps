### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
### local stuff
from .CommonCode import ListComponentsByType, listChildComponent, listJsonFiles, readJsonFiles
import commonCode.codeChunks as chnx
import commonCode.PDBTricks as pdbTrx

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload json files of the electrical test of modules or hybrids",
        " * review retrieved data & visualisation",
        " * review analysis checks",
        " * upload test schema to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("E-Test Upload", ":microscope: Upload E-Test Rsults", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "project" not in pageDict.keys():
            pageDict['project']="S"
        if "componentTypes" not in pageDict.keys():
            pageDict['componentTypes']=["HYBRID_TEST_PANEL", "MODULE"]
        
        for componentType in pageDict['componentTypes']:
          if "stage_"+componentType not in pageDict.keys():
            if componentType == "HYBRID_TEST_PANEL":
              pageDict['stage_'+componentType]=["HYBRIDS_ASSEMBLED"]
            elif componentType == "MODULE":
              pageDict['stage_'+componentType]=["TESTED", "FINISHED", "AT_LOADING_SITE"]
        
        # review componentTypes and stages from file
        listPath="/code/commonCode/"
        csvFileName="stageTestList_S.csv"
        if st.session_state.debug:
            st.write("looking in:",listPath[:listPath.rfind('/')])
        st.write("### Check stages and testTypes ###")
        compType = st.selectbox('Select componentType: ', pageDict['componentTypes'])

        # df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('compType in ["HYBRID_ASSEMBLY","MODULE"] and testType in ["NO_PPA", "RESPONSE_CURVE_PPA", "PEDESTAL_TRIM_PPA", "STROBE_DELAY_PPA"] and stage in ["FINISHED_HYBRID", "AT_MODULE_ASSEMBLY_SITE", "ON_MODULE"]').reset_index(drop=True)
        df_stageTest=pd.read_csv(listPath[:listPath.rfind('/')]+"/"+csvFileName).query('compType == @compType ').reset_index(drop=True)
        df_stageTest=df_stageTest.groupby(by=['compType','stage']).agg({'testType':list}).reset_index()

        st.dataframe(df_stageTest) #[['compType','stage','testType']])

        ## get SN of Module and Hybrid Test Panel
        if not "componentList" not in pageDict.keys():
          pageDict['componentList']= []
        componentList = ListComponentsByType(pageDict['componentTypes'], pageDict['stage_HYBRID_TEST_PANEL']+pageDict['stage_MODULE'])
        if componentList==None:
          st.write('No component is found!!!')
          st.stop()
        pageDict['componentList'] = componentList

        ## Select Module or Hybrid Test Panal
        st.write("-------------------------------------------")

        st.write("### Upload Data")
        st.write(f"Only {pageDict['componentTypes']} components in {pageDict['stage_HYBRID_TEST_PANEL']+pageDict['stage_MODULE']} stages included")

        component = st.selectbox(' Step 1: Select Module or Hybrid Test Panel: ', pageDict['componentList'])
        st.write('You selected:', component)
        #if "parentComponent" not in pageDict.keys():
        pageDict['parentComponent']= component

        if st.checkbox("Enter serialNumber by hand if required"):
          component = st.text_input('input SN:')
          if "20U" not in component:
            st.write("No SN input")
            st.stop()

        childList = listChildComponent(component)
        if childList == None:
          st.write(' No Hybrid is found!!! ')
          st.stop()
        st.write(' Hybrid List: ')
        st.write(pd.DataFrame({'Hybrid SN': childList,}))

        pageDict['childComponent'] = childList

        ## Load json files for uploading
        st.write("-------------------------------------------")
        inFileList = []
        uploaded_files = st.file_uploader("Step 2: Choose json files", accept_multiple_files=True, type=["json"])
        for uploaded_file in uploaded_files:
          #bytes_data = uploaded_file.read()
          ### get SN in name
          sn=[x.replace('SN','') for x in uploaded_file.name.split("_") if "20U" in x]
          # st.write("serialNumbers:",sn)
          if len(sn)==1:
             sn=sn[0]
          else:
             st.write("Can't identify serialNumber in file name. Please check ('_' delimited)")
             st.stop()
          ### assume single value is serialNumber
          if sn in pageDict['childComponent']:
            inFileList.append(uploaded_file.name)
        if len(inFileList)==0:
          st.write("No json file is loaded!!!")
          st.stop()

        ## list json file by SN
        st.write("-------------------------------------------")
        st.write("Step 3: List json files:")
        resultsMap = readJsonFiles(uploaded_files, pageDict['childComponent'])
        if resultsMap == None:
          st.write('__No json files are found__')
          st.stop()
        #st.write(resultsMap)
        df_results = pd.DataFrame([{'name':k, 'test':[k for k in v.keys()]} for k,v in  resultsMap.items()])
        # resultsDataFrame["i"] = list(range(7))
        for child in pageDict['childComponent']:
          testTypes = resultsMap[child].keys()
          if len(testTypes)<4:
            st.write('Warning: Not all test type json files are loaded!')
            st.write(f" - found testTypes ({child}):",testTypes)
            st.write(" - found resultsMap:",resultsMap)


        st.write(df_results)

        ## Check if all necessary json files are loaded
        st.write("-------------------------------------------")
        st.write("Step 4: Check json files:")
        for child in pageDict['childComponent']:
          st.write("Checking the test results of __"+child+"__ ... ")
          for testType in resultsMap[child].keys():
            st.write(f" - _checking_: {testType}")
            expectCount=1 
            if "RESPONSE_CURVE_PPA" in testType:
              st.write("   - _two files expected for 3PG and RS_")
              expectCount=2 
            if len(resultsMap[child][testType])<expectCount:
              st.write(f' - Warning: {testType} is not loaded correctly: __too few__ (expect {expectCount})! ❌')
            elif len(resultsMap[child][testType])>expectCount:
              st.write(f' - Warning: {testType} is not loaded correctly: __too many__ (expect {expectCount})! ❌')
            else:
              st.write(f" - {testType} is loaded correctly (expect {expectCount}) ✅")
               

        ## upload results
        st.write("-------------------------------------------")
        st.write("Step 5:  Upload:")
        if st.button("Upload"):
          for child in resultsMap.keys():
            for testType in resultsMap[child].keys():
              for FileName in resultsMap[child][testType].keys():
                data = json.loads(resultsMap[child][testType][FileName].getvalue().decode("utf-8"))
                data["component"] = child
                try:
                  st.session_state.myClient.post('uploadTestRunResults', json=data)
                  st.balloons()
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Test Upload  **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                # except Exception:
                #   st.write("Warning: uploading of "+testType+" "+child+" failed!!!!")
        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        #chnx.RegChunk(pageDict, "TestRun", "testSchema")
