### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

infoList=["  * Check components at institute"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("CheckComponent", ":microscope: Check component in PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # input id
        infra.TextBox(pageDict,'inCode',"What to check:")
        st.write("Using identication:",pageDict['inCode'])

        if st.button("Get!"):
            pageDict['retInfo']=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pageDict['inCode']})

            try:
                if pageDict['retInfo']!=None:
                    st.write("### :white_check_mark: Found!")
                    st.write("Found!",pageDict['retInfo'])

                else:
                    st.write("### :no_entry_sign: search unsuccessful")
            except KeyError:
                pass
