# ITk Web Apps

### [Streamlit](https://www.streamlit.io) (python**3**) code for interfacing with ITk Production Database (PDB).

**NB** Powered by [itkdb](https://pypi.org/project/itkdb/)


---

## Overview

Build docker images of custom webApps.

  * docker image build on [*itk PDB template multi*](https://gitlab.cern.ch/wraight/itkpdbtemplatemulti).

WebApp content selected from available pages.

**NB For a docker free example see [here](https://gitlab.cern.ch/wraight/itk_pdb_testapp).**

### Structure

  * dockerFiles: template files to create docker images
    - pixels, strips and single examples
  * mainFiles: example files for mainPages
  * _requirements_ and _temp_id_ files required for Cern distributed images
  * content from sub-directories pages - see READMEs in directories

---

## Existing webApp Flavours

### General: not specific to *Strips* or *Pixels* projects

  * general-app: non-project specific multi-function webApp

    - image on [dockerHub](https://hub.docker.com/repository/docker/kwraight/general-multi-app)
    - hosted at [Cern OKD](https://itk-pdb-webapps-general.web.cern.ch)

### Strips: webApps specific to *Strips* project

  * strips-app: multi-function webApp for strips work

    - image on [dockerHub](https://hub.docker.com/repository/docker/kwraight/strips-multi-app)
    - hosted at [Cern OKD](https://itk-pdb-webapps-strips.web.cern.ch)

### Pixels: webApps specific to *Pixels* project

* pixels-app: multi-function webApp for pixels work

  - image on [dockerHub](https://hub.docker.com/repository/docker/kwraight/pixels-multi-app)
  - hosted at [Cern OKD](https://itk-pdb-webapps-pixels.web.cern.ch)

* webApp for hybridisation vendors

  - hosted at [Cern OKD](https://cern.ch/itk-vendor-upload)

### Common Electronics: webApps specific to *Common Electronics* project

  * common-electronics-app: multi-function webApp for strips

    - hosted at [Cern OKD](https://itk-pdb-webapps-common-electronics.web.cern.ch)

  ---

## Build and Run Custom Docker Image

1. Duplicate/Edit _dockerFiles/DockerfileGeneral_

  * add content of interest: copy _userPages_ directories from specific subdirectories

2. Build image

  From top directory:
  > docker build . -f dockerFiles/DockerfileTester -t test-app

3. Run container

  From top directory:
  > docker run -p 8501:8501 test-app

  - Pass PDB tokens with docker run (avoid input in app):
    > docker run -p 8501:8501 test-app streamlit run mainApp.py "ac1 YOUR_TOKEN_1" "ac2 YOUR_TOKEN_2"

4. Open browser at _"localhost:8501"_

  Hopefully you see the webApp "Authentication" page.

5. (Development) Run with mounted volume:

  This allows you to make _hot_ changes to files in the mounted directory - speeds up development muchly.

  Mount the volume using "-v" argument with local directory path on LHS of colon and container directory path on RHS, e.g.:

  > docker run -p 8501:8501 -v $(pwd)/generalApps/genericApp/userPages/:/code/userPages/genericApp test-app

  **NB**

  a. Spaces in mounted directory paths are tricky (and hidden if you use symlinks or '$' paths). You may see an error:
  > docker: invalid reference format: repository name must be lowercase.

    * In this case use quotations around the mount paths, e.g.:
    > docker run -v "LOCAL_PATH:CONTAINER_PATH" my-image

  b. Windows users may have to use _PWD_ instead _pwd_.


6. (_optional_) Push to dockerHub with _buildx_

  * There are known issues running cross-architecture images, e.g. Apple Silicon (arm) and linux (amd)
  * this is fixed using docker's _buildx_ plugin for multi-platform images
  > docker buildx build --platform linux/amd64,linux/arm64 --push -f dockerFiles/DockerfileTest -t someRepo/test-app:multi .

  **NB** 
  
  No local image is made, it is pushed straight to repository. Hence, to run locally you must pull and run:
  > docker run -p 8501:8501 someRepo/test-app:multi

    * docker will select image appropriate to local architecture
  
  
## Troubleshooting

### Docker permission issue

```bash
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.40/containers/json: dial unix /var/run/docker.sock: connect: permission denied
```

In a terminal do ``sudo chmod 666 /var/run/docker.sock`` [source](https://www.digitalocean.com/community/questions/how-to-fix-docker-got-permission-denied-while-trying-to-connect-to-the-docker-daemon-socket)

