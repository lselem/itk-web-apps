# Using the webApp

Some user instructions are provided below (_suggestions welcome!_).

## User access

The application is accessed through the user's web browser and only has access to local files via browser interactions

 - __NB__ There is no access to user's directory structure

The webApps are arranged in different __themes__ (usually around a database interaction, e.g. testType upload, component registration, etc.). 
Each _theme_ has a several of __pages__. Common to all _themes_ is the _Authentication_ and _Broom Cupboard_ pages used for gaining access to the PDB and debugging, respectively.

## Logging in

Access to the ITk Production Database (PDB) via the API requires two passwords to generate an access token.

??? "Setting PDB passwords"

    If you require PDB passwords to access:
    
    1. Go to [unicorn website](https://itkpd-test.unicorncollege.cz/)
    2. Select the Plus4U button (top righthand side)
    3. Select “Cannot log in?” in the pop-up (bottom righthand side)
    4. Input your email to receive instructions to finish registration
    
    For screenshots please follow the instructions [here](https://indico.cern.ch/event/1350108/sessions/520750/attachments/2765634/4817284/PDB_access.pdf)

Accessing the webApps is done through the _authentication_ page (common to all app themes).
The webApp uses the pythonic _itkdb_ API wrapper to send user passwords to PDB and retrieve an access token.

 - itkdb does not use Cern's Single Sign-On, hence neither does the webApp 

Users can input their database credentials in two ways:

 1. enter directly into text boxes (default)
 2. upload a _json_ file through the browser

### 1. enter directly (default)

The default input method is to directly enter the passwords to text boxes.

Once the passwords have been entered click the "Get PDB Token" button.

<img src="/images/instructions/webApp_use/authenticate_directly.png" alt="Authentication with user passwords input directly (default)." width="400"/>

### 2. upload *json*

An alternative (and perhaps more convenient) method is to upload a _json_ file with passwords inside. 

- An example file can be downloaded from the page and editted by the user

Once you have a _json_ file ready:

 - Select the "Enter credentials via file" check box
 - Drag & drop a file into the grey box _or_ click the "Browse files" button and select the file using the pop-up window.
 - The passwords should be sent when the file is entered

<img src="/images/instructions/webApp_use/authenticate_json.png" alt="Authentication with json file." width="400"/>

__Check authentication__

When data is correctly entered the time and user's name should be diplayed below the input region. 

<img src="/images/instructions/webApp_use/authenticate_check.png" alt="Authentication confirmation." width="400"/>

The final part of authenitcation is to collect useful information from PDB:

 - list of projects
 - list of components
 - user's default institution and project (first if list) 

When this is complete a _green_ box will be shown.

<img src="/images/instructions/webApp_use/authenticate_ready.png" alt="Authentication confirmation." width="400"/>

## User Settings

Current user setting are displayed in "Current user settings" section.

If the user has multiple institutions associated with their PDB account and wishes to change from the default:

 - Select "select other?" checkbox
 - Select alternative institution from dropdown menu

<img src="/images/instructions/webApp_use/authenticate_multi.png" alt="Select alternative institution." width="400"/>

### Option to use EOS

If EOS use is required for large file uploads/queries (files >64kB,  e.g. images) a token must be requested with a flag (for itkdb) to use the API's EOS functionality.

This functionality is activated by  in the webApp. This can be verified as the "Use EOS for uploading files" box is checked in the "Token options" section above the authentication data entry.

- unchecking this box and obtaining a new token will result in loss of EOS functionality

<img src="/images/instructions/webApp_use/authenticate_eos_on.png" alt="webApp information." width="200"/>

If EOS functionality is required for a page (e.g. genericApp, Image Upload page) but not activated a message will alert the user. As EOS functionality is activated by default this is not expected. 

<img src="/images/instructions/webApp_use/eos_check.png" alt="Example EOS flag missing from token" width="400"/>


<!-- ## webApp Small Print

Details of the webApp can be found in thee "Small Print" section at the bottom of the lefthandside bar.

<img src="/images/instructions/webApp_use/small_print.png" alt="webApp information." width="200"/>

Docker Image Heirarchy details:

- streamlitTemplate: (base) streamlit image version
- itkPdbTemplate: (middle) image including itkdb version
- built on: (top) webApp image deployment date

Repositories:

- git repo: gitlab repository of webApp
- docker repo: dockerHub of webApp

Useful infomation:

- API info: ITk PDB API documentation
- other webApps: a link to page of links of ITk streamlit webApps
- developer email -->