import os
from datetime import datetime
import shutil

#####################
### useful functions
#####################

def GetDateTimeNotice():
    dtArr=["!!! notice",
          f"\tPage generated: {datetime.now().date()} ({datetime.now().time()})",
          "\tFor updates/fixes contact: wraightATcern.ch"
        ]
    ### convert to string
    dtStr=""
    for ia in dtArr:
        dtStr+=f"{ia}\n\n"
    
    return dtStr

# recursive function to find files
def FindREADMEs(dirName, rList, nameStr="README.md"):
    ## announce
    # print(f"in {dirName}")
    ## check for README
    if os.path.isfile(dirName+"/"+nameStr):
        # print(f" - found readme")
        rList.append(dirName+"/README.md")
    ## dig deeper
    for d in sorted(os.listdir(dirName)):
        if os.path.isdir(dirName+'/'+d):
            # print(f" - found directory: {d} ({dirName.split('/')[-1]})")
            FindREADMEs(dirName+'/'+d, rList)

def WriteFile(readmePath,target):

    print(f"- use readmePath: {readmePath}")
    fileName=readmePath.replace('/README','')
    dirPath=target+"/"+"/".join(fileName.split('/')[:-1])

    print(f"- copy file to: {dirPath}")
    os.makedirs(dirPath, exist_ok=True)
    shutil.copyfile(readmePath, dirPath+"/"+fileName.split('/')[-1])


def FindMDs(dirName, mList, nameStr=".md"):
    ## announce
    # print(f"in {dirName}")
    ## dig deeper
    for d in sorted(os.listdir(dirName)):
        # ignore images director or index (aka home) file
        if any(x in d for x in ["images","index","instructions"]):
            continue
        if os.path.isfile(dirName+'/'+d) and nameStr in d:
            # print(f" - found file: {d} ({dirName.split('/')[-1]})")
            indent=dirName.count('/') * "  "
            label=indent+"- "+d.replace('.md','').replace('_',' ')
            path=dirName+'/'+d
            mList.append({ label: path.replace('./docs/','') })
        if os.path.isdir(dirName+'/'+d):
            indent=dirName.count('/') * "  "
            label=indent+"- "+d.replace('.md','').replace('_',' ')+" contents"
            mList.append({ label: "" })
            FindMDs(dirName+'/'+d, mList)


#####################
### main function
#####################
def main():

    ### find insteresting directories
    print("\n### Finding READMEs")
    readmeList=[]
    for d in ['dockerFiles','mainFiles','commonCode','generalApps','pixelsApps','stripsApps','commonElectronicsApp', 'reportingApps']:
        FindREADMEs(f"./{d}",readmeList)
    print("\n### found files:")
    for r in readmeList:
        print(f"- {r}")

    ### copy to docs
    docsDir="./docs"
    print("\n### Copying READMEs")
    for rm in readmeList:
        WriteFile(rm, docsDir)

    ### write index (with timestamp)
    print("Writing index")
    shutil.copyfile(os.getcwd()+"/README.md", "docs/index.md")
    with open("docs/index.md", "a") as content_file:
        content_file.write(GetDateTimeNotice())

    ### Find .md files
    mdList=[]
    print("Writing index")
    FindMDs(docsDir,mdList)
    print("\n### found mds:")
    for m in mdList:
        print(f"- {m}")

    ### update yml
    with open("mkdocs.yml", "a") as mkdocs_file:
        for m in mdList:
            mkdocs_file.write([k+": "+v+"\n" for k,v in m.items()][0])


### for commandline running
if __name__ == "__main__":
    main()
    print("All done.")
