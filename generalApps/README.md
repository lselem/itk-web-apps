# General Apps

Applications for general use, _i.e._ non-project specific.

Currently hosted on [Cern OKD](https://itk-pdb-webapps-general.web.cern.ch).

---

## Examples

Non-comprehensive list of included apps.


### commonApp

The idea of the commonApp theme is to provide functionality for PDB interactions which are likely _common to all_ institutions.
Since time of developing several features have since been added to Unicorn GUI.

Pages:

 - Inventory: get components at institution

 - Shipping: get shipment history, create shipmnent, accept shipment

 - Component Check: get basic component information
    - select component by identifier or from institution inventory
    - view identifiers: serialNumber, barcode, alternativeIdentifier
    - view component relations: parents, children
        - option to disassembled components
    - view: stage history
        - option to set current stage
    - view: batch information
        - option to add component to batch

 - Stage Check: check (componentType specific) stafge and test uploads
    - select component by identifier or from institution inventory
    - get stage check list (optional download)
    - get test check list (optional download)

 - Json builder (__TBC__): construct object _json_ for PDB upload


### genericApp

The idea of the genericApp is to provide _generic_ functionality for the most common PDB tasks, e.g. component registration and test(Run) uploads. Some scaling functionality is provided for multiple tasks - a formatted file must be provided (examples are provided on the relevant pages).

__NB__ Uploader Beware: no data quality checks or analysis is performed in these generic cases. For component/test specific data manipulation use a specific tool (webApp or otherwise).

Pages:

 - Single Component: register a single component
    - select component type, sub-type
    - set properties (required and optional)
    - option: register to batch

 - Multi Component: register multiple components
    - data must be supplied via formatted _csv_ or _xls_ file - examples are provided on the page

 - Single Test: register data for a single test (_a.k.a._ testRun)
    - select component type and test type
    - set properties (i.e. _non-measurement_ data) and results (i.e. _measurement_ data)

 - Multi Test: register data for multiple tests
   - data must be supplied via formatted _csv_ or _xls_ file - examples are provided on the page

 - Single Assemble: assemble _child_ component(s) to _parent_ component (all components must be already registered in PDB)
    - select parent component
    - select child components

 - Multi Assemble: assemble _child_ component(s) to multiple _parent_ components (all components must be already registered in PDB)
   - data must be supplied via formatted _csv_ or _xls_ file - examples are provided on the page

 - Single Stage Set: set component _stage_
    - view: stage check: tests uploaded per stage 
    - Set component stage

 - Multi Stage Set
   - data must be supplied via formatted _csv_ or _xls_ file - examples are provided on the page

 - Batches: register and populate batches
    - select batch type - batch type registration is not supported. Use Unicorn
        - option: register new batch
    - select batch
    - select components to populate
        - option: remove components

 - Image Upload: upload/view component/test images
    - __NB__ PDB token with EOS flag required (see [above](#option-to-use-eos) for instructions)
    - select PDB object (component or testRun) to associate with image
        - option: view exisitng images assoiciated with component (if any)
    - upload image

- ParameterComp: compare test parameters from repeated tests across component production
    - select component
    - select test type
    - select parameter
        - supports single values and lists - dictionaries not currently supported
    - view: comparison plot

