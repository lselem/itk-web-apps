# Beta webApp

Legacy app from previous developments.

---

## Pages

### Schema Finder
  * Get componentType/testRunType schema

### History
  * Get component history and check relationship to other components

### Migration
  * movement of components through workflow

### Visual
  * some _experimental_ visualisation
