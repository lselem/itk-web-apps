### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

def ColourCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple','yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)


infoList=["  * select *institution*",
        "  * select *project*",
        "  * get list of *componentTypes*",
        "  * select *componentType*",
        "  * get *componentType* schema"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("SchemaFinder", ":clipboard: Schema Finder", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # get list of componentTypes
        st.write("### List of *ComponentTypes* for",st.session_state.Authenticate['inst']['code'],"("+st.session_state.Authenticate['proj']['code']+")")
        # Check available component names and codes
        if st.button("reset list") or "compTypeList" not in list(pageDict.keys()): # check list
            st.write("Get list")
            pageDict['compTypeList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
        else:
            st.write("Got compTypeList")
        if len(pageDict['compTypeList'])<1:
            st.write("No data found. Check *Institution* and *Project* settings")
            st.stop()
        st.write("total components size: "+str(len(pageDict['compTypeList'])))
        df_compType=pd.json_normalize(pageDict['compTypeList'], sep = "_")
        df_compType=df_compType[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compType)

        #st.write(df_compType.columns)
        #st.write(df_compType.query('componentType_name=="Module"'))
        infra.SelectBoxDf(pageDict,'compType',df_compType,'Select a componentType', 'componentType_code')
        if st.session_state.debug:
            st.write("**DEBUG** *componentType* id")
            st.write(pageDict['compType'])

        pageDict['compTypeObj']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['compType']['componentType_code'].values[0]})
        myCompTypes=[{'project':st.session_state.Authenticate['proj']['code'],'compCode':pageDict['compType']['componentType_code'].values[0],'subCode':x['code']} for x in pageDict['compTypeObj']['types']]

        ### Select how many registrations
        st.write("### Select type of schema")
        infra.Radio(pageDict,'schemaType',["component", "test"],"Type of schema?:")

        ### Component schema
        if pageDict['schemaType']=="component":
            st.write("Component sub-types")
            st.dataframe(pd.DataFrame(myCompTypes))

            infra.Radio(pageDict,'reqSet',["required only", "all"],"Schema setting:")
            reqSet={'required only':True,'all':False}[pageDict['reqSet']]
            compSchema = DBaccess.DbGet(st.session_state.myClient,'generateComponentTypeDtoSample', {'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['compType']['componentType_code'].values[0], 'requiredOnly':reqSet})
            st.write("schema:")
            st.write(compSchema)

        ### Test schema
        if pageDict['schemaType']=="test":
            st.write("### component test types")

            stageData=[]
            for s in pageDict['compTypeObj']['stages']:
                try:
                    for tt in s['testTypes']:
                        stageData.append({'stage':s['code'],'test':tt['testType']['code']})
                except KeyError:
                        stageData.append({'stage':s['code'],'test':None})
                except TypeError:
                        stageData.append({'stage':s['code'],'test':None})

            st.write("Available stages and tests")
            df_stages=pd.DataFrame(stageData)
            st.dataframe(df_stages.style.apply(ColourCells, df=df_stages, colName='stage', flip=True, axis=1))

            st.write("### Select *Stage Test*")
            df_stages['combName']=df_stages['test'].astype(str)+" : "+df_stages['stage'].astype(str)
            infra.SelectBoxDf(pageDict,'test',df_stages,'Select a test','combName')

            infra.Radio(pageDict,'reqSet',["required only", "all"],"Schema setting:")
            reqSet={'required only':True,'all':False}[pageDict['reqSet']]
            testObj= DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':st.session_state.Authenticate['proj']['code'], 'componentType':pageDict['compType']['componentType_code'].values[0], 'code':pageDict['test']['test'].values[0], 'requiredOnly':reqSet})
            st.write(testObj)

            infra.ToggleButton(pageDict,'togDeets','More details?')
            if pageDict['togDeets']:
                deetObj= DBaccess.DbGet(st.session_state.myClient,'getTestTypeByCode', {'project':st.session_state.Authenticate['proj']['code'], 'componentType':pageDict['compType']['componentType_code'].values[0], 'code':pageDict['test']['test'].values[0]})

                for x in ['properties','parameters']:
                    st.write("### "+x)
                    st.dataframe(pd.json_normalize(deetObj[x]))
    
                infra.ToggleButton(pageDict,'togFullDeets','Full details?')
                if pageDict['togFullDeets']:
                    st.write(deetObj)
