### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import altair as alt
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

def ColourCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)


infoList=["1. Input component code",
        "2. Get component history",
        "3. Select feature",
        " * a. Geneology plot (sankey)",
        " * b. Timeline (1D scatter)",
        " * c. Check relation to another component"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Migration", ":dizzy: Migration Plots", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # get list of componentTypes
        st.write("### List of *ComponentTypes* for",st.session_state.Authenticate['inst']['code'],"("+st.session_state.Authenticate['proj']['code']+")")
        # Check available component names and codes
        if st.button("reset list") or "compTypeList" not in list(pageDict.keys()): # check list
            st.write("Get list")
            pageDict['compTypeList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
        else:
            st.write("Got compTypeList")
        if len(pageDict['compTypeList'])<1:
            st.write("No data found. Check *Institution* and *Project* settings")
            st.stop()
        st.write("compTypeList size: "+str(len(pageDict['compTypeList'])))
        df_compList=pd.json_normalize(pageDict['compTypeList'], sep = "_")
        df_compType=df_compList[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compType)

        infra.SelectBoxDf(pageDict,'compType',df_compType,'Select a componentType', 'componentType_code')
        if st.session_state.debug:
            st.write("**DEBUG** *componentType* id")
            st.write(pageDict['compType'])

        df_type=df_compList.query('componentType_code=="'+pageDict['compType']['componentType_code'].values[0]+'"')[['serialNumber','componentType_code','currentStage_code','state']]

        #####################
        ### current stage per component
        #####################
        st.write("### Individual components *Current Stage*")
        st.dataframe(df_type.style.apply(ColourCells, df=df_type, colName='currentStage_code', flip=True, axis=1))

        st.altair_chart( alt.Chart(df_type).mark_bar().encode(
                        x='currentStage_code',
                        y='count()',
                        color='currentStage_code',
                        tooltip=['count()','currentStage_code']
                            ).properties(title='current stage of components', width=500))

        if st.session_state.debug:
            st.write(sorted(list(df_type.columns)))

        #####################
        ### all stages per component
        #####################
        stageMap=[]
        testMap=[]
        st.write(pageDict['compTypeList'][0]['componentType']['code'])
        st.write(sorted(list(pageDict['compTypeList'][0].keys())))
        st.write(pageDict['compTypeList'][0]['stages'])
        for x in pageDict['compTypeList']:
            if x['componentType']['code']!=pageDict['compType']['componentType_code'].values[0]: continue
            try:
                for y in x['stages']:
                    try:
                        stageMap.append({'serialNumber':x['serialNumber'],'stage':y['code'],'dateTime':y['dateTime'],'rework':y['rework']})
                        if y['code'] in ["BONDED", "THERMAL", "BURN_IN"]:
                            testMap.append({'serialNumber':x['serialNumber']})
                    except KeyError:
                        stageMap.append({'serialNumber':x['serialNumber'],'stage':y['code'],'dateTime':y['dateTime'],'rework':"NA"})
                    except TypeError:
                        stageMap.append({'serialNumber':x['serialNumber'],'stage':y['code'],'dateTime':y['dateTime'],'rework':"NA"})
            except TypeError:
                pass
        df_stages=pd.DataFrame(stageMap)
        st.write("### Individual component stages")
        st.dataframe(df_stages.style.apply(ColourCells, df=df_stages, colName='stage', flip=True, axis=1))

        for t in testMap:
            retVal=DBaccess.DbGet(st.session_state.myClient,'getComponent',{'component':t['serialNumber']})
            st.write(retVal)
            for i in retVal['tests']:
                if i['code'] not in ["AMSLOPE","AMOFFSET"]: continue
                t.update({'test':i['code'],'id':i['id']})
        df_test=pd.DataFrame(testMap)
        st.dataframe(df_test.style.apply(ColourCells, df=df_test, colName='test', flip=True, axis=1))


        st.altair_chart( alt.Chart(df_stages).mark_bar().encode(
                        x='stage',
                        y='count()',
                        color='stage',
                        tooltip=['count()','stage']
                            ).properties(title='stages of components', width=500))


        #####################
        ### stage migration
        #####################
        # get order of stages for componentType
        compTypeObj=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['compType']['componentType_code'].values[0] })
        stageList= sorted(compTypeObj['stages'], key=lambda k: k['order'])
        if st.session_state.debug:
            st.write("**DEBUG** stage list for",pageDict['compType']['componentType_code'].values[0])
            st.write(stageList)

        # order what we've got
        df_stages_g=pd.DataFrame({'num' : df_stages.groupby( ['stage'] ).size(), 'serialNumber': df_stages.groupby( ['stage'] )['serialNumber'].aggregate(lambda x: list(x))}).reset_index()

        ### checking stages of components against componentType list
        #st.write("stagelist")
        #st.write([x['code'] for x in stageList])
        #st.write("grouped components")
        #st.write(df_stages['stage'].value_counts())

        def GetOrder(code, list):
            try:
                return list.index(code)
            except ValueError:
                return -1

        df_stages_g['stageOrder']=df_stages_g['stage'].apply(lambda x: GetOrder(x,[s['code'] for s in stageList]))

        df_missing=df_stages_g.query('stageOrder=="-1"')
        if not df_missing.empty:
            st.write("Stages not found in componentType list")
            st.write(df_missing)

        df_stages_g = df_stages_g.query('stageOrder!="-1"')

        df_stages_g=df_stages_g.sort_values(by="stageOrder", ascending=True).reset_index(drop=True)
        st.write("### Migration through stages")
        st.dataframe(df_stages_g)

        # plot
        ### need to order stages and allow for fails
        labs=list(df_stages_g['stage'].unique())
        if st.session_state.debug: st.write(labs)
        plotDict=[]
        for i, r in df_stages_g.iterrows():
            for j, s in df_stages_g.iterrows():
                if i>=j: continue
                plotDict.append({'from':labs.index(r['stage']), 'to':labs.index(s['stage']), 'value':s['num'], 'ij':str(i)+str(j)})
                #plotDict.append({'from':labs.index(r['stage']), 'to':"fail_"+r['stage'], 'value':s['num'], 'ij':str(i)+str(j)})
                break
        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = list(df_stages_g['stage']),
              color = "blue"
            ),
            link = dict(
              source = [plotDict[p]['from'] for p in range(0,len(plotDict))], # indices correspond to labels, eg A1, A2, A2, B1, ...
              target = [plotDict[p]['to'] for p in range(0,len(plotDict))],
              value = [plotDict[p]['value'] for p in range(0,len(plotDict))]
          ))])
        fig.update_layout(title_text="Stage flow", font_size=10)
        st.write(fig)
        # infra.SelectBoxDf(pageDict,'comp',df_compList.query('componentType_code=="'+pageDict['compType']['componentType_code'].values[0]+'"'),'Select a component', 'serialNumber')
        # st.write(pageDict['comp'])
