# Visualisation webApp

Visualisations of PDB strucutres.

---

## Pages

### Lineage
  * generate sankey plot for component relationships
  * based on [code](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/blob/master/strips/staves/GetFamilyTree.py) by Jiayi Chen (Brandeis University) jennyz@brandeis.edu 

### Workflow
  * generate sankey plot for component stages and testTypes

### Counting
  * check componentType statistics in database
