### standard
import streamlit as st
from core.Page import Page
### custom
from datetime import datetime
import pandas as pd
import plotly.graph_objects as go
import altair as alt
import ast
import csv
import numpy as np
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX


#####################
### useful functions
#####################

infoList=["  * Get grouped list of componentTypes at selected institutes (takes time)",
        "  - download compiled list as csv",
        "  * **other time** Upload csv",
        "   * choose primary variable",
        "   * plotting"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("AllStages", ":microscope: _All Stages_ of selected components @ institute(s)", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### shpw list of componentTypes
        compTypeRet=DBaccess.DbGet(st.session_state.myClient,'listComponentTypes',{},True)
        #st.write(compTypeRet)
        if st.session_state.debug:
            st.write("Component type keys...")
            st.write(sorted(compTypeRet[0].keys()))
        compTypeList=[]
        for x in compTypeRet:
            compTypeList.append({'name':x['name'],'code':x['code']})
            if x['types']!=None:
                compTypeList[-1]['types']=([{'name':y['name'],'code':y['code']} for y in x['types']])
            if x['stages']!=None:
                compTypeList[-1]['stages']=([{'name':y['name'],'code':y['code']} for y in x['stages']])
        # compTypeList
        st.write("### List of componentTypes")
        df_compTypeList=pd.DataFrame(compTypeList)
        st.dataframe(df_compTypeList)

        ### select list of institutes (user institute as default)
        if "instList" not in pageDict.keys():
            pageDict['instList']=[st.session_state.Authenticate['inst']['code']]
        st.write("List of institues to query",pageDict['instList'])
        if st.checkbox('Add institute?'):
            instCode=st.text_input('Add institute *code*')
            if instCode not in [x['code'] for x in st.session_state.Authenticate['instList']]:
                st.write(instCode+" code *not* recognised")
            elif instCode in pageDict['instList']:
                st.write(instCode+" already included")
            else:
                pageDict['instList'].append(instCode)
                st.write("code added")
        if st.button("reset list"):
            pageDict['instList']=[st.session_state.Authenticate['inst']['code']]

        ### compile big list
        if st.button("get data list"):# or "compInstList" not in list(pageDict.keys()): # check list
            st.write("Compiling list (this will take _a few_ minutes)...")
            loopList=pageDict['instList']
            dataList=[]
            badList=[]
            cols=['componentType_code','currentLocation_code','state', 'stages']
            try:
                del df_total
            except NameError:
                pass

            status_bar=st.progress(0.0) #st.empty()
            status_text=st.empty()
            maxVal=500
            for c,i in enumerate(loopList):
                loopText="inst: "+i+", "+str(c+1)+" of "+str(len(loopList))
                status_text.text(loopText)
                status_bar.progress(1.0*c/len(loopList))
                #st.write("inst: "+i['code'])
                pgndx=0
                while pgndx>-1:
                    compList=st.session_state.myClient.get('listComponents', json={'project':st.session_state.Authenticate['proj']['code'], 'currentLocation':i, 'pageInfo':{'pageSize':maxVal,'pageIndex':pgndx}})
                    st.write("comp batch",str(pgndx)+":",len(compList.data),"/",compList.page_info['total'],"..",compList.data[0]['componentType']['code'])
                    df_instComps=pd.json_normalize(compList.data, sep = "_")
                    if df_instComps.empty:
                        badList.append(i)
                        continue
                    df_instComps['sum']=1
                    try:
                        df_total=df_total.append(df_instComps, ignore_index=True)
                    except NameError:
                        df_total=df_instComps.copy()
                    if compList.total>(pgndx+1)*maxVal:
                        pgndx+=1
                    else:
                        pgndx=0
                        break
            else:
                status_text.text("looping complete")
                status_bar.progress(1.0)
            pageDict['compInstList']=df_total#.groupby(["currentLocation_code","componentType_code","state"]).sum().reset_index()

        ### check data
        if "compInstList" not in list(pageDict.keys()):
            st.write("No data collected")
            st.stop()

        ### show grouped data
        cols=['componentType_code','currentLocation_code','state']
        st.dataframe(pageDict['compInstList'][cols+['sum']].groupby(cols).sum().reset_index())

        ### filter data
        st.write("### filtering...")
        filterList=["TEST","test","20USC","William","SVEN","Michael","uun"]
        if st.session_state.debug:
            st.write("filtering:",filterList)
        for x in filterList:
            df_filt=pageDict['compInstList'][pageDict['compInstList']['componentType_code'].str.contains(x)][cols]
            if not df_filt.empty:
                st.write("check:",x)
                st.write(df_filt)
            pageDict['compInstList']=pageDict['compInstList'][~pageDict['compInstList']['componentType_code'].str.contains(x)]
        else:
            st.write("filtering done")

        ### manipulate data
        def stageSort(x):
            try:
                return [y['code'] for y in x['stages']]
            except TypeError:
                return []
            return []
        pageDict['compInstList']['simpleStages']=pageDict['compInstList'].apply(lambda x: stageSort(x), axis=1)
        # for index,row in pageDict['compInstList'].query('componentType_code=="'+myComp+'"').iterrows():
        #     print(row['simpleStages'])

        ### select data
        infra.SelectBox(pageDict,'compType',pageDict['compInstList']['componentType_code'].unique(),'Select componentType')
        #df_plot=pageDict['compInstList'].query('componentType_code=="'+pageDict['compType']+'"')['simpleStages'].apply(pd.Series).reset_index().melt(id_vars=['index']).dropna()#[['index', 'value']].set_index('index')
        df_sub=pageDict['compInstList'].query('componentType_code=="'+pageDict['compType']+'"')
        df_plot=pd.DataFrame({'currentLocation_code':np.repeat(df_sub['currentLocation_code'].values, df_sub['simpleStages'].str.len()),
                                'sum':np.repeat(df_sub['sum'].values, df_sub['simpleStages'].str.len()),
                        'simpleStage':np.concatenate(df_sub['simpleStages'].values
                        )})
        if st.session_state.debug:
            st.dataframe(df_plot.groupby(['currentLocation_code','simpleStage']).sum().reset_index())

        ### plot data
        st.write("### Plotting")
        yType=st.radio("y-axis",('linear','log','symlog'))
        stageChart=alt.Chart(df_plot).mark_bar().encode(
            x='simpleStage',
            y=alt.Y('count()',scale=alt.Scale(type=yType)),
            color='simpleStage',
            row='currentLocation_code',
            tooltip=['simpleStage','count()']
        ).configure_legend(labelLimit= 0, symbolLimit=0).properties(width=800)

        st.altair_chart(stageChart)
