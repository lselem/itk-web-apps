### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import pandas as pd
import datetime
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
import cv2
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################
infoList=[" * check component sub-type",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("JSON", ":microscope: Embedded visualisations", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### 
        st.write("## store / retrieve Json")


        df_input = chnx.UploadJSONChunk(pageDict)

        st.write(df_input)
        # infra.Radio(pageDict,'inputType',["area","camera"],"Select input type:")


        # if pageDict['inputType'].lower()=="area":
        
        #     data_raw = st.text_area('thing',None)

        #     if data_raw==None or data_raw=="None":
        #         st.write("Please input dictionary")
        #         st.stop()
            
        #     # csvStrIO = StringIO(csvStr)
        #     try:
        #         st.write("raw data:",data_raw)
        #         data_eval=eval(data_raw)
        #         st.write("eval data:",data_eval)

        #         if type(data_eval)==type({}):
        #             st.write("dictionary found!")
        #             df_in = pd.DataFrame([data_eval])
        #             st.write(df_in)
        #         else:
        #             st.write("not sure what to do with type:",type(data_eval))
        #     except TypeError:
        #         st.write("type error")

        # elif pageDict['inputType'].lower()=="camera":
            
        #     inPic=st.camera_input("input pic")

        #     if inPic is not None:
        #         bytes_data = inPic.getvalue()
        #         cv2_img = cv2.imdecode(np.frombuffer(bytes_data, np.uint8), cv2.IMREAD_COLOR)

        #         detector = cv2.QRCodeDetector()

        #         data_raw, bbox, straight_qrcode = detector.detectAndDecode(cv2_img)

        #         st.write("raw data:",data_raw)

        #         # csvStrIO = StringIO(csvStr)
        #         try:
        #             data_eval=eval(data_raw)
        #             st.write("eval data:",data_eval)

        #             if type(data_eval)==type({}):
        #                 st.write("dictionary found!")
        #                 df_in = pd.DataFrame([data_eval])
        #                 st.write(df_in)
        #             else:
        #                 st.write("not sure what to do with type:",type(data_eval))
        #         except TypeError:
        #             st.write("type error")

        # else:
        #     st.write("Dunno man :shrug:")

        

