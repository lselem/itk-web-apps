### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import pandas as pd
import datetime
import ast
import csv
import json
import copy
import altair as alt
import numpy as np
import os
from io import StringIO
import io
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################
infoList=[" * check component sub-type",
        " * upload component schemas to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("QR", ":microscope: Embedded visualisations", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### Grafana
        st.write("## Read-in QR code")

        
