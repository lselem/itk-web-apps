### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList=[  " * Get components at institute",
            " * View componentTypes",
            "  - Select componentTypes",
            "  - View components",
            "  - Remove Assembled optional",
            " * Select Component",
            "  - View stage history",
            " * Compare to componentType",
            "  - View stages",
            "  - select stage",
            "  - View tests",
            "* Optional delete component"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Inventory", ":microscope: Components at Institute", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        if st.checkbox("Quick Component Check?"):
            if 'SCC' not in pageDict.keys():
                pageDict['SCC']={}
            chnx.SelectComponentChunk(pageDict['SCC'])
            stTrx.DebugOutput("SelectComponentChunk info.",pageDict['SCC'])
            try:
                st.dataframe(pd.json_normalize(pageDict['SCC']['comp'], sep = "_"))
            except TypeError:
                st.write("no component info.")

        st.write("## Check componentTypes in PDB")
        if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
            st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
        else:
            st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")

        ### stop if nothing found
        if len(pageDict['compList'])<1:
            st.write("### No components found")
            st.stop()

        df_compList=pd.json_normalize(pageDict['compList'], sep = "_")
        df_compTypes=df_compList[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compTypes)


        pageDict['selComps']=st.multiselect('Select componentTypes:', df_compTypes['componentType_code'].astype(str).unique().tolist())
        queryStr=""
        for e,c in enumerate(pageDict['selComps']):
            if e>0:
                queryStr+=' | '
            queryStr+='componentType_code=="'+c+'"'
        stTrx.DebugOutput("queryStr",queryStr)

        if len(pageDict['selComps'])<1 and not st.checkbox("Use all components?"):
            st.stop()

        if len(pageDict['selComps'])>0:
            st.write("### List of *"+", ".join(pageDict['selComps'])+"* components")
        else: 
            st.write("### List of *all* components")
        
        st.write("(colour--> _",'currentStage_code',"_)")
        df_comps=df_compList.sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        if len(pageDict['selComps'])>0:
            df_comps=df_compList.query(queryStr).sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)
        # df_comps=df_compList.query('componentType_code=="'+pageDict['compType']['componentType_code'].values[0]+'"')
        colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','assembled','cts']

        ### make all columns strings
        for c in colz:
            df_comps[c]=df_comps[c].astype(str)

        infra.ToggleButton(pageDict,'removeAss',"Remove assembled components list?")
        if pageDict['removeAss']:
            df_comps=df_comps.query('assembled=="False"')

        try:
            stTrx.ColourDF(df_comps[colz],'currentStage_code')
            #st.dataframe(df_comps[colz].style.apply(stTrx.ColourCells, df=df_comps[colz], colName='currentStage_code', flip=True, axis=1))
        except IndexError:
            st.dataframe(df_comps[colz])
        st.write("Total:",len(df_comps.index))
        ### download dataframe
        st.download_button(label="Write csv", data=df_comps[colz].to_csv(index=False),file_name="components_"+st.session_state.Authenticate['inst']['code']+"_"+st.session_state.Authenticate['proj']['code']+".csv")

        st.write("---")

        ### simple stats on components: status, currentStage
        st.write("## Group Component Statistics")
        # loop over componentTypes
        if len(pageDict['selComps'])>0:
            for compType in pageDict['selComps']:
                st.write(f"#### {compType}")
                df_compType=df_comps.query(f'componentType_code=="{compType}"').reset_index(drop=True)
                st.write(f"__All types total {len(df_compType.index)}__")
                
                st.write(df_compType.groupby(by='type_code').count().rename(columns={'id':"count"}).reset_index()[['type_code','count']])
                ### select sub-types
                if st.checkbox(f"See all {compType} types?"):
                    pageDict['selSubs']=df_compType['type_code'].unique()
                else:
                    pageDict['selSubs']=st.multiselect(f'Select {compType} types:', df_compType['type_code'].astype(str).unique().tolist())

                # status info.
                # for subType in df_compType['type_code'].unique():
                for subType in pageDict['selSubs']:
                    df_subType=df_compType.query(f'type_code=="{subType}"').reset_index(drop=True)
                    st.write(f"**{subType} Information**: total {len(df_subType.index)}")
                    # st.write(f"__type total {len(df_subType.index)}__")
                    df_subType=df_subType.rename(columns={'id':"count"})

                    st.write(f" - **Status** ({'/'.join(['ready','reworked','trashed'])})")
                    st.write(df_subType.groupby(by=["state"])['count'].count())
                    st.write(f" - **Current Stage** (excluding unpopulated stages)")
                    st.write(df_subType.groupby(by=["currentStage_code"])['count'].count())
                    st.write(f" - **Vintage** (by creation year)") 
                    df_subType['year']=df_subType['cts'].apply(lambda x: x.split('-')[0] if len(x.split('-'))>0 else x )
                    st.write(df_subType.groupby(by=["year"])['count'].count())

        else:
            # status info.
            st.write("**Status**:")
            for compState in ["ready","reworked","trashed"]:
                st.write("-",compState,":",df_comps.query('state=="'+compState+'"')['serialNumber'].count())
            # currentStage info.
            st.write("**Current Stage**:")
            for compStage in sorted(df_comps['currentStage_code'].unique()):
                st.write("-",compStage,":",df_comps.query('currentStage_code=="'+compStage+'"')['serialNumber'].count())
            # vintage info.
            st.write(f"**Vintage**:") 
            for cts_year in [str(t) for t in range(2020,2030,1)]:
                df_year=df_comps.query(f'cts.str.contains("{cts_year}")', engine="python")
                if not df_year.empty:
                    st.write("-",cts_year,":",df_year['serialNumber'].count())





        st.write("---")

        st.write("## Individiual Component Information")

        # filter components
        sel_compType=st.selectbox('Select componentType:', df_comps['componentType_code'].unique())
        sel_subType=st.selectbox('Select sub-Type:', df_comps.query(f'componentType_code=="{sel_compType}"')['type_code'].unique())
        df_subs=df_comps.query(f'componentType_code=="{sel_compType}" & type_code=="{sel_subType}"')
        
        if "compKey" not in pageDict.keys():
            pageDict['compKey']='serialNumber'
        stTrx.DebugOutput("using distinguishing key:",pageDict['compKey'])
        if st.session_state.debug:
            infra.SelectBox(pageDict,'compKey',df_comps.columns,'Select key')
            st.write("unique values:",len(df_subs[pageDict['compKey']].unique()))
            st.write("total length:",len(df_subs))

        infra.SelectBox(pageDict, 'selCompVal', df_subs[pageDict['compKey']].astype(str).unique().tolist(), 'Select component '+pageDict['compKey']+':')

        ### individual data
        st.write("#### Individual Information")
        df_compSel=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'"')
        st.dataframe(df_compSel[colz])

        if df_compSel.shape[0]>1:
            st.write("Multiple entries with "+pageDict['compKey']+":",pageDict['selCompVal'])
            infra.SelectBox(pageDict, 'selCompDate', df_compSel['stateTs'].astype(str).unique().tolist(), 'Select component creation date:')
            pageDict['selCompID']=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'" & stateTs=="'+pageDict['selCompDate']+'"')['id'].values[0]
        else:
            pageDict['selCompID']=df_comps.query(pageDict['compKey']+'=="'+pageDict['selCompVal']+'"')['id'].values[0]

        pageDict['thisComp']=df_comps.query('id=="'+pageDict['selCompID']+'"')

        ### delete component?
        pdbTrx.DeleteCoTeSt(pageDict,'delComp','deleteComponent',pageDict['thisComp']['serialNumber'].values[0])

        ### stage history
        st.write("#### Stage history")

        stTrx.DebugOutput("full component information",pageDict['thisComp'].to_dict())
        if "thisComp" not in pageDict.keys():
            st.write("Select component")
            st.stop()

        df_stages=pageDict['thisComp'][['serialNumber','stages']]
        df_stages=df_stages.explode('stages').reset_index(drop=True)
        ### split dictionary into columns
        df_life=pd.concat([df_stages.drop(['stages'], axis=1), df_stages['stages'].apply(pd.Series)], axis=1)
        stTrx.ColourDF(df_life,'code')
        #st.dataframe(df_life.style.apply(stTrx.ColourCells, df=df_life, colName='code', flip=True, axis=1))

        ### checklist
        st.write("#### Test Checklist")
        compVal=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pageDict['thisComp']['serialNumber'].values[0]})
    
        # test uploads
        df_cl=pdbTrx.FillCheckList(compVal)
        if df_cl.empty:
            st.write("Cannot generate Test Upload Summary :(")
            return 
        st.write("Test Upload Summary (green: PASSED, red: FAILED, grey: MISSING)")
        sum_cols=['stage_order','stage_code','test_code','present','passed']
        st.write(df_cl[sum_cols].style.apply(pdbTrx.HighlightTests, axis=1))


        ### Add comparison to archetype componentType stages
        infra.ToggleButton(pageDict,'allStages',"See all "+pageDict['thisComp']['componentType_code'].values[0]+" componentType stages?")
        if pageDict['allStages']:
            compTypeRet=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['thisComp']['componentType_code'].values[0]})
            df_compStages=pd.json_normalize(compTypeRet['stages'], sep = "_")
            st.dataframe(df_compStages)
            infra.SelectBoxDf(pageDict,'stageCode',df_compStages,'Select stage', 'code')
            try:
                df_stageTests=pd.json_normalize(pageDict['stageCode']['testTypes'].to_list()[0], sep = "_")
                st.dataframe(df_stageTests[['testType_code','order','nextStage']])
            except TypeError:
                st.write("No tests found")            
            except KeyError:
                st.write("No information found")

        
