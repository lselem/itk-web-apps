### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
import barcode
import cv2
import numpy as np
from barcode.writer import ImageWriter
from io import BytesIO
from PIL import Image
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import commonCode.StreamlitTricks as stTrx
import commonCode.PDBTricks as pdbTrx
import commonCode.codeChunks as chnx

from  .CommonCode import *

#####################
### useful functions
#####################

### define set batch function
### remove from batch if oldBatchName!=None, and add to batch if newBatchName!=None
def SetComponentBatch(myClient, projectCode, compId, oldBatchID, newBatchId):
    
    ### get batch ids
    # remove batch
    if oldBatchID==None:
        st.write("no OLD batch info. set. Continue without")
    # add batch
    if newBatchId==None:
        st.write("no NEW batch info. set. Continue without")
    
    ### make jsons & upload
    # remove batch
    if oldBatchID!=None:
        remove_json={
          'id': oldBatchID,
          'component': compId
        }
        # API calls
        try:
            try:
                remInfo=myClient.post('removeBatchComponent', json=remove_json)
                st.write("### **Successful removal ** for:",remInfo['number'])
            except KeyError:
                st.write("### **Successful removal, but don't understand return object")
        except itkX.BadRequest as b:
            st.write("### :no_entry_sign: batch removal **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks 
            return False
    
    # add batch
    if newBatchId!=None:
        add_json={
          'id': newBatchId,
          'component': compId
        }
        # API calls   
        try:
            try:
                addInfo=myClient.post('addBatchComponent', json=add_json)
                st.write("### **Successful addition ** for:",addInfo['number'])
            except KeyError:
                st.write("### **Successful addition, but don't understand return object")
        except itkX.BadRequest as b:
            st.write("### :no_entry_sign: batch addition **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            return False
    
    return True

### Stage Coherency stuff
def GetCoherentStage(compInfo):
    
    coStage=None
    ### perhaps componentType distinction unnecessary, but leaving for the moment
    # bare module
    if compInfo['componentType']['code']=="BARE_MODULE":
        st.write("recognise componentType: **BARE_MODULE**")
        coStage=compInfo['currentStage']['code']
    # module
    elif compInfo['componentType']['code']=="MODULE":
        st.write("recognise componentType: **MODULE**")
        # coStage="MODULE/"+compInfo['currentStage']['code']
        coStage=compInfo['currentStage']['code']
    # other (unknown)
    else:
        st.write(f"don't recognise parent componentType: {compInfo['componentType']['code']}") 

    return coStage


infoList=["  * select *component*",
        "  * get info. including relations"]
        
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Component Check", ":fast_forward: Component Check", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("## Select component")

        infra.Radio(pageDict,'inputType',["Enter identifier", "QRcode", "Select from inventory"],"Select input method:")
        st.write("### ",pageDict['inputType'])

        if "qrcode" in pageDict['inputType'].lower():
            
            inPic=st.camera_input("Input picture:")
            infra.Radio(pageDict,'altID',['serialNumber','alternativeIdentifier'],"Type of identifier")

            if inPic is not None:
                bytes_data = inPic.getvalue()
                cv2_img = cv2.imdecode(np.frombuffer(bytes_data, np.uint8), cv2.IMREAD_COLOR)
                detector = cv2.QRCodeDetector()
                data_raw, bbox, straight_qrcode = detector.detectAndDecode(cv2_img)

                st.write("raw input data:",data_raw)
                try:
                    stTrx.DebugOutput("raw data:",data_raw)

                    if 'comp' not in pageDict.keys() or st.button("Get component"):
                        pageDict['inComp']=data_raw
                        compQueryJson={'component':pageDict['inComp'],'type':"SN"}
                        if pageDict['altID']=='alternativeIdentifier':
                            compQueryJson['type']='altID'
                        stTrx.DebugOutput("queryJson:",compQueryJson)

                        pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

                except SyntaxError:
                    st.write("Syntax error --> No QR found")
                    st.write("Check image/brightness")


        elif "identifier" in pageDict['inputType'].lower():

            infra.TextBox(pageDict,'inComp',"Enter component identifier")
            infra.Radio(pageDict,'altID',['serialNumber','alternativeIdentifier'],"Type of identifier")

            if pageDict['inComp']==None or len(pageDict['inComp'])<1:
                st.write("Enter identifier to get component information")
                st.stop()

            if 'comp' not in pageDict.keys() or st.button("Get component"):
                compQueryJson={'component':pageDict['inComp'],'type':"SN"}
                if pageDict['altID']=='alternativeIdentifier':
                    compQueryJson['type']='altID'
                stTrx.DebugOutput("queryJson:",compQueryJson)

                pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

        else:
            ### select components by hand
            if st.button("reset list") or "compList" not in list(pageDict.keys()): # check list
                st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
                pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
            else:
                st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            df_compList=pd.json_normalize(pageDict['compList'], sep = "_")

            df_subComps=pdbTrx.SelectManyDf(pageDict,df_compList,'componentType_code')
            stTrx.Stringify(df_subComps)
            # st.write(df_subComps)
            infra.SelectBox(pageDict,'selSN',df_subComps['serialNumber'].to_list(),'Select serialNumbers:')
            stTrx.DebugOutput('Selected serialnumber: ',pageDict['selSN'])
            pageDict['comp'] = next(item for item in pageDict['compList'] if item['serialNumber'] == pageDict['selSN'])
        

        if 'comp' not in pageDict.keys():
            st.write("Enter identifier to get component information")
            st.stop()

        ### get full component info. (not just summary from list)
        if pageDict['comp']==None:# not in pageDict.keys():
            st.write("Please entry component identifier.")
            st.stop()
        pageDict['compFull']=DBaccess.DbGet(st.session_state.myClient,'getComponent',{'component':pageDict['comp']['code'] })


        stTrx.DebugOutput("ComponentJson:",pageDict['compFull'])
        if pageDict['compFull']==None:
            st.write("No component information found. Check inputs")
            st.stop()

        st.write("---")
        st.write("## ",pageDict['compFull']['serialNumber']," Results")

        st.write("### Component Highlights")

        st.write(f"project: **{pageDict['compFull']['project']['code']}**")
        st.write(f"componentType: **{pageDict['compFull']['componentType']['code']}** ({pageDict['compFull']['type']['code']})")
        st.write(f"origin institution: **{pageDict['compFull']['institution']['code']}**")
        st.write(f"current location: **{pageDict['compFull']['currentLocation']['code']}**")
        if st.checkbox("Check location coherency"):
            pdbTrx.CheckLocationCoherence(st.session_state.myClient,pageDict['compFull']['serialNumber'])
        st.write(f"current stage: **{pageDict['compFull']['currentStage']['code']}**")
        if st.checkbox("Check stage coherency"):
            ### check pixel component
            if pageDict['compFull']['project']['code']!="P":
                st.write("Only available for _Pixels_ components")
            else:                
                st.write("See details on Stage Coherency [here](https://gitlab.cern.ch/lmeng/stagecoherency)")
                pdbTrx.CheckStageCoherence(st.session_state.myClient,pageDict['compFull']['serialNumber'])
        st.write(f"assembled: **{str(pageDict['compFull']['assembled'])}**")

        st.write("barcode (code128):")
        bcFileName=MakeBcFile(pageDict['compFull']['serialNumber'])
        st.image(ViewFile(bcFileName))
        with open(bcFileName, "rb") as file:
            st.download_button(
                label="Download barcode",
                data=file,
                file_name=pageDict['compFull']['serialNumber']+".jpg",
                mime="image/jpeg"
            )

        ### display child and parent info.
        for relative in ["Children","Parents"]:
            st.write("__"+relative+"__")
            try:
                if len(pageDict['compFull'][relative.lower()])<1:
                    st.write("No",relative.lower(),"found")
                    continue
                df_relative=pd.json_normalize(pageDict['compFull'][relative.lower()], sep = "_")
                
                relCols=['component_serialNumber','componentType_code','component_state']
                if 'type_code' in df_relative.columns:
                    relCols.insert(2,'type_code')
                # st.write(df_relative[relCols])
                df_relative=df_relative.dropna(subset=['component_serialNumber'], inplace=False)
                stTrx.ColourDF(df_relative[relCols],'componentType_code')
                #st.dataframe(df_relative[relCols].style.apply(stTrx.ColourCells, df=df_relative[relCols], colName='componentType_code', flip=True, axis=1))

                if "child" in relative.lower():
                    if st.button(f"disassemble {relative}?"):
                        chnx.DisassembleChunk(pageDict['compFull'])
                if "parent" in relative.lower():
                    for k,sn in enumerate(df_relative[relCols]['component_serialNumber'].dropna().to_list()):
                        if st.button(f"disassemble {sn}?",key=k+300):
                            parComp=DBaccess.DbGet(st.session_state.myClient,'getComponent',{'component':sn})
                            chnx.DisassembleChunk(parComp)

            except TypeError:
                st.write("No",relative.lower(),"found")            
            except KeyError:
                st.write("No",relative.lower(),"found")
            # except KeyError:
            #     st.write("Issue getting",relative.lower(),". Check full component info.")
                

        st.write("### Component Stage History")
        df_comp=pd.json_normalize(pageDict['compFull'], sep = "_")
        df_stages=df_comp[['serialNumber','stages']]
        df_stages=df_stages.explode('stages').reset_index(drop=True)
        ### split dictionary into columns
        df_life=pd.concat([df_stages.drop(['stages'], axis=1), df_stages['stages'].apply(pd.Series)], axis=1)
        stTrx.ColourDF(df_life,'code')
        #st.dataframe(df_life.style.apply(stTrx.ColourCells, df=df_life, colName='code', flip=True, axis=1))


        infra.ToggleButton(pageDict,'togStageChange','Change Component Stage?')
        if pageDict['togStageChange']:
            st.write(":warning: __Warning__: Only to be used by _institute authorities_ :warning:")
            st.write(" - :arrow_right: *Optimise your workflow* :arrow_left: ")

            pageDict['componentType']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':pageDict['compFull']['project']['code'],'code':pageDict['compFull']['componentType']['code']})
            infra.SelectBox(pageDict,'stageChange',pageDict['componentType']['stages'],'Select Stage:','name')
            st.write("Stage selected (code):",pageDict['stageChange']['name'],"("+pageDict['stageChange']['code']+")")
            if st.button("Set stage"):
                ### set stage first!
                try:
                    pageDict['setVal']=DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':pageDict['compFull']['serialNumber'], 'stage':pageDict['stageChange']['code']})
                    st.write("### **Successful stage set** (",pageDict['stageChange']['code'],") for:",pdbTrx.FindKey(pageDict['setVal'],'id'))
                    st.balloons()
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Stage (",pageDict['stageChange']['code'],")setting **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks


        st.write("### Component batches")

        if "batchList" not in pageDict.keys() or st.button("re-check batches"):
            pageDict['batchList']=DBaccess.DbGet(st.session_state.myClient, 'listBatchesByComponent', {'component':pageDict['compFull']['code']},True)
        df_bl=pd.DataFrame(pageDict['batchList'])
        st.write(df_bl)
        if df_bl.empty:
            st.write("Component has no batch")

        infra.ToggleButton(pageDict,'togBatchChange','Change batch?')
        if pageDict['togBatchChange']:
            
            st.write("### Select target batch")
            methodList=['input info.','select from list']
            methodSel=st.radio("method:",methodList)

            if "input" in methodSel:
                st.write("### Input batch info.")

                inBatchType=st.text_input("Input batch type:")
                inBatchNum=st.text_input("Input batch number:")
                inBatch_json={
                        'project': st.session_state.Authenticate['proj']['code'],
                        'batchType': inBatchType,
                        'number': inBatchNum
                        }

                if inBatchType==None or inBatchType=="" or inBatchNum==None or inBatchNum=="":
                    st.write("input batch information")
                    st.stop()

                if "batchInfo" not in pageDict.keys() or st.button("reset batch"):
                    pageDict['batchInfo']=DBaccess.DbGet(st.session_state.myClient, 'getBatchByNumber', inBatch_json)

                ### get batch?

            elif "list" in methodSel:
                st.write("### Select batch from list")

                ### get batches?
                batchList=DBaccess.DbGet(st.session_state.myClient,'listBatches', {'project':st.session_state.Authenticate['proj']['code']}, True)
                st.write(f"Found {len(batchList)} batchTypes for project: {st.session_state.Authenticate['proj']['code']}")

                df_bl=pd.DataFrame(batchList)
                for col in df_bl.columns:
                    df_bl[col]=df_bl[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x else x)
                if st.checkbox('See batch list?'):
                    st.write("### list of batches")
                    st.dataframe(df_bl)
            

                selBatchType=st.selectbox('Select batchType',df_bl['batchType'].unique())

                selBatchNum=st.selectbox('Select number',df_bl.query('batchType=="'+selBatchType+'"')['number'].to_list())

                batchId=df_bl.query('batchType=="'+selBatchType+'" & number=="'+selBatchNum+'"')['id'].values[0]
            
                if batchId==None or batchId=="":
                    st.write("Input batch identifier")
                    st.stop()
            
                if "batchInfo" not in pageDict.keys() or st.button("reset selected batch") or batchId!=pageDict['batchInfo']['id']:
                    pageDict['batchInfo']=DBaccess.DbGet(st.session_state.myClient,'getBatch', {'id':batchId})

            oldBatchObj={'id':None}
            if len(pageDict['batchList'])<1:
                st.write("no previous batch used")
            else:
                oldBatchObj=pageDict['batchList'][0]
                st.write(f"OLD batchID: {oldBatchObj['id']} ({oldBatchObj['number']})")
                
            st.write(f"NEW batchID: {pageDict['batchInfo']['id']} ({pageDict['batchInfo']['number']})")

            # displaceBatchComponent had issue with permissions so use own function

            if st.button("Change batch"):
                if SetComponentBatch(st.session_state.myClient, st.session_state.Authenticate['proj']['code'], pageDict['compFull']['code'], oldBatchObj['id'], pageDict['batchInfo']['id']):
                    st.balloons()
            

        st.write("---")

        st.write("## Additional information")

        ### full info. option
        infra.ToggleButton(pageDict,'togFullInfo','Full Component Info.')
        if pageDict['togFullInfo']:
            st.write("### Full component information")
            st.write(pageDict['compFull'])

        ### lineage option
        infra.ToggleButton(pageDict,'togGenRel','Check Generic Relations')
        if pageDict['togGenRel']:
            st.write("This can take a minute...")
            try:
                if pageDict['compFull']['componentType']['name'].strip()!=pageDict['genRel']['label'][0].split('(')[0].strip():
                    pageDict['genRel']=GetRelationsGeneral(pageDict['compFull']['project']['code'],pageDict['compFull']['componentType']['code'],pageDict['compFull']['type']['code'])
                else:
                    st.write("got info. for:",pageDict['genRel']['label'][0].split('(')[0].strip())
            except KeyError:
                pageDict['genRel']=GetRelationsGeneral(pageDict['compFull']['project']['code'],pageDict['compFull']['componentType']['code'],pageDict['compFull']['type']['code'])
            #st.write(pageDict['genRel'])
            st.write("### Relations list")
            df_genRel=pd.DataFrame(pageDict['genRel'])
            infra.ToggleButton(pageDict,'noObs','Remove "obsolete" labels?')
            if pageDict['noObs']==True:
                st.dataframe(df_genRel[~df_genRel['label'].str.contains("bsolete")])
            else:
                st.dataframe(df_genRel)
            ### plotting
            src=[]
            trg=[]
            val=[]
            for i,x in enumerate(pageDict['genRel']['label']):
                if pageDict['noObs']==True and "obsolete" in x.lower():
                    continue
                #st.write("label:",x,"("+str(i)+")")
                par=pageDict['genRel']['parentNode'][i]
                if par=="None":
                    src.append(0)
                    trg.append(0)
                    val.append(0)
                    continue
                p=pageDict['genRel']['childNode'].index(par)
                #st.write("parent:",par,"("+str(p)+")")
                src.append(i)
                trg.append(p)
                val.append(1)
            st.write("### Plotting")
            fig = go.Figure(data=[go.Sankey(
                                        node = dict(
                                          pad = 15,
                                          thickness = 20,
                                          line = dict(color = "black", width = 0.5),
                                          label = pageDict['genRel']['label'],
                                          color = "blue"
                                        ),
                                        link = dict(
                                          source = src, # indices correspond to labels, eg A1, A2, A1, B1, ...
                                          target = trg,
                                          value = val
                                      ))])
            fig.update_layout(title_text=pageDict['compFull']['componentType']['code']+"("+pageDict['compFull']['project']['code']+") lineage", font_size=10)
            st.plotly_chart(fig)


        ### workflow option
        infra.ToggleButton(pageDict,'togGenWork','Generic Workflow')
        if pageDict['togGenWork']:
            try:
                if pageDict['compFull']['componentType']['code']!=pageDict['componentType']['code'] or pageDict['compFull']['project']['code']!=pageDict['project']['code']:
                    pageDict['componentType']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':pageDict['compFull']['project']['code'],'code':pageDict['compFull']['componentType']['code']})
                else:
                    st.write("got info. for:",pageDict['componentType']['code']," ("+pageDict['project']['code']+")")
            except KeyError:
                pageDict['componentType']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':pageDict['compFull']['project']['code'],'code':pageDict['compFull']['componentType']['code']})
            st.write("### Stage list")
            compStageList=[]
            for x in pageDict['componentType']['stages']:
                compStageList.append({'name':x['name'],'code':x['code'],'order':x['order']})
                try:
                    #st.write([y['testType'] for y in x['testTypes']])
                    compStageList[-1]['testTypes']=[{'name':y['testType']['name'],'code':y['testType']['code']} for y in x['testTypes']]
                except TypeError:
                    pass
                except KeyError:
                    pass
            st.dataframe(compStageList)

            pageDict['workflow']={'label':[],'source':[],'target':[],'value':[]}
            stageCount=0
            count=0
            for cs in compStageList:
                pageDict['workflow']['label'].append(cs['name'])
                pageDict['workflow']['source'].append(stageCount)
                stageCount=len(pageDict['workflow']['label'])-1
                pageDict['workflow']['target'].append(stageCount)
                pageDict['workflow']['value'].append(0)
                try:
                    for t in cs['testTypes']:
                        pageDict['workflow']['source'].append(len(pageDict['workflow']['label']))
                        pageDict['workflow']['label'].append(t['name'])
                        pageDict['workflow']['target'].append(stageCount)
                        pageDict['workflow']['value'].append(1)
                except KeyError:
                    pass
            st.write("### Plotting")
            fig = go.Figure(data=[go.Sankey(
                                    orientation="h",
                                        node = dict(
                                          pad = 15,
                                          thickness = 20,
                                          line = dict(color = "black", width = 0.5),
                                          label = pageDict['workflow']['label'],
                                          color = "blue"
                                        ),
                                        link = dict(
                                          source = pageDict['workflow']['source'],
                                          target = pageDict['workflow']['target'],
                                          value = pageDict['workflow']['value']
                                      ))])
            fig.update_layout(title_text=pageDict['compFull']['componentType']['code']+"("+pageDict['compFull']['project']['code']+") stages and tests", font_size=10)
            st.plotly_chart(fig)

        ### specific relations option
        infra.ToggleButton(pageDict,'togSpecRel','Specific Relations')
        if pageDict['togSpecRel']:
            try:
                if pageDict['compFull']['serialNumber']!=pageDict['origin']:
                    parentTree=GetParentTree(st.session_state.myClient, pageDict['compFull']['serialNumber'], "origin")
                    st.write(parentTree)
                    pageDict['origin']=parentTree[-1]['serialNumber']
                    # get full tree from primary ancestor
                    childTree=GetChildTree(st.session_state.myClient, pageDict['compFull']['serialNumber'], "origin")
                    genTree=parentTree+childTree
                    pageDict['df_hist']=pd.DataFrame(genTree).drop_duplicates(subset='serialNumber', keep="first").sort_values(by ='gen' ).reset_index(drop=True)
                    pageDict['df_hist']['gen']=pageDict['df_hist']['gen']-pageDict['df_hist']['gen'].min()
                else:
                    st.write("got info. for:",pageDict['origin'])
            except KeyError:
                parentTree=GetParentTree(st.session_state.myClient, pageDict['compFull']['serialNumber'], "origin")
                pageDict['origin']=parentTree[-1]['serialNumber']
                # get full tree from primary ancestor
                childTree=GetChildTree(st.session_state.myClient, pageDict['compFull']['serialNumber'], "origin")
                genTree=parentTree+childTree
                pageDict['df_hist']=pd.DataFrame(genTree).drop_duplicates(subset='serialNumber', keep="first").sort_values(by ='gen' ).reset_index(drop=True)
                pageDict['df_hist']['gen']=pageDict['df_hist']['gen']-pageDict['df_hist']['gen'].min()
            stTrx.ColourDF(pageDict['df_hist'],'gen')
            #st.dataframe(pageDict['df_hist'].style.apply(stTrx.ColourCells, df=pageDict['df_hist'], colName='gen', flip=True, axis=1))
            ### plotting
            src=[]
            trg=[]
            val=[]
            for index,row in pageDict['df_hist'].iterrows():
                rel=row['relation']
                #st.write("check:",rel)
                if rel=="origin":
                    src.append(0)
                    trg.append(0)
                    val.append(0)
                    continue
                p=pageDict['df_hist'].query('serialNumber=="'+str(rel)+'"').index[0]
                #st.write("parent:",par,"("+str(p)+")")
                src.append(index)
                trg.append(p)
                val.append(1)
            st.write("### Plotting")
            fig = go.Figure(data=[go.Sankey(
                                        node = dict(
                                          pad = 15,
                                          thickness = 20,
                                          line = dict(color = "black", width = 0.5),
                                          label = [x+'\n ('+y+')' for x,y in zip(list(pageDict['df_hist']['componentType_code'].values),list(pageDict['df_hist']['serialNumber'].values))],
                                          color = "blue"
                                        ),
                                        link = dict(
                                          source = src,
                                          target = trg,
                                          value = val
                                      ))])
            fig.update_layout(title_text=pageDict['compFull']['componentType']['code']+"("+pageDict['compFull']['project']['code']+") lineage", font_size=10)
            st.plotly_chart(fig)
