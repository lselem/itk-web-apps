### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import copy
import csv
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
### common stuff
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

#####################
### useful functions
#####################

infoList= [ "* select inputs",
            " - component serialNumber",
            " - stage to copy test from",
            " - stage to copy test to",
            " - test type(s)",
            " - use local institution",
            " - use retroactive upload functionality",
            " - delete previous tests ",
            "* Collect tests and migrate",
            " - find component test runs for defined test type(s)",
            " - filter test runs for defined stage",
            " - keep latest test run for test type(s)",
            " - upload tests",
            "   - if retroactive mode selected component stage will not change",
            "   - if retroactive mode is _not_ selected stage will be change to defined stage for upload then returned to original stage",
            " - delete uploaded tests option",
            "* Delete previous tests (to do)",
            " - delete test runs which were used for migration",
            " - undelete delted tests option" ]

#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("Test Migration", ":microscope: Migrate Test Runs Across Stages", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        if st.checkbox("Quick Component Check?"):
            if 'SCC' not in pageDict.keys():
                pageDict['SCC']={}
            chnx.SelectComponentChunk(pageDict['SCC'])
            stTrx.DebugOutput("SelectComponentChunk info.",pageDict['SCC'])
            try:
                st.dataframe(pd.json_normalize(pageDict['SCC']['comp'], sep = "_"))
            except TypeError:
                st.write("no component info.")

        st.write("## Settings")
        st.write("We're going to need a few details...")
        ### component
        infra.TextBox(pageDict,'inComp',"Enter component identifier")
        infra.Radio(pageDict,'altID',['serialNumber','alternativeIdentifier'],"Type of identifier")
        if pageDict['inComp']==None or len(pageDict['inComp'])<1:
            st.write("Enter identifier to get component information")
            st.stop()
        if 'comp' not in pageDict.keys() or st.button("Get component") or pageDict['inComp']!=pageDict['comp']['serialNumber']:
            pdk=[k for k in pageDict.keys()]
            for k in pdk:
                if k not in ['comp','inComp','altID','SCC']:
                    pageDict.pop(k)
            compQueryJson={'component':pageDict['inComp'],'type':"SN"}
            if pageDict['altID']=='alternativeIdentifier':
                compQueryJson['type']='altID'
            stTrx.DebugOutput("queryJson:",compQueryJson)
            pageDict['comp']=pdbTrx.GetComponentByIdentifier(compQueryJson['component'],compQueryJson['type'])

        st.write(f"- Got {pageDict['comp']['serialNumber']} ({pageDict['comp']['componentType']['code']}) @ {pageDict['comp']['currentStage']['code']} in {pageDict['comp']['currentLocation']['code']}")
        pageDict['compType']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'code': pageDict['comp']['componentType']['code'], 'project': pageDict['comp']['project']['code'] })

        # st.write([x['code'] for x in pageDict['compType']['stages'] if type(x)==type({}) and "code" in x.keys()])
        ### stage details
        if "originalStage" not in pageDict.keys():
            pageDict['originalStage']=pageDict['comp']['currentStage']['code']
        if "locationCodes" not in pageDict.keys():
            pageDict['locationCodes']=[st.session_state.myClient.get('getInstitution', json={'id':inst['institution']})['code'] for inst in pageDict['comp']['locations']]
        pageDict['stageFrom']=st.selectbox("Move test FROM stage?",[x['code'] for x in pageDict['compType']['stages'] if type(x)==type({}) and "code" in x.keys()])
        pageDict['stageTo']=st.selectbox("Move test TO stage?",[x['code'] for x in pageDict['compType']['stages'] if type(x)==type({}) and "code" in x.keys()])
        ### tests
        pageDict['testTypes']=st.multiselect("Which test types?",[tt['testType']['code'] for st in pageDict['compType']['stages'] for tt in st['testTypes'] if st['code']==pageDict['stageFrom'] ] )
        ### use local?
        if "migrationInstitution" not in pageDict.keys():
            pageDict['migrationInstitution']=pageDict['comp']['currentLocation']['code']
        if st.checkbox("Use local institution?"):
            pageDict['migrationInstitution']=st.session_state.Authenticate['inst']['code']
            st.write(f"- setting migration institution to {pageDict['migrationInstitution']}")
        else:
            pageDict['migrationInstitution']=pageDict['comp']['currentLocation']['code']
        ### retroactive?
        pageDict['isRetroactive']=st.checkbox("Retroactive upload?")
        if pageDict['isRetroactive']:
                if pageDict['migrationInstitution'] in pageDict['locationCodes']:
                    st.write(f"- using {pageDict['migrationInstitution']} is OK")
                else:
                    st.write(f"Migration institution ({pageDict['migrationInstitution']}) not found in component locations ({pageDict['locationCodes']})")
                    st.write(" - __reactroactive uploads not possible__ :(")
                    st.write("Please do not use local institution")
                    st.stop()
        ### retroactive?
        pageDict['deletePrevious']=st.checkbox("Delete Previous tests?")

        st.write("### Settings")
        st.write("* serialNumber",pageDict['comp']['serialNumber'])
        for x in ['stageFrom','stageTo','testTypes','migrationInstitution','isRetroactive','deletePrevious']:
            st.write(f"* {x}: {pageDict[x]}")

        st.write("This script will copy _test data (results & properties), defects and comments_")
        st.write("__NB__ This script will __not__ copy _attachments_")

        if not st.checkbox("Ready to proceed?"):
            st.stop()

        st.write("---")

        ### get tests
        st.write("## Collect Data and Migrate")
        if "testDict" not in pageDict.keys() or st.button("re-check tests"):
            st.write("Searching:")
            testDict={k:[] for k in pageDict['testTypes']}
            for test in pageDict['comp']['tests']:
                if test['code'] in testDict.keys():
                    st.write(f"- found {test['code']}")
                    testDict[test['code']]=[tr for tr in test['testRuns'] if tr['state']=="ready"]
            pageDict['foundDict']=testDict

            ### get test data
            pageDict['filterDict']={}
            for k,v in pageDict['foundDict'].items():
                tests=st.session_state.myClient.get('getTestRunBulk', json={'testRun':[tr['id'] for tr in v]})
                tests=[t for t in tests for c in t['components'] if c['testedAtStage']['code']==pageDict['stageFrom']]
                pageDict['filterDict'][k]=tests

            ### get specific test: here last one
            pageDict['testDict']={}
            for k,v in pageDict['filterDict'].items():
                # st.write(f"- {k} tests: {len(v)}")
                df_tests=pd.DataFrame(v)
                if not df_tests.empty:
                    df_tests['cts']=pd.to_datetime(df_tests['cts'], format="%Y-%m-%dT%H:%M:%S.%fZ")
                    last=df_tests.sort_values(by='cts', ascending=False).reset_index(drop=True).iloc[0].to_dict()
                    # display(last)
                    # st.write(f"  - got last: {last['cts']}")
                    pageDict['testDict'][k]=[last]
                # else:
                #     st.write(f"  - no tests")

        if st.checkbox("See found tests?"):
            st.write("__Found Tests__: component tests for defined test type(s)")
            for k,v in pageDict['foundDict'].items():
                st.write(f"- {k} tests: {len(v)}")
                if st.checkbox("see tests",key="found_"+k):
                    df_tests=pd.DataFrame(v)
                    if not df_tests.empty:
                        df_tests['stateTs']=pd.to_datetime(df_tests['stateTs'], format="%Y-%m-%dT%H:%M:%S.%fZ")
                        st.write(df_tests.sort_values(by='stateTs').reset_index(drop=True))
                    else:
                        st.write(f"  - no tests")

        if st.checkbox("See filtered tests?"):
            st.write("__Filtered Tests__: keeping test runs at defined stage")
            for k,v in pageDict['filterDict'].items():
                st.write(f"- {k} tests: {len(v)}")
                if st.checkbox("see tests",key="filters_"+k):
                    df_tests=pd.DataFrame(v)
                    if not df_tests.empty:
                        df_tests['cts']=pd.to_datetime(df_tests['cts'], format="%Y-%m-%dT%H:%M:%S.%fZ")
                        st.write(df_tests.sort_values(by='cts').reset_index(drop=True))
                    else:
                        st.write(f"  - no tests")

        if st.checkbox("See latest tests?"):
            ### get specific test: here last one
            st.write("__Latest Tests__: use latest test runs to migrate")
            for k,v in pageDict['testDict'].items():
                st.write(f"- {k} tests: {len(v)}")
                if len(v)>0:
                    st.write(f"  - got last: {v[0]['cts']}")
                else:
                    st.write(f"  - no tests")

        ### gather information
        st.write("Gathering test data...")
        updateList=[]
        for k,v in pageDict['testDict'].items():
            st.write(k)
            # get testType schema
            testSchema=st.session_state.myClient.get('generateTestTypeDtoSample', json={'code':k,'componentType':pageDict['comp']['componentType']['code'],'project':pageDict['comp']['project']['code']})
            
            # loop over test data
            for t in v:
                ### set up data dictionary
                updateDict={'test':None,'defects':None,'comments':None}
                # copy schema
                newDict=copy.deepcopy(testSchema)
                newDict['component']=pageDict['comp']['serialNumber']
                newDict['stage']=pageDict['stageTo']
                for x in ['passed','problems','date','runNumber']:
                    newDict[x]=t[x]
                ### update to current location?
                newDict['institution']=pageDict['migrationInstitution']
                ### properties and results
                for pr in ["properties","results"]:
                    # st.write(f"- {pr}:")
                    prDict={p['code']:p['value'] for p in t[pr]}
                    # st.write(" ",prDict)
                    newDict[pr]=prDict
                st.json(newDict, expanded=False)
                updateDict['test']=newDict
                ### comments and defects
                for dc in ["defects","comments"]:
                    st.write(f" - {dc}")
                    dcList=t[dc]
                    # if content...
                    if dcList!=None and dcList!=[]:
                        if "defect" in dc:
                            dcList=[{l,w} for d in dcList for l,w in d.items() if k in ["name","description","properties"]]
                        if "comment" in dc:
                            dcList=[d['comment'] for d in dcList]
                    st.write(" ",dcList)
                    updateDict[dc]={'testRun':None, dc:dcList}
                updateList.append(updateDict)
        pageDict['updateList']=updateList
    
        ### check list for updates
        if "updateList" in pageDict.keys():
            st.write("__Data to Migrate__")
            st.write(pd.DataFrame(pageDict['updateList']))


        ### upload data!
        if st.button("Upload Tests"):
        
            # change component stage if required!
            if pageDict['isRetroactive']==False:
                st.write(f"Changing component stage ({pageDict['stageTo']})")
                try:
                    retVal=st.session_state.myClient.post('setComponentStage', json={'component':pageDict['comp']['serialNumber'], 'stage':pageDict['stageTo']})
                    st.write(" - Move complete")
                except itkX.BadRequest as b:
                    st.write(" - :no_entry_sign: **Unsuccessful** stage setting")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            else:
                st.write("No stage moves for retroactive upload")

            ### loop over data to upload
            pageDict['retValList']=[]
            for up in pageDict['updateList']:
                # st.write(up)
                st.write(f"update: {up['test']['testType']} for {up['test']['component']}")
                up['retVals']=[]

                # update retroactive flag 
                if "isRetroactive" not in up['test'].keys():
                    st.write(f" - add retroactive flag ({pageDict['isRetroactive']})")
                    up['test']['isRetroactive']=pageDict['isRetroactive']

                # do upload 
                uploadCheck=False
                try:
                    retVal=st.session_state.myClient.post('uploadTestRunResults', json=up['test'])
                    st.write(f" - uploaded: {retVal['testRun']['id']}")
                    # note upload
                    pageDict['retValList'].append(retVal)
                    uploadCheck=True
                except itkX.BadRequest as b:
                    st.write(f" - **Unsuccessful** uploadTestRunResults for {up['test']['component']}")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                    st.write(b)
                    continue

                ### update defects if test upload successful
                if uploadCheck==True:
                    st.write(" - update defects")
                    if uploadCheck==True and up['defects']['defects']==None or up['defects']['defects']==[]:
                        st.write("   - no defects to update")
                    else:
                        try:
                            up['defects'].update({'testRun':retVal['testRun']['id']})
                            retVal=st.session_state.myClient.post('createTestRunDefect', json=up['defects'] )
                            st.write(f"   - uploaded defects to: {retVal['testRun']['id']}")
                            # note upload
                            # pageDict['retValList'].append(retVal)
                        except itkX.BadRequest as b:
                            st.write(f"   - **Unsuccessful** defects upload for {up['test']['component']}")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

                ### update comments if test upload successful
                if uploadCheck==True:
                    st.write(" - update comments")
                    if up['comments']['comments']==None or up['comments']['comments']==[]:
                        st.write("   - no comments to update")
                    else:
                        try:
                            up['comments'].update({'testRun':retVal['testRun']['id']})
                            retVal=st.session_state.myClient.post('createTestRunComment', json=up['comments'] )
                            st.write(f"   - uploaded comments to: {retVal['testRun']['id']}")
                            # note upload
                            # pageDict['retValList'].append(retVal)
                        except itkX.BadRequest as b:
                            st.write(f"   - **Unsuccessful** comments upload for {up['test']['component']}")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                
                else:
                    st.write("No stage moves for retroactive upload")

            # change component stage back required!
            if pageDict['isRetroactive']==False:
                st.write(f"Changing component stage back ({pageDict['originalStage']})")
                try:
                    retVal=st.session_state.myClient.post('setComponentStage', json={'component':pageDict['comp']['serialNumber'], 'stage':pageDict['originalStage']})
                    st.write(" - Move complete")
                except itkX.BadRequest as b:
                    st.write(" - :no_entry_sign: **Unsuccessful** stage setting")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

        if "retValList" in pageDict.keys():
            st.write("Uploaded data...")
            st.write(pd.DataFrame(pageDict['retValList']))
            if len(pageDict['retValList'])>0 and st.checkbox("Delete uploaded tests?"):
                st.write("Deleting uploaded tests...")
                ### loop over data
                for up in pageDict['retValList']:
                    # st.write(up)
                    if "testRun" in up.keys():
                    
                        st.write(f"deleting: {up['testRun']['id']}")
                    
                        # do delete 
                        try:
                            retVal=st.session_state.myClient.post('deleteTestRun', json={'testRun':up['testRun']['id'], "reason":"webApp debugging"})
                            st.write(f" - deleted: {up['testRun']['id']}")
                        except itkX.BadRequest as b:
                            st.write(f" - **Unsuccessful** deleteTestRun for {up['testRun']['id']}")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                            continue


        st.write("---")

        ### delete previous stuff
        if pageDict['deletePrevious']:
            st.write(f"## Delete Tests At Previous Stage ({pageDict['stageFrom']})")


            st.write("🚧 under construction 🚧")
            st.stop()
            
            ### check option selected
            st.button("Deleting previous tests?")

            ### loop over data to upload
            pageDict['delValList']=[]
            ### loop over data
            for k,v in pageDict['testDict'].items():
                st.write(k)
                # loop over test data
                for t in v:
                    
                    # do upload 
                    try:
                        retVal=st.session_state.myClient.post('deleteTestRun', json={'testRun':t['id'], "reason":"migrated data to correct stage"})
                        st.write(f" - deleted: {retVal['testRun']['id']}")
                        # note upload
                        pageDict['delValList'].append(retVal)
                    except itkX.BadRequest as b:
                        st.write(f" - **Unsuccessful** deleteTestRun for {t['id']}")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                        continue
            
            ### loop over data
            if "delValList" in pageDict.keys():
                st.write("Deleted data...")
                st.write(pd.DataFrame(pageDict['delValList']))
                if len(pageDict['delValList'])>0 and st.checkbox("Deleted previous tests?"):
                
                    for dv in pageDict['delValList']:
                        # undo delete 
                        try:
                            retVal=st.session_state.myClient.post('undoDeleteTestRun', json={'testRun':dv['testRun']['id'], "reason":"debugging"})
                            print(f" - undeleted: {retVal['testRun']['id']}")
                            
                        except itkX.BadRequest as b:
                            print(f" - **Unsuccessful** undoDeleteTestRun for {dv['testRun']['id']}")
                            print(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
                            continue
