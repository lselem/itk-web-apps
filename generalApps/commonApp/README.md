# Common webApp

Common institute level PDB interactions.

---

## Pages

### Inventory

Components available at institution

- List of component types at institution

  - select componentType(s) of interest
  
- Group Component Statistics: component populations per selected componentType and (sub-)type

  - State: ready, reworked or trashed
  - Current Stage: using currentStage code
  - Vintage: split by (cts) year

- Individiual Component Information

  - select component by serial number

    - view component data

      - option to delete component (__BEWARE!__)

    - view component stage history
    - view component test checklist

  Further individual information available in _compCheck_ and _stageCheck_ pages.

### Shipping
  - View existing shipments and create new shipments

### compCheck
  - standard information on component

    - project, componentType, origin, location, barcode
    - generic relations and workflow
    - specific relations

### stageCheck
  - stage information on component

    - project, componentType, origin, location, barcode
    - stage history
    - test history

### testMigration
  - migrate test data between stage

    - select inputs

      - component serialNumber
      - stage to copy test from
      - stage to copy test to
      - test type(s)
      - use local institution
      - use retroactive upload functionality
      - delete previous tests 

    - Collect tests and migrate

      - find component test runs for defined test type(s)
      - filter test runs for defined stage
      - keep latest test run for test type(s)
      - upload tests

        - if retroactive mode selected component stage will not change
        - if retroactive mode is _not_ selected stage will be change to defined stage for upload then returned to original stage

      - delete uploaded tests option

    - Delete previous tests (to do)

      - delete test runs which were used for migration
      - undelete deleted tests option
