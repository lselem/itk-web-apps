# Original webApp

Legacy app.

---

## Pages

### Registration
  * Register single/multiple component in PDB

### Shipping
  * Upload list and ship Components
  * Check status of shipment

### Upload Test Runs
  * single/multiple testRuns in PDB

### Quick Check
  * check component info. in PDB

### Plot
  * some _basic_ plotting
