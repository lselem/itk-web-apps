### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
import numpy as np
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

def CalcStats(arr,name=""):
    myarr=[float(x) for x in arr]
    st.write("*"+str(name)+"* stats...")
    st.write("  * ave+/-std:",np.mean(myarr),"+/-",np.std(myarr))
    st.write("  * max,min:",np.max(myarr),",",np.min(myarr))


infoList=["  * select *institution*",
        "  * select *project*",
        "  * get list of *componentTypes*",
        "  * select *componentType*",
        "  * select *component*",
        "  * get list of *testTypes*",
        "  * select *testType*",
        "  * get list of *testRuns* of *testType*",
        "  * select *testRuns*",
        "  * display results (plot for arrays)",
        "  * select variable for plotting if required"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("Plots", ":chart_with_upwards_trend: Test Plots", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # get list of componentTypes
        st.write("### List of *ComponentTypes* for",st.session_state.Authenticate['inst']['code'],"("+st.session_state.Authenticate['proj']['code']+")")
        # Check available component names and codes
        if st.button("reset list") or "compTypeList" not in list(pageDict.keys()): # check list
            st.write("Get list")
            pageDict['compTypeList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
        else:
            st.write("Got compTypeList")
        if len(pageDict['compTypeList'])<1:
            st.write("No data found. Check *Institution* and *Project* settings")
            st.stop()
        st.write("compTypeList size: "+str(len(pageDict['compTypeList'])))
        df_compType=pd.json_normalize(pageDict['compTypeList'], sep = "_")
        df_compType=df_compType[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compType)

        #st.write(df_compType.columns)
        #st.write(df_compType.query('componentType_name=="Module"'))
        st.write("### Select component")
        infra.SelectBoxDf(pageDict,'compType',df_compType,'Select a componentType', 'componentType_code')
        if st.session_state.debug:
            st.write("**DEBUG** *componentType* id")
            st.write(pageDict['compType'])

        # select component
        compMap = [{"id":w['id'], "code":w['code'], "ASN":w['serialNumber'], "altID":w['alternativeIdentifier'], "state":w['state']} for w in pageDict['compTypeList'] if w['componentType']['code']==pageDict['compType']['componentType_code'].values[0] and w['state']=="ready"]
        if st.session_state.debug:
            st.write("**DEBUG** *component* list")
            st.dataframe(pd.DataFrame(compMap))
        infra.SelectBox(pageDict,'comp',compMap,'Select a component','ASN')

        if st.session_state.debug:
            st.write("**DEBUG** *component* info.")
            st.write(pageDict['comp'])

        # select testType
        thisComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pageDict['comp']['code']})
        if st.session_state.debug:
            st.write("**DEBUG** *tests for Component*...")
            st.write(thisComp['tests'])
        df_testType=pd.DataFrame(thisComp['tests'], columns=["name","code"])
        df_testType=df_testType.drop_duplicates()
        if df_testType.empty:
            st.write("No testRuns to plot")
            st.stop()
        else:
            st.dataframe(df_testType)

        # select test type
        st.write("### Select *TestType*")
        thisTestCode=st.selectbox('Select a testType', thisComp['tests'], format_func=lambda x: x['name'])
        runList=[y['id'] for x in thisComp['tests'] for y in x['testRuns'] if x['code']==thisTestCode['code']]

        #get test runs of test type
        myTest=DBaccess.DbGet(st.session_state.myClient,'getTestRun', {'testRun':runList[0]})
        if st.session_state.debug:
            st.write("**DEBUG** *results for Test*...")
            st.write(myTest['results'])
        df_results=pd.DataFrame(myTest['results'], columns=["name","order","dataType","valueType","value"])
        if st.session_state.debug:
            st.write("**DEBUG** *results dataframe*...")
            st.dataframe(df_results)

        #st.write(list(df_results['name'].values))
        # display results (plot for arrays)
        for tn in list(df_results['name'].values):
            if "NoneType" in str(type(tn)):
                st.write("Skipping",tn)
                continue
            df_subPlot=df_results.query("name=='"+tn+"'")

            #st.write("this:",df_subPlot['valueType'].iloc[0])
            if df_subPlot['valueType'].iloc[0]!="array":
                #st.write("this is not an array")
                st.write("Results for:*",tn,"*")
                st.dataframe(df_subPlot)
                continue

            st.write("Plotting for",tn,"...")
            plotData=df_subPlot['value'].iloc[0]
            if st.session_state.debug:
                st.write("**DEBUG** *plot data*...")
                st.write(plotData)

            # try dictionary
            try:
                # select axes variables
                valList= list(plotData.keys())
                #st.write(valList)
                axisX=st.selectbox("Select X-axis variable",valList)
                valListY=[v for v in valList if axisX not in v]
                axisYs=st.multiselect("Select (multiple) Y-axis variable(s)",valListY)
                if st.session_state.debug: st.write(axisX,"vs...",axisYs)

                combFigs=go.Figure(layout_title_text=str("X="+axisX+" vs..."))
                combFigs.update_layout(legend=dict(yanchor="bottom",y=1.02,xanchor="left",x=0,orientation="h"))
                combFigs.update_xaxes(title=axisX)
                try:
                    CalcStats(plotData[axisX],axisX)
                except TypeError:
                    pass
                #yDictList=[{'col':"#1f77b4",'pos':0},{'col':"#d62728",'pos':0.15},{'col':"#73ca98"},{'col':"#9467bd"}]
                for c,axy in enumerate(axisYs,1):
                    try: #
                        CalcStats(plotData[axy],axy)
                    except TypeError:
                        pass
                    subFig=go.Scatter(
                            x=plotData[axisX],
                            y=plotData[axy],
                            mode='lines+markers',
                            name=axy,
                            yaxis="y"+str(c),
                            #text=row['altID'],
                            #name=row['altID'],)#+'('+row['altID']+')')
                            )
                    combFigs.add_trace(subFig)
                # Create axis objects
                combFigs.update_layout(
                    xaxis=dict(
                        domain=[0.1, 0.9]
                    ))
                # yAxis 1
                try:
                    combFigs.update_layout(
                        yaxis=dict(
                            title=axisYs[0],
                            titlefont=dict(
                                color="#1f77b4"
                            ),
                            tickfont=dict(
                                color="#1f77b4"
                            )
                        ))
                except:
                    pass
                # yAxis 2
                try:
                    combFigs.update_layout(
                        yaxis2=dict(
                            title=axisYs[1],
                            titlefont=dict(
                                color="#d62728"
                            ),
                            tickfont=dict(
                                color="#d62728"
                            ),
                            anchor="free",
                            overlaying="y",
                            side="left",
                            position=0.0
                        ))
                except:
                    pass
                # yAxis 3
                try:
                    combFigs.update_layout(
                        yaxis3=dict(
                        title=axisYs[2],
                        titlefont=dict(
                            color="#73ca98"
                        ),
                        tickfont=dict(
                            color="#73ca98"
                        ),
                        anchor="x",
                        overlaying="y",
                        side="right"
                    ))
                except:
                    pass
                # yAxis 4
                try:
                    combFigs.update_layout(
                        yaxis4=dict(
                            title=axisYs[3],
                            titlefont=dict(
                                color="#9467bd"
                            ),
                            tickfont=dict(
                                color="#9467bd"
                            ),
                            anchor="free",
                            overlaying="y",
                            side="right",
                            position=1.0
                        ))
                except:
                    pass

                # Update layout properties
                combFigs.update_layout(
                    width=800,
                )
                st.plotly_chart(combFigs)

            # if array is list not dictionary
            except AttributeError:
                try:
                    CalcStats(plotData,tn)
                except TypeError:
                    pass
                soloFig=go.Figure(layout_title_text=str(tn+"Plot"))
                soloFig.update_layout(legend=dict(yanchor="bottom",y=1.02,xanchor="right",x=1,orientation="h"))
                soloFig.update_xaxes(title=tn)
                subFig=go.Scatter(
                        x=list(range(0,len(plotData),1)),
                        y=plotData,
                        mode='lines+markers',
                        name=tn,
                        #text=row['altID'],
                        #name=row['altID'],)#+'('+row['altID']+')')
                        )
                soloFig.add_trace(subFig)
                st.plotly_chart(soloFig)
