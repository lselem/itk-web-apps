### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX
import os
import sys
cwd=os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, cwd)
import batchScript as btch

#####################
### useful functions
#####################

def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
    # try: # avoid bool trouble
    #     if val.isnumeric(): # for integers formatted as strings by schema
    #         val=int(val)
    # except AttributeError:
    #     pass
    return val

infoList=["  * select *institution*",
        "  * select *project*",
        "  * get list of *componentTypes*"
        "  * select *componentType*",
        "  * get *componentType* schema",
        "  * update *componentType* schema",
        "  * review and upload",
        "  * get *registerInfo* (if upload successful)"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Registration", ":clipboard: Component Registration", infoList)

    def main(self):
        super().main()

        #btch.SimpleTest()
        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # get list of componentTypes
        st.write("### List of *ComponentTypes* for",st.session_state.Authenticate['inst']['code'],"("+st.session_state.Authenticate['proj']['code']+")")
        # Check available component names and codes
        if st.button("reset list") or "compTypeList" not in list(pageDict.keys()): # check list
            st.write("Get list")
            pageDict['compTypeList']=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'institution':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
        else:
            st.write("Got compTypeList")
        if len(pageDict['compTypeList'])<1:
            st.write("No data found. Check *Institution* and *Project* settings")
            st.stop()
        st.write("compTypeList size: "+str(len(pageDict['compTypeList'])))
        df_compType=pd.json_normalize(pageDict['compTypeList'], sep = "_")
        df_compType=df_compType[['componentType_name','componentType_code']].drop_duplicates()
        st.dataframe(df_compType)

        #st.write(df_compType.columns)
        #st.write(df_compType.query('componentType_name=="Module"'))
        infra.SelectBoxDf(pageDict,'compType',df_compType,'Select a componentType', 'componentType_code')
        if st.session_state.debug:
            st.write("**DEBUG** *componentType* id")
            st.write(pageDict['compType'])

        pageDict['compTypeObj']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':st.session_state.Authenticate['proj']['code'], 'code':pageDict['compType']['componentType_code'].values[0]})
        mySubProjs=[x['code'] for x in pageDict['compTypeObj']['subprojects']]
        myCompTypes=[x['code'] for x in pageDict['compTypeObj']['types']]

        ### Select how many registrations
        st.write("### Select *how many* registrations")
        infra.Radio(pageDict,'multOpt',["single", "multiple"],"How many registrations?:")

        ### SINGLE registration
        if pageDict['multOpt']=="single":
            st.write("**SINGLE** registration")
            # get componentType schema
            st.write("### Component Schema")
            infra.Radio(pageDict,'reqSet',["required only", "all"],"Schema setting:")
            reqSet={'required only':True,'all':False}[pageDict['reqSet']]

            compSchema = DBaccess.DbGet(st.session_state.myClient,'generateComponentTypeDtoSample', {'project':st.session_state.Authenticate['proj']['code'], 'code': pageDict['compType']['componentType_code'].values[0], 'requiredOnly' : reqSet})
            # if st.session_state.debug:
            #     st.write("**DEBUG** Original *schema*")
            st.write("Original schema:")
            st.write(compSchema)

            # get set up schema for selected component, test, institution
            try:
                if st.session_state.debug:
                    st.write("**DEBUG** *componentSchema* keys...")
                    try:
                        st.write("new compSchema type:",pageDict['compSchema']['componentType'])
                    except:
                        pass
                    st.write("original compSchema type:",compSchema['componentType'])
                if set(pageDict['compSchema']['componentType'])!=set(compSchema['componentType']):
                    pageDict['compSchema']=compSchema.copy()
                    pageDict['compSchema']['institution']=st.session_state.Authenticate['inst']['code']
            except:
                pageDict['compSchema']=compSchema.copy()
                pageDict['compSchema']['institution']=st.session_state.Authenticate['inst']['code']

            # reset test schema if required
            if st.button("reset schema"):
                pageDict['compSchema']=compSchema.copy()
                #state.compSchema['component']=state.myComp['code']
                pageDict['compSchema']['institution']=st.session_state.Authenticate['inst']['code']

            # list widgets to update
            for k,v in pageDict['compSchema'].items():
                #st.write(k,":",type(v))
                if type(v)==type({}):
                    for l,w in pageDict['compSchema'][k].items():
                        pageDict['compSchema'][k][l]=SelectCheck(l,w)
                else:
                    if k=="subproject":
                        try:
                            pageDict['compSchema'][k]=st.selectbox(k, list(mySubProjs), index=list(mySubProjs).index(pageDict['compSchema'][k]))
                        except:
                            pageDict['compSchema'][k]=st.selectbox(k, list(mySubProjs) )
                        continue
                    elif k=="type":
                        try:
                            pageDict['compSchema'][k]=st.selectbox(k, list(myCompTypes), index=list(myCompTypes).index(pageDict['compSchema'][k]))
                        except:
                            pageDict['compSchema'][k]=st.selectbox(k, list(myCompTypes) )
                        continue
                    else:
                        pageDict['compSchema'][k]=SelectCheck(k,v)

            # final review of test schema
            st.write("### Component Schema")
            ### convert integers
            infra.ToggleButton(pageDict,'convert',"Convert integers")
            if st.session_state.debug:
                st.write("**DEBUG** check convert")
                st.write(pageDict['convert'])
            if pageDict['convert']:
                try:
                    for k,v in pageDict['compSchema']['properties'].items():
                        try:
                            pageDict['compSchema']['properties'][k]=int(v)
                        except ValueError:
                            pass
                except KeyError:
                    st.write("no properties to check")
            ### review final schema
            st.write(pageDict['compSchema'])

            # upload(!) component schema: change DBaccess.DbGet(get) --> DBaccess.DbPost(post)
            if st.button("Register Component"):
                #st.write(pageDict['compSchema'])
                pageDict['retInfo']=DBaccess.DbPost(st.session_state.myClient,'registerComponent', pageDict['compSchema'])
                try:
                    if pageDict['retInfo']!=None:
                        st.write("### :white_check_mark: Successful register:",pageDict['retInfo']['component']['sys']['cts'])
                    else:
                        st.write("### :no_entry_sign: register unsuccessful")
                except KeyError:
                    pass

            if st.session_state.debug:
                st.write("**DEBUG** Full register return")
                try:
                    st.write("### Full upload return from ("+pageDict['retInfo']['component']['sys']['cts']+")")
                except KeyError:
                    pass

            # see test data
            st.write("### Check uploaded component database entry")
            try:
                st.write("### Component uploaded from ("+pageDict['retInfo']['component']['sys']['cts']+")")
                compInfo=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component':pageDict['retInfo']['component']['code']})
                st.write(compInfo)
            except KeyError:
                st.write("No upload data found in state")
            except TypeError:
                st.write("No upload data found in database")

        ### MULTIPLE registrations
        if pageDict['multOpt']=="multiple":
            st.write("**MULTIPLE** registrations")

            # select subProject
            st.write("### Select *subProject*")
            if st.session_state.debug:
                st.write("**DEBUG** *subProject* list (name:code)")
                st.write([{x['name']:x['code']} for x in pageDict['proj']['subprojects']])
            infra.SelectBox(pageDict,'subProj',pageDict['compTypeObj']['subprojects'],'Select a sub-project','code')

            ## get data file
            st.write("### Upload *Registration data*")
            st.write("Follow data format:")
            df_example=pd.DataFrame([{'serialNumber':'20Uxxyynnnnnnn','componentType':'COMP_CODE','type':'TYPE_CODE','PROP_NAMEX':'value1','PROP_NAMEY':'value2'}])
            st.write(df_example.style.hide_index())
            pageDict['file']= st.file_uploader("Upload a file", type=["csv"])
            if st.session_state.debug: st.write(pageDict['file'])

            #dataSel = GetData(join(state.selectDir, state.selectFile))
            if pageDict['file'] is not None:
                pageDict['file'].seek(0)
                pageDict['df_data']=pd.read_csv(pageDict['file'])
                st.dataframe(pageDict['df_data'])
                st.write("length of dataframe:",len(pageDict['df_data']))
            else:
                st.write("No data file set")
                st.stop()


            ### convert integers
            infra.ToggleButton(pageDict,'convert',"Convert integers to strings")
            if st.session_state.debug:
                st.write("**DEBUG** check convert")
                st.write(pageDict['convert'])
            if pageDict['convert']:
                for c in list(pageDict['df_data'].columns):
                    if "int" in str(pageDict['df_data'][c].dtypes):
                        st.write("converted:\"",c,"\". uncheck to undo!")
                        pageDict['df_data'][c]= pageDict['df_data'][c].astype(str)


            # run batch upload
            if st.button("Run PDB update"):
                btch.MultiRegistration(pageDict, st.session_state.Authenticate, st.session_state.myClient, st.session_state.debug)
