import streamlit as st
import pandas as pd
### PDB stuff
import os
import sys
from datetime import datetime
### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### Fudges
#####################
def SimpleTest():
    st.write("Got it!")

### checking schema - cast strings to other objects where necessary
def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
    try: # avoid bool trouble
        if val.isnumeric(): # for integers formatted as strings by schema
            val=int(val)
    except AttributeError:
        pass
    return val

#####################
### Batch uploading
#####################

# def GetTestSchema(client, projCode, compTypeCode, testCode):
#     # get testType schema
#     # st.write("GetTestSchema stuff:",projCode, compTypeCode, testCode)
#     testObj=dba.DbGet(client,'generateTestTypeDtoSample', {'project':projCode, 'componentType':compTypeCode, 'code':testCode})
#         return testObj
#     except itkX.BadRequest as b: # catch double registrations
#         st.write("GetTestSchema: something went wrong")
#         st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8])
#     except:
#         st.write("GetTestSchema: something went wrong. Unknown")
#     return None
def MultiRegistration(pageDict, authDict, client, debug, regReq=False):
    df_input=pageDict['df_data']
    # renew token
    client=AuthenticateUser(authDict['ac1'],authDict['ac2'])
    df=df_input.copy()
    snName=[x for x in list(df.columns) if "serialnumber" in x.lower()][0]
    compCodeName=[x for x in list(df.columns) if "componenttype" in x.lower()][0]
    typeCodeName=[x for x in list(df.columns) if "type" in x.lower() and "component" not in x.lower()][0]
    # loop through rows
    prog_bar=st.progress(0.0)
    prog_text = st.empty()
    count=1
    lenRows=len(df.index)
    for index, row in df.iterrows():
        ### check SN in row
        try:
            st.write("using component SN: "+row[snName])
        except KeyError:
            st.write("could not find SN (cf."+snName+")")
            continue
        try:
            st.write("using component compCode: "+row[compCodeName])
        except KeyError:
            st.write("could not find compCode (cf."+compCodeName+")")
            continue
        try:
            st.write("using component typeCode: "+row[typeCodeName])
        except KeyError:
            st.write("could not find typeCode (cf."+typeCodeName+")")
            continue
        ### check component already registered (avoid errors in reregistration)
        try:
            comp=dba.DbGet(client,'getComponent', {"component":row[snName]})
            compCode=comp['code']
            st.write("**found** "+comp['serialNumber']+" in PDB")
        except:
            st.write("no existing component found!")
            ### register if requested
            regObj=dba.DbGet(client,'generateComponentTypeDtoSample', {'project':authDict['proj']['code'], 'code':row[compCodeName], 'requiredOnly':True})
            if regObj!=None:
                #st.write("registering...")
                regObj['serialNumber']=row[snName]
                regObj['subproject']=pageDict['subProj']['code']
                regObj['institution']=authDict['inst']['code']
                regObj['type']=row[typeCodeName]

                for c in list(df.columns):
                    if c in [snName,compCodeName,typeCodeName]: continue
                    regObj['properties'][c]=row[c]
                #GetPCBObj(row[snName])
                retVal=dba.DbPost(client,'registerComponent',regObj)
                if retVal!=None:
                    compCode=retVal['component']['code']
                    st.write("**registered** "+retVal['component']['serialNumber']+"!")
                else:
                    st.write("registration failed")

        prog_bar.progress(1.0*count/lenRows)
        prog_text.text("loop "+str(count)+" of "+str(lenRows))
        count+=1


def MultiTestUpload(pageDict, authDict, client, debug, regReq=False):
    # quick variables. Clean up later(!)
    df_input=pageDict['df_data']
    regObj=pageDict['compSchema']
    # renew token
    client=AuthenticateUser(authDict['ac1'],authDict['ac2'])
    # useful definitions
    projCode, instCode, compTypeCode, compStage = authDict['proj']['code'],authDict['inst']['code'],pageDict['compType']['componentType_code'].values[0],pageDict['stage']['code']
    if debug:
        st.write("MultiTestUpload stuff:",projCode, instCode, compTypeCode, compStage)
    # copy dataframe
    df=df_input.copy()
    # loop through rows
    prog_bar=st.progress(0.0)
    prog_text = st.empty()
    count=1
    lenRows=len(df.index)
    for index, row in df.iterrows():
        compCode="NYS"
        ### check SN in row
        snName=[x for x in list(df.columns) if "serialnumber" in x.lower()][0]
        dateName=[x for x in list(df.columns) if "date" in x.lower()][0]
        try:
            st.write("using component SN: "+row[snName])
        except KeyError:
            st.write("could not find SN (cf."+snName+")")
            continue
        ### check component already registered (avoid errors in reregistration)
        try:
            comp=dba.DbGet(client,'getComponent', {"component":row[snName]})
            compCode=comp['code']
            st.write("**found** "+comp['serialNumber']+" in PDB")
        except:
            st.write("no component found!")
            ### register if requested
            if regObj!=None:
                #st.write("registering...")
                regObj['serialNumber']=row[snName]
                #GetPCBObj(row[snName])
                retVal=dba.DbPost(client,'registerComponent',regObj)
                if retVal!=None:
                    compCode=retVal['component']['code']
                    st.write("**registered** "+retVal['component']['serialNumber']+"!")
                else:
                    st.write("registration failed")


        if compCode!="NYS":
            ### SET STAGE
            retVal=dba.DbPost(client,'setComponentStage', {'component':compCode, 'stage':compStage})
            if debug:
                st.write("Current stage:",retVal['component']['currentStage'])

            # make testdictionary
            testDict={}
            for c in list(df.columns):
                mySpl=c.split(':')
                if len(mySpl)<2: continue
                if mySpl[0] not in list(testDict.keys()):
                    testDict[mySpl[0]]=[mySpl[1]]
                else:
                    testDict[mySpl[0]].append(mySpl[1])
            #st.write(testDict)
            if len(testDict.keys())<1:
                if debug:
                    st.write("MultiTestUpload: no test data recognised. Check column names!")

            for k,v in testDict.items():
                testObj={}
                #st.write(projCode, compTypeCode, k)
                testObj=dba.DbGet(client,'generateTestTypeDtoSample', {'project':projCode, 'componentType':compTypeCode, 'code':k})
                if testObj==None:
                    st.write("testObj failed")
                    continue
                #st.write(testObj)
                testObj['component']=compCode
                testObj['institution']=instCode
                try:
                    testObj['date']=datetime.strptime(row[dateName]+" 21:00", '%d/%m/%Y %H:%M').strftime("%d.%m.%Y %H:%M")
                except:
                    pass
                for vv in v:
                    try: # assume test result
                        testObj['results'][vv]=row[k+':'+vv]
                    except KeyError: # try test property
                        testObj['properties'][vv]=row[k+':'+vv]
                    except: # leave it
                        pass
                #st.write("Updating: ",[row[k+':'+vv] for vv in v])
                if debug:
                    st.write("Updating:",", ".join([str(k+':'+vv) for vv in v]))

                retVal=dba.DbPost(client,'uploadTestRunResults',testObj)
                if retVal!=None:
                    st.write("**updated**",k,"!")
                else:
                    st.write("**update failed**",k,"!")
        else:
            st.write("Do not have component code.")
        prog_bar.progress(1.0*count/lenRows)
        prog_text.text("loop "+str(count)+" of "+str(lenRows))
        count+=1
    st.write("Upload complete!")
