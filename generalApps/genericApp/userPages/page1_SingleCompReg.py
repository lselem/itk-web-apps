### standard
import copy

### PDB stuff
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
### custom
import pandas as pd
import streamlit as st
from core.Page import Page

import commonCode.codeChunks as chnx
import commonCode.StreamlitTricks as stTrx

### analyses

#####################
### useful functions
#####################

infoList=[" * select component type",
        " * select component sub-type",
        " * select component sub-project",
        " * get component schema from PDB",
        " * fill required fields",
        " * upload to PDB",
        " * delete component from PDB if required"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Single Component", ":microscope: Upload Single component to PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['project']=st.session_state.Authenticate['proj']['code']

        # review componentTypes and stages (from file)
        filePath="/code/commonCode/"
        stTrx.DebugOutput("filePath:",filePath)
        csvFileName="stageTestList_"+pageDict['project']+".csv"
        stTrx.DebugOutput("looking in: "+filePath[:filePath.rfind('/')])
        df_all=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName)
        infra.SelectBox(pageDict,'componentType',df_all['compType'].unique(),"Select componentType code:")

        if "componentType" not in pageDict.keys():
            st.write("No componentType selected.")

        # selection of subprojects and types
        pageDict['compTypeObj']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':pageDict['project'], 'code':pageDict['componentType']})
        
        try:
            if "subprojects" in pageDict['compTypeObj'].keys():
                subProjs=[x['code'] for x in pageDict['compTypeObj']['subprojects']]
                infra.SelectBox(pageDict,'subProj',subProjs,"Select component subType (for "+pageDict['componentType']+")")
        except TypeError:
            st.write(f"Subprojects not found for {pageDict['compTypeObj']['code']}")
            pageDict['subProj']==None
        
        try:
            if "types" in pageDict['compTypeObj'].keys():
                compTypes=[x['code'] for x in pageDict['compTypeObj']['types']]
            infra.SelectBox(pageDict,'subType',compTypes,"Select component type")
        except TypeError:
            st.write(f"Component types not found for {pageDict['compTypeObj']['code']}")
            pageDict['subType']==None


        # set upload schema
        if "origSchema" not in pageDict.keys() or pageDict['compSchema']['componentType']!=pageDict['componentType'] or st.button("Reset Schema: "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateComponentTypeDtoSample', {'project':pageDict['project'], 'code':pageDict['componentType'], 'requiredOnly' :True})
            pageDict['fullSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateComponentTypeDtoSample', {'project':pageDict['project'], 'code':pageDict['componentType'], 'requiredOnly' :False})
            pageDict['compSchema']=copy.deepcopy(pageDict['origSchema'])
        pageDict['compSchema']['institution']=st.session_state.Authenticate['inst']['code']
        if pageDict['subProj']!=None:
            pageDict['compSchema']['subproject']=pageDict['subProj']
        if pageDict['subType']!=None:
            pageDict['compSchema']['type']=pageDict['subType']
        stTrx.DebugOutput("Original *schema*:",pageDict['origSchema'])

        ### add additional properties
        if "properties" in pageDict['compSchema'].keys():
            if st.checkbox("add optional property?"):
                st.write("__optional: properties__")
                optList=list(set(pageDict['fullSchema']['properties']) - set(pageDict['origSchema']['properties']))
                optSels=st.multiselect("add optional parameter:",optList)
                for os in optSels:
                    if os not in pageDict['compSchema']['properties'].keys():
                        pageDict['compSchema']['properties'][os]=None

        ### Upload JSON
        exampleFileName = pageDict["componentType"] + "_example.json"
        df_input = chnx.UploadJSONChunk(pageDict, exampleName=exampleFileName, exampleDict=pageDict['compSchema'])
        if df_input:
            pageDict['compSchema'] = df_input
        
        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict, 'compSchema')

        # upload(!)
        chnx.RegChunk(pageDict, 'Component', 'compSchema')
