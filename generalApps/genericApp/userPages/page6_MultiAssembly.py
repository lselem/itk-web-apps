### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
import io
import xlsxwriter
import xlrd
import openpyxl
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

infoList=[" * check test type",
        " * upload _xlsx_ or xls formatted file",
        " * review retrieved data & visualisation",
        " * (optional) add pass/fail - default pass",
        " * upload test schemas to PDB",
        " * delete tests from PDB if required"]
#####################
### main part
#####################

class Page6(Page):
    def __init__(self):
        super().__init__("Multi Assemble", ":microscope: Assemble Multiple components in PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['project']=st.session_state.Authenticate['proj']['code']

        # review componentTypes and stages from file
        filePath=os.path.realpath(__file__)
        exampleFileName="QCAssembleExample.csv"
        df_input=chnx.UploadFileChunk(pageDict,['csv','xls'],filePath[:filePath.rfind('/')]+"/"+exampleFileName,"Sheet1")

        ### read input data
        st.dataframe(df_input)

        ### input visualisations
        st.write("### Assembly data visualisation")
        visChart=alt.Chart(df_input).mark_bar().encode(
                x=alt.X('parCompType:N'),
                y=alt.Y('count(parCompType):Q'),
                color='testType:N',
                tooltip=['parCompType:N','count(parCompType):Q']
        ).configure_axisX(labelAngle=-45).properties(
            width=250,
        ).interactive()
        st.altair_chart(visChart, use_container_width=True)

        ### read input data
        if "regList" not in pageDict.keys() or st.button("re-read data"):
            pageDict['regList']=[]
            for index,row in df_input.iterrows():
                pageDict['regList'].append({'comb':{'SN':row['parentSN'],'compType':row['parCompType']}, 'parts':[] } )
                ### 100 loop, should be enough (?)
                for n in range(1,100,1):
                    try:
                        ### check and skip any nulls
                        if row['childSN'+str(n)]!=row['childSN'+str(n)]:
                            continue
                        pageDict['regList'][-1]['parts'].append({'SN':row['childSN'+str(n)],'compType':row['childCompType'+str(n)]})
                        # try to add slot if exists
                        try:
                            pageDict['regList'][-1]['parts'][-1]['slot']=row['slot'+str(n)]
                        except KeyError:
                            pass
                    except KeyError:
                        break

        st.write("### Compiled assembly list")
        st.dataframe(pd.DataFrame(pageDict['regList']).explode('parts'))

        stTrx.DebugOutput("regList",pageDict['regList'])

        infra.ToggleButton(pageDict,'tog_slots',"Use components slots?")

        ### assembly
        if st.button("Assemble"):

            for rl in pageDict['regList']:
                if pageDict['tog_slots']:
                    if 'slot' in rl['parts'][0].keys():
                        assRet=chnx.AssembleChunk({'comb':rl['comb'],'parts':rl['parts']},True)
                    else:
                        st.write("No sliots defined. Please check input file.")
                        continue
                else:
                    assRet=chnx.AssembleChunk({'comb':rl['comb'],'parts':rl['parts']})
                # st.write("return is:",assRet)
                rl['assembly']=assRet

        try:
            myAsss=[x['assembly'] for x in pageDict['regList'] if x['assembly']==True]
        except KeyError:
            st.stop()

        st.write(pageDict['regList'])

        st.write("### Assembled components:",len(myAsss))
        for k,rl in enumerate(pageDict['regList']):
            # if rl['assembly']==False: continue
            infra.ToggleButton(pageDict,'togAss_'+str(k),"A"+str(k)+": "+rl['comb']['SN'])
            #st.write("Component:",rl['comb']['SN'])
            if pageDict['togAss_'+str(k)]:
                combComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {"component" : rl['comb']['SN']} )
                st.write("Children:",[x['component']['serialNumber'] for x in combComp['children'] if x['component']!=None])
                stTrx.DebugOutput("Children details: ",combComp['children'])

                if st.button("disassemble?",key=k+500):
                    chnx.DisassembleChunk(combComp)
