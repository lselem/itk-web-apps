### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import copy
import datetime
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

#####################
### useful functions
#####################

infoList=[" * select component type",
        " * select component stage",
        " * select component testType",
        " * get testType schema from PDB",
        " * fill required fields",
        " * upload to PDB",
        " * delete test from PDB if required"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Single Test", ":microscope: Upload Single test to PDB", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        pageDict['project']=st.session_state.Authenticate['proj']['code']

        # review componentTypes and stages (from file)
        filePath="/code/commonCode/"
        stTrx.DebugOutput("filePath:",filePath)
        csvFileName="stageTestList_"+pageDict['project']+".csv"
        stTrx.DebugOutput("looking in: "+filePath[:filePath.rfind('/')])
        df_all=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName)
        infra.SelectBox(pageDict,'componentType',df_all['compType'].unique(),"Select componentType code:")

        if "componentType" not in pageDict.keys():
            st.write("No componentType selected.")

        # catch empty stages
        if df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].empty:
            st.write(f"No stages found for {pageDict['componentType']}. Cannot proceed")
            st.stop()

        # catch unset stages
        if df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list()[0]!=df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].to_list()[0]:
            st.write(f"No stages found for {pageDict['componentType']}. Cannot proceed")
            st.stop()

        infra.SelectBox(pageDict,'stage',df_all.query('compType=="'+pageDict['componentType']+'"')['stage'].unique(),"Select stage code:")
        ### catch no tests (use self identity check)
        # st.write(df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].to_list()[0])
        if df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].to_list()[0]!=df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].to_list()[0]:
            st.write("No test found @",pageDict['stage'])
            st.stop()
        infra.SelectBox(pageDict,'code',df_all.query('compType=="'+pageDict['componentType']+'" & stage=="'+pageDict['stage']+'"')['testType'].unique(),"Select testType code (in "+pageDict['stage']+"):")

        # st.write(df_all.query('stage=="'+pageDict['stage']+'"'))

        st.write("### stages and testTypes for "+pageDict['componentType'])
        df_stageTest=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+csvFileName).query('compType=="'+pageDict['componentType']+'"').reset_index(drop=True)
        try:
            st.dataframe(df_stageTest[['compType','stage','altStage','testType']].style.apply(stTrx.ColourCells, df=df_stageTest[['compType','stage','altStage','testType']], colName='stage', flip=True, axis=1))
        except IndexError:
            st.dataframe(df_stageTest)


        # get test schema
        if "origSchema" not in pageDict.keys() or pageDict['testSchema']['testType']!=pageDict['code'] or st.button("Reset Schema: "+pageDict['code']+"@"+pageDict['stage']+" for "+pageDict['componentType']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':True})
            pageDict['fullSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':False})
            pageDict['testSchema']=copy.deepcopy(pageDict['origSchema'])
        pageDict['testSchema']['institution']=st.session_state.Authenticate['inst']['code']
        dateStr=datetime.datetime.now().strftime("%Y-%m-%dT%H:%MZ")
        pageDict['testSchema']['date']=pdbTrx.TimeStampConverter(dateStr, "%Y-%m-%dT%H:%MZ")
        # try:
        #     pageDict['testSchema']['properties']["OPERATOR"]=" ".join([st.session_state.Authenticate['user'][x] for x in ['firstName','middleName','lastName']])
        # except KeyError:
        #     pass
        stTrx.DebugOutput("Original *schema*:",pageDict['origSchema'])
        st.write(pageDict['fullSchema'])

        ### add additional properties
        if st.checkbox("add optional parameter?"):
            for pr in ['properties','results']:
                st.write(f"__optional: {pr}__")
                if pr not in pageDict['fullSchema'].keys(): 
                    st.write("none found. skipping")
                    continue
                optList=list(set(pageDict['fullSchema'][pr]) - set(pageDict['origSchema'][pr]))
                optSels=st.multiselect("add optional parameter:",optList)
                for os in optSels:
                    if os not in pageDict['testSchema'][pr].keys():
                        pageDict['testSchema'][pr][os]=None

        ### Upload JSON
        exampleFileName = pageDict["code"] + "_example.json"
        df_input = chnx.UploadJSONChunk(pageDict, exampleName=exampleFileName, exampleDict=pageDict['testSchema'])
        if df_input:
            pageDict['testSchema'] = df_input

        ### Review and edit
        chnx.ReviewAndEditChunk(pageDict,'testSchema')

        # upload(!)
        chnx.RegChunk(pageDict, "TestRun", "testSchema")
