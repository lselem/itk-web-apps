### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import copy
import altair as alt
from datetime import datetime
import numpy as np
import os
from io import StringIO
import io
import xlsxwriter
import xlrd
import openpyxl
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb.exceptions as itkX
import commonCode.PDBTricks as pdbTrx
import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx
### analyses

### analyses

#####################
### useful functions
#####################

infoList=["Select mode",
        " - registration:",
        "   - input user information",
        "   - review information",
        "   - registration",
        " - update:",
        "   - select method",
        "     - input info.:",
        "       - name",
        "     - list",
        "       - select from list",
        "    - input user information",
        "   - review information",
        "   - update"
        ]

def ResetPageDict(pageDict,keeps=[]):
    for k in pageDict.keys():
        if k not in keeps:
            pageDict.pop(k)


#####################
### main part
#####################

class Page9(Page):
    def __init__(self):
        super().__init__("Batches", ":microscope: Register and populate batches", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        pageDict['projectCode']=st.session_state.Authenticate['proj']['code']
        pageDict['objectType']="batch"

        ### mode selection
        modeList=['register new batch','add/remove components and set properties']
        modeSel=st.radio("select function:",modeList)
        st.write("---")

        ###################
        ### register batch
        ###################
        if "register" in modeSel:
            # # reset pageDict object
            # ResetPageDict(pageDict,['projectCode','objectType'])
            st.write("## Register batch")

            batchTypeList=DBaccess.DbGet(st.session_state.myClient,'listBatchTypes', {'project':pageDict['projectCode']}, True)
            st.write(f"Found {len(batchTypeList)} batchTypes for project: {pageDict['projectCode']}")

            df_btl=pd.DataFrame(batchTypeList)
            for col in df_btl.columns:
                df_btl[col]=df_btl[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x else x)
            if st.checkbox('See batchType list?'):
                st.write("### list of batch types")
                st.dataframe(df_btl)
            
            if df_btl.empty:
                st.write("__No batches found__")
                st.stop()
                
            selBatchType=st.selectbox('Select batchType',df_btl['code'].to_list())

            ### get schema?
            if "origSchema" not in pageDict.keys() or st.button("Reset batch schema") or pageDict['userSchema']['batchType']!=selBatchType:
                pageDict['origSchema'] = {
                                        'project': "PROJECT_CODE", 
                                        'batchType': "BATCHTYPE_CODE", 
                                        'number': "BATCH_NUMBER", 
                                        'ownerInstituteList': []
                                        }
            pageDict['userSchema']=copy.deepcopy(pageDict['origSchema'])
            pageDict['userSchema']['ownerInstituteList'].append(st.session_state.Authenticate['inst']['code'])
            pageDict['userSchema']['project']=pageDict['projectCode']
            pageDict['userSchema']['batchType']=selBatchType
            
            stTrx.DebugOutput("Original *schema*:",pageDict['origSchema'])
            st.success("Got schema")

            st.write("### Input user information")
            batchNum=st.text_input('Input batch name')
            pageDict['userSchema']['number']=batchNum

            ### other inputs? - properties later

            st.write("---")

            ### Review and edit
            chnx.ReviewAndEditChunk(pageDict, 'userSchema')

            st.write("### Registration")

            # upload(!)
            chnx.RegChunk(pageDict, pageDict['objectType'], 'userSchema')


        ###################
        ### add/remove components in batch
        ###################
        elif "add/remove" in modeSel:
            st.write("## Add/remove components to batch")

            methodList=['input info.','select from list']
            methodSel=st.radio("method:",methodList)

            ### input info. to identify batch
            if "input" in methodSel:
                st.write("### Input batch info.")

                inBatchType=st.text_input("Input batch type (code):")
                inBatchNum=st.text_input("Input batch number (i.e. name):")
                inBatch_json={
                        'project': pageDict['projectCode'],
                        'batchType': inBatchType,
                        'number': inBatchNum
                        }

                if inBatchType==None or inBatchType=="" or inBatchNum==None or inBatchNum=="":
                    st.write("Please input batch information")
                    st.stop()

                if "batchInfo" not in pageDict.keys() or st.button("reset batch") or str(pageDict['batchInfo']['number'])!=inBatchNum or str(pageDict['batchInfo']['batchType']['code'])!=inBatchType:
                    pageDict['batchInfo']=DBaccess.DbGet(st.session_state.myClient,'getBatchByNumber', inBatch_json)


            ### select batch from list
            elif "list" in methodSel:
                st.write("### Select "+pageDict['objectType']+" from list")

                ### get batches?
                batchList=DBaccess.DbGet(st.session_state.myClient,'listBatches', {'project':pageDict['projectCode']}, True)
                st.write(f"Found {len(batchList)} batchTypes for project: {pageDict['projectCode']}")

                df_bl=pd.DataFrame(batchList)
                for col in df_bl.columns:
                    df_bl[col]=df_bl[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x else x)
                if st.checkbox('See batch list?'):
                    st.write("### list of batches")
                    st.dataframe(df_bl)
            

                selBatchType=st.selectbox('Select batchType',df_bl['batchType'].unique())

                selBatchNum=st.selectbox('Select number',df_bl.query('batchType=="'+selBatchType+'"')['number'].to_list())

                batchId=df_bl.query('batchType=="'+selBatchType+'" & number=="'+selBatchNum+'"')['id'].values[0]
            
                if batchId==None or batchId=="":
                    st.write("Input batch identifier")
                    st.stop()
            
                if "batchInfo" not in pageDict.keys() or st.button("reset batch") or str(pageDict['batchInfo']['number'])!=selBatchNum or str(pageDict['batchInfo']['batchType']['code'])!=selBatchType:
                    pageDict['batchInfo']=DBaccess.DbGet(st.session_state.myClient,'getBatch', {'id':batchId})

            else:
                st.info("Please select input method")
                st.stop()

            if st.checkbox('See full batch info.?'):
                st.write("### full batch info.")
                st.write(pageDict['batchInfo'])
            
            ### deleted check
            if pageDict['batchInfo']['state']!="ready":
                st.write(f"### batch is not ready ({pageDict['batchInfo']['state']})")
                st.stop()

            ### display current component info.
            st.write("### Batch component information")
            emptyFlag=False
            try:
                if len(pageDict['batchInfo']['components'])<1:
                    st.write("no components in batch")
                    emptyFlag=True
                else:
                    df_comps=pd.DataFrame(pageDict['batchInfo']['components'])
                    st.write(df_comps)
                    st.write(f"total components: {len(df_comps.index)}")
                    
                    st.download_button(label="Download list", data=df_comps.to_csv(index=False),file_name=selBatchType+"_"+selBatchNum+".csv")
                        
            except KeyError:
                st.write("no component information (key) found for batch")
                st.stop()
            except TypeError:
                st.write("no component information (type) found for batch")
                st.stop()
            
            ### empty check
            if emptyFlag:
                if st.button("delete batch?"):
                    try:
                        try:
                            retVal=DBaccess.DbPost(st.session_state.myClient,'deleteBatch',{'id':pageDict['batchInfo']['id']})
                            st.write("### **Successful deletion ** for:",pageDict['retVal']['number'])
                        except KeyError:
                            st.write("### **Successful deletion, but don't understand return object")
                        st.balloons()
                        st.write(pageDict['retVal'])
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: batch deletion **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            ###
            arList=['add components','remove components','set properties']
            arSel=st.radio("add/remove:",arList)


            ###

            if "properties" in arSel:
                st.write("## Set properties")

                df_props=pd.DataFrame(pageDict['batchInfo']['properties'])

                # stop if no properties found
                if df_props.empty:
                    st.write("No properties found :shrug:")
                    st.stop()
                st.write(df_props)
                
                propSel=st.selectbox("Select property code:",df_props['code'].to_list())
                inVal=st.text_input("Value:")

                if st.button("update "+propSel):
                    try:
                        retVal=DBaccess.DbPost(st.session_state.myClient,'setBatchProperty',{'id':pageDict['batchInfo']['id'], 'code':propSel, 'value':inVal})
                        try:
                            st.write("### **Successful update property** for:",retVal['number'])
                        except:
                            st.write("### **Successful update property, but don't understand return object")
                        st.balloons()
                        st.write(retVal)
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: update property **Unsuccessful**")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks


            elif "remove" in arSel:
                st.write("### Remove components")

                if len(pageDict['batchInfo']['components'])<1:
                    st.write("no components in batch")
                    st.stop()

                compIdList=[]
                selSNs=st.multiselect('Select serialNumbers:',df_comps['serialNumber'].to_list())
                if len(selSNs)<1:
                    st.write("Please select serialNumbers")
                    st.stop()
                else:
                    stTrx.DebugOutput('Selected serialnumbers: ',selSNs)
                    if st.button("remove "+str(len(selSNs))+" components"):
                        for sn in selSNs:
                            st.write(f"trying to remove {sn}")
                            rem_json={
                            'id': pageDict['batchInfo']['id'],
                            'component': sn
                                }
                            try:
                                pageDict['retVal']=DBaccess.DbPost(st.session_state.myClient,'removeBatchComponent', rem_json)
                                try:
                                    st.write("### **Successful component removal ** for:",pageDict['retVal']['number'])
                                except:
                                    st.write("### **Successful component removal, but don't understand return object")
                                st.balloons()
                                st.write(pageDict['retVal'])
                            except itkX.BadRequest as b:
                                st.write("### :no_entry_sign: component removal **Unsuccessful**")
                                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            ### add components
            elif "add" in arSel:
                st.write("### Add components")

                addList=['input info.','select from list','upload csv']
                addSel=st.radio("add by:",addList)

                if "compIdList" not in pageDict.keys() or st.button("clear component list"):
                    pageDict['compIdList']=[]

                if "input" in addSel:
                    compId=st.text_input("Input component SN")
                    if st.button("add "+compId+" to list for upload"):
                        pageDict['compIdList'].append(compId)
                        st.write("added "+compId)
                
                elif "select" in addSel:
                    st.write("### Check componentTypes in PDB")
                    if st.button("reset list",key="shipping") or "compList" not in list(pageDict.keys()): # check list
                        st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
                        compList=DBaccess.DbGet(st.session_state.myClient,'listComponents',{'currentLocation':st.session_state.Authenticate['inst']['code'], 'project':st.session_state.Authenticate['proj']['code'] },True)
                    else:
                        st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
                    df_compList=pd.json_normalize(compList, sep = "_")

                    selCompType=st.multiselect('select componentType',df_compList['componentType_code'].unique())
                    selSNs=st.multiselect('Select serialNumbers:',df_compList.query('componentType_code in @selCompType')['serialNumber'].to_list())
                    if len(selSNs)<1:
                        st.write("Please select serialNumbers")
                        st.stop()
                    else:
                        stTrx.DebugOutput('Selected serialnumbers: ',selSNs)
                        if st.button("add "+str(len(selSNs))+" SNs"):
                            pageDict['compIdList'].extend(selSNs)
                            st.write("added "+str(len(selSNs))+" SNs")

                elif "csv" in addSel:
                    st.write("Upload *component codes* (ASNs/codes) from **csv** file")
                    st.write("Follow data format:")
                    df_example=pd.DataFrame([{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'},{'serialNumber':'20Uxxyynnnnnnn'}])
                    st.write(df_example.style.hide_index())
                    csvFile= st.file_uploader("Upload a file", type=["csv"])
                    stTrx.DebugOutput("file: ",csvFile)

                    if csvFile is not None:
                        csvFile.seek(0)
                        df_csv=pd.read_csv(csvFile)
                        if "serialNumber" not in df_csv.columns:
                            st.write("adding serialNumber header") # reset input file reader
                            csvFile.seek(0)
                            df_csv=pd.read_csv(csvFile, header=None)
                            df_csv=df_csv.rename(columns={0: "serialNumber"})
                        if st.button("add "+str(len(df_csv['serialNumber'].to_list()))+" SNs"):
                            pageDict['compIdList'].extend(df_csv['serialNumber'].to_list())
                            st.write("added "+str(len(df_csv['serialNumber'].to_list()))+" SNs")
                        
                    else:
                        st.write("No data file set")
                        st.stop()

                else:
                    st.info("Please select input method")
                    st.stop()

                ### check PDB
                st.write("### Check components in PDB before upload to batch")
                addComps=DBaccess.DbGet(st.session_state.myClient,'getComponentBulk', {'component':pageDict['compIdList']})
                if len(addComps)<1:
                    st.write("No matching components found in PDB")
                    st.stop()
                # pageDict['df_data']=pd.DataFrame([{'serialNumber':x['serialNumber'],'code':x['code'],'compCode':x['componentType']['code'],'curLoc':x['currentLocation']['code']} for x in myComps])
                df_addComps=pd.json_normalize(addComps, sep = "_")
                stTrx.DebugOutput("uploaded components",df_addComps)
                colz=['serialNumber','alternativeIdentifier','componentType_code','type_code','currentStage_code','currentLocation_code','assembled','cts']
                st.dataframe(df_addComps[colz])
                st.write("length of dataframe:",len(df_addComps))

                ### review info.
                st.write(f"### Review info. to add to batch ({pageDict['batchInfo']['number']})")
                addList=df_addComps['serialNumber'].to_list()
                st.write(addList)

                ### update
                if st.button("add list of components to batch"):
                    compList=[comp['serialNumber'] for comp in pageDict['batchInfo']['components'] if type(pageDict['batchInfo'])==type({}) and "components" in pageDict['batchInfo'].keys()]
                    for al in addList:
                        if al in compList:
                            st.write(f"__{al} already in batch {pageDict['batchInfo']['number']}__")
                            continue
                        st.write(f"adding {al} to {pageDict['batchInfo']['number']}")
                        add_json={
                            'id': pageDict['batchInfo']['id'],
                            'component': al
                                }
                        try:
                            pageDict['retVal']=DBaccess.DbPost(st.session_state.myClient,'addBatchComponent', add_json)
                            try:
                                st.write("### **Successful component addition ** for:",pageDict['retVal']['number'])
                            except:
                                st.write("### **Successful component addition, but don't understand return object")
                            st.balloons()
                            st.write(pageDict['retVal'])
                        except itkX.BadRequest as b:
                            st.write("### :no_entry_sign: component addition **Unsuccessful**")
                            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

            else:
                st.write("### Select option")

        else:
            st.info("Please select mode")
