### standard
import datetime
import os
from io import StringIO
import io
import json
import altair as alt
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
### PDB stuff
import numpy as np
### custom
import pandas as pd
import streamlit as st
import itkdb
import itkdb.exceptions as itkX
from core.Page import Page

import commonCode.StreamlitTricks as stTrx
import commonCode.codeChunks as chnx

### eos stuff
from PIL import Image
from requests.exceptions import ConnectionError


### analyses

#####################
### useful functions
#####################

def UploadImage(myClient,upID,fileDict,title=None):
    
    file_name=fileDict['object'].name
    file_path="/tmp/"+file_name

    fnew=open(file_path, "wb")
    ### hack to use latin-1 encoding: https://stackoverflow.com/questions/5552555/unicodedecodeerror-invalid-continuation-byte
    # fnew.write(fileDict['object'].getvalue().decode('latin-1'))
    fnew.write(fileDict['object'].getvalue())
    # fnew.write(fileDict['object'])
    fnew.close()

    image = Image.open(file_path)
    st.image(image)

    data = {
        "testRun": upID,
        "title": fileDict['name'],
        "description": fileDict['description'],
        "url": file_name,
        "type": "file"
    }

    with open(file_path, "rb") as f: 
        thing=(file_name , f , fileDict['object'].type , {})
        st.write(thing)
        files = {"data": thing }
        try:
            response = myClient.post("createTestRunAttachment", data=data, files=files)
            # print(response)
            return response
        except itkX.BadRequest as b:
            print("### :no_entry_sign: File Upload **Unsuccessful**")
            print(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

    return None



def UploadImageNew(myClient,upID,fileDict,title=None):

    # filename = "/tmp"+"/"+fileDict['object'].name
    filename = itkdb.data / "1x1.jpg"

    fnew=open(filename, "wb")
    ### hack to use latin-1 encoding: https://stackoverflow.com/questions/5552555/unicodedecodeerror-invalid-continuation-byte
    # fnew.write(fileDict['object'].getvalue().decode('latin-1'))
    fnew.write(fileDict['object'].getvalue())
    # fnew.write(fileDict['object'])
    fnew.close()



    data = {
        "testRun": upID,
        "title": fileDict['name'],
        "description": fileDict['description'],
        "url": filename,
        "type": "file"
    }

    image = Image.open(filename)
    st.image(image)

    # with filename.open("rb") as fpointer:
    with (itkdb.data / filename).open("rb") as image_fp:
        files = {"data": itkdb.utils.get_file_components({"data": image_fp}) }
    
        try:
            response = myClient.post("createTestRunAttachment", data=data, files=files)
            st.write("### **Successful File Upload**:",response['title'])
            return response
        except itkX.BadRequest as b:
            print("### :no_entry_sign: File Upload **Unsuccessful**")
            print(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

    return None


### upload file function
def UploadFileSWAN(myClient,data_json):
    st.write("Using:",data_json)
    response=None
    with open(data_json['url'], "rb") as fpointer:
        try:
            files = {"data": itkdb.utils.get_file_components({"data": fpointer})} 
            response = myClient.post("createTestRunAttachment", data=data_json, files=files)
        except itkX.BadRequest as b:
            st.write("### :no_entry_sign: File Upload **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks

    return response

def DeleteEOSLink(myClient, objType, objCode, attachCode):
    try:
        delObj=myClient.post("delete"+objType[:1].upper()+objType[1:]+"Attachment", json={objType: objCode, "code": attachCode})
        st.write(f"Successful deletion: {delObj}")
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: deleting **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks



infoList=[" * check test parameters for componentType at institute(s)",
        " * select componentType at institute",
        " * select stage and testType (from test schema)",
        " * select parameter (currently no arrays supported)",
        " * visualise and download csv"]
#####################
### main part
#####################

class Page12(Page):
    def __init__(self):
        super().__init__("EOS Loader", "🏹 Upload/download files (inc. images) to PDB space on EOS", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            stTrx.DebugOutput(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()


        ##############
        ### check eos
        ##############

        st.write("### ready for EOS?")
        st.write(st.session_state.myClient.use_eos)
        if st.session_state.myClient.use_eos:
            st.success("__ready__")
        else:
            st.info("EOS flag not set. Please use check box in Authenication page and get new token")
            st.stop()


        pageDict['projectCode']=st.session_state.Authenticate['proj']['code']
        pageDict['institutionCode']=st.session_state.Authenticate['inst']['code']
        
        ##############
        ### select object to attach image
        ##############
        st.write("## Get PDB Object")
        
        ##############
        ### select object type: component or testRun
        ##############
        objList=['component', 'testRun']
        objSel=st.radio("select object type:", objList)

        st.write("---")


        st.write(f"## Identify {objSel} in database to attach file")
        
        st.write(f"### Select component")
        ##############
        ### select identification method
        ##############
        methodList=['input identifier', 'select from lists']
        methodSel=st.radio("select function:", methodList)

        if 'identifier' in methodSel:
            compId=st.text_input("Input component identifier:")
            if type(compId)==type(None) or len(compId)<1: #, value="20USBRT0731003")  
                st.write("Please input identifier")
                st.stop()
        elif 'list' in methodSel:
            ### get list of components
            if "compList" not in pageDict.keys():
                # later migration to filterMap
                # filterMap={'project':pageDict['projectCode'], 'currentLocation':[pageDict['institutionCode']], "state":"ready"}
                # pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents', {'filterMap':filterMap}, True)
                pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents', {'project':pageDict['projectCode'], 'currentLocation':pageDict['institutionCode']}, True)

            else:
                st.write(f"Component list for {pageDict['institutionCode']} ({pageDict['projectCode']})")
                if st.button("rest component list"):
                    # later migration to filterMap
                    # filterMap={'project':pageDict['projectCode'], 'currentLocation':[pageDict['institutionCode']]}
                    # pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents', {'filterMap':filterMap}, True)
                    pageDict['compList']=DBaccess.DbGet(st.session_state.myClient,'listComponents', {'project':pageDict['projectCode'], 'currentLocation':pageDict['institutionCode']}, True)

            compTypeSel=st.selectbox("Select component type:", set([ct['componentType']['code'] for ct in pageDict['compList'] if type(ct['componentType'])==type({}) and "code" in ct['componentType']]) )
            comp_subList=[ct['serialNumber'] for ct in pageDict['compList'] if type(ct)==type({}) if type(ct['componentType'])==type({}) and compTypeSel==ct['componentType']['code'] and ct['serialNumber']!=None]
            st.write(f"- found {len(comp_subList)} components")
            compId=st.selectbox("Select component:", comp_subList )
        else:
            st.write("select input method")

        ### automatic update (missing or empty) or on demand
        if "retComp" not in pageDict.keys() or st.button("reset component"):
            # st.write("prompt update")
            retComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component': compId, 'noEosToken':False})
            pageDict['retComp']=retComp
        elif type(pageDict['retComp'])==type({}) and pageDict['retComp']['serialNumber']!=compId:
            # st.write("mismatch update")
            retComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component': compId, 'noEosToken':False})
            pageDict['retComp']=retComp
        elif pageDict['retComp']==None:
            # st.write("None update")
            retComp=DBaccess.DbGet(st.session_state.myClient,'getComponent', {'component': compId, 'noEosToken':False})
            pageDict['retComp']=retComp
        else:
            pass

        ### catch nothing selected
        if "retComp" not in pageDict.keys():
            st.write("Input component info.")
            st.stop()

        ### announce component and offer full info.
        if pageDict['retComp']==None or type(pageDict['retComp'])!=type({}):
            st.write("Returned component doesn't look right :(")
            st.write(" - if _Granting token from EoS failed_ remove broken EOS links and try again")
            st.write(pageDict['retComp'])
            st.stop()

        ### announce component and offer full info.
        st.write(f"__Got {pageDict['retComp']['serialNumber']}__ ({pageDict['retComp']['code']})")
        check_comp=st.checkbox("See full component object")
        if check_comp:
            st.write("Full object")
            st.write(pageDict['retComp'])

        if objSel=="testRun":

            ### select test form list
            st.write("### Select testRun")
            df_tests=pd.DataFrame(pageDict['retComp']['tests'])
            try:
                df_tests=df_tests.explode('testRuns').reset_index()
            except KeyError:
                st.write("No testRuns found")
                st.stop()
            colNames=['id','date','state']
            for col in colNames:
                df_tests['testRun_'+col]=df_tests['testRuns'].apply(lambda x: x[col])
            st.write("Tests:",df_tests[['id','code']+['testRun_'+col for col in colNames]])
            selTestRun=st.selectbox("Select testRun (by test_id):", df_tests['testRun_id'].to_list())


        st.write("---")

        ##############
        ### select mode: upload/retrieve
        ##############
        modeList=['upload file to EOS','retrieve file from EOS']
        modeSel=st.radio("select function:",modeList)
        st.write("---")

        ##############
        ### read in image file
        ##############
        if "upload" in modeSel.lower():

            st.write("## Upload file")

            imageExts=['jpg','jpeg','png','tif','tiff']
            fileExts=['dat','sta','csv']
            uploaded_file = st.file_uploader("Upload image file", type=imageExts+fileExts )

            if uploaded_file is not None: 
                st.write("Got file "+uploaded_file.name)
                pageDict['file']=uploaded_file
            else:
                st.write("Upload file")
                st.stop()
        
            if any(ie in pageDict['file'].type for ie in imageExts):
                st.write(" - recognise image")
                if st.checkbox("See image?"):
                    st.write("details:",pageDict['file'])
                    st.image(pageDict['file'])
            else:
                if st.checkbox("See file?"):
                    st.write("details:",pageDict['file'])
                    st.write(pageDict['file'].read())


            descStr=st.text_input("Optional description:",value="from webApp "+datetime.datetime.now().strftime("%Y-%m-%dT%H:%MZ"))

            ### make json for upload
            pageDict['data_json'] = {
                "title": f"a {objSel} image attachment",
                "description": descStr,
                "url": "dummy",
                "type": "file"
            }

            if objSel=="component":
                pageDict['data_json']['component']= pageDict['retComp']['id']
            elif objSel=="testRun":
                pageDict['data_json']['testRun']= selTestRun
            else:
                st.write(f"unknown object: {objSel}")
                st.stop()

            ### review and edit
            chnx.ReviewAndEditChunk(pageDict,'data_json')

            # st.write(pageDict['file'].__dir__())

            if st.button("upload"):

                files = {"data": itkdb.utils.get_file_components({"data": pageDict['file']})}
                st.write("uploading files:",files)
                try:            
                    if objSel=="component":
                        response = st.session_state.myClient.post("createComponentAttachment", data=pageDict['data_json'], files=files)
                    elif objSel=="testRun":
                        response = st.session_state.myClient.post("createTestRunAttachment", data=pageDict['data_json'], files=files)
                    st.write(f"{response}")
                    st.balloons()
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: File Upload **Unsuccessful**")
                    st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks


        ##############
        ### view available images in app
        ##############
        elif "retrieve" in modeSel.lower():
            
            st.write(f"## Check {objSel}")

            if objSel=="component":
                st.write(f"__Got {pageDict['retComp']['id']}__ ({pageDict['retComp']['componentType']['code']})")
            
                if st.checkbox("See all attachment info?"):
                    st.write(pageDict['retComp']['attachments'])

                st.write(f"Found _total_ {len(pageDict['retComp']['attachments'])} attachments")
                df_attach=pd.DataFrame(pageDict['retComp']['attachments'])
            
            elif objSel=="testRun":

                if "retTest" not in pageDict.keys() or st.button("reset testRun") or pageDict['retTest']['id']!=selTestRun:
                    pageDict['retTest']=DBaccess.DbGet(st.session_state.myClient,'getTestRun', {'testRun': selTestRun, 'noEosToken':False})

                st.write(f"__Got {pageDict['retTest']['id']}__ ({pageDict['retTest']['testType']['code']})")
                check_testRun=st.checkbox("See full testRun object")
                if check_testRun:
                    st.write("Full object")
                    st.write(pageDict['retTest'])

                if st.checkbox("See all attachment info?"):
                    st.write(pageDict['retTest']['attachments'])

                st.write(f"Found _total_ {len(pageDict['retTest']['attachments'])} attachments")
                df_attach=pd.DataFrame(pageDict['retTest']['attachments'])
                
            if df_attach.empty:
                st.write("No attachments found")
                st.stop()

            st.write(f"### {objSel} attachments")
            ### split by type
            df_eos=df_attach.query('type=="eos"')
            df_non=df_attach.query('type!="eos"')

            ### eos attachments not empty --> i.e. found
            if not df_eos.empty:
                st.write(f"__eos__ attachments ({len(df_eos.index)})",df_eos[['dateTime','title','description','filename','url']])

                selEos=st.selectbox("Select eos file",df_attach.query('type=="eos"')['dateTime'].to_list())
                
                selUrl=df_attach.query('dateTime=="'+selEos+'"')['url'].values[0]
                selCode=df_attach.query('dateTime=="'+selEos+'"')['code'].values[0]
                st.write(f"download via link: {selUrl}")

                ##############
                ### select object type: component or testRun
                ##############
                fileTypeList=['image', 'non-image']
                fileTypeSel=st.radio("select file type:", fileTypeList)

                if st.checkbox(f"view {fileTypeSel}"):
                    try:
                        eosFile=st.session_state.myClient.get(selUrl)
                    except ConnectionError:
                        st.write("__Connection issue__ Try un-check and re-check")
                        st.stop()
                    if "non" in fileTypeSel.lower():
                        ### try converting to json
                        try:
                            eosDict = json.loads(eosFile.content.decode("utf-8"))
                            selKey=st.selectbox("Select key",list(eosDict.keys()))
                            st.write(eosDict[selKey])
                        except:
                            try:
                                st.write(eosFile.content.decode("utf-8"))
                            except:
                                st.write("__Reading issue__ Is the data file type correct?")
                    else:
                        try:
                            st.image(eosFile.content)
                        except:
                            st.write("__Reading issue__ Is the data file type correct?")

                if st.checkbox("Delete attachment?"):
                    st.write(f"Double check: delete attachment {selCode}")
                    if st.button("Delete!"):
                        if objSel=="testRun":
                                DeleteEOSLink(st.session_state.myClient, objSel, pageDict['retTest']['id'], selCode)
                        if objSel=="component":
                                DeleteEOSLink(st.session_state.myClient, objSel, pageDict['retComp']['id'], selCode)

            ### announce no eos found
            else:
                st.write("No __eos__ attachments found")

            ### view other attachments (if exist)
            if st.checkbox('see _non_ eos attachments?'):
                if  not df_non.empty:
                    st.write(f"__non eos__ attachments ({len(df_non.index)})",df_non[['dateTime','title','description','filename','contentType']])
                else:
                    st.write("No __non eos__ attachments found")

        else:
            st.write("Select mode")
