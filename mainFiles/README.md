# Main Files

Files with app specific information used for webApps title page.

---

## Small Print

WebApp build information and additional links are given at the bottom lefthand side:

<figure markdown>
![image info](/images/mainFiles/small_print_example.png){ width="400" }
<figcaption>small print example</figcaption>
</figure>

This is specified as the _smalls_ dictionary:

- built on: build date of webApp deployment
- git repo: link to code repository
    - current commit: SHA key for commit used for build
- docker repo: link to dockerHub version of app (may not be same code version as OKD deployed app)
- itk-docs: main dockumentation for ITk PDB information
- API info: link to PDB API documentation
- other webapps: link to pointer webApp with links to other apps and documentation landing pages
- contact: point of contact email


## Name & Title

Set in class definition:
 
> myapp = App(WEB_APP_NAME, WEB_APP_TITLE, smalls)

